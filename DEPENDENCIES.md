# Dependencies

## For Building and Running the Server

* `>=caosdb-proto 0.3.0`
* `>=caosdb-mysqlbackend 8.0.0`
* `>=Java 11`
* `>=Apache Maven 3.6.0`
* `>=Make 4.2`
* `>=gcc 8` (if PAM authentication is required)
* `libpam` (if PAM authentication is required)
* More dependencies are being pulled and installed automatically by Maven. See the complete list of dependencies in the [pom.xml](pom.xml)

## For Deploying a Web User Interface (optional)

* `>=caosdb-webui 0.13.0`

## For Building the Documentation (optional)

* `>=Python 3.8`
* `>=pip 21.0.0`

## For Server-side Scripting (optional)

* `>=Python 3.8`
* `>=pip 21.0.0`
* `openpyxl` (for XLS/ODS export)

## Recommended Packages

* `openssl` (if a custom TLS certificate is required)
