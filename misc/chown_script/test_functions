#!/bin/bash
#
# ** header v3.0
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2018 Research Group Biomedical Physics,
# Max-Planck-Institute for Dynamics and Self-Organization Göttingen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# ** end header
#



# create file in dropoffbox, owned by root, change-mod/own/grp to dbuser
function test1 {
    echo -n "Test 1 "
    local FILENAME=$LOC_DROPOFFBOX/chmod_test.dat
    echo "blablabla" > $FILENAME
    $CMD_CHOWN root:root $FILENAME

    if [ ! -e $FILENAME ]; then
        echo "[FAILED] - could not create a testfile in the DropOffBox."
        return 1
    fi


    local lsstr=$(ls -la $FILENAME)
    local matchlen=$(expr match "$lsstr" "[-drwx]\{10\}\s[0-9]*\sroot\sroot\s[0-9]")
    if [ $matchlen -lt 24 ]; then
        echo -e "[FAILED] - 'ls -la' did not indicate that the owner was root at the beginning of this test."
        rm $FILENAME
        return 2
    fi

    sudo -u $DB_USER -H sh -c "sudo $LOC_SCRIPT $FILENAME" 

    local lsstr=$(ls -la $FILENAME)
    local matchlen=$(expr match "$lsstr" "[-drwx]\{10\}\s[0-9]*\s$DB_USER\s$DB_GROUP\s[0-9]")
    if [ $matchlen -lt 18 ]; then
        echo -e "[FAILED] - 'ls -la' did not indicate that the owner has changed successfully."
        rm $FILENAME
        return 3
    fi

    rm $FILENAME

    echo "[OK]"
    return 0
}
