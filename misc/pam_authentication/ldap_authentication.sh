#!/bin/bash

# ** header v3.0
# This file is a part of the CaosDB Project.
#
# Copyright (c) 2020 IndiScale GmbH
# Copyright (c) 2020 Daniel Hornung <d.hornung@indiscale.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# ** end header

# Try to authenticate a user ($1) via LDAP, either via stdin or a password file ($2, if given).

[[ "$#" == "1" || "$#" == "2" ]] || {
    echo "Call this script as: $0 <user> [-|<password file>]"
    exit 1
}

# Load all LDAP client settings
exe_dir=$(dirname "$0")
. "$exe_dir/ldap.env"
LDAPTLS_REQCERT="${LDAP_TLS_REQCERT:-hard}"
BIND_DN_PATTERN="${BIND_DN_PATTERN:-"cn=\${USER_NAME},\${USER_BASE}"}"
WHO_AM_I_PATTERN="${WHO_AM_I_PATTERN:-"dn:cn=\${USER_NAME},\${USER_BASE}"}"

# If the second argument is empty or "-", take password from stdin, else use the argument as a file.
testpw() {
    local USER_NAME bind_dn who_am_i pwfile pwargs result

    # cn is case-insensitive https://ldapwiki.com/wiki/Distinguished%20Name%20Case%20Sensitivity
    USER_NAME="$(echo "$1" | tr '[:upper:]' '[:lower:]')"

    bind_dn="$(eval "echo \"$BIND_DN_PATTERN\"")"
    who_am_i="$(eval "echo \"$WHO_AM_I_PATTERN\"")"

    pwfile="$2"
    pwargs=("-y" "$pwfile")
    if [ "$pwfile" = "-" ] ; then
        pwargs=("-W")
    elif [ -z "$pwfile" ] ; then
        pwargs=("-W")
    fi

    result="$(ldapwhoami -o "nettimeout=10" -x -D "$bind_dn" "${pwargs[@]}")"
    if [ "$?" -ne "0" ] ; then
        return 1
    elif [ "$result" = "$who_am_i" ] ; then
        return 0
    fi
    echo "result : $result"
    echo "pattern: $who_am_i"
    return 1

}

if testpw "$1" "$2" ; then
    echo "[OK]"
    exit 0
else
    echo "[FAILED]"
    exit 1
fi
