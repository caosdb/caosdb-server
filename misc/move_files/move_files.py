#!/usr/bin/env python3
# encoding: utf-8
#
# ** header v3.0
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2019 Henrik tom Wörden
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# ** end header
#

"""
This script moves files in the internal file system of the CaosDB server. It
reads file paths form a tsv file with columns from and to. For each line it
creates an update of a caosdb file object where the path that equals "from" is
changed to "to".

Version: 0.1
"""

import argparse
import sys
import time
from argparse import ArgumentParser

import pandas as pd
from tqdm import tqdm

import caosdb as db


def rename(changes, chunksize=10):
    """change the path of files based on a two-column table (from, to).

    Parameters
    ----------

    changes : pd.DataFrame
        A table with two columns, the old path and the new path.
    chunksize : int, optional
        How many files are being moved in one go (default is 10).
    """
    i = 0

    for i in tqdm(range(changes.shape[0]//chunksize+1)):
        chunk = changes.iloc[i*chunksize:(i+1)*chunksize]

        if chunk.shape[0] == 0:
            continue
        cont = db.Container()

        for _, (old, new) in chunk.iterrows():
            try:
                fi = db.File(path=old)
                fi.retrieve()

                if not fi.is_valid():
                    continue
                assert fi.path == old
                fi.path = new
                cont.append(fi)
            except Exception as e:
                print(e)

        if len(cont) > 0:
            cont.update(unique=False)
        i += 1


def main(argv=None):
    '''Command line options.'''

    if argv is None:
        argv = sys.argv
    else:
        sys.argv.extend(argv)

    # Setup argument parser
    parser = ArgumentParser(description=__doc__)
    parser.add_argument("changes", help="The file that defines the renames")
    args = parser.parse_args()

    changes = pd.read_csv(args.changes, sep="\t")

    if ("to" not in changes.columns or "from" not in changes.columns):
        raise ValueError("The file supplied under changes shall have a 'to'"
                         " and a 'from' column.")

    assert 0 == pd.isnull(changes).sum().sum()
    rename(changes)


if __name__ == "__main__":
    sys.exit(main())
