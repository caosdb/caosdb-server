# About

Version: 0.1

Usage: `./move_files.py [-h] changes`

This script moves files in the internal file system of the CaosDB server. It
reads file paths form a tsv file with columns from and to. For each line it
creates an update of a caosdb file object where the path that equals "from" is
changed to "to".

positional arguments:
  changes     The file that defines the renames

optional arguments:
  -h, --help  show this help message and exit

# Tests

The tests a integration tests which require a running test database and a sufficiently configured caosdb-pylib.

Run `pytest test_move_files.py` to insert a bunch of test files, rename and subsequently delete them.


