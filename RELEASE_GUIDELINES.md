# Release Guidelines for the CaosDB Server

This document specifies release guidelines in addition to the general release
guidelines of the CaosDB Project
([RELEASE_GUIDELINES.md](https://gitlab.com/caosdb/caosdb/blob/dev/RELEASE_GUIDELINES.md))

## General Prerequisites

* All tests are passing.
* FEATURES.md is up-to-date and a public API is being declared in that document.
* CHANGELOG.md is up-to-date.
* DEPENDENCIES.md is up-to-date.

## Steps

1. Create a release branch from the dev branch. This prevents further changes
   to the code base and a never ending release process. Naming: `release-<VERSION>`

2. Check all general prerequisites.

3. Update the versions in:
  * [pom.xml](./pom.xml) (probably this means to remove the `-SNAPSHOT`)
  * `src/doc/conf.py`
  * `CHANGELOG.md`
  * `CITATION.cff` (update version and date)

5. Merge the release branch into the main branch.

6. Wait for the main branch pipelines to pass.

7. Tag the latest commit of the main branch with `v<VERSION>`.

8. Delete the release branch.

9. Merge the main branch back into the dev branch.

10. Update the versions for the next developement round:
  *  [pom.xml](./pom.xml) with a `-SNAPSHOT` suffix
  * `src/doc/conf.py`
  * `CHANGELOG.md`: Re-add the `[Unreleased]` section.

11. Add a gitlab release in the respective repository:
    https://gitlab.com/linkahead/linkahead-server/-/releases

    Add a description, which can be a copy&paste from the CHANGELOG, possibly prepended by:
    ```md
# Changelog

[See full changelog](https://gitlab.com/linkahead/linkahead-server/-/blob/${TAG}/CHANGELOG.md)
    ```
