#
# ** header v3.0
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2018 Research Group Biomedical Physics,
# Max-Planck-Institute for Dynamics and Self-Organization Göttingen
# Copyright (C) 2019 IndiScale GmbH
# Copyright (C) 2019 Timm Fitschen (t.fitschen@indiscale.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# ** end header
#

# TODO Remove the "sed" part of the command after the fix of
# https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1039607
CAOSDB_SERVER_VERSION != mvn org.apache.maven.plugins:maven-help-plugin:3.5.0:evaluate -Dexpression=project.version -q -DforceStdout | sed -e 's/\x1B.*//g'
CAOSDB_COMMAND_LINE_OPTIONS ?=
SHELL:=/bin/bash
JPDA_PORT ?= 9000
JMX_PORT ?= 9090

compile: print-version easy-units
	mvn compile

print-version:
	@echo "This is CaosDB $(CAOSDB_SERVER_VERSION)"

runserver: print-version
	mvn exec:java@run

run: compile
	mvn exec:java@run

run-debug: jar
	java -Xrunjdwp:transport=dt_socket,address=0.0.0.0:$(JPDA_PORT),server=y,suspend=n -Dcaosdb.debug=true -jar target/caosdb-server.jar $(CAOSDB_COMMAND_LINE_OPTIONS)


run-debug-single:
	java -Dcom.sun.management.jmxremote -Dcom.sun.management.jmxremote.port=$(JMX_PORT) -Dcom.sun.management.jmxremote.ssl=false -Dcom.sun.management.jmxremote.authenticate=false -Xrunjdwp:transport=dt_socket,address=0.0.0.0:$(JPDA_PORT),server=y,suspend=n -Dcaosdb.debug=true -jar target/caosdb-server.jar $(CAOSDB_COMMAND_LINE_OPTIONS)

run-single:
	java -jar target/caosdb-server.jar $(CAOSDB_COMMAND_LINE_OPTIONS)

formatting:
	mvn fmt:format
	autopep8 -ari scripting/

# Compile into a standalone jar file
jar: print-version easy-units
	mvn -e package -DskipTests
	@pushd target ; \
		ln -s "caosdb-server-$(CAOSDB_SERVER_VERSION)-jar-with-dependencies.jar" caosdb-server.jar; \
		popd

antlr:
	mvn antlr4:antlr4

test: print-version easy-units
	MAVEN_DEBUG_OPTS="-Xdebug -Xnoagent -Djava.compiler=NONE -Dcaosdb.debug=true -Xrunjdwp:transport=dt_socket,server=y,suspend=y,address=0.0.0.0:$(JPDA_PORT)"
	mvn test -X

test_misc:
	cd misc/bend_symlinks/ && ./bend_symlinks.sh -h && test/test_suite.sh | tee test_output
	cat misc/bend_symlinks/test_output | grep "Ran 10 tests."
	cat misc/bend_symlinks/test_output | grep "OK"
	rm misc/bend_symlinks/test_output

clean: clean-antlr
	mvn clean
	rm -rf .m2-local

clean-antlr:
	rm -rf target/generated-sources/antlr4/

.PHONY: run-server-screen
run-server-screen:
	@if test "$$(screen -ls | grep -c 'caosdb-screen')" -eq "1"; then \
		echo "server is probably running. try 'screen -ls'"; \
		exit 1 ; \
	fi
	# start and wait for server
	@screen -L -S caosdb-screen -t server -d -m -A make run
	@sleep 2
	@while [ 1 -eq 1 ] ; do \
		screen -S caosdb-screen -X hardcopy .screen.log || break ; \
		[ $$(grep -c "org.restlet.ext.jetty.JettyServerHelper start" .screen.log) -eq 0 ] || break ; \
	done; \


.PHONY: stop-server-screen
stop-server-screen:
	# stop screen session with server
	@screen -S caosdb-screen -X hardcopy screen.log || true
	@screen -S caosdb-screen -p server -X stuff "^C"

.PHONY: run-debug-screen
run-debug-screen:
	@if test "$$(screen -ls | grep -c 'caosdb-debug-screen')" -eq "1"; then \
		echo "server is probably running. try 'screen -ls'"; \
		exit 1 ; \
	fi
	# start and wait for server
	@screen -L -S caosdb-debug-screen -t server -d -m -A make run-debug
	@sleep 2
	@while [ 1 -eq 1 ] ; do \
		screen -S caosdb-debug-screen -X hardcopy .screen.log || break ; \
		[ $$(grep -c "org.restlet.ext.jetty.JettyServerHelper start" .screen.log) -eq 0 ] || break ; \
	done; \


.PHONY: stop-debug-screen
stop-debug-screen:
	# stop screen session with debug server
	@screen -S caosdb-debug-screen -X hardcopy screen.log || true
	@screen -S caosdb-debug-screen -p server -X stuff "^C"

.m2-local:
	mkdir .m2-local

easy-units: .m2-local
	mvn clean
	mvn deploy:deploy-file -DgroupId=de.timmfitschen -DartifactId=easy-units -Dversion=0.0.1 -Durl=file:./.m2-local/ -DrepositoryId=local-maven-repo -DupdateReleaseInfo=true -Dfile=./lib/easy-units-0.0.1-jar-with-dependencies.jar

# Compile the standalone documentation
.PHONY: doc
doc:
	$(MAKE) -C src/doc html
