/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2020,2023 Timm Fitschen <t.fitschen@indiscale.com>
 * Copyright (C) 2020,2023 IndiScale GmbH <info@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.caosdb.server.database.backend.transaction;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.Callable;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authz.AuthorizationException;
import org.apache.shiro.authz.Permission;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.ExecutionException;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.Subject;
import org.caosdb.server.CaosDBServer;
import org.caosdb.server.accessControl.Principal;
import org.caosdb.server.datatype.ReferenceValue;
import org.caosdb.server.entity.Entity;
import org.caosdb.server.entity.EntityID;
import org.caosdb.server.entity.EntityInterface;
import org.caosdb.server.entity.RetrieveEntity;
import org.caosdb.server.entity.wrapper.Property;
import org.caosdb.server.entity.xml.PropertyToElementStrategyTest;
import org.caosdb.server.permissions.EntityACL;
import org.caosdb.server.query.Query.Selection;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class RetrieveFullEntityTest {

  @BeforeAll
  public static void setup() throws IOException {
    CaosDBServer.initServerProperties();
  }

  @Test
  public void testRetrieveSubEntities() {
    final RetrieveFullEntityTransaction r =
        new RetrieveFullEntityTransaction(
            new EntityID("0"),
            new Subject() {

              @Override
              public Object getPrincipal() {
                return new Principal("Bla", "Blub");
              }

              @Override
              public PrincipalCollection getPrincipals() {
                return null;
              }

              @Override
              public boolean isPermitted(String permission) {
                assertEquals(
                    permission, org.caosdb.server.permissions.EntityPermission.RETRIEVE_ENTITY);
                return true;
              }

              @Override
              public boolean isPermitted(Permission permission) {
                return false;
              }

              @Override
              public boolean[] isPermitted(String... permissions) {
                return null;
              }

              @Override
              public boolean[] isPermitted(List<Permission> permissions) {
                return null;
              }

              @Override
              public boolean isPermittedAll(String... permissions) {
                return false;
              }

              @Override
              public boolean isPermittedAll(Collection<Permission> permissions) {
                return false;
              }

              @Override
              public void checkPermission(String permission) throws AuthorizationException {}

              @Override
              public void checkPermission(Permission permission) throws AuthorizationException {}

              @Override
              public void checkPermissions(String... permissions) throws AuthorizationException {}

              @Override
              public void checkPermissions(Collection<Permission> permissions)
                  throws AuthorizationException {}

              @Override
              public boolean hasRole(String roleIdentifier) {
                return false;
              }

              @Override
              public boolean[] hasRoles(List<String> roleIdentifiers) {
                return null;
              }

              @Override
              public boolean hasAllRoles(Collection<String> roleIdentifiers) {
                return false;
              }

              @Override
              public void checkRole(String roleIdentifier) throws AuthorizationException {}

              @Override
              public void checkRoles(Collection<String> roleIdentifiers)
                  throws AuthorizationException {}

              @Override
              public void checkRoles(String... roleIdentifiers) throws AuthorizationException {}

              @Override
              public void login(AuthenticationToken token) throws AuthenticationException {}

              @Override
              public boolean isAuthenticated() {
                return false;
              }

              @Override
              public boolean isRemembered() {
                return false;
              }

              @Override
              public Session getSession() {
                return null;
              }

              @Override
              public Session getSession(boolean create) {
                return null;
              }

              @Override
              public void logout() {}

              @Override
              public <V> V execute(Callable<V> callable) throws ExecutionException {
                return null;
              }

              @Override
              public void execute(Runnable runnable) {}

              @Override
              public <V> Callable<V> associateWith(Callable<V> callable) {
                return null;
              }

              @Override
              public Runnable associateWith(Runnable runnable) {
                return null;
              }

              @Override
              public void runAs(PrincipalCollection principals)
                  throws NullPointerException, IllegalStateException {}

              @Override
              public boolean isRunAs() {
                return false;
              }

              @Override
              public PrincipalCollection getPreviousPrincipals() {
                return null;
              }

              @Override
              public PrincipalCollection releaseRunAs() {
                return null;
              }
            }) {

          /** Mock-up */
          @Override
          public void retrieveFullEntity(
              final EntityInterface e, final List<Selection> selections) {
            // The id of the referenced window
            assertEquals(new EntityID("1234"), e.getId());

            // The level of selectors has been reduced by 1
            assertEquals("description", selections.get(0).getSelector());

            e.setDescription("A heart-shaped window.");
            e.setEntityACL(new EntityACL());
          }
          ;
        };

    final Property window = new Property(new RetrieveEntity(new EntityID("2345")));
    window.setName("Window");
    window.setDatatype("Window");
    window.setValue(new ReferenceValue("1234"));

    final Entity house = new RetrieveEntity(new EntityID("3456"));
    house.addProperty(window);
    final ReferenceValue value =
        (ReferenceValue) house.getProperties().getEntityById(new EntityID("2345")).getValue();
    assertEquals(new EntityID("1234"), value.getId());
    assertNull(value.getEntity());

    final List<Selection> selections = new ArrayList<>();
    selections.add(PropertyToElementStrategyTest.parse("window.description"));

    r.retrieveSubEntities(house, selections);

    assertEquals(new EntityID("1234"), value.getId());
    assertNotNull(value.getEntity());
    assertEquals("A heart-shaped window.", value.getEntity().getDescription());
  }
}
