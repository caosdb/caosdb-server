/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2019-2021 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2019-2021 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.caosdb.server.database;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

import java.util.LinkedList;
import java.util.List;
import org.junit.jupiter.api.Test;

public class DatabaseAccessManagerTest {
  Thread createReadThread(long wait, String name, ReadAccessSemaphore readAccess) {
    return new Thread(
        new Runnable() {

          @Override
          public void run() {
            try {
              readAccess.acquire();
              Thread.sleep(wait);
            } catch (InterruptedException e) {
              e.printStackTrace();
            } finally {
              readAccess.release();
            }
          }
        },
        name);
  }

  Thread createWriteThread(long wait, String name, WriteAccessLock writeAccess) {
    return new Thread(
        new Runnable() {

          @Override
          public void run() {
            try {
              writeAccess.reserve();
              Thread.sleep(wait);
              writeAccess.lockInterruptibly();
              Thread.sleep(wait);
            } catch (InterruptedException e) {
              e.printStackTrace();
            } finally {
              writeAccess.release();
            }
          }
        },
        name);
  }

  @Test
  public void testDeadLock() throws InterruptedException {
    final ReadAccessSemaphore readAccess = new ReadAccessSemaphore();
    final WriteAccessLock writeAccess = new WriteAccessLock(readAccess);
    List<Thread> ts = new LinkedList<>();
    for (int i = 0; i < 1000; i++) {
      Thread t1 = createReadThread(1, "Ra" + i, readAccess);
      Thread t2 = createReadThread(2, "Rb" + i, readAccess);
      Thread t3 = createReadThread(3, "Rc" + i, readAccess);
      Thread t5 = createReadThread(5, "Rd" + i, readAccess);
      Thread t7 = createReadThread(7, "Re" + i, readAccess);
      Thread t11 = createReadThread(11, "Rf" + i, readAccess);
      Thread w5 = createWriteThread(2, "W" + i, writeAccess);

      t1.start();
      t2.start();
      w5.start();
      t3.start();
      t5.start();
      t7.start();
      t11.start();

      ts.add(t1);
      ts.add(t2);
      ts.add(t3);
      ts.add(t5);
      ts.add(t7);
      ts.add(t11);
      ts.add(w5);
    }

    for (Thread t : ts) {
      t.join(10000);
      assertFalse(t.isAlive());
    }
  }

  public static final ReadAccessSemaphore readAccess = new ReadAccessSemaphore();
  public static final WriteAccessLock writeAccess = new WriteAccessLock(readAccess);

  /**
   * Two read-, two write-threads. The read-threads request read access, the write-threads request
   * write access.<br>
   * The read-thread rt1 is started and gets read access. Then the write-thread wt1 starts and
   * reserves write access.<br>
   * A second read-thread rt2 starts and also gets read access. A second write thread wt2 starts and
   * requests allocation of write access. It has to wait until wt2 releases it. <br>
   * wt1 acquires write access as soon as both read-threads released their read-accesss.<br>
   *
   * @throws InterruptedException
   */
  @Test
  public void test1() throws InterruptedException {
    // first invoke a read thread
    final ReadThread rt1 = new ReadThread("rt1");
    rt1.start(); // paused, has acquired read access now

    // invoke a write thread
    final WriteThread wt1 = new WriteThread("wt1");
    wt1.start(); // paused, has reserved write access now
    synchronized (this) {
      this.wait(500);
    }
    // waiting means any processing
    assertEquals(wt1.getState(), Thread.State.WAITING);

    final ReadThread rt2 = new ReadThread("rt2");
    rt2.start(); // paused, has acquired a second read access now
    synchronized (this) {
      this.wait(500);
    }
    synchronized (rt2) {
      rt2.notify();
    }
    synchronized (this) {
      this.wait(500);
    }

    // rt2 was processed while wt1 has reserved but not yet acquired write
    // access. rt2 terminated after releasing its read access.
    assertEquals(rt2.getState(), Thread.State.TERMINATED);

    final WriteThread wt2 = new WriteThread("wt2");
    wt2.start(); // wt2 immediatelly reserves write access and is block
    // since wt1 already yields it.
    synchronized (this) {
      this.wait(500);
    }
    assertEquals(wt2.getState(), Thread.State.WAITING);

    // wt1 request write access.
    synchronized (wt1) {
      wt1.notify();
    }
    // it is blocked as rt1 yet yields it.
    assertEquals(wt1.getState(), Thread.State.BLOCKED);

    // rt1 is waiting (due to pause()).
    assertEquals(rt1.getState(), Thread.State.WAITING);
    synchronized (rt1) {
      rt1.notify();
    }
    synchronized (this) {
      this.wait(500);
    }
    // rt1 was notified an terminated, releasing the read acccess.
    // so wt1 acquires write access and pauses the second time.
    assertEquals(rt1.getState(), Thread.State.TERMINATED);

    synchronized (wt1) {
      wt1.notify();
    }
    synchronized (this) {
      this.wait(1000);
    }
    // wt2 reserves write access as wt1 released it now.
    assertEquals(wt1.getState(), Thread.State.TERMINATED);
    assertEquals(wt2.getState(), Thread.State.WAITING);

    // while wt2 has not yet acquired write access, rt3 acquires read
    // access
    final ReadThread rt3 = new ReadThread("rt3");
    rt3.start();
    synchronized (this) {
      this.wait(500);
    }
    assertEquals(rt3.getState(), Thread.State.WAITING);

    synchronized (wt2) {
      wt2.notify();
    }
    synchronized (this) {
      this.wait(500);
    }
    assertEquals(wt2.getState(), Thread.State.WAITING);

    synchronized (rt3) {
      rt3.notify();
    }
    synchronized (this) {
      this.wait(500);
    }
    assertEquals(rt3.getState(), Thread.State.TERMINATED);

    synchronized (wt2) {
      wt2.notify();
    }
    synchronized (this) {
      this.wait(500);
    }
    assertEquals(wt2.getState(), Thread.State.TERMINATED);
  }

  @Test
  public void test2() throws InterruptedException {

    // start a write-thread
    final WriteThread wt1 = new WriteThread("wt1");
    wt1.start();
    // start another write-thread. It is blocked until wt1 releases the
    // write access.
    final WriteThread wt2 = new WriteThread("wt2");
    wt2.start();
    synchronized (this) {
      this.wait(500);
    }

    // and interrupt wt1 after allocating, but before acquiring the write
    // access.
    wt1.interrupt();

    synchronized (this) {
      this.wait(500);
    }
    synchronized (wt2) {
      wt2.notify();
    }

    // read access should still be blocked.
    final ReadThread rt1 = new ReadThread("rt1");
    rt1.start();

    synchronized (this) {
      this.wait(500);
    }
    assertEquals(rt1.getState(), Thread.State.WAITING);
    synchronized (wt2) {
      wt2.notify();
    }
  }

  class WriteThread extends Thread {
    @Override
    public void run() {
      try {
        System.out.println(currentThread().getName() + " request to reserve write access");
        writeAccess.reserve();
        System.out.println(currentThread().getName() + " has reserved write access ");
        process("reserved write access");
        writeAccess.lockInterruptibly();
        System.out.println(currentThread().getName() + " acquires write access");
        process("acquired write access");
        writeAccess.unlock();
        System.out.println(currentThread().getName() + " releases write access");
      } catch (final InterruptedException e) {
        System.out.println(currentThread().getName() + " was interrupted");
        writeAccess.unlock();
      }
    }

    private synchronized void process(String access) throws InterruptedException {
      System.out.println(currentThread().getName() + " processes with " + access);
      this.wait();
      System.out.println(currentThread().getName() + " is ready with " + access);
    }

    public WriteThread(String name) {
      super(name);
    }
  }

  class ReadThread extends Thread {
    @Override
    public void run() {
      try {
        System.out.println(currentThread().getName() + " requests read access");
        readAccess.acquire();
        System.out.println(currentThread().getName() + " acquires read access");
        pause();
        readAccess.release();
        System.out.println(currentThread().getName() + " releases read access");
      } catch (final InterruptedException e) {
        e.printStackTrace();
      }
    }

    private synchronized void pause() throws InterruptedException {
      System.out.println(currentThread().getName() + " waits ");
      this.wait();
      System.out.println(currentThread().getName() + " goes on");
    }

    public ReadThread(String name) {
      super(name);
    }
  }
}
