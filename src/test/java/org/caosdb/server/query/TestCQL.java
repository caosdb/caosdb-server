/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2019-2021 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2019-2021 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.caosdb.server.query;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrowsExactly;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.IOException;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.regex.Matcher;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.Vocabulary;
import org.antlr.v4.runtime.tree.ParseTree;
import org.caosdb.server.CaosDBServer;
import org.caosdb.server.database.backend.implementation.MySQL.ConnectionException;
import org.caosdb.server.database.exceptions.TransactionException;
import org.caosdb.server.query.CQLParser.CqContext;
import org.caosdb.server.query.Query.Pattern;
import org.caosdb.server.query.Query.QueryException;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

public class TestCQL {

  @BeforeAll
  public static void initServerProperties() throws IOException {
    CaosDBServer.initServerProperties();
  }

  String query1 = "FIND ename.pname1=val1";
  String query1a = "FIND ename WITH pname1=val1";
  String query1b = "FIND ename WHICH HAS A PROPERTY pname1=val1";
  String query1c = "FIND ename WHICH HAS A pname1=val1";
  String query1d = "FIND RECORD ename WHICH HAS A pname1=val1";
  String query1e = "FIND ename . pname1";
  String query2 = "FIND ename.pname1=val1 OR pname2=val2";
  String query3 = "FIND ename.pname1=val1 AND pname2=val2 AND pname3=val3";
  String query4 =
      "FIND ename WHICH HAS A PROPERTY pname1=val1 AND pname1 HAS A PROPERTY pname2=val2";
  String query5 = "FIND ename WHICH HAS A PROPERTY pname1=val1 AND (pname2=val2 OR pname3=val3)";
  String query6 = "FIND ename1 WHICH HAS A pname REFERENCE TO ename2";
  String query6a = "FIND ename1 WHICH REFERENCES ename2 AS A pname";
  String query7 = "FIND ename1 WHICH HAS A REFERENCE TO ename2";
  String query11 = "FIND ename1 WHICH IS REFERENCED BY ename2";
  String query12 = "FIND ename1 WHICH IS REFERENCED BY ename2 AS A pname1";
  String query12a = "FIND ename1 WHICH IS REFERENCED BY AN ename2 AS A pname1";
  String query13 = "FIND ename WITH NOT pname1=val1";
  String query13a = "FIND ename WHICH DOES NOT HAVE A pname1!=val1";
  String query13b = "FIND ename.!pname1=val1";
  String query14 = "FIND ename.pname1=val1 AND .pname2=val2";
  String query14a = "FIND ename WHICH DOESN'T HAVE A pname1=val1 AND WHICH HAS A pname2=val2";
  String ticket85a = "FIND FILE";
  String query15 = "FIND File WHICH IS NOT REFERENCED";
  String query16 = "FIND FILE WHICH IS STORED AT \"/bla/bla/bla\"";
  String query16a = "FIND FILE WHICH IS STORED AT /bla/bla/bla";
  String query16b = "FIND FILE WHICH IS STORED AT /bla/bla/bla/";
  String query16c = "FIND FILE WHICH IS STORED AT /bla/bla/bla.html";
  String query16d = "FIND FILE WHICH IS STORED AT //bla///bla.html";
  String query16e = "FIND FILE WHICH IS STORED AT /bla/bla_bla.html";
  String query16f = "FIND FILE WHICH IS STORED AT /bla/blubb/../bla.html";
  String query17 = "FIND FILE WHICH IS STORED AT \"/bla/bla/bla\" OR HAS A pname2=val2";
  String query18 = "FIND FILE WHICH HAS A pname2=val2 OR IS STORED AT \"/bla/bla/bla\"";
  String query19 =
      "FIND ename WHICH HAS A PROPERTY ( pname1=val1 WHICH HAS A PROPERTY pname2=val2 )";
  String query19a = "FIND ename WHICH HAS A PROPERTY pname1=val1 WHICH HAS A PROPERTY pname2=val2";
  String query19b = "FIND ename .pname1=val1.pname2=val2";
  String query19c = "FIND ename .(pname1=val1.pname2=val2)";
  String query19d = "FIND ename WHICH HAS A pname1=val1 WITH pname2=val2";
  String query20 =
      "FIND ename1 WHICH HAS A REFERENCE TO ename2 WHICH HAS A REFERENCE TO ename3 WHICH HAS A PROPERTY pname1=val1";
  String query21 = "FIND ename1.ename2.ename3.pname1=val1";
  String query22 = "FIND ename1 WHICH HAS A PROPERTY ( WHICH HAS A PROPERTY pname1=val1)";
  String query22a =
      "FIND ename1 WHICH HAS A PROPERTY ( WHICH HAS A PROPERTY ( WHICH HAS A PROPERTY (pname1=val1) ) )";
  String query23 =
      "FIND ename1 WHICH HAS A PROPERTY ename2->ename3 WHICH HAS A PROPERTY pname1=val1";
  String query23a =
      "FIND ename1 WHICH HAS A REFERENCE TO ename2 WHICH HAS A REFERENCE TO ename3 . pname1=val1";
  String query23b =
      "FIND ename1 WHICH IS REFERENCED BY ename2 WHICH IS REFERENCED BY ename3 WHICH HAS A PROPERTY pname1=val1";
  String query24 = "FIND RECORD . pname->ename";
  String ticket128 = "FIND 5\\#):xw;;-`;BY6~PjsI^*g.$+eY#n.aA9zm";
  String ticket123a = "FIND ename#";
  String ticket123b = "FIND #ename";
  String ticket123c = "FIND <<ename-regexp>>";
  String ticket123d = "FIND <<ename\\>>regexp>>";
  String ticket123e = "FIND *ename";
  String ticket123f = "FIND en*ame";
  String ticket123f2 = "FIND SimpleD*Property";
  String ticket123g = "FIND ename*";
  String ticket123h = "FIND <<SimpleD.*Property>>";
  String query25 = "FIND ename . THE GREATEST pname";
  String query26 = "FIND ename . THE SMALLEST pname";
  String query26a = "FIND SimpleRecordType WITH THE GREATEST SimpleDoubleProperty>0";
  String ticket147 =
      "FIND RECORD FrequencyMeasurement WHICH HAS A PROPERTY ObstacleRadius = '2.0' AND A PROPERTY ( BarkleyModelSimulation WHICH HAS A PROPERTY TimeStep='0.0016')";
  String ticket147a = "FIND ename WHICH HAS A PROPERTY pname=val1 AND (pname=val2)";
  String ticket147b =
      "FIND RECORD FrequencyMeasurement WHICH HAS A PROPERTY ObstacleRadius = '2.0' AND ( BarkleyModelSimulation WHICH HAS A PROPERTY TimeStep='0.0016')";
  String ticket147c =
      "FIND RECORD FrequencyMeasurement WHICH HAS A PROPERTY ObstacleRadius = '2.0' AND BarkleyModelSimulation WHICH HAS A PROPERTY TimeStep='0.0016'";
  String ticket147d =
      "FIND RECORD FrequencyMeasurement WHICH HAS A PROPERTY ObstacleRadius = '2.0' AND A PROPERTY BarkleyModelSimulation WHICH HAS A PROPERTY TimeStep='0.0016'";
  String ticket147e =
      "FIND RECORD FrequencyMeasurement WHICH HAS A PROPERTY ObstacleRadius = '2.0' AND A PROPERTY ( BarkleyModelSimulation )";
  String ticket147f = "FIND ename WHICH HAS A PROPERTY pname1=val1 AND ( pname2 )";
  String ticket147g =
      "FIND RECORD FrequencyMeasurement WHICH HAS A PROPERTY ObstacleRadius = '2.0' AND A PROPERTY ( BarkleyModelSimulation WHICH HAS A TimeStep='0.0016')";
  String ticket147h =
      "FIND RECORD FrequencyMeasurement WHICH HAS A PROPERTY ObstacleRadius = '2.0' AND A PROPERTY ( BarkleyModelSimulation WHICH HAS TimeStep='0.0016')";
  String ticket147i =
      "FIND RECORD FrequencyMeasurement WHICH HAS A PROPERTY ObstacleRadius = '2.0' AND A PROPERTY ( BarkleyModelSimulation WHICH HAS THE SMALLES TimeStep )";
  String ticket147j =
      "FIND RECORD ticket147_FrequencyMeasurement WHICH HAS A PROPERTY ticket147_ObstacleRadius = '2.0' AND A PROPERTY ( ticket147_BarkleyModelSimulation WHICH HAS ticket147_TimeStep='0.0016')";
  String query27 = "FIND 'Some name with spaces and 1234 numbers'";
  String query27a = "FIND \"Some name with spaces and 1234 numbers\"";
  String query27b = "FIND 'Some name with spaces and 1234 numbers and \"'";
  String query27c = "FIND 'Some name with spaces and 1234 numbers and \\*'";
  String query27d = "FIND 'Some name with spaces and 1234 numbers and *'";
  String query28 = "FIND ename . pname=2.02";
  String query28a = "FIND ename . pname=2.0prop=test";
  String query28b = "FIND ename . pname = 1.02m";
  String query28c = "FIND ename . pname = .02";
  String query28d = "FIND ename . pname =.02m";
  String query28e = "FIND ename . pname =.02 1/m^2";

  String ticket148 =
      "FIND RECORD FrequencyMeasurement WHICH HAS A PROPERTY ( BarkleyModelSimulation WHICH HAS A PROPERTY TimeStep='0.0016') AND A PROPERTY MinPeakHeight = '0.5'";
  String ticket148a =
      "FIND RECORD FrequencyMeasurement . ( BarkleyModelSimulation . TimeStep='0.0016') AND A PROPERTY MinPeakHeight = '0.5'";
  String ticket148b =
      "FIND RECORD FrequencyMeasurement WHICH HAS A PROPERTY MinPeakHeight = '0.5' AND A PROPERTY ( BarkleyModelSimulation . TimeStep='0.0016')";
  String query29 = "FIND ename WHICH pname IS NULL";
  String query29a = "FIND ename WHICH pname IS NOT NULL";
  String query30 = "FIND RECORD.CREATED BY 'henrik.tomwoerden' AND THE GREATEST ID";
  String query31 = "FIND PROPERTIES WHICH WERE INSERTED TODAY";
  String query32 = "FIND USER username";
  String ticket173 = "COUNT ename.pname1=val1";
  String ticket163 = "FIND Entity . pname=2.0";
  String ticket165 = "FIND Record WHICH IS REFERENCED BY annotation WITH comment='blablabla'";
  String ticket187 = "FIND Entity WITH pname=-1";
  String ticket207 = "FIND RECORD WHICH REFERENCES 10594";
  String query33 = "FIND ename WITH a date IN 2015";
  String query33a = "FIND ename WITH a date IN \"2015\"";
  String query34 = "FIND ename WITH a date NOT IN 2015";
  String query34a = "FIND ename WITH a date NOT IN \"2015\"";
  String query35 = "FIND ename WITH a date IN 2015-01-01";
  String query35a = "FIND ename WITH a date IN \"2015-01-01\"";
  String query36 = "FIND ename.pname LIKE \"wil*card\"";
  String query37 = "FIND ename.pname LIKE wil*card";
  String query38 = "FIND ename WHICH HAS A pname LIKE \"wil*\"";
  String query39 = "FIND ename WHICH HAS A pname LIKE wil*";
  String query40 = "FIND ename WHICH HAS A pname = wil*";
  String query41 = "FIND FILE WHICH IS STORED AT /data/bla.acq";
  String query42 = "FIND FILE WHICH IS STORED AT /*";
  String query43 = "FIND FILE WHICH IS STORED AT /*/";
  String query44 = "FIND FILE WHICH IS STORED AT /**/";
  String query45 = "FIND FILE WHICH IS STORED AT /**/*";
  String query46 = "FIND FILE WHICH IS STORED AT /*/*";
  String query47 = "FIND FILE WHICH IS STORED AT /*/*.acq";
  String query48 = "FIND FILE WHICH IS STORED AT /**/*.acq";
  String query49 = "FIND FILE WHICH IS STORED AT /*.acq";
  String query50 = "FIND FILE WHICH IS STORED AT *.acq";
  String query51 = "FIND FILE WHICH IS STORED AT *";
  String query52 = "FIND FILE WHICH IS STORED AT *%.acq";
  String query53 = "FIND FILE WHICH IS STORED AT *\\*.acq";
  String ticket241 = "FIND RECORD WHICH HAS been created by some*";
  String ticket242 = "FIND RECORD WHICH HAS been created by some.user";
  String ticket262a = "COUNT FILE which is not referenced";
  String ticket262b = "COUNT FILE WHICH IS NOT REFERENCED BY";
  String ticket262c = "COUNT FILE WHICH IS NOT REFERENCED BY entity";
  String ticket262d = "COUNT FILE WHICH IS NOT REFERENCED AND WHICH WAS created by me";
  String ticket262e = "COUNT FILE WHICH IS NOT REFERENCED AND WAS created by me";
  String ticket262f = "COUNT FILE WHICH IS NOT REFERENCED BY entity AND WAS created by me";
  String ticket262g = "COUNT FILE WHICH IS NOT REFERENCED BY entity AND WHICH WAS created by me";
  String ticket262h = "COUNT FILE WHICH IS NOT REFERENCED BY entity WHICH WAS created by me";
  String ticket262i = "COUNT FILE WHICH IS NOT REFERENCED BY AN entity WHICH WAS created by me";
  String ticket262j = "COUNT FILE WHICH IS REFERENCED BY A e1 AS A e2 AND IS STORED AT *config.p";
  String ticket262k = "COUNT FILE WHICH IS STORED AT *config.p AND IS REFERENCED BY A e1 AS A e2";
  String ticket262l =
      "COUNT FILE WHICH IS REFERENCED BY A e1 AS A e2 AND WHICH IS STORED AT *config.p";
  String ticket262m =
      "COUNT FILE WHICH IS STORED AT *config.p AND WHICH IS REFERENCED BY A e1 AS A e2";
  String ticket228a = "COUNT *_*";
  String ticket228b = "COUNT '*.*'";
  String query54a = "SELECT field FROM RECORD ename";
  String query54b = "SELECT field with spaces FROM RECORD ename";
  String query54c = "SELECT field1, field2, field3 FROM RECORD ename";
  String query54d = "SELECT field1.subfield1, field1.subfield2, field2.*, field3 FROM RECORD ename";
  String query54e = "SELECT id FROM ename";
  String query55a = "FIND FILE WHICH IS STORED AT /dir/with/date/2016-05-15";
  String query55b = "FIND FILE WHICH IS STORED AT /dir/with/date/2016-05-15/**";
  String query56a = "FIND RECORD WHICH REFERENCES anna";
  String query56b = "FIND RECORD WHICH REFERENCES AN ename2";
  String query56c = "FIND RECORD WHICH REFERENCES atom";
  String query56d = "FIND RECORD WHICH REFERENCES A tom";
  String query57a = "FIND ENTITY";
  String query57b = "FIND ENTITY WITH ID";
  String query57c = "FIND ENTITY WITH ID = 123";

  // strange names and values
  String query58a = "FIND ENTITY WITH endswith";
  String query58b = "FIND ENTITY WITH endswith = val1";
  String query58c = "FIND ENTITY WITH 0with = val1";
  String query58d = "FIND ENTITY.withdrawn=TRUE";
  String query58e = "FIND ENTITY WITH pname=with";

  String queryIssue31 = "FIND FILE WHICH IS STORED AT /data/in0.foo";
  String queryIssue116 = "FIND *";
  String queryIssue132a = "FIND ENTITY WHICH HAS BEEN INSERTED AFTER TODAY";
  String queryIssue132b = "FIND ENTITY WHICH HAS BEEN CREATED TODAY BY ME";
  String queryIssue134 = "SELECT pname FROM  ename";
  String queryIssue131 = "FIND ENTITY WITH pname = 13 €";
  String queryIssue145 = "FIND ENTITY WITH pname145 = 10000000000";

  // File paths ///////////////////////////////////////////////////////////////
  String filepath_verb01 = "/foo/";
  String filepath_verb02 = "/foo%/";
  String filepath_verb03 = "/foo_/";
  String filepath_verb04 = "/foo\\\\/"; // -> \\
  String filepath_verb05 = "/foo\\*/"; // -> \* (1)
  String filepath_verb06 = "/foo\\\\\\*/"; // -> \\\* (3)
  String filepath_verb07 = "/foo\\\\\\\\\\*/"; // -> \\\\\* (5)
  String filepath_pat01 = "/foo*/"; // -> * (0)
  String filepath_pat02 = "/foo\\\\*/"; // -> \\* (2)
  String filepath_pat03 = "/foo\\\\\\\\*/"; // -> \\\\* (4)
  String filepath_pat04 = "/foo**/";
  String query_filepath_quotes =
      "FIND FILE WHICH IS STORED AT '/SimulationData/2016_single/2018-01-10/**'";
  String query_filepath_quotes_2 =
      "FIND FILE WHICH IS STORED AT /SimulationData/2016_single/2018-01-10/**";

  String referenceByLikePattern = "FIND ENTITY WHICH IS REFERENCED BY *name*";

  String emptyTextValue = "FIND ENTITY WITH prop=''";
  String queryMR56 = "FIND ENTITY WITH ((p0 = v0 OR p1=v1) AND p2=v2)";

  String versionedQuery1 = "FIND ANY VERSION OF ENTITY e1";
  // https://gitlab.com/caosdb/caosdb-server/-/issues/131
  String issue131a = "FIND ename WITH pname1.x AND pname2";
  String issue131b = "FIND ename WITH (pname1.x < 10) AND (pname1.x)";
  String issue131c = "FIND ename WITH pname2 AND pname1.x ";
  String issue131d = "FIND ename WITH (pname1.x) AND pname2";
  String issue131e = "FIND ename WITH (pname1.pname2 > 30) AND (pname1.pname2 < 40)";
  String issue131f = "FIND ename WITH (pname1.pname2 > 30) AND pname1.pname2 < 40";
  String issue144 = "FIND ename WITH pname = ";

  // https://gitlab.com/caosdb/caosdb-server/-/issues/130
  String issue130a =
      "SELECT 'name with spaces.and dot', 'name with spaces'.name, name with spaces.name, name with\\,comma and\\.dot and \\'single_quote.sub FROM ENTITY";
  String issue130b = "SELECT 'Wrapper' FROM RECORD TestRT";

  // quotation marks gone rogue
  String quotation1 =
      "FIND ENTITY WHICH HAS A PROPERTY LIKE '*with double*' AND A PROPERTY LIKE '*and single*' AND A PROPERTY LIKE '*what\\'s wrong?*' AND A PROPERTY LIKE '*\\'*' AND A PROPERTY LIKE '*nothin\\'*' AND A PROPERTY LIKE '*\"\\'bla*'";
  String issue203a = "FIND WHICH ( HAS pname )";
  String issue203b = "FIND WHICH ( HAS pname1 AND REFERENCES pname2 )";

  @Test
  public void testQuery1()
      throws InterruptedException, SQLException, ConnectionException, QueryException {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.query1));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    System.out.println(sfq.toStringTree(parser));

    // 4 children: FIND, EMPTY_SPACE, entity, entity_filter
    assertEquals(4, sfq.getChildCount());
    assertEquals("FIND ", sfq.getChild(0).getText());
    assertEquals("ename", sfq.getChild(1).getText());
    assertEquals(Pattern.TYPE_NORMAL, sfq.e.type);

    // entity_filter
    assertEquals(".pname1=val1", sfq.getChild(2).getText());
    final ParseTree entity_filter = sfq.getChild(2);

    // 2 children: WHICH_EXP, conjunction
    assertEquals(2, entity_filter.getChildCount());
    assertEquals(".", entity_filter.getChild(0).getText());

    // conjunction
    assertEquals("pname1=val1", entity_filter.getChild(1).getText());
    final ParseTree conjunction = entity_filter.getChild(1);

    // 1 child: pov
    assertEquals(1, conjunction.getChildCount());
    assertEquals("pname1=val1", conjunction.getChild(0).getText());

    // pov
    final ParseTree pov1 = conjunction.getChild(0);

    // 3 chidren: property, operator, value
    assertEquals(3, pov1.getChildCount());
    assertEquals("pname1", pov1.getChild(0).getText());
    assertEquals("=", pov1.getChild(1).getText());
    assertEquals("val1", pov1.getChild(2).getText());

    assertEquals("ename", sfq.e.toString());
    assertNotNull(sfq.filter);
    assertEquals(POV.class.getName(), sfq.filter.getClass().getName());
    final EntityFilterInterface f = sfq.filter;

    assertNotNull(f);
    assertEquals("POV(pname1,=,val1)", f.toString());
  }

  @Test
  public void testQuery1a() {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.query1a));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    // 5 children: FIND, entity, WHITE_SPACE, entity_filter, EOF
    assertEquals(5, sfq.getChildCount());
    assertEquals("FIND ", sfq.getChild(0).getText());
    assertEquals("ename", sfq.getChild(1).getText());
    assertEquals(Pattern.TYPE_NORMAL, sfq.e.type);

    // entity_filter
    assertEquals("WITH pname1=val1", sfq.getChild(3).getText());
    final ParseTree entity_filter = sfq.getChild(3);

    // 2 children: WHICH_EXP, conjunction
    assertEquals(2, entity_filter.getChildCount());
    assertEquals("WITH ", entity_filter.getChild(0).getText());

    // conjunction
    assertEquals("pname1=val1", entity_filter.getChild(1).getText());
    final ParseTree conjunction = entity_filter.getChild(1);

    // 1 child: pov
    assertEquals(1, conjunction.getChildCount());
    assertEquals("pname1=val1", conjunction.getChild(0).getText());

    // pov
    final ParseTree pov1 = conjunction.getChild(0);

    // 3 chidren: property, operator, value
    assertEquals(3, pov1.getChildCount());
    assertEquals("pname1", pov1.getChild(0).getText());
    assertEquals("=", pov1.getChild(1).getText());
    assertEquals("val1", pov1.getChild(2).getText());

    assertEquals("ename", sfq.e.toString());
    assertNotNull(sfq.filter);
    assertEquals(POV.class.getName(), sfq.filter.getClass().getName());
    assertNotNull(sfq.filter);
    assertEquals("POV(pname1,=,val1)", sfq.filter.toString());
  }

  @Test
  public void testQuery1b() {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.query1b));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    // 5 children: FIND, entity, WHITE_SPACE, entity_filter, EOF
    assertEquals(5, sfq.getChildCount());
    assertEquals("FIND ", sfq.getChild(0).getText());
    assertEquals("ename", sfq.getChild(1).getText());
    assertEquals(Pattern.TYPE_NORMAL, sfq.e.type);

    // entity_filter
    assertEquals("WHICH HAS A PROPERTY pname1=val1", sfq.getChild(3).getText());
    final ParseTree entity_filter = sfq.getChild(3);

    // 2 children: WHICH_EXP,conjunction
    assertEquals(2, entity_filter.getChildCount());
    assertEquals("WHICH HAS A PROPERTY ", entity_filter.getChild(0).getText());

    // conjunction
    assertEquals("pname1=val1", entity_filter.getChild(1).getText());
    final ParseTree conjunction = entity_filter.getChild(1);

    // 1 child: pov
    assertEquals(1, conjunction.getChildCount());
    assertEquals("pname1=val1", conjunction.getChild(0).getText());

    // pov
    final ParseTree pov1 = conjunction.getChild(0);

    // 3 chidren: property, operator, value
    assertEquals(3, pov1.getChildCount());
    assertEquals("pname1", pov1.getChild(0).getText());
    assertEquals("=", pov1.getChild(1).getText());
    assertEquals("val1", pov1.getChild(2).getText());

    assertEquals("ename", sfq.e.toString());
    assertNotNull(sfq.filter);
    assertEquals(POV.class.getName(), sfq.filter.getClass().getName());

    assertNotNull(sfq.filter);
    assertEquals("POV(pname1,=,val1)", sfq.filter.toString());
  }

  @Test
  public void testQuery1c() {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.query1c));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    // 5 children: FIND, entity, WHITE_SPACE, entity_filter, EOF
    assertEquals(5, sfq.getChildCount());
    assertEquals("FIND ", sfq.getChild(0).getText());
    assertEquals("ename", sfq.getChild(1).getText());
    assertEquals(Pattern.TYPE_NORMAL, sfq.e.type);

    // entity_filter
    assertEquals("WHICH HAS A pname1=val1", sfq.getChild(3).getText());
    final ParseTree entity_filter = sfq.getChild(3);

    // 2 children: WHICH_EXP, conjunction
    assertEquals(2, entity_filter.getChildCount());
    assertEquals("WHICH HAS A ", entity_filter.getChild(0).getText());

    // conjunction
    assertEquals("pname1=val1", entity_filter.getChild(1).getText());
    final ParseTree conjunction = entity_filter.getChild(1);

    // 1 child: pov
    assertEquals(1, conjunction.getChildCount());
    assertEquals("pname1=val1", conjunction.getChild(0).getText());

    // pov
    final ParseTree pov1 = conjunction.getChild(0);

    // 3 chidren: property, operator, value
    assertEquals(3, pov1.getChildCount());
    assertEquals("pname1", pov1.getChild(0).getText());
    assertEquals("=", pov1.getChild(1).getText());
    assertEquals("val1", pov1.getChild(2).getText());

    assertEquals("ename", sfq.e.toString());
    assertNotNull(sfq.filter);
    assertEquals(POV.class.getName(), sfq.filter.getClass().getName());

    assertNotNull(sfq.filter);
    assertEquals("POV(pname1,=,val1)", sfq.filter.toString());
  }

  @Test
  public void testQuery1d() {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.query1d));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    for (final Token t : tokens.getTokens()) {
      System.out.println(t.toString());
    }
    System.out.println("query1d: " + sfq.toStringTree(parser));

    // 6 children: FIND, role, entity, WHITE_SPACE, filter, EOF
    // entity_filter
    assertEquals(6, sfq.getChildCount());
    assertEquals("FIND ", sfq.getChild(0).getText());
    assertEquals("RECORD ", sfq.getChild(1).getText());
    assertEquals("ename", sfq.getChild(2).getText());

    // entity_filter
    assertEquals("WHICH HAS A pname1=val1", sfq.getChild(4).getText());
    assertEquals(Pattern.TYPE_NORMAL, sfq.e.type);
    final ParseTree entity_filter = sfq.getChild(4);

    // 2 children: WHICH_EXP, conjunction
    assertEquals(2, entity_filter.getChildCount());
    assertEquals("WHICH HAS A ", entity_filter.getChild(0).getText());

    // conjunction
    assertEquals("pname1=val1", entity_filter.getChild(1).getText());
    final ParseTree conjunction = entity_filter.getChild(1);

    // 1 child: pov
    assertEquals(1, conjunction.getChildCount());
    assertEquals("pname1=val1", conjunction.getChild(0).getText());

    // pov
    final ParseTree pov1 = conjunction.getChild(0);

    // 3 chidren: property, operator, value
    assertEquals(3, pov1.getChildCount());
    assertEquals("pname1", pov1.getChild(0).getText());
    assertEquals("=", pov1.getChild(1).getText());
    assertEquals("val1", pov1.getChild(2).getText());

    assertEquals("ename", sfq.e.toString());
    assertNotNull(sfq.filter);
    assertEquals(POV.class.getName(), sfq.filter.getClass().getName());

    assertNotNull(sfq.filter);
    assertEquals("POV(pname1,=,val1)", sfq.filter.toString());
  }

  /*
   * String query1e = "FIND ename . pname1";
   */
  @Test
  public void testQuery1e() {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.query1e));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    for (final Token t : tokens.getTokens()) {
      System.out.println(t.toString());
    }
    System.out.println("query1e: " + sfq.toStringTree(parser));

    // 5 children: FIND, entity, WHITE_SPACE, entity_filter, EOF
    assertEquals(5, sfq.getChildCount());
    assertEquals("FIND ", sfq.getChild(0).getText());
    assertEquals("ename", sfq.getChild(1).getText());
    assertEquals(Pattern.TYPE_NORMAL, sfq.e.type);

    // entity_filter
    assertEquals(". pname1", sfq.getChild(3).getText());
    final ParseTree entity_filter = sfq.getChild(3);

    // 2 children: WHICH_EXP, conjunction
    assertEquals(2, entity_filter.getChildCount());
    assertEquals(". ", entity_filter.getChild(0).getText());

    // conjunction
    assertEquals("pname1", entity_filter.getChild(1).getText());
    final ParseTree conjunction = entity_filter.getChild(1);

    // 1 child: pov
    assertEquals(1, conjunction.getChildCount());
    assertEquals("pname1", conjunction.getChild(0).getText());

    // pov
    final ParseTree pov1 = conjunction.getChild(0);

    // 1 child: property
    assertEquals(1, pov1.getChildCount());
    assertEquals("pname1", pov1.getChild(0).getText());

    assertEquals("ename", sfq.e.toString());
    assertNotNull(sfq.filter);
    assertEquals(POV.class.getName(), sfq.filter.getClass().getName());

    assertNotNull(sfq.filter);
    assertEquals("POV(pname1,null,null)", sfq.filter.toString());
  }

  @Test
  public void testQuery2() {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.query2));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    System.out.println(sfq.toStringTree(parser));

    // 4 children: FIND, entity, entity_filter, EOF
    assertEquals(4, sfq.getChildCount());
    assertEquals("FIND ", sfq.getChild(0).getText());
    assertEquals("ename", sfq.getChild(1).getText());
    assertEquals(Pattern.TYPE_NORMAL, sfq.e.type);

    // entity_filter
    assertEquals(".pname1=val1 OR pname2=val2", sfq.getChild(2).getText());
    final ParseTree entity_filter = sfq.getChild(2);

    // 2 children: WHICH_EXP, disjunction
    assertEquals(2, entity_filter.getChildCount());
    assertEquals(".", entity_filter.getChild(0).getText());

    // disjunction
    assertEquals("pname1=val1 OR pname2=val2", entity_filter.getChild(1).getText());
    final ParseTree disjunction = entity_filter.getChild(1);

    // 3 children: pov, OR, pov
    assertEquals(3, disjunction.getChildCount());
    assertEquals("pname1=val1 ", disjunction.getChild(0).getText());
    assertEquals("OR ", disjunction.getChild(1).getText());
    assertEquals("pname2=val2", disjunction.getChild(2).getText());

    // pov
    final ParseTree pov1 = disjunction.getChild(0).getChild(0);
    final ParseTree pov2 = disjunction.getChild(2).getChild(0);

    // 3 chidren: property, operator, value
    assertEquals(3, pov1.getChildCount());
    // 3 chidren: property, operator, value
    assertEquals(3, pov2.getChildCount());

    assertEquals("pname1", pov1.getChild(0).getText());
    assertEquals("pname2", pov2.getChild(0).getText());

    assertEquals("=", pov1.getChild(1).getText());
    assertEquals("=", pov2.getChild(1).getText());

    assertEquals("val1 ", pov1.getChild(2).getText());
    assertEquals("val2", pov2.getChild(2).getText());

    assertEquals("ename", sfq.e.toString());
    assertNotNull(sfq.filter);
    assertEquals(Disjunction.class.getName(), sfq.filter.getClass().getName());
    final Disjunction c = (Disjunction) sfq.filter;
    assertNotNull(c.getFilters());
    assertFalse(c.getFilters().isEmpty());
    Integer i = 0;
    for (final EntityFilterInterface f : c.getFilters()) {
      i++;
      assertNotNull(f);
      assertEquals("POV(pname" + i.toString() + ",=,val" + i.toString() + ")", f.toString());
    }
  }

  @Test
  public void testQuery3() {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.query3));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    // 4 children: FIND, entity, entity_filter EOF
    assertEquals(4, sfq.getChildCount());
    assertEquals("FIND ", sfq.getChild(0).getText());

    // entity
    assertEquals("ename", sfq.getChild(1).getText());
    assertEquals(Pattern.TYPE_NORMAL, sfq.e.type);

    // entity filter
    assertEquals(".pname1=val1 AND pname2=val2 AND pname3=val3", sfq.getChild(2).getText());
    final ParseTree entity_filter = sfq.getChild(2);

    // 2 children: WHICH_EXP, conjunction
    assertEquals(2, entity_filter.getChildCount());
    assertEquals(".", entity_filter.getChild(0).getText());

    // conjunction
    assertEquals(
        "pname1=val1 AND pname2=val2 AND pname3=val3", entity_filter.getChild(1).getText());
    final ParseTree conjunction = entity_filter.getChild(1);

    // 9 children: pov, AND, pov, AND, pov
    assertEquals(5, conjunction.getChildCount());
    assertEquals("pname1=val1 ", conjunction.getChild(0).getText());
    assertEquals("AND ", conjunction.getChild(1).getText());
    assertEquals("pname2=val2 ", conjunction.getChild(2).getText());
    assertEquals("AND ", conjunction.getChild(3).getText());
    assertEquals("pname3=val3", conjunction.getChild(4).getText());

    // pov
    final ParseTree pov1 = conjunction.getChild(0).getChild(0);
    final ParseTree pov2 = conjunction.getChild(2).getChild(0);
    final ParseTree pov3 = conjunction.getChild(4).getChild(0);

    // 3 children: property, operator, value
    assertEquals(3, pov1.getChildCount());
    // 3 children: property, operator, value
    assertEquals(3, pov2.getChildCount());
    // 3 children: property, operator, value
    assertEquals(3, pov3.getChildCount());

    assertEquals("pname1", pov1.getChild(0).getText());
    assertEquals("pname2", pov2.getChild(0).getText());
    assertEquals("pname3", pov3.getChild(0).getText());

    assertEquals("=", pov1.getChild(1).getText());
    assertEquals("=", pov2.getChild(1).getText());
    assertEquals("=", pov3.getChild(1).getText());

    assertEquals("val1 ", pov1.getChild(2).getText());
    assertEquals("val2 ", pov2.getChild(2).getText());
    assertEquals("val3", pov3.getChild(2).getText());

    assertEquals("ename", sfq.e.toString());
    assertNotNull(sfq.filter);
    assertEquals(Conjunction.class.getName(), sfq.filter.getClass().getName());
    final Conjunction c = (Conjunction) sfq.filter;
    assertNotNull(c.getFilters());
    assertFalse(c.getFilters().isEmpty());
    Integer i = 0;
    for (final EntityFilterInterface f : c.getFilters()) {
      i++;
      assertNotNull(f);
      assertEquals("POV(pname" + i.toString() + ",=,val" + i.toString() + ")", f.toString());
    }
  }

  @Test
  public void testQuery4() {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(query4));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    System.out.println("query4: " + sfq.toStringTree(parser));

    // 5 children: FIND, entity, EMPTY_SPACE, entity_filter, EOF
    assertEquals(5, sfq.getChildCount());
    assertEquals("FIND ", sfq.getChild(0).getText());
    assertEquals("ename", sfq.getChild(1).getText());
    // entity_filter
    assertEquals(
        "WHICH HAS A PROPERTY pname1=val1 AND pname1 HAS A PROPERTY pname2=val2",
        sfq.getChild(3).getText());
    final ParseTree entity_filter1 = sfq.getChild(3);

    // 2 children: WHICH_EXP, conjunction
    assertEquals(2, entity_filter1.getChildCount());
    assertEquals("WHICH HAS A PROPERTY ", entity_filter1.getChild(0).getText());

    // conjunction
    assertEquals(
        "pname1=val1 AND pname1 HAS A PROPERTY pname2=val2", entity_filter1.getChild(1).getText());
    final ParseTree conjunction = entity_filter1.getChild(1);

    // 3 children: pov, AND, pov
    assertEquals(3, conjunction.getChildCount());
    assertEquals("pname1=val1 ", conjunction.getChild(0).getText());
    assertEquals("AND ", conjunction.getChild(1).getText());
    assertEquals("pname1 HAS A PROPERTY pname2=val2", conjunction.getChild(2).getText());

    // subproperty
    final ParseTree subproperty = conjunction.getChild(2);

    // 3 children: property, entity_filter
    assertEquals(2, subproperty.getChildCount());
    assertEquals("pname1 ", subproperty.getChild(0).getText());
    assertEquals("HAS A PROPERTY pname2=val2", subproperty.getChild(1).getText());

    // entity_filter
    final ParseTree entity_filter2 = subproperty.getChild(1).getChild(0);

    // 3 children: WHICH_EXP pov
    assertEquals(2, entity_filter2.getChildCount());
    assertEquals("HAS A PROPERTY ", entity_filter2.getChild(0).getText());
    assertEquals("pname2=val2", entity_filter2.getChild(1).getText());

    final ParseTree pov2 = entity_filter2.getChild(1).getChild(0);

    // 3 children: property, operator, value
    assertEquals(3, pov2.getChildCount());
    assertEquals("pname2", pov2.getChild(0).getText());
    assertEquals("=", pov2.getChild(1).getText());
    assertEquals("val2", pov2.getChild(2).getText());

    assertEquals("ename", sfq.e.toString());
    assertNotNull(sfq.filter);
    assertEquals(Conjunction.class.getName(), sfq.filter.getClass().getName());
    assertNotNull(sfq.filter);

    EntityFilterInterface f = sfq.filter;
    assertNotNull(f);
    assertEquals(Conjunction.class.getName(), f.getClass().getName());
    assertEquals("POV(pname1,=,val1)", ((Conjunction) f).getFilters().get(0).toString());

    f = ((Conjunction) sfq.filter).getFilters().get(1);
    assertEquals("POV(pname1,null,null)", f.toString());

    f = ((POV) f).getSubProperty().getFilter();
    assertEquals("POV(pname2,=,val2)", f.toString());
  }

  @Test
  public void testQuery5() {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.query5));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    System.out.println(sfq.toStringTree(parser));

    // r children: FIND, entity, entity_filter, EOF
    //    assertEquals(4, sfq.getChildCount());
    assertEquals("FIND ", sfq.getChild(0).getText());
    assertEquals("ename", sfq.getChild(1).getText());
    assertEquals(Pattern.TYPE_NORMAL, sfq.e.type);

    // entity_filter
    assertEquals(
        "WHICH HAS A PROPERTY pname1=val1 AND (pname2=val2 OR pname3=val3)",
        sfq.getChild(3).getText());
    final ParseTree entity_filter1 = sfq.getChild(3);

    // 2 children: WHICH_EXP, conjunction
    assertEquals(2, entity_filter1.getChildCount());
    assertEquals("WHICH HAS A PROPERTY ", entity_filter1.getChild(0).getText());

    // conjunction
    assertEquals(
        "pname1=val1 AND (pname2=val2 OR pname3=val3)", entity_filter1.getChild(1).getText());
    final ParseTree conjunction = entity_filter1.getChild(1);

    // 5 children: pov, AND, LPAREN, disjunction, RPAREN
    assertEquals(5, conjunction.getChildCount());
    assertEquals("pname1=val1 ", conjunction.getChild(0).getText());
    assertEquals("AND ", conjunction.getChild(1).getText());
    assertEquals("(", conjunction.getChild(2).getText());
    assertEquals("pname2=val2 OR pname3=val3", conjunction.getChild(3).getText());
    assertEquals(")", conjunction.getChild(4).getText());

    // pov
    final ParseTree pov1 = conjunction.getChild(0).getChild(0);

    // 3 children: property, operator, value
    assertEquals(3, pov1.getChildCount());
    assertEquals("pname1", pov1.getChild(0).getText());
    assertEquals("=", pov1.getChild(1).getText());
    assertEquals("val1 ", pov1.getChild(2).getText());

    // disjunction
    final ParseTree disjunction = conjunction.getChild(3);

    // 3 children: pov, OR, pov
    assertEquals(3, disjunction.getChildCount());
    assertEquals("pname2=val2 ", disjunction.getChild(0).getText());
    assertEquals("OR ", disjunction.getChild(1).getText());
    assertEquals("pname3=val3", disjunction.getChild(2).getText());

    // pov
    final ParseTree pov2 = disjunction.getChild(0).getChild(0);
    final ParseTree pov3 = disjunction.getChild(2).getChild(0);

    // 3 children: property, operator, value
    assertEquals(3, pov2.getChildCount());
    assertEquals(3, pov3.getChildCount());

    assertEquals("pname2", pov2.getChild(0).getText());
    assertEquals("=", pov2.getChild(1).getText());
    assertEquals("val2 ", pov2.getChild(2).getText());

    assertEquals("pname3", pov3.getChild(0).getText());
    assertEquals("=", pov3.getChild(1).getText());
    assertEquals("val3", pov3.getChild(2).getText());

    assertEquals("ename", sfq.e.toString());
    assertNotNull(sfq.filter);
    assertEquals(Conjunction.class.getName(), sfq.filter.getClass().getName());
    assertEquals(Conjunction.class.getName(), sfq.filter.getClass().getName());
    final Conjunction c = (Conjunction) sfq.filter;
    assertNotNull(c.getFilters());
    assertFalse(c.getFilters().isEmpty());

    assertEquals(2, c.getFilters().size());

    Integer i = 0;
    EntityFilterInterface f = c.getFilters().get(i);
    assertNotNull(f);
    assertEquals(POV.class.getName(), f.getClass().getName());
    assertEquals("POV(pname1,=,val1)", f.toString());

    i++;
    f = c.getFilters().get(i);
    assertNotNull(f);
    assertEquals(Disjunction.class.getName(), f.getClass().getName());
    final Disjunction d = (Disjunction) f;
    assertNotNull(d.getFilters());
    assertFalse(d.getFilters().isEmpty());
    assertEquals(2, d.getFilters().size());

    assertEquals(POV.class.getName(), d.getFilters().get(0).getClass().getName());
    assertEquals(POV.class.getName(), d.getFilters().get(1).getClass().getName());

    assertEquals("POV(pname2,=,val2)", d.getFilters().get(0).toString());
    assertEquals("POV(pname3,=,val3)", d.getFilters().get(1).toString());
  }

  @Test
  public void testQuery6() {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.query6));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    // 4 children: FIND, entity, WHITE_SPACE, entity_filter, EOF
    assertEquals(5, sfq.getChildCount());
    assertEquals("FIND ", sfq.getChild(0).getText());
    assertEquals("ename1", sfq.getChild(1).getText());
    assertEquals(Pattern.TYPE_NORMAL, sfq.e.type);

    // entity_filter
    assertEquals("WHICH HAS A pname -> ename2", sfq.getChild(3).getText());
    final ParseTree entity_filter = sfq.getChild(3);

    // 2 children: WHICH_EXP, conjunction
    assertEquals(2, entity_filter.getChildCount());
    assertEquals("WHICH HAS A ", entity_filter.getChild(0).getText());
    assertEquals("pname -> ename2", entity_filter.getChild(1).getText());
    final ParseTree conjunction = entity_filter.getChild(1);

    // 1 child: pov
    assertEquals(1, conjunction.getChildCount());
    assertEquals("pname -> ename2", conjunction.getChild(0).getText());

    // pov
    final ParseTree pov1 = conjunction.getChild(0);

    // 4 chidren: property, operator, WHITE_SPACE, value
    assertEquals(4, pov1.getChildCount());
    assertEquals("pname ", pov1.getChild(0).getText());
    assertEquals("->", pov1.getChild(1).getText());
    assertEquals("ename2", pov1.getChild(3).getText());

    assertEquals("ename1", sfq.e.toString());
    assertNotNull(sfq.filter);
    assertEquals(POV.class.getName(), sfq.filter.getClass().getName());

    assertNotNull(sfq.filter);
    assertEquals("POV(pname,->,ename2)", sfq.filter.toString());
  }

  @Test
  public void testQuery6a() {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.query6a));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    for (final Token t : tokens.getTokens()) {
      System.out.println(t.toString());
    }
    System.out.println(sfq.toStringTree(parser));

    // 5 children: FIND, entity, WHITE_SPACE, entity_filter, EOF
    assertEquals(5, sfq.getChildCount());
    assertEquals("FIND ", sfq.getChild(0).getText());
    assertEquals("ename1", sfq.getChild(1).getText());
    assertEquals(Pattern.TYPE_NORMAL, sfq.e.type);

    // entity_filter
    assertEquals("WHICH -> ename2 AS A pname", sfq.getChild(3).getText());
    final ParseTree entity_filter = sfq.getChild(3);

    // 2 children: WHICH_EXP, conjunction
    assertEquals(2, entity_filter.getChildCount());
    assertEquals("WHICH ", entity_filter.getChild(0).getText());
    assertEquals("-> ename2 AS A pname", entity_filter.getChild(1).getText());
    final ParseTree conjunction = entity_filter.getChild(1);

    // 1 child: pov
    assertEquals(1, conjunction.getChildCount());
    assertEquals("-> ename2 AS A pname", conjunction.getChild(0).getText());

    // pov
    final ParseTree pov1 = conjunction.getChild(0);

    // 5 children: operator, WHITE_SPACE, entity, AS_A, property
    assertEquals(5, pov1.getChildCount());
    assertEquals("->", pov1.getChild(0).getText());
    assertEquals("ename2 ", pov1.getChild(2).getText());
    assertEquals("AS A ", pov1.getChild(3).getText());
    assertEquals("pname", pov1.getChild(4).getText());

    assertEquals("ename1", sfq.e.toString());
    assertNotNull(sfq.filter);
    assertEquals(POV.class.getName(), sfq.filter.getClass().getName());

    assertNotNull(sfq.filter);
    assertEquals("POV(pname,->,ename2)", sfq.filter.toString());
  }

  @Test
  public void testQuery7() {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.query7));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    // 5 children: FIND, entity, WHITE_SPACE, entity_filter, EOF
    assertEquals("FIND ", sfq.getChild(0).getText());
    assertEquals("ename1", sfq.getChild(1).getText());
    assertEquals(Pattern.TYPE_NORMAL, sfq.e.type);
    assertEquals(" ", sfq.getChild(2).getText());

    // entity_filter
    assertEquals("WHICH HAS A -> ename2", sfq.getChild(3).getText());
    final ParseTree entity_filter = sfq.getChild(3);
    assertEquals(5, sfq.getChildCount());

    // 2 children: WHICH_EXP, conjunction
    assertEquals(2, entity_filter.getChildCount());
    assertEquals("WHICH HAS A ", entity_filter.getChild(0).getText());
    assertEquals("-> ename2", entity_filter.getChild(1).getText());
    final ParseTree conjunction = entity_filter.getChild(1);

    // 1 child: pov
    assertEquals(1, conjunction.getChildCount());
    assertEquals("-> ename2", conjunction.getChild(0).getText());

    // pov
    final ParseTree pov1 = conjunction.getChild(0);

    // 3 children: operator, WHITE_SPACE, value
    assertEquals(3, pov1.getChildCount());
    assertEquals("->", pov1.getChild(0).getText());
    assertEquals("ename2", pov1.getChild(2).getText());

    assertEquals("ename1", sfq.e.toString());
    assertNotNull(sfq.filter);

    assertEquals(POV.class.getName(), sfq.filter.getClass().getName());

    assertNotNull(sfq.filter);
    assertEquals("POV(null,->,ename2)", sfq.filter.toString());
  }

  @Test
  public void testQuery11() {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.query11));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    // 5 children: FIND, entity, WHITE_SPACE, entity_filter, EOF
    assertEquals(5, sfq.getChildCount());
    assertEquals("FIND ", sfq.getChild(0).getText());
    assertEquals("ename1", sfq.getChild(1).getText());
    assertEquals(Pattern.TYPE_NORMAL, sfq.e.type);
    assertEquals("WHICH IS REFERENCED BY ename2", sfq.getChild(3).getText());

    // entity_filter
    final ParseTree entity_filter = sfq.getChild(3);

    // 2 children: WHICH_EXP, conjunction
    assertEquals(2, entity_filter.getChildCount());
    assertEquals("WHICH ", entity_filter.getChild(0).getText());
    assertEquals("IS REFERENCED BY ename2", entity_filter.getChild(1).getText());
    final ParseTree conjunction = entity_filter.getChild(1);

    // 1 child: backreference
    assertEquals(1, conjunction.getChildCount());
    assertEquals("IS REFERENCED BY ename2", conjunction.getChild(0).getText());

    // backreference
    final ParseTree backreference = conjunction.getChild(0);

    // 3 children: IS_REFERENCED, BY, entity
    assertEquals(3, backreference.getChildCount());
    assertEquals("IS REFERENCED ", backreference.getChild(0).getText());
    assertEquals("BY ", backreference.getChild(1).getText());
    assertEquals("ename2", backreference.getChild(2).getText());

    assertEquals("ename1", sfq.e.toString());
    assertNotNull(sfq.filter);
    assertEquals(Backreference.class.getName(), sfq.filter.getClass().getName());

    assertNotNull(sfq.filter);
    assertEquals("@(ename2,null)", sfq.filter.toString());
  }

  @Test
  public void testQuery12() {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.query12));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    System.out.println(sfq.toStringTree(parser));

    // 5 children: FIND, entity, WHITE_SPACE, entity_filter, EOF
    assertEquals(5, sfq.getChildCount());
    assertEquals("FIND ", sfq.getChild(0).getText());
    assertEquals("ename1", sfq.getChild(1).getText());
    assertEquals(Pattern.TYPE_NORMAL, sfq.e.type);
    assertEquals("WHICH IS REFERENCED BY ename2 AS A pname1", sfq.getChild(3).getText());

    // entity_filter
    final ParseTree entity_filter = sfq.getChild(3);

    // 2 children: WHICH_EXP, conjunction
    assertEquals(2, entity_filter.getChildCount());
    assertEquals("WHICH ", entity_filter.getChild(0).getText());
    assertEquals("IS REFERENCED BY ename2 AS A pname1", entity_filter.getChild(1).getText());
    final ParseTree conjunction = entity_filter.getChild(1);

    // 1 child: backreference
    assertEquals(1, conjunction.getChildCount());
    assertEquals("IS REFERENCED BY ename2 AS A pname1", conjunction.getChild(0).getText());

    // backreference
    final ParseTree backreference = conjunction.getChild(0);

    assertEquals(Backreference.class.getName(), sfq.filter.getClass().getName());

    assertNotNull(sfq.filter);
    assertEquals("@(ename2,pname1)", sfq.filter.toString());

    // 6 children: IS_REFERENCED, BY, entity, WHITE_SPACE, AS_A, property
    assertEquals(6, backreference.getChildCount());
    assertEquals("IS REFERENCED ", backreference.getChild(0).getText());
    assertEquals("BY ", backreference.getChild(1).getText());
    assertEquals("ename2", backreference.getChild(2).getText());
    assertEquals("AS A ", backreference.getChild(4).getText());
    assertEquals("pname1", backreference.getChild(5).getText());
    assertEquals("ename1", sfq.e.toString());
    assertNotNull(sfq.filter);
  }

  @Test
  public void testQuery12a() {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.query12a));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    // 5 children: FIND, entity, WHITE_SPACE, entity_filter, EOF
    assertEquals(5, sfq.getChildCount());
    assertEquals("FIND ", sfq.getChild(0).getText());
    assertEquals("ename1", sfq.getChild(1).getText());
    assertEquals(Pattern.TYPE_NORMAL, sfq.e.type);
    assertEquals("WHICH IS REFERENCED BY AN ename2 AS A pname1", sfq.getChild(3).getText());

    // entity_filter
    final ParseTree entity_filter = sfq.getChild(3);

    // 2 children: WHICH_EXP, conjunction
    assertEquals(2, entity_filter.getChildCount());
    assertEquals("WHICH ", entity_filter.getChild(0).getText());
    assertEquals("IS REFERENCED BY AN ename2 AS A pname1", entity_filter.getChild(1).getText());
    final ParseTree conjunction = entity_filter.getChild(1);

    // 1 child: backreference
    assertEquals(1, conjunction.getChildCount());
    assertEquals("IS REFERENCED BY AN ename2 AS A pname1", conjunction.getChild(0).getText());

    // backreference
    final ParseTree backreference = conjunction.getChild(0);

    // 6 children: IS_REFERENCED, BY, AN, entity, WHITE_SPACE, AS_A, property
    assertEquals(7, backreference.getChildCount());
    assertEquals("IS REFERENCED ", backreference.getChild(0).getText());
    assertEquals("BY ", backreference.getChild(1).getText());
    assertEquals("AN ", backreference.getChild(2).getText());
    assertEquals("ename2", backreference.getChild(3).getText());
    assertEquals("AS A ", backreference.getChild(5).getText());
    assertEquals("pname1", backreference.getChild(6).getText());
    assertEquals("ename1", sfq.e.toString());
    assertNotNull(sfq.filter);

    assertEquals(Backreference.class.getName(), sfq.filter.getClass().getName());
    assertEquals("@(ename2,pname1)", sfq.filter.toString());
  }

  @Test
  public void testQuery13() {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.query13));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    // 5 children: FIND, entity, WHITE_SPACE, entity_filter, EOF
    assertEquals(5, sfq.getChildCount());
    assertEquals("FIND ", sfq.getChild(0).getText());
    assertEquals("ename", sfq.getChild(1).getText());
    assertEquals(Pattern.TYPE_NORMAL, sfq.e.type);
    assertEquals("WITH NOT pname1=val1", sfq.getChild(3).getText());

    // entity_filter
    final ParseTree entity_filter = sfq.getChild(3);

    // 2 children: WHICH_EXP, conjunction
    assertEquals(2, entity_filter.getChildCount());
    assertEquals("WITH ", entity_filter.getChild(0).getText());

    // conjunction
    assertEquals("NOT pname1=val1", entity_filter.getChild(1).getText());
    final ParseTree conjunction = entity_filter.getChild(1);

    // 1 child: negation
    assertEquals(1, conjunction.getChildCount());
    assertEquals("NOT pname1=val1", conjunction.getChild(0).getText());
    final ParseTree negation = conjunction.getChild(0);

    // 2 children: NOT, pov
    assertEquals(2, negation.getChildCount());
    assertEquals("NOT ", negation.getChild(0).getText());
    assertEquals("pname1=val1", negation.getChild(1).getText());
    final ParseTree pov1 = negation.getChild(1).getChild(0);

    // 3 chidren: property, operator, value
    assertEquals(3, pov1.getChildCount());
    assertEquals("pname1", pov1.getChild(0).getText());
    assertEquals("=", pov1.getChild(1).getText());
    assertEquals("val1", pov1.getChild(2).getText());

    assertEquals("ename", sfq.e.toString());
    assertNotNull(sfq.filter);
    assertEquals(Negation.class.getName(), sfq.filter.getClass().getName());
    final Negation c = (Negation) sfq.filter;
    assertNotNull(c.getFilter());
    assertEquals("POV(pname1,=,val1)", c.getFilter().toString());
  }

  @Test
  public void testQuery13a() {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.query13a));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    // 5 children: FIND, entity, WHITE_SPACE, entity_filter, EOF
    assertEquals(5, sfq.getChildCount());
    assertEquals("FIND ", sfq.getChild(0).getText());
    assertEquals("ename", sfq.getChild(1).getText());
    assertEquals(Pattern.TYPE_NORMAL, sfq.e.type);
    assertEquals("WHICH DOES NOT HAVE A pname1!=val1", sfq.getChild(3).getText());

    // entity_filter
    final ParseTree entity_filter = sfq.getChild(3);

    // 2 children: WHICH_EXP, conjunction
    assertEquals(2, entity_filter.getChildCount());
    assertEquals("WHICH ", entity_filter.getChild(0).getText());

    // conjunction
    assertEquals("DOES NOT HAVE A pname1!=val1", entity_filter.getChild(1).getText());
    final ParseTree conjunction = entity_filter.getChild(1);

    // 1 child: negation
    assertEquals(1, conjunction.getChildCount());
    assertEquals("DOES NOT HAVE A pname1!=val1", conjunction.getChild(0).getText());
    final ParseTree negation = conjunction.getChild(0);

    // 2 children: DOESN'T, pov
    assertEquals(2, negation.getChildCount());
    assertEquals("DOES NOT HAVE A ", negation.getChild(0).getText());
    assertEquals("pname1!=val1", negation.getChild(1).getText());
    final ParseTree pov1 = negation.getChild(1).getChild(0);

    // 3 chidren: property, operator, value
    assertEquals(3, pov1.getChildCount());
    assertEquals("pname1", pov1.getChild(0).getText());
    assertEquals("!=", pov1.getChild(1).getText());
    assertEquals("val1", pov1.getChild(2).getText());

    assertEquals("ename", sfq.e.toString());
    assertNotNull(sfq.filter);
    assertEquals(Negation.class.getName(), sfq.filter.getClass().getName());
    final Negation c = (Negation) sfq.filter;
    assertNotNull(c.getFilter());
    assertEquals("POV(pname1,!=,val1)", c.getFilter().toString());
  }

  @Test
  public void testQuery13b() {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.query13b));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    // 4 children: FIND, entity, entity_filter, EOF
    assertEquals("FIND ", sfq.getChild(0).getText());
    assertEquals("ename", sfq.getChild(1).getText());
    assertEquals(Pattern.TYPE_NORMAL, sfq.e.type);
    assertEquals(".!pname1=val1", sfq.getChild(2).getText());
    assertEquals(4, sfq.getChildCount());

    // entity_filter
    final ParseTree entity_filter = sfq.getChild(2);

    // 2 children: WHICH_EXP, conjunction
    assertEquals(2, entity_filter.getChildCount());
    assertEquals(".", entity_filter.getChild(0).getText());

    // conjunction
    assertEquals("!pname1=val1", entity_filter.getChild(1).getText());
    final ParseTree conjunction = entity_filter.getChild(1);

    // 1 child: negation
    assertEquals(1, conjunction.getChildCount());
    assertEquals("!pname1=val1", conjunction.getChild(0).getText());
    final ParseTree negation = conjunction.getChild(0);

    // children: NOT, pov
    assertEquals(2, negation.getChildCount());
    assertEquals("!", negation.getChild(0).getText());
    assertEquals("pname1=val1", negation.getChild(1).getText());
    final ParseTree pov1 = negation.getChild(1).getChild(0);

    // 3 children: property, operator, value
    assertEquals(3, pov1.getChildCount());
    assertEquals("pname1", pov1.getChild(0).getText());
    assertEquals("=", pov1.getChild(1).getText());
    assertEquals("val1", pov1.getChild(2).getText());

    assertEquals("ename", sfq.e.toString());
    assertNotNull(sfq.filter);
    assertEquals(Negation.class.getName(), sfq.filter.getClass().getName());
    final Negation c = (Negation) sfq.filter;
    assertNotNull(c.getFilter());
    assertEquals("POV(pname1,=,val1)", c.getFilter().toString());
  }

  @Test
  public void testQuery14() {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.query14));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    // 4 children: FIND, entity, entity_filter, EOF
    assertEquals(4, sfq.getChildCount());
    assertEquals("FIND ", sfq.getChild(0).getText());
    assertEquals("ename", sfq.getChild(1).getText());
    assertEquals(Pattern.TYPE_NORMAL, sfq.e.type);

    // entity_filter
    assertEquals(".pname1=val1 AND .pname2=val2", sfq.getChild(2).getText());
    final ParseTree entity_filter = sfq.getChild(2);

    // 2 children: WHICH_EXP, conjunction
    assertEquals(2, entity_filter.getChildCount());
    assertEquals(".", entity_filter.getChild(0).getText());

    // conjunction
    assertEquals("pname1=val1 AND .pname2=val2", entity_filter.getChild(1).getText());
    final ParseTree conjunction = entity_filter.getChild(1);

    // 4 child: pov1, AND, which_exp, pov2
    assertEquals(4, conjunction.getChildCount());
    assertEquals("pname1=val1 ", conjunction.getChild(0).getText());
    assertEquals("AND ", conjunction.getChild(1).getText());
    assertEquals(".", conjunction.getChild(2).getText());
    assertEquals("pname2=val2", conjunction.getChild(3).getText());
    final ParseTree pov1 = conjunction.getChild(0).getChild(0);
    final ParseTree pov2 = conjunction.getChild(3).getChild(0);

    // 3 children: property, operator, value
    assertEquals(3, pov1.getChildCount());
    assertEquals("pname1", pov1.getChild(0).getText());
    assertEquals("=", pov1.getChild(1).getText());
    assertEquals("val1 ", pov1.getChild(2).getText());

    assertEquals(3, pov2.getChildCount());
    assertEquals("pname2", pov2.getChild(0).getText());
    assertEquals("=", pov2.getChild(1).getText());
    assertEquals("val2", pov2.getChild(2).getText());

    assertEquals("ename", sfq.e.toString());
    assertNotNull(sfq.filter);
    assertEquals(Conjunction.class.getName(), sfq.filter.getClass().getName());
    final Conjunction c = (Conjunction) sfq.filter;
    assertNotNull(c.getFilters());
    assertFalse(c.getFilters().isEmpty());
    for (final EntityFilterInterface f : c.getFilters()) {
      assertNotNull(f);
      assertEquals(POV.class.getName(), f.getClass().getName());
    }
  }

  @Test
  public void testQuery14a() {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.query14a));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    // 5 children: FIND, entity, WHITE_SPACE, entity_filter, EOF
    assertEquals(5, sfq.getChildCount());
    assertEquals("FIND ", sfq.getChild(0).getText());
    assertEquals("ename", sfq.getChild(1).getText());
    assertEquals(Pattern.TYPE_NORMAL, sfq.e.type);

    // entity_filter
    assertEquals(
        "WHICH DOESN'T HAVE A pname1=val1 AND WHICH HAS A pname2=val2", sfq.getChild(3).getText());
    final ParseTree entity_filter = sfq.getChild(3);

    // 2 children: WHICH_EXP, conjunction
    assertEquals(2, entity_filter.getChildCount());
    assertEquals("WHICH ", entity_filter.getChild(0).getText());

    // conjunction
    assertEquals(
        "DOESN'T HAVE A pname1=val1 AND WHICH HAS A pname2=val2",
        entity_filter.getChild(1).getText());
    final ParseTree conjunction = entity_filter.getChild(1);

    // 4 children: pov1, AND, which_exp, pov2
    assertEquals(4, conjunction.getChildCount());
    assertEquals("DOESN'T HAVE A pname1=val1 ", conjunction.getChild(0).getText());
    assertEquals("AND ", conjunction.getChild(1).getText());
    assertEquals("WHICH HAS A ", conjunction.getChild(2).getText());
    assertEquals("pname2=val2", conjunction.getChild(3).getText());
    final ParseTree pov2 = conjunction.getChild(3).getChild(0);

    final ParseTree negation = conjunction.getChild(0).getChild(0);
    // 2 children: NOT, pov
    assertEquals(2, negation.getChildCount());
    assertEquals("DOESN'T HAVE A ", negation.getChild(0).getText());
    assertEquals("pname1=val1 ", negation.getChild(1).getText());
    final ParseTree pov1 = negation.getChild(1).getChild(0);

    // 3 children: property, operator, value
    assertEquals(3, pov1.getChildCount());
    assertEquals("pname1", pov1.getChild(0).getText());
    assertEquals("=", pov1.getChild(1).getText());
    assertEquals("val1 ", pov1.getChild(2).getText());

    assertEquals(3, pov2.getChildCount());
    assertEquals("pname2", pov2.getChild(0).getText());
    assertEquals("=", pov2.getChild(1).getText());
    assertEquals("val2", pov2.getChild(2).getText());

    assertEquals("ename", sfq.e.toString());
    assertNotNull(sfq.filter);
    assertEquals(Conjunction.class.getName(), sfq.filter.getClass().getName());
    final Conjunction c = (Conjunction) sfq.filter;
    assertNotNull(c.getFilters());
    assertFalse(c.getFilters().isEmpty());
    assertEquals(2, c.getFilters().size());

    assertNotNull(c.getFilters().get(0));
    assertEquals(Negation.class.getName(), c.getFilters().get(0).getClass().getName());
    assertEquals("POV(pname1,=,val1)", ((Negation) c.getFilters().get(0)).getFilter().toString());

    assertNotNull(c.getFilters().get(1));
    assertEquals(POV.class.getName(), c.getFilters().get(1).getClass().getName());
    assertEquals("POV(pname2,=,val2)", c.getFilters().get(1).toString());
  }

  @Test
  public void TestTicket85() {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.ticket85a));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    System.out.println(sfq.toStringTree(parser));

    // 3 children: FIND, role, EOF
    assertEquals(3, sfq.getChildCount());
    assertEquals("FIND ", sfq.getChild(0).getText());
    assertEquals("FILE", sfq.getChild(1).getText());
    assertEquals(null, sfq.e);
    assertEquals(Query.Role.FILE, sfq.r);
    assertEquals(null, sfq.filter);
  }

  @Test
  public void testQuery15() {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.query15));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    System.out.println(sfq.toStringTree(parser));
    // for (final Token t : tokens.getTokens()) {
    // System.out.println(t.toString());
    // }

    // 4 children: FIND, role, entity_filter, EOF
    assertEquals(4, sfq.getChildCount());
    assertEquals("FIND ", sfq.getChild(0).getText());
    assertEquals("File ", sfq.getChild(1).getText());
    assertEquals("WHICH IS NOT REFERENCED", sfq.getChild(2).getText());

    // entity_filter
    final ParseTree entity_filter = sfq.getChild(2);

    // 2 children: WHICH_EXP, conjunction
    assertEquals(2, entity_filter.getChildCount());
    assertEquals("WHICH IS ", entity_filter.getChild(0).getText());
    assertEquals("NOT REFERENCED", entity_filter.getChild(1).getText());
    final ParseTree conjunction = entity_filter.getChild(1);

    // 1 child: negation
    assertEquals(1, conjunction.getChildCount());
    assertEquals("NOT REFERENCED", conjunction.getChild(0).getText());
    final ParseTree negation = conjunction.getChild(0);

    // 2 children: NOT, backreference
    assertEquals(2, negation.getChildCount());
    assertEquals("NOT ", negation.getChild(0).getText());
    assertEquals("REFERENCED", negation.getChild(1).getText());

    // backreference
    final ParseTree backreference = negation.getChild(1);

    // 3 children: IS_REFERENCED_BY, entity
    assertEquals(1, backreference.getChildCount());
    assertEquals("REFERENCED", backreference.getChild(0).getText());

    assertEquals(null, sfq.e);
    assertEquals(Query.Role.FILE, sfq.r);
    assertNotNull(sfq.filter);
    assertEquals(Negation.class.getName(), sfq.filter.getClass().getName());
    final Negation c = (Negation) sfq.filter;
    assertNotNull(c.getFilter());
    assertEquals("@(null,null)", c.getFilter().toString());
  }

  @Test
  public void testQuery16() {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.query16));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    System.out.println(sfq.toStringTree(parser));

    assertEquals(null, sfq.e);
    assertEquals(Query.Role.FILE, sfq.r);
    assertNotNull(sfq.filter);
    assertEquals(StoredAt.class.getName(), sfq.filter.getClass().getName());

    // 4 children: FIND, role, entity_filter, EOF
    assertEquals(4, sfq.getChildCount());
    assertEquals("FIND ", sfq.getChild(0).getText());
    assertEquals("FILE ", sfq.getChild(1).getText());
    assertEquals("WHICH IS STORED AT \"/bla/bla/bla\"", sfq.getChild(2).getText());
    // entity_filter
    final ParseTree entity_filter = sfq.getChild(2);

    // 2 children: WHICH_EXP, conjunction
    assertEquals(2, entity_filter.getChildCount());
    assertEquals("WHICH ", entity_filter.getChild(0).getText());
    assertEquals("IS STORED AT \"/bla/bla/bla\"", entity_filter.getChild(1).getText());
    final ParseTree conjunction = entity_filter.getChild(1);

    // 1 child: storedAt
    assertEquals(1, conjunction.getChildCount());
    assertEquals("IS STORED AT \"/bla/bla/bla\"", conjunction.getChild(0).getText());
    final ParseTree storedat = conjunction.getChild(0);

    // 2 children: IS_STORED_AT, loc
    assertEquals(2, storedat.getChildCount());
    assertEquals("IS STORED AT ", storedat.getChild(0).getText());
    assertEquals("\"/bla/bla/bla\"", storedat.getChild(1).getText());

    assertEquals(null, sfq.e);
    assertEquals(Query.Role.FILE, sfq.r);
    assertNotNull(sfq.filter);
    assertEquals(StoredAt.class.getName(), sfq.filter.getClass().getName());

    assertEquals("SAT(/bla/bla/bla)", ((StoredAt) sfq.filter).toString());
  }

  @Test
  public void testQuery16a() {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.query16a));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    assertEquals(null, sfq.e);
    assertEquals(Query.Role.FILE, sfq.r);
    assertNotNull(sfq.filter);
    assertEquals(StoredAt.class.getName(), sfq.filter.getClass().getName());

    // 4 children: FIND, role, entity_filter, EOF
    //    assertEquals(4, sfq.getChildCount());
    assertEquals("FIND ", sfq.getChild(0).getText());
    assertEquals("FILE ", sfq.getChild(1).getText());
    assertEquals("WHICH IS STORED AT /bla/bla/bla", sfq.getChild(2).getText());
    // entity_filter
    final ParseTree entity_filter = sfq.getChild(2);

    // 2 children: WHICH_EXP, conjunction
    assertEquals(2, entity_filter.getChildCount());
    assertEquals("WHICH ", entity_filter.getChild(0).getText());
    assertEquals("IS STORED AT /bla/bla/bla", entity_filter.getChild(1).getText());
    final ParseTree conjunction = entity_filter.getChild(1);

    // 1 child: storedAt
    assertEquals(1, conjunction.getChildCount());
    assertEquals("IS STORED AT /bla/bla/bla", conjunction.getChild(0).getText());
    final ParseTree storedat = conjunction.getChild(0);

    // 2 children: IS_STORED_AT, loc
    assertEquals(2, storedat.getChildCount());
    assertEquals("IS STORED AT ", storedat.getChild(0).getText());
    assertEquals("/bla/bla/bla", storedat.getChild(1).getText());

    assertEquals(null, sfq.e);
    assertEquals(Query.Role.FILE, sfq.r);
    assertNotNull(sfq.filter);
    assertEquals(StoredAt.class.getName(), sfq.filter.getClass().getName());

    assertEquals("SAT(/bla/bla/bla)", ((StoredAt) sfq.filter).toString());
  }

  @Test
  public void testQuery16b() {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.query16b));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    assertEquals(null, sfq.e);
    assertEquals(Query.Role.FILE, sfq.r);
    assertNotNull(sfq.filter);
    assertEquals(StoredAt.class.getName(), sfq.filter.getClass().getName());

    // 4 children: FIND, role, entity_filter, EOF
    assertEquals(4, sfq.getChildCount());
    assertEquals("FIND ", sfq.getChild(0).getText());
    assertEquals("FILE ", sfq.getChild(1).getText());
    assertEquals("WHICH IS STORED AT /bla/bla/bla/", sfq.getChild(2).getText());
    // entity_filter
    final ParseTree entity_filter = sfq.getChild(2);

    // 2 children: WHICH_EXP, conjunction
    assertEquals(2, entity_filter.getChildCount());
    assertEquals("WHICH ", entity_filter.getChild(0).getText());
    assertEquals("IS STORED AT /bla/bla/bla/", entity_filter.getChild(1).getText());
    final ParseTree conjunction = entity_filter.getChild(1);

    // 1 child: storedAt
    assertEquals(1, conjunction.getChildCount());
    assertEquals("IS STORED AT /bla/bla/bla/", conjunction.getChild(0).getText());
    final ParseTree storedat = conjunction.getChild(0);

    // 2 children: IS_STORED_AT, loc
    assertEquals(2, storedat.getChildCount());
    assertEquals("IS STORED AT ", storedat.getChild(0).getText());
    assertEquals("/bla/bla/bla/", storedat.getChild(1).getText());

    assertEquals(null, sfq.e);
    assertEquals(Query.Role.FILE, sfq.r);
    assertEquals(StoredAt.class.getName(), sfq.filter.getClass().getName());

    assertEquals("SAT(/bla/bla/bla/)", ((StoredAt) sfq.filter).toString());
  }

  /**
   * 16c: Query String with double slash in path.
   *
   * <p>Query string is: "FIND FILE WHICH IS STORED AT /bla/bla/bla.html"
   */
  @Test
  public void testQuery16c() {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.query16c));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    assertEquals(null, sfq.e);
    assertEquals(Query.Role.FILE, sfq.r);
    assertNotNull(sfq.filter);
    assertEquals(StoredAt.class.getName(), sfq.filter.getClass().getName());

    // 4 children: FIND, role, entity_filter, EOF
    //    assertEquals(4, sfq.getChildCount());
    assertEquals("FIND ", sfq.getChild(0).getText());
    assertEquals("FILE ", sfq.getChild(1).getText());
    assertEquals("WHICH IS STORED AT /bla/bla/bla.html", sfq.getChild(2).getText());
    // entity_filter
    final ParseTree entity_filter = sfq.getChild(2);

    // 2 children: WHICH_EXP, conjunction
    assertEquals(2, entity_filter.getChildCount());
    assertEquals("WHICH ", entity_filter.getChild(0).getText());
    assertEquals("IS STORED AT /bla/bla/bla.html", entity_filter.getChild(1).getText());
    final ParseTree conjunction = entity_filter.getChild(1);

    // 1 child: storedAt
    assertEquals(1, conjunction.getChildCount());
    assertEquals("IS STORED AT /bla/bla/bla.html", conjunction.getChild(0).getText());
    final ParseTree storedat = conjunction.getChild(0);

    // 2 children: IS_STORED_AT, loc
    assertEquals(2, storedat.getChildCount());
    assertEquals("IS STORED AT ", storedat.getChild(0).getText());
    assertEquals("/bla/bla/bla.html", storedat.getChild(1).getText());

    assertEquals(null, sfq.e);
    assertEquals(Query.Role.FILE, sfq.r);
    assertNotNull(sfq.filter);
    assertEquals(StoredAt.class.getName(), sfq.filter.getClass().getName());

    assertEquals("SAT(/bla/bla/bla.html)", ((StoredAt) sfq.filter).toString());
  }

  /**
   * 16d: Query String with double slash in path.
   *
   * <p>Query string is: "FIND FILE WHICH IS STORED AT //bla///bla.html"
   */
  @Test
  public void testQuery16d() {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.query16d));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    assertEquals(null, sfq.e);
    assertEquals(Query.Role.FILE, sfq.r);
    assertNotNull(sfq.filter);
    assertEquals(StoredAt.class.getName(), sfq.filter.getClass().getName());

    // 4 children: FIND, role, entity_filter, EOF
    assertEquals(4, sfq.getChildCount());
    assertEquals("FIND ", sfq.getChild(0).getText());
    assertEquals("FILE ", sfq.getChild(1).getText());
    assertEquals("WHICH IS STORED AT //bla///bla.html", sfq.getChild(2).getText());
    // entity_filter
    final ParseTree entity_filter = sfq.getChild(2);

    // 2 children: WHICH_EXP, conjunction
    assertEquals(2, entity_filter.getChildCount());
    assertEquals("WHICH ", entity_filter.getChild(0).getText());
    assertEquals("IS STORED AT //bla///bla.html", entity_filter.getChild(1).getText());
    final ParseTree conjunction = entity_filter.getChild(1);

    // 1 child: storedAt
    assertEquals(1, conjunction.getChildCount());
    assertEquals("IS STORED AT //bla///bla.html", conjunction.getChild(0).getText());
    final ParseTree storedat = conjunction.getChild(0);

    // 2 children: IS_STORED_AT, loc
    assertEquals(2, storedat.getChildCount());
    assertEquals("IS STORED AT ", storedat.getChild(0).getText());
    assertEquals("//bla///bla.html", storedat.getChild(1).getText());

    assertEquals(null, sfq.e);
    assertEquals(Query.Role.FILE, sfq.r);
    assertNotNull(sfq.filter);
    assertEquals(StoredAt.class.getName(), sfq.filter.getClass().getName());

    assertEquals("SAT(/bla/bla.html)", ((StoredAt) sfq.filter).toString());
  }

  /**
   * 16e: Query String with underscore in path.
   *
   * <p>Query string is "FIND FILE WHICH IS STORED AT /bla/bla_bla.html"
   */
  @Test
  public void testQuery16e() {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.query16e));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    assertEquals(null, sfq.e);
    assertEquals(Query.Role.FILE, sfq.r);
    assertNotNull(sfq.filter);
    assertEquals(StoredAt.class.getName(), sfq.filter.getClass().getName());

    // 4 children: FIND, role, entity_filter, EOF
    assertEquals(4, sfq.getChildCount());
    assertEquals("FIND ", sfq.getChild(0).getText());
    assertEquals("FILE ", sfq.getChild(1).getText());
    assertEquals("WHICH IS STORED AT /bla/bla_bla.html", sfq.getChild(2).getText());
    // entity_filter
    final ParseTree entity_filter = sfq.getChild(2);

    // 2 children: WHICH_EXP, conjunction
    assertEquals(2, entity_filter.getChildCount());
    assertEquals("WHICH ", entity_filter.getChild(0).getText());
    assertEquals("IS STORED AT /bla/bla_bla.html", entity_filter.getChild(1).getText());
    final ParseTree conjunction = entity_filter.getChild(1);

    // 1 child: storedAt
    assertEquals(1, conjunction.getChildCount());
    assertEquals("IS STORED AT /bla/bla_bla.html", conjunction.getChild(0).getText());
    final ParseTree storedat = conjunction.getChild(0);

    // 2 children: IS_STORED_AT, loc
    assertEquals(2, storedat.getChildCount());
    assertEquals("IS STORED AT ", storedat.getChild(0).getText());
    assertEquals("/bla/bla_bla.html", storedat.getChild(1).getText());

    assertEquals(null, sfq.e);
    assertEquals(Query.Role.FILE, sfq.r);
    assertNotNull(sfq.filter);
    assertEquals(StoredAt.class.getName(), sfq.filter.getClass().getName());

    assertEquals("SAT(/bla/bla_bla.html)", ((StoredAt) sfq.filter).toString());
  }

  /**
   * 16f: Query String with `..` in path.
   *
   * <p>Query string is "FIND FILE WHICH IS STORED AT /bla/blubb/../bla.html"
   */
  @Test
  public void testQuery16f() {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.query16f));
    final String origPath = "/bla/blubb/../bla.html";
    final String absPath = "/bla/bla.html";
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    assertEquals(null, sfq.e);
    assertEquals(Query.Role.FILE, sfq.r);
    assertNotNull(sfq.filter);
    assertEquals(StoredAt.class.getName(), sfq.filter.getClass().getName());

    // 4 children: FIND, role, entity_filter, EOF
    assertEquals(4, sfq.getChildCount());
    assertEquals("FIND ", sfq.getChild(0).getText());
    assertEquals("FILE ", sfq.getChild(1).getText());
    assertEquals("WHICH IS STORED AT " + origPath, sfq.getChild(2).getText());
    // entity_filter
    final ParseTree entity_filter = sfq.getChild(2);

    // 2 children: WHICH_EXP, conjunction
    assertEquals(2, entity_filter.getChildCount());
    assertEquals("WHICH ", entity_filter.getChild(0).getText());
    assertEquals("IS STORED AT " + origPath, entity_filter.getChild(1).getText());
    final ParseTree conjunction = entity_filter.getChild(1);

    // 1 child: storedAt
    assertEquals(1, conjunction.getChildCount());
    assertEquals("IS STORED AT " + origPath, conjunction.getChild(0).getText());
    final ParseTree storedat = conjunction.getChild(0);

    // 2 children: IS_STORED_AT, loc
    assertEquals(2, storedat.getChildCount());
    assertEquals("IS STORED AT ", storedat.getChild(0).getText());
    assertEquals(origPath, storedat.getChild(1).getText());

    assertEquals(null, sfq.e);
    assertEquals(Query.Role.FILE, sfq.r);
    assertNotNull(sfq.filter);
    assertEquals(StoredAt.class.getName(), sfq.filter.getClass().getName());

    final StoredAt storedAt = (StoredAt) sfq.filter;
    assertFalse(StoredAt.requiresPatternMatching(storedAt.location));
    assertEquals(
        "SAT(/" + absPath.toString().replaceFirst("^/", "") + ")",
        ((StoredAt) sfq.filter).toString());
  }

  @Test
  public void testQuery17() {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.query17));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    assertEquals(null, sfq.e);
    assertEquals(Query.Role.FILE, sfq.r);
    assertNotNull(sfq.filter);
    assertEquals(Disjunction.class.getName(), sfq.filter.getClass().getName());

    // 4 children: FIND, role, entity_filter, EOF
    assertEquals(4, sfq.getChildCount());
    assertEquals("FIND ", sfq.getChild(0).getText());
    assertEquals("FILE ", sfq.getChild(1).getText());
    assertEquals(
        "WHICH IS STORED AT \"/bla/bla/bla\" OR HAS A pname2=val2", sfq.getChild(2).getText());
    // entity_filter
    final ParseTree entity_filter = sfq.getChild(2);

    // 2 children: WHICH_EXP, conjunction
    assertEquals(2, entity_filter.getChildCount());
    assertEquals("WHICH ", entity_filter.getChild(0).getText());
    assertEquals(
        "IS STORED AT \"/bla/bla/bla\" OR HAS A pname2=val2", entity_filter.getChild(1).getText());
    final ParseTree disjunction = entity_filter.getChild(1);

    // 4 child: storedAt, OR, HAS_A, POV
    assertEquals(4, disjunction.getChildCount());
    assertEquals("IS STORED AT \"/bla/bla/bla\" ", disjunction.getChild(0).getText());
    assertEquals("OR ", disjunction.getChild(1).getText());
    assertEquals("HAS A ", disjunction.getChild(2).getText());
    assertEquals("pname2=val2", disjunction.getChild(3).getText());
    final ParseTree storedat = disjunction.getChild(0).getChild(0);

    // 3 children: IS_STORED_AT, loc, WHITE_SPACE
    assertEquals(3, storedat.getChildCount());
    assertEquals("IS STORED AT ", storedat.getChild(0).getText());
    assertEquals("\"/bla/bla/bla\"", storedat.getChild(1).getText());

    assertEquals(null, sfq.e);
    assertEquals(Query.Role.FILE, sfq.r);
    assertNotNull(sfq.filter);
    assertEquals(Disjunction.class.getName(), sfq.filter.getClass().getName());

    final Disjunction c = (Disjunction) sfq.filter;
    assertNotNull(c.getFilters());
    assertFalse(c.getFilters().isEmpty());

    assertNotNull(c.getFilters());
    assertFalse(c.getFilters().isEmpty());
    assertNotNull(c.getFilters().get(0));
    assertEquals(StoredAt.class.getName(), c.getFilters().get(0).getClass().getName());
    assertEquals("SAT(/bla/bla/bla)", ((StoredAt) c.getFilters().get(0)).toString());
    assertNotNull(c.getFilters().get(1));
    assertEquals(POV.class.getName(), c.getFilters().get(1).getClass().getName());
    assertEquals("POV(pname2,=,val2)", ((POV) c.getFilters().get(1)).toString());
  }

  @Test
  public void testQuery18() {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.query18));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    assertEquals(null, sfq.e);
    assertEquals(Query.Role.FILE, sfq.r);
    assertNotNull(sfq.filter);
    assertEquals(Disjunction.class.getName(), sfq.filter.getClass().getName());

    // 4 children: FIND, role, entity_filter, EOF
    assertEquals(4, sfq.getChildCount());
    assertEquals("FIND ", sfq.getChild(0).getText());
    assertEquals("FILE ", sfq.getChild(1).getText());
    assertEquals(
        "WHICH HAS A pname2=val2 OR IS STORED AT \"/bla/bla/bla\"", sfq.getChild(2).getText());
    // entity_filter
    final ParseTree entity_filter = sfq.getChild(2);

    // 2 children: WHICH_EXP, conjunction
    assertEquals(2, entity_filter.getChildCount());
    assertEquals("WHICH HAS A ", entity_filter.getChild(0).getText());
    assertEquals(
        "pname2=val2 OR IS STORED AT \"/bla/bla/bla\"", entity_filter.getChild(1).getText());
    final ParseTree disjunction = entity_filter.getChild(1);

    // 3 children: POV, OR, storedAt
    assertEquals(3, disjunction.getChildCount());
    assertEquals("pname2=val2 ", disjunction.getChild(0).getText());
    assertEquals("OR ", disjunction.getChild(1).getText());
    assertEquals("IS STORED AT \"/bla/bla/bla\"", disjunction.getChild(2).getText());
    final ParseTree pov = disjunction.getChild(0).getChild(0);

    // 3 children: p, o, v
    assertEquals(3, pov.getChildCount());
    assertEquals("pname2", pov.getChild(0).getText());
    assertEquals("=", pov.getChild(1).getText());
    assertEquals("val2 ", pov.getChild(2).getText());

    final ParseTree storedat = disjunction.getChild(2).getChild(0);

    // 2 children: IS_STORED_AT, loc
    assertEquals(2, storedat.getChildCount());
    assertEquals("IS STORED AT ", storedat.getChild(0).getText());
    assertEquals("\"/bla/bla/bla\"", storedat.getChild(1).getText());

    assertEquals(null, sfq.e);
    assertEquals(Query.Role.FILE, sfq.r);
    assertNotNull(sfq.filter);
    assertEquals(Disjunction.class.getName(), sfq.filter.getClass().getName());

    final Disjunction c = (Disjunction) sfq.filter;
    assertNotNull(c.getFilters());
    assertFalse(c.getFilters().isEmpty());

    assertNotNull(c.getFilters());
    assertFalse(c.getFilters().isEmpty());
    assertNotNull(c.getFilters().get(1));
    assertEquals(StoredAt.class.getName(), c.getFilters().get(1).getClass().getName());
    assertEquals("SAT(/bla/bla/bla)", ((StoredAt) c.getFilters().get(1)).toString());
    assertNotNull(c.getFilters().get(0));
    assertEquals(POV.class.getName(), c.getFilters().get(0).getClass().getName());
    assertEquals("POV(pname2,=,val2)", ((POV) c.getFilters().get(0)).toString());
  }

  @Test
  public void testQuery19() {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.query19));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    assertEquals("ename", sfq.e.toString());
    assertEquals(null, sfq.r);
    // assertNotNull(sfq.filter);
    // assertEquals(Conjunction.class.getName(),
    // sfq.filter.getClass().getName());

    // 5 children: FIND, ename, WHITE_SPACE, entity_filter, EOF
    assertEquals(5, sfq.getChildCount());
    assertEquals("FIND ", sfq.getChild(0).getText());
    assertEquals("ename", sfq.getChild(1).getText());
    assertEquals(Pattern.TYPE_NORMAL, sfq.e.type);
    assertEquals(
        "WHICH HAS A PROPERTY ( pname1=val1 WHICH HAS A PROPERTY pname2=val2 )",
        sfq.getChild(3).getText());
    // entity_filter
    final ParseTree entity_filter = sfq.getChild(3);

    // 4 children: WHICH_EXP, (, conjunction, )
    assertEquals(4, entity_filter.getChildCount());
    assertEquals("WHICH HAS A PROPERTY ", entity_filter.getChild(0).getText());
    assertEquals("( ", entity_filter.getChild(1).getText());
    assertEquals(
        "pname1=val1 WHICH HAS A PROPERTY pname2=val2 ", entity_filter.getChild(2).getText());
    assertEquals(")", entity_filter.getChild(3).getText());
    final ParseTree conjunction = entity_filter.getChild(2);

    // 2 children: pov subp
    assertEquals(2, conjunction.getChildCount());
    assertEquals("pname1=val1 ", conjunction.getChild(0).getText());
    assertEquals("WHICH HAS A PROPERTY pname2=val2 ", conjunction.getChild(1).getText());
    final ParseTree pov = conjunction.getChild(0);

    // 3 children: p, o, v
    assertEquals(3, pov.getChildCount());
    assertEquals("pname1", pov.getChild(0).getText());
    assertEquals("=", pov.getChild(1).getText());
    assertEquals("val1 ", pov.getChild(2).getText());

    final ParseTree subproperty = conjunction.getChild(1);

    // 1 child: entity_filter
    assertEquals(1, subproperty.getChildCount());
    assertEquals("WHICH HAS A PROPERTY pname2=val2 ", subproperty.getChild(0).getText());

    final ParseTree subEntityFilter = subproperty.getChild(0);

    // 2 children: WHICH_EXP, pov
    assertEquals(2, subEntityFilter.getChildCount());
    assertEquals("WHICH HAS A PROPERTY ", subEntityFilter.getChild(0).getText());
    assertEquals("pname2=val2 ", subEntityFilter.getChild(1).getText());

    assertEquals("ename", sfq.e.toString());
    assertEquals(null, sfq.r);
    assertNotNull(sfq.filter);
    assertEquals(POV.class.getName(), sfq.filter.getClass().getName());

    final POV c = (POV) sfq.filter;

    assertEquals("POV(pname1,=,val1)", c.toString());
    assertTrue(c.hasSubProperty());
    assertNotNull(c.getSubProperty());
    assertEquals("SUBPROPERTY()", c.getSubProperty().toString());
    assertEquals(POV.class.getName(), c.getSubProperty().getFilter().getClass().getName());
    final POV scon = (POV) c.getSubProperty().getFilter();
    assertEquals("POV(pname2,=,val2)", scon.toString());
  }

  @Test
  public void testQuery19a() {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.query19a));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    assertEquals("ename", sfq.e.toString());
    assertEquals(Pattern.TYPE_NORMAL, sfq.e.type);
    assertEquals(null, sfq.r);

    // 5 children: FIND, ename, WHITE_SPACE, entity_filter, EOF
    assertEquals(5, sfq.getChildCount());
    assertEquals("FIND ", sfq.getChild(0).getText());
    assertEquals("ename", sfq.getChild(1).getText());
    assertEquals(Pattern.TYPE_NORMAL, sfq.e.type);
    assertEquals(
        "WHICH HAS A PROPERTY pname1=val1 WHICH HAS A PROPERTY pname2=val2",
        sfq.getChild(3).getText());
    // entity_filter
    final ParseTree entity_filter = sfq.getChild(3);

    // 2 children: WHICH_EXP, conjunction
    assertEquals(2, entity_filter.getChildCount());
    assertEquals("WHICH HAS A PROPERTY ", entity_filter.getChild(0).getText());
    assertEquals(
        "pname1=val1 WHICH HAS A PROPERTY pname2=val2", entity_filter.getChild(1).getText());
    final ParseTree conjunction = entity_filter.getChild(1);

    // 2 children: pov, subp
    assertEquals(2, conjunction.getChildCount());
    assertEquals("pname1=val1 ", conjunction.getChild(0).getText());
    assertEquals("WHICH HAS A PROPERTY pname2=val2", conjunction.getChild(1).getText());
    final ParseTree pov = conjunction.getChild(0);

    // 3 children: p, o, v
    assertEquals(3, pov.getChildCount());
    assertEquals("pname1", pov.getChild(0).getText());
    assertEquals("=", pov.getChild(1).getText());
    assertEquals("val1 ", pov.getChild(2).getText());

    final ParseTree subproperty = conjunction.getChild(1);

    // 1 child: entity_filter
    assertEquals(1, subproperty.getChildCount());
    assertEquals("WHICH HAS A PROPERTY pname2=val2", subproperty.getChild(0).getText());

    final ParseTree subEntityFilter = subproperty.getChild(0);

    // 2 children: WHICH_EXP, pov
    assertEquals(2, subEntityFilter.getChildCount());
    assertEquals("WHICH HAS A PROPERTY ", subEntityFilter.getChild(0).getText());
    assertEquals("pname2=val2", subEntityFilter.getChild(1).getText());

    assertEquals("ename", sfq.e.toString());
    assertEquals(null, sfq.r);
    assertNotNull(sfq.filter);
    assertEquals(POV.class.getName(), sfq.filter.getClass().getName());
    final POV c = (POV) sfq.filter;

    assertEquals("POV(pname1,=,val1)", c.toString());
    assertTrue(c.hasSubProperty());
    assertNotNull(c.getSubProperty());
    assertEquals("SUBPROPERTY()", c.getSubProperty().toString());
    assertEquals(POV.class.getName(), c.getSubProperty().getFilter().getClass().getName());
    final POV scon = (POV) c.getSubProperty().getFilter();
    assertEquals("POV(pname2,=,val2)", scon.toString());
  }

  @Test
  public void testQuery19b() {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.query19b));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    assertEquals("ename", sfq.e.toString());
    assertEquals(Pattern.TYPE_NORMAL, sfq.e.type);
    assertEquals(null, sfq.r);

    // 5 children: FIND, ename, WHITE_SPACE, entity_filter, EOF
    assertEquals(5, sfq.getChildCount());
    assertEquals("FIND ", sfq.getChild(0).getText());
    assertEquals("ename", sfq.getChild(1).getText());
    assertEquals(Pattern.TYPE_NORMAL, sfq.e.type);
    assertEquals(".pname1=val1.pname2=val2", sfq.getChild(3).getText());
    // entity_filter
    final ParseTree entity_filter = sfq.getChild(3);

    // 2 children: WHICH_EXP, conjunction
    assertEquals(2, entity_filter.getChildCount());
    assertEquals(".", entity_filter.getChild(0).getText());
    assertEquals("pname1=val1.pname2=val2", entity_filter.getChild(1).getText());
    final ParseTree conjunction = entity_filter.getChild(1);

    // 2 children: pov, subp
    assertEquals(2, conjunction.getChildCount());
    assertEquals("pname1=val1", conjunction.getChild(0).getText());
    assertEquals(".pname2=val2", conjunction.getChild(1).getText());
    final ParseTree pov = conjunction.getChild(0);

    // 3 children: p, o, v
    assertEquals(3, pov.getChildCount());
    assertEquals("pname1", pov.getChild(0).getText());
    assertEquals("=", pov.getChild(1).getText());
    assertEquals("val1", pov.getChild(2).getText());

    final ParseTree subproperty = conjunction.getChild(1);

    // 1 child: entity_filter
    assertEquals(1, subproperty.getChildCount());
    assertEquals(".pname2=val2", subproperty.getChild(0).getText());

    final ParseTree subEntityFilter = subproperty.getChild(0);

    // 2 children: WHICH_EXP, pov
    assertEquals(2, subEntityFilter.getChildCount());
    assertEquals(".", subEntityFilter.getChild(0).getText());
    assertEquals("pname2=val2", subEntityFilter.getChild(1).getText());

    assertEquals("ename", sfq.e.toString());
    assertEquals(null, sfq.r);
    assertNotNull(sfq.filter);

    assertEquals(POV.class.getName(), sfq.filter.getClass().getName());
    final POV c = (POV) sfq.filter;

    assertEquals("POV(pname1,=,val1)", c.toString());
    assertTrue(c.hasSubProperty());
    assertNotNull(c.getSubProperty());
    assertEquals("SUBPROPERTY()", c.getSubProperty().toString());
    assertEquals(POV.class.getName(), c.getSubProperty().getFilter().getClass().getName());
    final POV scon = (POV) c.getSubProperty().getFilter();
    assertEquals("POV(pname2,=,val2)", scon.toString());
  }

  @Test
  public void testQuery19c() {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.query19c));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    assertEquals("ename", sfq.e.toString());
    assertEquals(Pattern.TYPE_NORMAL, sfq.e.type);
    assertEquals(null, sfq.r);

    // 5 children: FIND, ename, WHITE_SPACE, entity_filter, EOF
    assertEquals(5, sfq.getChildCount());
    assertEquals("FIND ", sfq.getChild(0).getText());
    assertEquals("ename", sfq.getChild(1).getText());
    assertEquals(Pattern.TYPE_NORMAL, sfq.e.type);
    assertEquals(".(pname1=val1.pname2=val2)", sfq.getChild(3).getText());
    // entity_filter
    final ParseTree entity_filter = sfq.getChild(3);

    // 4 children: WHICH_EXP, (, conjunction, )
    assertEquals(4, entity_filter.getChildCount());
    assertEquals(".", entity_filter.getChild(0).getText());
    assertEquals("(", entity_filter.getChild(1).getText());
    assertEquals("pname1=val1.pname2=val2", entity_filter.getChild(2).getText());
    assertEquals(")", entity_filter.getChild(3).getText());
    final ParseTree conjunction = entity_filter.getChild(2);

    // 2 children: pov, subp
    assertEquals(2, conjunction.getChildCount());
    assertEquals("pname1=val1", conjunction.getChild(0).getText());
    assertEquals(".pname2=val2", conjunction.getChild(1).getText());
    final ParseTree pov = conjunction.getChild(0);

    // 3 children: p, o, v
    assertEquals(3, pov.getChildCount());
    assertEquals("pname1", pov.getChild(0).getText());
    assertEquals("=", pov.getChild(1).getText());
    assertEquals("val1", pov.getChild(2).getText());

    final ParseTree subproperty = conjunction.getChild(1);

    // 1 child: entity_filter
    assertEquals(1, subproperty.getChildCount());
    assertEquals(".pname2=val2", subproperty.getChild(0).getText());

    final ParseTree subEntityFilter = subproperty.getChild(0);

    // 2 children: WHICH_EXP, pov
    assertEquals(2, subEntityFilter.getChildCount());
    assertEquals(".", subEntityFilter.getChild(0).getText());
    assertEquals("pname2=val2", subEntityFilter.getChild(1).getText());

    assertEquals("ename", sfq.e.toString());
    assertEquals(null, sfq.r);
    assertNotNull(sfq.filter);
    assertEquals(POV.class.getName(), sfq.filter.getClass().getName());

    final POV c = (POV) sfq.filter;

    assertEquals("POV(pname1,=,val1)", c.toString());
    assertTrue(c.hasSubProperty());
    assertNotNull(c.getSubProperty());
    assertEquals("SUBPROPERTY()", c.getSubProperty().toString());

    assertEquals(POV.class.getName(), c.getSubProperty().getFilter().getClass().getName());
    final POV scon = (POV) c.getSubProperty().getFilter();

    assertEquals("POV(pname2,=,val2)", scon.toString());
  }

  @Test
  public void testQuery19d() {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.query19d));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    assertEquals("ename", sfq.e.toString());
    assertEquals(Pattern.TYPE_NORMAL, sfq.e.type);
    assertEquals(null, sfq.r);

    // 5 children: FIND, ename, WHITE_SPACE, entity_filter, EOF
    assertEquals(5, sfq.getChildCount());
    assertEquals("FIND ", sfq.getChild(0).getText());
    assertEquals("ename", sfq.getChild(1).getText());
    assertEquals(Pattern.TYPE_NORMAL, sfq.e.type);
    assertEquals("WHICH HAS A pname1=val1 WITH pname2=val2", sfq.getChild(3).getText());
    // entity_filter
    final ParseTree entity_filter = sfq.getChild(3);

    // 2 children: WHICH_EXP, conjunction
    assertEquals(2, entity_filter.getChildCount());
    assertEquals("WHICH HAS A ", entity_filter.getChild(0).getText());
    assertEquals("pname1=val1 WITH pname2=val2", entity_filter.getChild(1).getText());
    final ParseTree conjunction = entity_filter.getChild(1);

    // 2 children: pov, subp
    assertEquals(2, conjunction.getChildCount());
    assertEquals("pname1=val1 ", conjunction.getChild(0).getText());
    assertEquals("WITH pname2=val2", conjunction.getChild(1).getText());
    final ParseTree pov = conjunction.getChild(0);

    // 3 children: p, o, v
    assertEquals(3, pov.getChildCount());
    assertEquals("pname1", pov.getChild(0).getText());
    assertEquals("=", pov.getChild(1).getText());
    assertEquals("val1 ", pov.getChild(2).getText());

    final ParseTree subproperty = conjunction.getChild(1);

    // 1 child: entity_filter
    assertEquals(1, subproperty.getChildCount());
    assertEquals("WITH pname2=val2", subproperty.getChild(0).getText());

    final ParseTree subEntityFilter = subproperty.getChild(0);

    // 2 children: WHICH_EXP, pov
    assertEquals(2, subEntityFilter.getChildCount());
    assertEquals("WITH ", subEntityFilter.getChild(0).getText());
    assertEquals("pname2=val2", subEntityFilter.getChild(1).getText());

    assertEquals("ename", sfq.e.toString());
    assertEquals(null, sfq.r);
    assertNotNull(sfq.filter);
    assertEquals(POV.class.getName(), sfq.filter.getClass().getName());
    final POV c = (POV) sfq.filter;

    assertEquals("POV(pname1,=,val1)", c.toString());
    assertTrue(c.hasSubProperty());
    assertNotNull(c.getSubProperty());
    assertEquals("SUBPROPERTY()", c.getSubProperty().toString());
    assertEquals(POV.class.getName(), c.getSubProperty().getFilter().getClass().getName());
    final POV scon = (POV) c.getSubProperty().getFilter();

    assertEquals("POV(pname2,=,val2)", scon.toString());
  }

  @Test
  public void testQuery20() {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.query20));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    assertEquals("ename1", sfq.e.toString());
    assertEquals(Pattern.TYPE_NORMAL, sfq.e.type);
    assertEquals(null, sfq.r);

    // 5 children: FIND, ename, WHITE_SPACE, entity_filter, EOF
    assertEquals(5, sfq.getChildCount());
    assertEquals("FIND ", sfq.getChild(0).getText());
    assertEquals("ename1", sfq.getChild(1).getText());
    assertEquals(Pattern.TYPE_NORMAL, sfq.e.type);
    assertEquals(
        "WHICH HAS A -> ename2 WHICH HAS A -> ename3 WHICH HAS A PROPERTY pname1=val1",
        sfq.getChild(3).getText());
    // entity_filter
    final ParseTree entity_filter = sfq.getChild(3);

    // 2 children: WHICH_EXP, conjunction
    assertEquals(2, entity_filter.getChildCount());
    assertEquals("WHICH HAS A ", entity_filter.getChild(0).getText());
    assertEquals(
        "-> ename2 WHICH HAS A -> ename3 WHICH HAS A PROPERTY pname1=val1",
        entity_filter.getChild(1).getText());
    final ParseTree conjunction = entity_filter.getChild(1);

    // 2 children: pov, subp
    assertEquals(2, conjunction.getChildCount());
    assertEquals("-> ename2 ", conjunction.getChild(0).getText());
    assertEquals(
        "WHICH HAS A -> ename3 WHICH HAS A PROPERTY pname1=val1",
        conjunction.getChild(1).getText());
    final ParseTree pov = conjunction.getChild(0);

    // 3 children: ->, WHITE_SPACE, ename2
    assertEquals(3, pov.getChildCount());
    assertEquals("->", pov.getChild(0).getText());
    assertEquals("ename2 ", pov.getChild(2).getText());

    final ParseTree subproperty = conjunction.getChild(1);

    // 1 child: entity_filter
    assertEquals(1, subproperty.getChildCount());
    assertEquals(
        "WHICH HAS A -> ename3 WHICH HAS A PROPERTY pname1=val1",
        subproperty.getChild(0).getText());

    final ParseTree subEntityFilter = subproperty.getChild(0);

    // 2 children: WHICH_EXP, subp
    assertEquals(2, subEntityFilter.getChildCount());
    assertEquals("WHICH HAS A ", subEntityFilter.getChild(0).getText());
    assertEquals(
        "-> ename3 WHICH HAS A PROPERTY pname1=val1", subEntityFilter.getChild(1).getText());

    assertEquals("ename1", sfq.e.toString());
    assertEquals(null, sfq.r);
    final POV c = (POV) sfq.filter;

    assertNotNull(c);
    assertEquals("POV(null,->,ename2)", c.toString());
    assertTrue(c.hasSubProperty());
    assertNotNull(c.getSubProperty());
    assertEquals("SUBPROPERTY()", c.getSubProperty().toString());

    final POV scon = (POV) c.getSubProperty().getFilter();

    assertEquals(POV.class.getName(), scon.getClass().getName());
    assertEquals("POV(null,->,ename3)", scon.toString());
    assertNotNull(scon.getSubProperty());
    assertNotNull(scon.getSubProperty().getFilter());
    assertEquals(POV.class.getName(), scon.getSubProperty().getFilter().getClass().getName());
    final POV scon2 = (POV) scon.getSubProperty().getFilter();
    assertEquals("POV(pname1,=,val1)", scon2.toString());
  }

  @Test
  public void testQuery21() {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.query21));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    assertEquals("ename1", sfq.e.toString());
    assertEquals(Pattern.TYPE_NORMAL, sfq.e.type);
    assertEquals(null, sfq.r);

    // 4 children: FIND, ename, entity_filter, EOF
    assertEquals(4, sfq.getChildCount());
    assertEquals("FIND ", sfq.getChild(0).getText());
    assertEquals("ename1", sfq.getChild(1).getText());
    assertEquals(Pattern.TYPE_NORMAL, sfq.e.type);
    assertEquals(".ename2.ename3.pname1=val1", sfq.getChild(2).getText());
    // entity_filter
    final ParseTree entity_filter = sfq.getChild(2);

    // children: WHICH_EXP, conjunction
    assertEquals(2, entity_filter.getChildCount());
    assertEquals(".", entity_filter.getChild(0).getText());
    assertEquals("ename2.ename3.pname1=val1", entity_filter.getChild(1).getText());
    final ParseTree conjunction = entity_filter.getChild(1);

    // 2 children: pov, subp
    assertEquals(2, conjunction.getChildCount());
    assertEquals("ename2", conjunction.getChild(0).getText());
    assertEquals(".ename3.pname1=val1", conjunction.getChild(1).getText());
    final ParseTree subp = conjunction.getChild(1);

    // 1 children: filter
    assertEquals(1, subp.getChildCount());
    assertEquals(".ename3.pname1=val1", subp.getChild(0).getText());

    final ParseTree subproperty = conjunction.getChild(1);

    // 1 child: entity_filter
    assertEquals(1, subproperty.getChildCount());
    assertEquals(".ename3.pname1=val1", subproperty.getChild(0).getText());

    final ParseTree subEntityFilter = subproperty.getChild(0);

    // 2 children: WHICH_EXP, filter
    assertEquals(2, subEntityFilter.getChildCount());
    assertEquals(".", subEntityFilter.getChild(0).getText());
    assertEquals("ename3.pname1=val1", subEntityFilter.getChild(1).getText());

    assertEquals("ename1", sfq.e.toString());
    assertEquals(null, sfq.r);
    final POV c = (POV) sfq.filter;
    assertNotNull(c);
    assertEquals("POV(ename2,null,null)", c.toString());
    assertTrue(c.hasSubProperty());
    assertNotNull(c.getSubProperty());
    assertEquals("SUBPROPERTY()", c.getSubProperty().toString());

    final POV scon = (POV) c.getSubProperty().getFilter();
    assertEquals("POV(ename3,null,null)", scon.toString());
    assertNotNull(scon.getSubProperty());
    assertNotNull(scon.getSubProperty().getFilter());
    assertEquals(POV.class.getName(), scon.getSubProperty().getFilter().getClass().getName());
    final POV scon2 = (POV) scon.getSubProperty().getFilter();
    assertEquals("POV(pname1,=,val1)", scon2.toString());
  }

  @Test
  public void testQuery22() {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.query22));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    assertEquals("ename1", sfq.e.toString());
    assertEquals(Pattern.TYPE_NORMAL, sfq.e.type);
    assertEquals(null, sfq.r);

    // 5 children: FIND, ename, WHITE_SPACE, entity_filter, EOF
    assertEquals(5, sfq.getChildCount());
    assertEquals("FIND ", sfq.getChild(0).getText());
    assertEquals("ename1", sfq.getChild(1).getText());
    assertEquals(Pattern.TYPE_NORMAL, sfq.e.type);
    assertEquals(
        "WHICH HAS A PROPERTY ( WHICH HAS A PROPERTY pname1=val1)", sfq.getChild(3).getText());
    final ParseTree entity_filter = sfq.getChild(3);

    // 4 children: WHICH_EXP, (, subproperty, )
    assertEquals(4, entity_filter.getChildCount());
    assertEquals("WHICH HAS A PROPERTY ", entity_filter.getChild(0).getText());
    assertEquals("( ", entity_filter.getChild(1).getText());
    assertEquals("WHICH HAS A PROPERTY pname1=val1", entity_filter.getChild(2).getText());
    assertEquals(")", entity_filter.getChild(3).getText());
    final ParseTree subproperty1 = entity_filter.getChild(2);

    // 1 child: filter
    assertEquals(1, subproperty1.getChildCount());
    assertEquals("WHICH HAS A PROPERTY pname1=val1", subproperty1.getChild(0).getText());
    final ParseTree filter = subproperty1.getChild(0);

    // 1 children: filter
    assertEquals(1, filter.getChildCount());
    assertEquals("WHICH HAS A PROPERTY pname1=val1", filter.getChild(0).getText());

    final ParseTree subproperty = filter.getChild(0);

    // 2 children: WHICH entity_filter
    assertEquals(2, subproperty.getChildCount());
    assertEquals("WHICH HAS A PROPERTY ", subproperty.getChild(0).getText());
    assertEquals("pname1=val1", subproperty.getChild(1).getText());

    assertEquals("ename1", sfq.e.toString());
    assertEquals(null, sfq.r);
    final SubProperty c = (SubProperty) sfq.filter;
    assertNotNull(c.getFilter());

    assertEquals(POV.class.getName(), c.getFilter().getClass().getName());
    final POV scon = (POV) c.getFilter();

    assertEquals("POV(pname1,=,val1)", scon.toString());
  }

  @Test
  public void testQuery22a() {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.query22a));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    assertEquals("ename1", sfq.e.toString());
    assertEquals(Pattern.TYPE_NORMAL, sfq.e.type);
    assertEquals(null, sfq.r);

    // 5 children: FIND, ename, WHITE_SPACE, entity_filter, EOF
    assertEquals(5, sfq.getChildCount());
    assertEquals("FIND ", sfq.getChild(0).getText());
    assertEquals("ename1", sfq.getChild(1).getText());

    // entity_filter
    final ParseTree entity_filter = sfq.getChild(3);

    // 4 children: WHICH_EXP, (, conjunction, )
    assertEquals(4, entity_filter.getChildCount());
    assertEquals("WHICH HAS A PROPERTY ", entity_filter.getChild(0).getText());
    assertEquals("( ", entity_filter.getChild(1).getText());
    assertEquals(
        "WHICH HAS A PROPERTY ( WHICH HAS A PROPERTY (pname1=val1) ) ",
        entity_filter.getChild(2).getText());
    assertEquals(")", entity_filter.getChild(3).getText());
    final ParseTree conjunction = entity_filter.getChild(2);

    // 1 children
    assertEquals(1, conjunction.getChildCount());
    assertEquals(
        "WHICH HAS A PROPERTY ( WHICH HAS A PROPERTY (pname1=val1) ) ",
        conjunction.getChild(0).getText());
    final ParseTree subp = conjunction.getChild(0);

    // 1 children: filter
    assertEquals(1, subp.getChildCount());
    assertEquals(
        "WHICH HAS A PROPERTY ( WHICH HAS A PROPERTY (pname1=val1) ) ", subp.getChild(0).getText());

    final ParseTree subproperty = subp.getChild(0);

    // 4 children: which, (, conjunction, )
    assertEquals(4, subproperty.getChildCount());
    assertEquals("WHICH HAS A PROPERTY ", subproperty.getChild(0).getText());
    assertEquals("( ", subproperty.getChild(1).getText());
    assertEquals("WHICH HAS A PROPERTY (pname1=val1) ", subproperty.getChild(2).getText());
    assertEquals(") ", subproperty.getChild(3).getText());

    assertEquals("ename1", sfq.e.toString());
    assertEquals(null, sfq.r);

    final SubProperty c = (SubProperty) sfq.filter;
    assertNotNull(c);
    assertNotNull(c.getFilter());

    assertEquals(SubProperty.class.getName(), c.getFilter().getClass().getName());

    final SubProperty scon = (SubProperty) c.getFilter();

    final POV scon2 = (POV) scon.getFilter();

    assertEquals("POV(pname1,=,val1)", scon2.toString());
  }

  @Test
  public void testQuery24() {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.query24));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    assertEquals(null, sfq.e);
    assertEquals(Query.Role.RECORD, sfq.r);
    final POV c = (POV) sfq.filter;
    assertNotNull(c);

    // 4 children: FIND, ename, entity_filter, EOF
    assertEquals(4, sfq.getChildCount());
    assertEquals("FIND ", sfq.getChild(0).getText());
    assertEquals("RECORD ", sfq.getChild(1).getText());
    final ParseTree entity_filter = sfq.getChild(2);

    // 2 children: WHICH_EXP, conjunction
    assertEquals(2, entity_filter.getChildCount());
    assertEquals(". ", entity_filter.getChild(0).getText());
    assertEquals("pname->ename", entity_filter.getChild(1).getText());
    final ParseTree conjunction = entity_filter.getChild(1);

    // 1 children
    assertEquals(1, conjunction.getChildCount());
    assertEquals("pname->ename", conjunction.getChild(0).getText());

    assertNotNull(c);
    assertEquals(POV.class.getName(), c.getClass().getName());
    assertEquals("POV(pname,->,ename)", c.toString());
  }

  @Test
  public void TestTicket128() {

    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.ticket128));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);
    tokens.getText();

    final CQLParser parser = new CQLParser(tokens);
    final CQLParsingErrorListener el = new CQLParsingErrorListener(CQLLexer.UNKNOWN_CHAR);
    parser.removeErrorListeners();
    parser.addErrorListener(el);
    parser.cq();

    for (final Token t : tokens.getTokens()) {
      System.out.println(t.toString());
    }
    System.out.println(parser.cq().toStringTree(parser));

    assertTrue(el.hasErrors());
  }

  @Test
  public void TestTicket123a()
      throws InterruptedException, SQLException, ConnectionException, QueryException {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.ticket123a));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    for (final Token t : tokens.getTokens()) {
      System.out.println(t.toString());
    }
    System.out.println(sfq.toStringTree(parser));

    // 3 children: FIND, entity, EOF
    assertEquals(3, sfq.getChildCount());
    assertEquals("FIND ", sfq.getChild(0).getText());
    assertEquals("ename#", sfq.getChild(1).getText());
    assertEquals(Query.Pattern.TYPE_NORMAL, sfq.e.type);
  }

  @Test
  public void TestTicket123b()
      throws InterruptedException, SQLException, ConnectionException, QueryException {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.ticket123b));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    // 3 children: FIND, entity, EOF
    assertEquals(3, sfq.getChildCount());
    assertEquals("FIND ", sfq.getChild(0).getText());
    assertEquals("#ename", sfq.getChild(1).getText());
    assertEquals(Query.Pattern.TYPE_NORMAL, sfq.e.type);
  }

  @Test
  public void TestTicket123c()
      throws InterruptedException, SQLException, ConnectionException, QueryException {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.ticket123c));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    System.out.println(sfq.toStringTree(parser));

    // 3 children: FIND, entity, EOF
    assertEquals(3, sfq.getChildCount());
    assertEquals("FIND ", sfq.getChild(0).getText());
    assertEquals("<<ename-regexp>>", sfq.getChild(1).getText());
    assertEquals(Query.Pattern.TYPE_REGEXP, sfq.e.type);
    assertEquals("ename-regexp", sfq.e.toString());
  }

  @Test
  public void TestTicket123d()
      throws InterruptedException, SQLException, ConnectionException, QueryException {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.ticket123d));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    for (final Token t : tokens.getTokens()) {
      System.out.println(t.toString());
    }
    System.out.println(sfq.toStringTree(parser));

    // 3 children: FIND, entity, EOF
    assertEquals(3, sfq.getChildCount());
    assertEquals("FIND ", sfq.getChild(0).getText());
    assertEquals("<<ename\\>>regexp>>", sfq.getChild(1).getText());
    assertEquals(Query.Pattern.TYPE_REGEXP, sfq.e.type);
    assertEquals("ename>>regexp", sfq.e.toString());
  }

  // String ticket123e = "FIND *ename";
  @Test
  public void TestTicket123e()
      throws InterruptedException, SQLException, ConnectionException, QueryException {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.ticket123e));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    for (final Token t : tokens.getTokens()) {
      System.out.println(t.toString());
    }
    System.out.println(sfq.toStringTree(parser));

    // 3 children: FIND, entity, EOF
    assertEquals(3, sfq.getChildCount());
    assertEquals("FIND ", sfq.getChild(0).getText());
    assertEquals("*ename", sfq.getChild(1).getText());
    assertEquals(Query.Pattern.TYPE_LIKE, sfq.e.type);
    assertEquals("%ename", sfq.e.toString());
  }

  /*
   * String ticket123f = "FIND en*ame";
   */
  @Test
  public void TestTicket123f()
      throws InterruptedException, SQLException, ConnectionException, QueryException {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.ticket123f));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    System.out.println(sfq.toStringTree(parser));

    // 3 children: FIND, entity, EOF
    assertEquals(3, sfq.getChildCount());
    assertEquals("FIND ", sfq.getChild(0).getText());
    assertEquals("en*ame", sfq.getChild(1).getText());
    assertEquals(Query.Pattern.TYPE_LIKE, sfq.e.type);
    assertEquals("en%ame", sfq.e.toString());
  }

  /*
   * String ticket123f2 = "FIND SimpleD*Property";
   */
  @Test
  public void TestTicket123f2()
      throws InterruptedException, SQLException, ConnectionException, QueryException {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.ticket123f2));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    System.out.println(sfq.toStringTree(parser));

    // 3 children: FIND, entity, EOF
    assertEquals(3, sfq.getChildCount());
    assertEquals("FIND ", sfq.getChild(0).getText());
    assertEquals("SimpleD*Property", sfq.getChild(1).getText());
    assertEquals(Query.Pattern.TYPE_LIKE, sfq.e.type);
    assertEquals("SimpleD%Property", sfq.e.toString());
  }

  /*
   * String ticket123g = "FIND ename*";
   */
  @Test
  public void TestTicket123g()
      throws InterruptedException, SQLException, ConnectionException, QueryException {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.ticket123g));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    System.out.println(sfq.toStringTree(parser));

    // 3 children: FIND, entity, EOF
    assertEquals(3, sfq.getChildCount());
    assertEquals("FIND ", sfq.getChild(0).getText());
    assertEquals("ename*", sfq.getChild(1).getText());
    assertEquals(Query.Pattern.TYPE_LIKE, sfq.e.type);
    assertEquals("ename%", sfq.e.toString());
  }

  /*
   * String ticket123h = "FIND <<SimpleD.*Property>>";
   */
  @Test
  public void TestTicket123h()
      throws InterruptedException, SQLException, ConnectionException, QueryException {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.ticket123h));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    System.out.println(sfq.toStringTree(parser));

    // 3 children: FIND, entity, EOF
    assertEquals(3, sfq.getChildCount());
    assertEquals("FIND ", sfq.getChild(0).getText());
    assertEquals("<<SimpleD.*Property>>", sfq.getChild(1).getText());
    assertEquals(Query.Pattern.TYPE_REGEXP, sfq.e.type);
    assertEquals("SimpleD.*Property", sfq.e.toString());
  }

  /*
   * String query25 = "FIND ename . THE GREATEST pname";
   */
  @Test
  public void testQuery25()
      throws InterruptedException, SQLException, ConnectionException, QueryException {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.query25));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    System.out.println(sfq.toStringTree(parser));

    // 5 children: FIND, entity, WHITE_SPACE, filter, EOF
    assertEquals(5, sfq.getChildCount());
    assertEquals("FIND ", sfq.getChild(0).getText());
    assertEquals("ename", sfq.getChild(1).getText());
    assertEquals(". THE GREATEST pname", sfq.getChild(3).getText());
    assertEquals(Query.Pattern.TYPE_NORMAL, sfq.e.type);
  }

  /*
   * String query26 = "FIND ename . THE SMALLEST pname";
   */
  @Test
  public void testQuery26()
      throws InterruptedException, SQLException, ConnectionException, QueryException {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.query26));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    System.out.println(sfq.toStringTree(parser));

    // 5 children: FIND, entity, WHITE_SPACE, filter, EOF
    assertEquals(5, sfq.getChildCount());
    assertEquals("FIND ", sfq.getChild(0).getText());
    assertEquals("ename", sfq.getChild(1).getText());
    assertEquals(". THE SMALLEST pname", sfq.getChild(3).getText());
    assertEquals(Query.Pattern.TYPE_NORMAL, sfq.e.type);
  }

  /*
   * String query26a =
   * "FIND SimpleRecordType WITH THE GREATEST SimpleDoubleProperty>0";
   */
  @Test
  public void testQuery26a()
      throws InterruptedException, SQLException, ConnectionException, QueryException {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.query26a));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    System.out.println(sfq.toStringTree(parser));

    // 5 children: FIND, entity, WHITE_SPACE, filter, EOF
    assertEquals(5, sfq.getChildCount());
    assertEquals("FIND ", sfq.getChild(0).getText());
    assertEquals("SimpleRecordType", sfq.getChild(1).getText());
    assertEquals("WITH THE GREATEST SimpleDoubleProperty>0", sfq.getChild(3).getText());
    assertEquals(Query.Pattern.TYPE_NORMAL, sfq.e.type);

    assertEquals(2, sfq.getChild(3).getChildCount());
    final ParseTree filter = sfq.getChild(3);
    assertEquals("WITH ", filter.getChild(0).getText());
    assertEquals("THE GREATEST SimpleDoubleProperty>0", filter.getChild(1).getText());

    assertEquals(1, filter.getChild(1).getChildCount());
    assertEquals(3, filter.getChild(1).getChild(0).getChildCount());
  }

  /*
   * String ticket147 =
   * "FIND RECORD FrequencyMeasurement WHICH HAS A PROPERTY ObstacleRadius = '2.0' AND A PROPERTY ( BarkleyModelSimulation WHICH HAS A PROPERTY TimeStep='0.0016')"
   * ;
   */
  @Test
  public void TestTicket147()
      throws InterruptedException, SQLException, ConnectionException, QueryException {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.ticket147));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);
    System.out.println(tokens.getText());

    final CQLParser parser = new CQLParser(tokens);
    for (final Token t : tokens.getTokens()) {
      System.out.println(t.toString());
    }

    final CqContext sfq = parser.cq();

    System.out.println(sfq.toStringTree(parser));

    // 5 children: FIND, RECORD, entity, entity_filter, EOF
    assertEquals(6, sfq.getChildCount());
    assertEquals("FIND ", sfq.getChild(0).getText());
    assertEquals("RECORD ", sfq.getChild(1).getText());
    assertEquals("FrequencyMeasurement", sfq.getChild(2).getText());
    assertEquals(
        "WHICH HAS A PROPERTY ObstacleRadius = '2.0' AND A PROPERTY ( BarkleyModelSimulation WHICH HAS A PROPERTY TimeStep='0.0016')",
        sfq.getChild(4).getText());
    assertEquals(Query.Pattern.TYPE_NORMAL, sfq.e.type);

    assertNotNull(sfq.filter);
    assertEquals(Conjunction.class.getName(), sfq.filter.getClass().getName());
    final Conjunction c1 = (Conjunction) sfq.filter;
    assertNotNull(c1.getFilters());
    assertEquals(2, c1.getFilters().size());
    assertEquals(POV.class.getName(), c1.getFilters().get(0).getClass().getName());
    assertEquals("POV(ObstacleRadius,=,2.0)", c1.getFilters().get(0).toString());
    assertEquals(POV.class.getName(), c1.getFilters().get(1).getClass().getName());
    assertEquals("POV(BarkleyModelSimulation,null,null)", c1.getFilters().get(1).toString());
    assertNotNull(((POV) c1.getFilters().get(1)).getSubProperty());
    final SubProperty sb = ((POV) c1.getFilters().get(1)).getSubProperty();
    assertNotNull(sb.getFilter());
    assertEquals(POV.class.getName(), sb.getFilter().getClass().getName());
    assertEquals("POV(TimeStep,=,0.0016)", sb.getFilter().toString());
  }

  /*
   * String ticket147a =
   * "FIND ename WHICH HAS A PROPERTY pname=val1 AND (pname=val2)";
   */
  @Test
  public void TestTicket147a()
      throws InterruptedException, SQLException, ConnectionException, QueryException {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.ticket147a));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    for (final Token t : tokens.getTokens()) {
      System.out.println(t.toString());
    }

    System.out.println(sfq.toStringTree(parser));

    // 5 children: FIND, entity, WHITE_SPACE, entity_filter, EOF
    assertEquals(5, sfq.getChildCount());
    assertEquals("FIND ", sfq.getChild(0).getText());
    assertEquals("ename", sfq.getChild(1).getText());
    assertEquals("WHICH HAS A PROPERTY pname=val1 AND (pname=val2)", sfq.getChild(3).getText());
    assertEquals(Query.Pattern.TYPE_NORMAL, sfq.e.type);

    final ParseTree filter = sfq.getChild(3);
    assertEquals(2, filter.getChildCount());
    assertEquals("WHICH HAS A PROPERTY ", filter.getChild(0).getText());
    assertEquals("pname=val1 AND (pname=val2)", filter.getChild(1).getText());

    final ParseTree conjunction1 = filter.getChild(1);

    // 5 children: pov, AND, (, pov, )
    assertEquals(5, conjunction1.getChildCount());
    assertEquals("pname=val1 ", conjunction1.getChild(0).getText());
    assertEquals("AND ", conjunction1.getChild(1).getText());
    assertEquals("(", conjunction1.getChild(2).getText());
    assertEquals("pname=val2", conjunction1.getChild(3).getText());
    assertEquals(")", conjunction1.getChild(4).getText());

    assertNotNull(sfq.filter);
    assertEquals(Conjunction.class.getName(), sfq.filter.getClass().getName());
    final Conjunction c1 = (Conjunction) sfq.filter;
    assertNotNull(c1.getFilters());
    assertEquals(2, c1.getFilters().size());
    assertEquals(POV.class.getName(), c1.getFilters().get(0).getClass().getName());
    assertEquals(POV.class.getName(), c1.getFilters().get(1).getClass().getName());
  }

  /*
   * String ticket147b =
   * "FIND RECORD FrequencyMeasurement WHICH HAS A PROPERTY ObstacleRadius = '2.0' AND ( BarkleyModelSimulation WHICH HAS A PROPERTY TimeStep='0.0016')"
   * ;
   */
  @Test
  public void TestTicket147b()
      throws InterruptedException, SQLException, ConnectionException, QueryException {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.ticket147b));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    System.out.println(tokens.getText());

    for (final Token t : tokens.getTokens()) {
      System.out.println(t.toString());
    }

    System.out.println(sfq.toStringTree(parser));

    // 6 children: FIND, RECORD, entity, WHITE_SPACE, entity_filter, EOF
    assertEquals(6, sfq.getChildCount());
    assertEquals("FIND ", sfq.getChild(0).getText());
    assertEquals("RECORD ", sfq.getChild(1).getText());
    assertEquals("FrequencyMeasurement", sfq.getChild(2).getText());
    assertEquals(
        "WHICH HAS A PROPERTY ObstacleRadius = '2.0' AND ( BarkleyModelSimulation WHICH HAS A PROPERTY TimeStep='0.0016')",
        sfq.getChild(4).getText());
    assertEquals(Query.Pattern.TYPE_NORMAL, sfq.e.type);

    final ParseTree filter = sfq.getChild(4);
    assertEquals(2, filter.getChildCount());
    assertEquals("WHICH HAS A PROPERTY ", filter.getChild(0).getText());
    assertEquals(
        "ObstacleRadius = '2.0' AND ( BarkleyModelSimulation WHICH HAS A PROPERTY TimeStep='0.0016')",
        filter.getChild(1).getText());

    final ParseTree conjunction1 = filter.getChild(1);
    assertEquals(5, conjunction1.getChildCount());
    assertEquals("ObstacleRadius = '2.0' ", conjunction1.getChild(0).getText());
    assertEquals("AND ", conjunction1.getChild(1).getText());
    assertEquals("( ", conjunction1.getChild(2).getText());
    assertEquals(
        "BarkleyModelSimulation WHICH HAS A PROPERTY TimeStep='0.0016'",
        conjunction1.getChild(3).getText());
    assertEquals(")", conjunction1.getChild(4).getText());

    final ParseTree sub = conjunction1.getChild(3);
    assertEquals(2, sub.getChildCount());
    assertEquals("BarkleyModelSimulation ", sub.getChild(0).getText());
    assertEquals("WHICH HAS A PROPERTY TimeStep='0.0016'", sub.getChild(1).getText());

    final ParseTree con2 = sub.getChild(1).getChild(0);
    assertEquals(2, con2.getChildCount());
    assertEquals("WHICH HAS A PROPERTY ", con2.getChild(0).getText());
    assertEquals("TimeStep='0.0016'", con2.getChild(1).getText());

    assertNotNull(sfq.filter);
    assertEquals(Conjunction.class.getName(), sfq.filter.getClass().getName());
    final Conjunction c1 = (Conjunction) sfq.filter;
    assertNotNull(c1.getFilters());
    assertEquals(2, c1.getFilters().size());
    assertEquals(POV.class.getName(), c1.getFilters().get(0).getClass().getName());
    assertEquals(POV.class.getName(), c1.getFilters().get(1).getClass().getName());
    assertNotNull(((POV) c1.getFilters().get(1)).getSubProperty());
    final SubProperty sp = ((POV) c1.getFilters().get(1)).getSubProperty();
    assertNotNull(sp.getFilter());
    assertEquals(POV.class.getName(), sp.getFilter().getClass().getName());
    assertEquals("POV(TimeStep,=,0.0016)", sp.getFilter().toString());
  }

  /*
   * String ticket147c =
   * "FIND RECORD FrequencyMeasurement WHICH HAS A PROPERTY ObstacleRadius = '2.0' AND BarkleyModelSimulation WHICH HAS A PROPERTY TimeStep='0.0016'"
   * ;
   */
  @Test
  public void TestTicket147c()
      throws InterruptedException, SQLException, ConnectionException, QueryException {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.ticket147c));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    System.out.println(tokens.getText());

    for (final Token t : tokens.getTokens()) {
      System.out.println(t.toString());
    }

    System.out.println(sfq.toStringTree(parser));

    // 6 children: FIND, RECORD, entity, WHITE_SPACE, entity_filter, EOF
    assertEquals(6, sfq.getChildCount());
    assertEquals("FIND ", sfq.getChild(0).getText());
    assertEquals("RECORD ", sfq.getChild(1).getText());
    assertEquals("FrequencyMeasurement", sfq.getChild(2).getText());
    assertEquals(
        "WHICH HAS A PROPERTY ObstacleRadius = '2.0' AND BarkleyModelSimulation WHICH HAS A PROPERTY TimeStep='0.0016'",
        sfq.getChild(4).getText());
    assertEquals(Query.Pattern.TYPE_NORMAL, sfq.e.type);

    final ParseTree filter = sfq.getChild(4);
    assertEquals(2, filter.getChildCount());
    assertEquals("WHICH HAS A PROPERTY ", filter.getChild(0).getText());
    assertEquals(
        "ObstacleRadius = '2.0' AND BarkleyModelSimulation WHICH HAS A PROPERTY TimeStep='0.0016'",
        filter.getChild(1).getText());

    final ParseTree conjunction1 = filter.getChild(1);
    assertEquals(3, conjunction1.getChildCount());
    assertEquals("ObstacleRadius = '2.0' ", conjunction1.getChild(0).getText());
    assertEquals("AND ", conjunction1.getChild(1).getText());
    assertEquals(
        "BarkleyModelSimulation WHICH HAS A PROPERTY TimeStep='0.0016'",
        conjunction1.getChild(2).getText());
    final ParseTree filter_exp = conjunction1.getChild(2);
    assertEquals(2, filter_exp.getChildCount());
    assertEquals("BarkleyModelSimulation ", filter_exp.getChild(0).getText());
    assertEquals("WHICH HAS A PROPERTY TimeStep='0.0016'", filter_exp.getChild(1).getText());

    final ParseTree sub = filter_exp.getChild(1);
    assertEquals(1, sub.getChildCount());
    assertEquals("WHICH HAS A PROPERTY TimeStep='0.0016'", sub.getChild(0).getText());

    final ParseTree sub1 = sub.getChild(0);
    assertEquals(2, sub1.getChildCount());
    assertEquals("WHICH HAS A PROPERTY ", sub1.getChild(0).getText());
    assertEquals("TimeStep='0.0016'", sub1.getChild(1).getText());

    assertNotNull(sfq.filter);
    assertEquals(Conjunction.class.getName(), sfq.filter.getClass().getName());
    final Conjunction c1 = (Conjunction) sfq.filter;
    assertNotNull(c1.getFilters());
    assertEquals(2, c1.getFilters().size());
    assertEquals(POV.class.getName(), c1.getFilters().get(0).getClass().getName());
    assertEquals("POV(ObstacleRadius,=,2.0)", c1.getFilters().get(0).toString());
    assertEquals(POV.class.getName(), c1.getFilters().get(1).getClass().getName());
    assertEquals("POV(BarkleyModelSimulation,null,null)", c1.getFilters().get(1).toString());
    assertNotNull(((POV) c1.getFilters().get(1)).getSubProperty());
    final SubProperty sb = ((POV) c1.getFilters().get(1)).getSubProperty();
    assertNotNull(sb.getFilter());
    assertEquals(POV.class.getName(), sb.getFilter().getClass().getName());
    assertEquals("POV(TimeStep,=,0.0016)", sb.getFilter().toString());
  }

  /*
   * String ticket147d =
   * "FIND RECORD FrequencyMeasurement WHICH HAS A PROPERTY ObstacleRadius = '2.0' AND A PROPERTY BarkleyModelSimulation WHICH HAS A PROPERTY TimeStep='0.0016'"
   * ;
   */
  @Test
  public void TestTicket147d()
      throws InterruptedException, SQLException, ConnectionException, QueryException {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.ticket147d));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    System.out.println(tokens.getText());

    for (final Token t : tokens.getTokens()) {
      System.out.println(t.toString());
    }

    System.out.println(sfq.toStringTree(parser));

    // 6 children: FIND, RECORD, entity, WHITE_SPACE, entity_filter, EOF
    assertEquals(6, sfq.getChildCount());
    assertEquals("FIND ", sfq.getChild(0).getText());
    assertEquals("RECORD ", sfq.getChild(1).getText());
    assertEquals("FrequencyMeasurement", sfq.getChild(2).getText());
    assertEquals(
        "WHICH HAS A PROPERTY ObstacleRadius = '2.0' AND A PROPERTY BarkleyModelSimulation WHICH HAS A PROPERTY TimeStep='0.0016'",
        sfq.getChild(4).getText());
    assertEquals(Query.Pattern.TYPE_NORMAL, sfq.e.type);

    final ParseTree filter = sfq.getChild(4);
    assertEquals(2, filter.getChildCount());
    assertEquals("WHICH HAS A PROPERTY ", filter.getChild(0).getText());
    assertEquals(
        "ObstacleRadius = '2.0' AND A PROPERTY BarkleyModelSimulation WHICH HAS A PROPERTY TimeStep='0.0016'",
        filter.getChild(1).getText());

    final ParseTree conjunction1 = filter.getChild(1);
    assertEquals(5, conjunction1.getChildCount());
    assertEquals("ObstacleRadius = '2.0' ", conjunction1.getChild(0).getText());
    assertEquals("AND ", conjunction1.getChild(1).getText());
    assertEquals("A ", conjunction1.getChild(2).getText());
    assertEquals("PROPERTY ", conjunction1.getChild(3).getText());
    assertEquals(
        "BarkleyModelSimulation WHICH HAS A PROPERTY TimeStep='0.0016'",
        conjunction1.getChild(4).getText());
    final ParseTree filter_exp = conjunction1.getChild(4);
    assertEquals("BarkleyModelSimulation ", filter_exp.getChild(0).getText());
    assertEquals("WHICH HAS A PROPERTY TimeStep='0.0016'", filter_exp.getChild(1).getText());

    final ParseTree sub = filter_exp.getChild(1);
    assertEquals(1, sub.getChildCount());
    assertEquals("WHICH HAS A PROPERTY TimeStep='0.0016'", sub.getChild(0).getText());

    final ParseTree sub1 = sub.getChild(0);
    assertEquals(2, sub1.getChildCount());
    assertEquals("WHICH HAS A PROPERTY ", sub1.getChild(0).getText());
    assertEquals("TimeStep='0.0016'", sub1.getChild(1).getText());

    assertNotNull(sfq.filter);
    assertEquals(Conjunction.class.getName(), sfq.filter.getClass().getName());
    final Conjunction c1 = (Conjunction) sfq.filter;
    assertNotNull(c1.getFilters());
    assertEquals(2, c1.getFilters().size());
    assertEquals(POV.class.getName(), c1.getFilters().get(0).getClass().getName());
    assertEquals("POV(ObstacleRadius,=,2.0)", c1.getFilters().get(0).toString());
    assertEquals(POV.class.getName(), c1.getFilters().get(1).getClass().getName());
    assertEquals("POV(BarkleyModelSimulation,null,null)", c1.getFilters().get(1).toString());
    assertNotNull(((POV) c1.getFilters().get(1)).getSubProperty());
    final SubProperty sb = ((POV) c1.getFilters().get(1)).getSubProperty();
    assertNotNull(sb.getFilter());
    assertEquals(POV.class.getName(), sb.getFilter().getClass().getName());
    assertEquals("POV(TimeStep,=,0.0016)", sb.getFilter().toString());
  }

  /*
   * String ticket147e =
   * "FIND RECORD FrequencyMeasurement WHICH HAS A PROPERTY ObstacleRadius = '2.0' AND A PROPERTY ( BarkleyModelSimulation )"
   * ;
   */
  @Test
  public void TestTicket147e()
      throws InterruptedException, SQLException, ConnectionException, QueryException {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.ticket147e));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    System.out.println(tokens.getText());

    for (final Token t : tokens.getTokens()) {
      System.out.println(t.toString());
    }

    System.out.println(sfq.toStringTree(parser));

    // 6 children: FIND RECORD, entity, WHITE_SPACE, entity_filter, EOF
    assertEquals(6, sfq.getChildCount());
    assertEquals("FIND ", sfq.getChild(0).getText());
    assertEquals("RECORD ", sfq.getChild(1).getText());
    assertEquals("FrequencyMeasurement", sfq.getChild(2).getText());
    assertEquals(
        "WHICH HAS A PROPERTY ObstacleRadius = '2.0' AND A PROPERTY ( BarkleyModelSimulation )",
        sfq.getChild(4).getText());
    assertEquals(Query.Pattern.TYPE_NORMAL, sfq.e.type);

    final ParseTree filter = sfq.getChild(4);
    assertEquals(2, filter.getChildCount());
    assertEquals("WHICH HAS A PROPERTY ", filter.getChild(0).getText());
    assertEquals(
        "ObstacleRadius = '2.0' AND A PROPERTY ( BarkleyModelSimulation )",
        filter.getChild(1).getText());

    final ParseTree conjunction1 = filter.getChild(1);
    assertEquals(7, conjunction1.getChildCount());
    assertEquals("ObstacleRadius = '2.0' ", conjunction1.getChild(0).getText());
    assertEquals("AND ", conjunction1.getChild(1).getText());
    assertEquals("A ", conjunction1.getChild(2).getText());
    assertEquals("PROPERTY ", conjunction1.getChild(3).getText());
    assertEquals("( ", conjunction1.getChild(4).getText());
    assertEquals("BarkleyModelSimulation ", conjunction1.getChild(5).getText());
    assertEquals(")", conjunction1.getChild(6).getText());

    assertNotNull(sfq.filter);
    assertEquals(Conjunction.class.getName(), sfq.filter.getClass().getName());
    final Conjunction c1 = (Conjunction) sfq.filter;
    assertNotNull(c1.getFilters());
    assertEquals(2, c1.getFilters().size());
    assertEquals(POV.class.getName(), c1.getFilters().get(0).getClass().getName());
    assertEquals("POV(ObstacleRadius,=,2.0)", c1.getFilters().get(0).toString());
    assertEquals(POV.class.getName(), c1.getFilters().get(1).getClass().getName());
    assertEquals("POV(BarkleyModelSimulation,null,null)", c1.getFilters().get(1).toString());
    assertNull(((POV) c1.getFilters().get(1)).getSubProperty());
  }

  /*
   * String ticket147f =
   * "FIND ename WHICH HAS A PROPERTY pname=val1 AND ( pname )";
   */
  @Test
  public void TestTicket147f()
      throws InterruptedException, SQLException, ConnectionException, QueryException {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.ticket147f));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    System.out.println(tokens.getText());

    for (final Token t : tokens.getTokens()) {
      System.out.println(t.toString());
    }

    System.out.println(sfq.toStringTree(parser));

    // 5 children: FIND, entity, WHITE_SPACE, entity_filter, EOF
    assertEquals(5, sfq.getChildCount());
    assertEquals("FIND ", sfq.getChild(0).getText());
    assertEquals("ename", sfq.getChild(1).getText());
    assertEquals("WHICH HAS A PROPERTY pname1=val1 AND ( pname2 )", sfq.getChild(3).getText());
    assertEquals(Query.Pattern.TYPE_NORMAL, sfq.e.type);

    final ParseTree filter = sfq.getChild(3);
    assertEquals(2, filter.getChildCount());
    assertEquals("WHICH HAS A PROPERTY ", filter.getChild(0).getText());
    assertEquals("pname1=val1 AND ( pname2 )", filter.getChild(1).getText());

    final ParseTree conjunction1 = filter.getChild(1);
    assertEquals(5, conjunction1.getChildCount());
    assertEquals("pname1=val1 ", conjunction1.getChild(0).getText());
    assertEquals("AND ", conjunction1.getChild(1).getText());
    assertEquals("( ", conjunction1.getChild(2).getText());
    assertEquals("pname2 ", conjunction1.getChild(3).getText());
    assertEquals(")", conjunction1.getChild(4).getText());

    final ParseTree sub = conjunction1.getChild(3);
    assertEquals(1, sub.getChildCount());
    assertEquals("pname2 ", sub.getChild(0).getText());

    final ParseTree sub1 = sub.getChild(0);
    assertEquals(1, sub1.getChildCount());
    assertEquals("pname2 ", sub1.getChild(0).getText());

    assertNotNull(sfq.filter);
    assertEquals(Conjunction.class.getName(), sfq.filter.getClass().getName());
    final Conjunction c1 = (Conjunction) sfq.filter;
    assertNotNull(c1.getFilters());
    assertEquals(2, c1.getFilters().size());
    assertEquals(POV.class.getName(), c1.getFilters().get(0).getClass().getName());
    assertEquals("POV(pname1,=,val1)", c1.getFilters().get(0).toString());
    assertEquals(POV.class.getName(), c1.getFilters().get(1).getClass().getName());
    assertEquals("POV(pname2,null,null)", c1.getFilters().get(1).toString());
    assertNull(((POV) c1.getFilters().get(1)).getSubProperty());
  }

  @Test
  public void TestTicket147g()
      throws InterruptedException, SQLException, ConnectionException, QueryException {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.ticket147g));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);
    System.out.println(tokens.getText());

    final CQLParser parser = new CQLParser(tokens);
    for (final Token t : tokens.getTokens()) {
      System.out.println(t.toString());
    }

    final CqContext sfq = parser.cq();

    System.out.println(sfq.toStringTree(parser));

    // 6 children: FIND, RECORD, entity, WHITE_SPACE, entity_filter, EOF
    assertEquals(6, sfq.getChildCount());
    assertEquals("FIND ", sfq.getChild(0).getText());
    assertEquals("RECORD ", sfq.getChild(1).getText());
    assertEquals("FrequencyMeasurement", sfq.getChild(2).getText());
    assertEquals(
        "WHICH HAS A PROPERTY ObstacleRadius = '2.0' AND A PROPERTY ( BarkleyModelSimulation WHICH HAS A TimeStep='0.0016')",
        sfq.getChild(4).getText());
    assertEquals(Query.Pattern.TYPE_NORMAL, sfq.e.type);

    assertNotNull(sfq.filter);
    assertEquals(Conjunction.class.getName(), sfq.filter.getClass().getName());
    final Conjunction c1 = (Conjunction) sfq.filter;
    assertNotNull(c1.getFilters());
    assertEquals(2, c1.getFilters().size());
    assertEquals(POV.class.getName(), c1.getFilters().get(0).getClass().getName());
    assertEquals("POV(ObstacleRadius,=,2.0)", c1.getFilters().get(0).toString());
    assertEquals(POV.class.getName(), c1.getFilters().get(1).getClass().getName());
    assertEquals("POV(BarkleyModelSimulation,null,null)", c1.getFilters().get(1).toString());
    assertNotNull(((POV) c1.getFilters().get(1)).getSubProperty());
    final SubProperty sb = ((POV) c1.getFilters().get(1)).getSubProperty();
    assertNotNull(sb.getFilter());
    assertEquals(POV.class.getName(), sb.getFilter().getClass().getName());
    assertEquals("POV(TimeStep,=,0.0016)", sb.getFilter().toString());
  }

  @Test
  public void TestTicket147h()
      throws InterruptedException, SQLException, ConnectionException, QueryException {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.ticket147h));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);
    System.out.println(tokens.getText());

    final CQLParser parser = new CQLParser(tokens);
    for (final Token t : tokens.getTokens()) {
      System.out.println(t.toString());
    }

    final CqContext sfq = parser.cq();

    System.out.println(sfq.toStringTree(parser));

    // 6 children: FIND, RECORD, entity, WHITE_SPACE, entity_filter, EOF
    assertEquals(6, sfq.getChildCount());
    assertEquals("FIND ", sfq.getChild(0).getText());
    assertEquals("RECORD ", sfq.getChild(1).getText());
    assertEquals("FrequencyMeasurement", sfq.getChild(2).getText());
    assertEquals(
        "WHICH HAS A PROPERTY ObstacleRadius = '2.0' AND A PROPERTY ( BarkleyModelSimulation WHICH HAS TimeStep='0.0016')",
        sfq.getChild(4).getText());
    assertEquals(Query.Pattern.TYPE_NORMAL, sfq.e.type);
  }

  /*
   * String ticket147i =
   * "FIND RECORD FrequencyMeasurement WHICH HAS A PROPERTY ObstacleRadius = '2.0' AND A PROPERTY ( BarkleyModelSimulation WHICH HAS THE SMALLES TimeStep )"
   * ; -> 'SMALLES' should raise error
   */
  @Test
  public void TestTicket147i()
      throws InterruptedException,
          SQLException,
          ConnectionException,
          QueryException,
          TransactionException {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.ticket147i));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);
    System.out.println(tokens.getText());

    final CQLParser parser = new CQLParser(tokens);
    for (final Token t : tokens.getTokens()) {
      System.out.println(t.toString());
    }

    final CqContext sfq = parser.cq();

    System.out.println(sfq.toStringTree(parser));

    final Query query = new Query(this.ticket147i);
    try {
      query.execute();
      fail("This should throw a QueryException!");
    } catch (final QueryException q) {
    }
  }

  /*
   * String ticket147j =
   * "FIND RECORD ticket147_FrequencyMeasurement WHICH HAS A PROPERTY ticket147_ObstacleRadius = '2.0' AND A PROPERTY ( ticket147_BarkleyModelSimulation WHICH HAS ticket147_TimeStep='0.0016')"
   * ;
   */
  @Test
  public void TestTicket147j()
      throws InterruptedException, SQLException, ConnectionException, QueryException {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.ticket147j));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);
    System.out.println(tokens.getText());

    final CQLParser parser = new CQLParser(tokens);
    for (final Token t : tokens.getTokens()) {
      System.out.println(t.toString());
    }

    final CqContext sfq = parser.cq();

    System.out.println(sfq.toStringTree(parser));

    // 6 children: FIND, RECORD, entity, WHITE_SPACE, entity_filter, EOF
    assertEquals(6, sfq.getChildCount());
    assertEquals("FIND ", sfq.getChild(0).getText());
    assertEquals("RECORD ", sfq.getChild(1).getText());
    assertEquals("ticket147_FrequencyMeasurement", sfq.getChild(2).getText());
    assertEquals(
        "WHICH HAS A PROPERTY ticket147_ObstacleRadius = '2.0' AND A PROPERTY ( ticket147_BarkleyModelSimulation WHICH HAS ticket147_TimeStep='0.0016')",
        sfq.getChild(4).getText());
    assertEquals(Query.Pattern.TYPE_NORMAL, sfq.e.type);
  }

  /*
   * String query27 = "FIND 'Some name with spaces and 1234 numbers'";
   */
  @Test
  public void testQuery27()
      throws InterruptedException, SQLException, ConnectionException, QueryException {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.query27));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    System.out.println(sfq.toStringTree(parser));

    // 3 children: FIND, entity, EOF
    assertEquals(3, sfq.getChildCount());
    assertEquals("FIND ", sfq.getChild(0).getText());
    assertEquals("'Some name with spaces and 1234 numbers'", sfq.getChild(1).getText());
    assertEquals(Pattern.TYPE_NORMAL, sfq.e.type);

    assertEquals("Some name with spaces and 1234 numbers", sfq.e.toString());
    assertNull(sfq.filter);
  }

  /*
   * String query27a = "FIND \"Some name with spaces and 1234 numbers\"";
   */
  @Test
  public void testQuery27a()
      throws InterruptedException, SQLException, ConnectionException, QueryException {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.query27a));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    System.out.println(sfq.toStringTree(parser));

    // 3 children: FIND, entity, EOF
    assertEquals(3, sfq.getChildCount());
    assertEquals("FIND ", sfq.getChild(0).getText());
    assertEquals("\"Some name with spaces and 1234 numbers\"", sfq.getChild(1).getText());
    assertEquals(Pattern.TYPE_NORMAL, sfq.e.type);

    assertEquals("Some name with spaces and 1234 numbers", sfq.e.toString());
    assertNull(sfq.filter);
  }

  /*
   * String query27b = "FIND 'Some name with spaces and 1234 numbers and \"'";
   */
  @Test
  public void testQuery27b()
      throws InterruptedException, SQLException, ConnectionException, QueryException {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.query27b));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    System.out.println(sfq.toStringTree(parser));

    // 3 children: FIND, entity, EOF
    assertEquals(3, sfq.getChildCount());
    assertEquals("FIND ", sfq.getChild(0).getText());
    assertEquals("'Some name with spaces and 1234 numbers and \"'", sfq.getChild(1).getText());
    assertEquals(Pattern.TYPE_NORMAL, sfq.e.type);

    assertEquals("Some name with spaces and 1234 numbers and \"", sfq.e.toString());
    assertNull(sfq.filter);
  }

  /*
   * String query27c =
   * "FIND 'Some name with spaces and 1234 numbers and \\*'";
   */
  @Test
  public void testQuery27c()
      throws InterruptedException, SQLException, ConnectionException, QueryException {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.query27c));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    System.out.println(sfq.toStringTree(parser));

    // 3 children: FIND, entity, EOF
    assertEquals(3, sfq.getChildCount());
    assertEquals("FIND ", sfq.getChild(0).getText());
    assertEquals("'Some name with spaces and 1234 numbers and \\*'", sfq.getChild(1).getText());
    assertEquals(Pattern.TYPE_NORMAL, sfq.e.type);

    assertEquals("Some name with spaces and 1234 numbers and *", sfq.e.toString());
    assertNull(sfq.filter);
  }

  /*
   * String query27d = "FIND 'Some name with spaces and 1234 numbers and *'";
   */
  @Test
  public void testQuery27d()
      throws InterruptedException, SQLException, ConnectionException, QueryException {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.query27d));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    System.out.println(sfq.toStringTree(parser));

    // 3 children: FIND, entity, EOF
    assertEquals(3, sfq.getChildCount());
    assertEquals("FIND ", sfq.getChild(0).getText());
    assertEquals("'Some name with spaces and 1234 numbers and *'", sfq.getChild(1).getText());
    assertEquals(Pattern.TYPE_LIKE, sfq.e.type);

    assertEquals("Some name with spaces and 1234 numbers and %", sfq.e.toString());
    assertNull(sfq.filter);
  }

  /*
   * String query28 = "FIND ename . pname=2.02";
   */
  @Test
  public void testQuery28()
      throws InterruptedException, SQLException, ConnectionException, QueryException {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.query28));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    System.out.println(sfq.toStringTree(parser));
    assertEquals("POV(pname,=,2.02)", sfq.filter.toString());

    // 5 children: FIND, entity, WHITE_SPACE, filter, EOF
    assertEquals(5, sfq.getChildCount());
    assertEquals("FIND ", sfq.getChild(0).getText());
    assertEquals("ename", sfq.getChild(1).getText());
    assertEquals(". pname=2.02", sfq.getChild(3).getText());
    assertEquals(Pattern.TYPE_NORMAL, sfq.e.type);

    assertEquals(2, sfq.getChild(3).getChildCount());
    assertEquals(". ", sfq.getChild(3).getChild(0).getText());
    assertEquals("pname=2.02", sfq.getChild(3).getChild(1).getText());
    assertEquals(1, sfq.getChild(3).getChild(1).getChildCount());
    assertEquals("pname=2.02", sfq.getChild(3).getChild(1).getChild(0).getText());
    assertEquals("2.02", sfq.getChild(3).getChild(1).getChild(0).getChild(2).getText());
  }

  /*
   * String query28a ="FIND ename . pname=2.0prop=test";
   */
  @Test
  public void testQuery28a()
      throws InterruptedException, SQLException, ConnectionException, QueryException {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.query28a));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    System.out.println(sfq.toStringTree(parser));
    assertEquals("POV(pname,=,2.0prop=test)", sfq.filter.toString());

    // 5 children: FIND, entity, WHITE_SPACE, filter, EOF
    assertEquals(5, sfq.getChildCount());
    assertEquals("FIND ", sfq.getChild(0).getText());
    assertEquals("ename", sfq.getChild(1).getText());
    assertEquals(". pname=2.0prop=test", sfq.getChild(3).getText());
    assertEquals(Pattern.TYPE_NORMAL, sfq.e.type);
  }

  /** String query28b = "FIND ename . pname = 1.02m"; */
  @Test
  public void testQuery28b()
      throws InterruptedException, SQLException, ConnectionException, QueryException {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.query28b));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    System.out.println(sfq.toStringTree(parser));
    assertEquals("POV(pname,=,1.02m)", sfq.filter.toString());
    assertEquals(1.02, ((POV) sfq.filter).getVDouble(), 0.0);

    // 5 children: FIND, entity, WHITE_SPACE, filter, EOF
    assertEquals(5, sfq.getChildCount());
    assertEquals("FIND ", sfq.getChild(0).getText());
    assertEquals("ename", sfq.getChild(1).getText());
    assertEquals(". pname = 1.02m", sfq.getChild(3).getText());
    assertEquals(Pattern.TYPE_NORMAL, sfq.e.type);

    assertEquals(2, sfq.getChild(3).getChildCount());
    assertEquals(". ", sfq.getChild(3).getChild(0).getText());
    assertEquals("pname = 1.02m", sfq.getChild(3).getChild(1).getText());
    assertEquals(1, sfq.getChild(3).getChild(1).getChildCount());
    assertEquals("pname = 1.02m", sfq.getChild(3).getChild(1).getChild(0).getText());
    assertEquals("1.02m", sfq.getChild(3).getChild(1).getChild(0).getChild(3).getText());
  }

  /** String query28c = "FIND ename . pname = .02"; */
  @Test
  public void testQuery28c()
      throws InterruptedException, SQLException, ConnectionException, QueryException {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.query28c));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    System.out.println(sfq.toStringTree(parser));
    assertEquals("POV(pname,=,.02)", sfq.filter.toString());
  }

  /** String query28d = "FIND ename . pname =.02m"; */
  @Test
  public void testQuery28d()
      throws InterruptedException, SQLException, ConnectionException, QueryException {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.query28d));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    System.out.println(sfq.toStringTree(parser));
    assertEquals("POV(pname,=,.02m)", sfq.filter.toString());
  }

  /** String query28e = "FIND ename . pname =.02 1/m^2"; */
  @Test
  public void testQuery28e()
      throws InterruptedException, SQLException, ConnectionException, QueryException {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.query28e));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    System.out.println(sfq.toStringTree(parser));
    assertEquals("POV(pname,=,.02 1/m^2)", sfq.filter.toString());
  }

  /*
   * String ticket148 =
   * "FIND RECORD FrequencyMeasurement WHICH HAS A PROPERTY ( BarkleyModelSimulation WHICH HAS A PROPERTY TimeStep='0.0016') AND A PROPERTY MinPeakHeight = '0.5'"
   * ;
   */
  @Test
  public void TestTicket148()
      throws InterruptedException, SQLException, ConnectionException, QueryException {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.ticket148));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    System.out.println(sfq.toStringTree(parser));

    assertEquals(6, sfq.getChildCount());
    assertEquals("FIND ", sfq.getChild(0).getText());
    assertEquals("RECORD ", sfq.getChild(1).getText());
    assertEquals("FrequencyMeasurement", sfq.getChild(2).getText());
    assertEquals(
        "WHICH HAS A PROPERTY ( BarkleyModelSimulation WHICH HAS A PROPERTY TimeStep='0.0016') AND A PROPERTY MinPeakHeight = '0.5'",
        sfq.getChild(4).getText());

    assertEquals(Pattern.TYPE_NORMAL, sfq.e.type);

    final ParseTree filter = sfq.getChild(4);
    // 2 children: WHICH , conjunction
    assertEquals(2, filter.getChildCount());
    assertEquals("WHICH HAS A PROPERTY ", filter.getChild(0).getText());
    assertEquals(
        "( BarkleyModelSimulation WHICH HAS A PROPERTY TimeStep='0.0016') AND A PROPERTY MinPeakHeight = '0.5'",
        filter.getChild(1).getText());

    final ParseTree conjunction = filter.getChild(1);
    // 7 children: (, filter, ), AND, A, PROPERTY, filter
    assertEquals(7, conjunction.getChildCount());
    assertEquals("( ", conjunction.getChild(0).getText());
    assertEquals(
        "BarkleyModelSimulation WHICH HAS A PROPERTY TimeStep='0.0016'",
        conjunction.getChild(1).getText());
    assertEquals(") ", conjunction.getChild(2).getText());
    assertEquals("AND ", conjunction.getChild(3).getText());
    assertEquals("A ", conjunction.getChild(4).getText());
    assertEquals("PROPERTY ", conjunction.getChild(5).getText());
    assertEquals("MinPeakHeight = '0.5'", conjunction.getChild(6).getText());

    final ParseTree pov1 = conjunction.getChild(1).getChild(0);
    assertEquals("BarkleyModelSimulation ", pov1.getText());
    final ParseTree subp = conjunction.getChild(1).getChild(1);
    assertEquals("WHICH HAS A PROPERTY TimeStep='0.0016'", subp.getText());
    final ParseTree pov2 = subp.getChild(0).getChild(1);
    assertEquals("TimeStep='0.0016'", pov2.getText());
    final ParseTree pov3 = conjunction.getChild(6).getChild(0);
    assertEquals("MinPeakHeight = '0.5'", pov3.getText());

    assertEquals(sfq.e.toString(), "FrequencyMeasurement");
    assertEquals(sfq.r, Query.Role.RECORD);
    assertEquals(Conjunction.class.getName(), sfq.filter.getClass().getName());
    final Conjunction c = (Conjunction) sfq.filter;
    assertEquals(2, c.getFilters().size());

    assertEquals(POV.class.getName(), c.getFilters().get(0).getClass().getName());
    final POV pov = (POV) c.getFilters().get(0);
    assertEquals("POV(BarkleyModelSimulation,null,null)", pov.toString());
    assertEquals("SUBPROPERTY()", pov.getSubProperty().toString());
    assertEquals("POV(TimeStep,=,0.0016)", pov.getSubProperty().getFilter().toString());
    assertEquals(POV.class.getName(), c.getFilters().get(1).getClass().getName());
    assertEquals("POV(MinPeakHeight,=,0.5)", c.getFilters().get(1).toString());
  }

  /*
   * String ticket148a =
   * "FIND RECORD FrequencyMeasurement . ( BarkleyModelSimulation . TimeStep='0.0016') AND A PROPERTY MinPeakHeight = '0.5'"
   * ;
   */

  @Test
  public void TestTicket148a()
      throws InterruptedException, SQLException, ConnectionException, QueryException {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.ticket148a));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    System.out.println(sfq.toStringTree(parser));

    // 6 children: FIND, role, entity, WHITE_SPACE, filter, EOF
    assertEquals(6, sfq.getChildCount());
    assertEquals("FIND ", sfq.getChild(0).getText());
    assertEquals("RECORD ", sfq.getChild(1).getText());
    assertEquals("FrequencyMeasurement", sfq.getChild(2).getText());
    assertEquals(
        ". ( BarkleyModelSimulation . TimeStep='0.0016') AND A PROPERTY MinPeakHeight = '0.5'",
        sfq.getChild(4).getText());

    assertEquals(Pattern.TYPE_NORMAL, sfq.e.type);

    final ParseTree filter = sfq.getChild(4);
    // 2 children: WHICH , conjunction
    assertEquals(2, filter.getChildCount());
    assertEquals(". ", filter.getChild(0).getText());
    assertEquals(
        "( BarkleyModelSimulation . TimeStep='0.0016') AND A PROPERTY MinPeakHeight = '0.5'",
        filter.getChild(1).getText());

    final ParseTree conjunction = filter.getChild(1);
    // 7 children: (, filter, ), AND, A, PROPERTY, filter
    assertEquals(7, conjunction.getChildCount());
    assertEquals("( ", conjunction.getChild(0).getText());
    assertEquals("BarkleyModelSimulation . TimeStep='0.0016'", conjunction.getChild(1).getText());
    assertEquals(") ", conjunction.getChild(2).getText());
    assertEquals("AND ", conjunction.getChild(3).getText());
    assertEquals("A ", conjunction.getChild(4).getText());
    assertEquals("PROPERTY ", conjunction.getChild(5).getText());
    assertEquals("MinPeakHeight = '0.5'", conjunction.getChild(6).getText());

    final ParseTree pov1 = conjunction.getChild(1).getChild(0);
    assertEquals("BarkleyModelSimulation ", pov1.getText());
    final ParseTree subp = conjunction.getChild(1).getChild(1);
    assertEquals(". TimeStep='0.0016'", subp.getText());
    final ParseTree pov2 = subp.getChild(0).getChild(1);
    assertEquals("TimeStep='0.0016'", pov2.getText());
    final ParseTree pov3 = conjunction.getChild(6).getChild(0);
    assertEquals("MinPeakHeight = '0.5'", pov3.getText());

    assertEquals(sfq.e.toString(), "FrequencyMeasurement");
    assertEquals(sfq.r, Query.Role.RECORD);
    assertEquals(Conjunction.class.getName(), sfq.filter.getClass().getName());
    final Conjunction c = (Conjunction) sfq.filter;
    assertEquals(2, c.getFilters().size());

    assertEquals(POV.class.getName(), c.getFilters().get(0).getClass().getName());
    final POV pov = (POV) c.getFilters().get(0);
    assertEquals("POV(BarkleyModelSimulation,null,null)", pov.toString());
    assertEquals("SUBPROPERTY()", pov.getSubProperty().toString());
    assertEquals("POV(TimeStep,=,0.0016)", pov.getSubProperty().getFilter().toString());
    assertEquals(POV.class.getName(), c.getFilters().get(1).getClass().getName());
    assertEquals("POV(MinPeakHeight,=,0.5)", c.getFilters().get(1).toString());
  }

  /*
   * String ticket148b =
   * "FIND RECORD FrequencyMeasurement WHICH HAS A PROPERTY MinPeakHeight = '0.5' AND A PROPERTY ( BarkleyModelSimulation . TimeStep='0.0016')"
   * ;
   */
  @Test
  public void TestTicket148b()
      throws InterruptedException, SQLException, ConnectionException, QueryException {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.ticket148b));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    System.out.println(sfq.toStringTree(parser));

    // 6 children: FIND, role, entity, WHITE_SPACE, filter, EOF
    assertEquals(6, sfq.getChildCount());
    assertEquals("FIND ", sfq.getChild(0).getText());
    assertEquals("RECORD ", sfq.getChild(1).getText());
    assertEquals("FrequencyMeasurement", sfq.getChild(2).getText());
    assertEquals(
        "WHICH HAS A PROPERTY MinPeakHeight = '0.5' AND A PROPERTY ( BarkleyModelSimulation . TimeStep='0.0016')",
        sfq.getChild(4).getText());

    assertEquals(Pattern.TYPE_NORMAL, sfq.e.type);

    final ParseTree filter = sfq.getChild(4);
    // 2 children: WHICH , conjunction,
    assertEquals(2, filter.getChildCount());
    assertEquals("WHICH HAS A PROPERTY ", filter.getChild(0).getText());
    assertEquals(
        "MinPeakHeight = '0.5' AND A PROPERTY ( BarkleyModelSimulation . TimeStep='0.0016')",
        filter.getChild(1).getText());

    final ParseTree conjunction = filter.getChild(1);

    // 7 children: filter_exp, AND, A, PROPERTY, (, filter_exp, )
    assertEquals(7, conjunction.getChildCount());
    assertEquals("MinPeakHeight = '0.5' ", conjunction.getChild(0).getText());
    assertEquals("AND ", conjunction.getChild(1).getText());
    assertEquals("A ", conjunction.getChild(2).getText());
    assertEquals("PROPERTY ", conjunction.getChild(3).getText());
    assertEquals("( ", conjunction.getChild(4).getText());
    assertEquals("BarkleyModelSimulation . TimeStep='0.0016'", conjunction.getChild(5).getText());
    assertEquals(")", conjunction.getChild(6).getText());

    final ParseTree pov1 = conjunction.getChild(0).getChild(0);
    assertEquals(4, pov1.getChildCount());
    assertEquals("MinPeakHeight ", pov1.getChild(0).getText());
    assertEquals("=", pov1.getChild(1).getText());
    assertEquals("'0.5' ", pov1.getChild(3).getText());

    final ParseTree ref = conjunction.getChild(5);
    assertEquals(2, ref.getChildCount());
    assertEquals("BarkleyModelSimulation ", ref.getChild(0).getText());
    assertEquals(". TimeStep='0.0016'", ref.getChild(1).getText());

    // this is a pov
    final ParseTree subp = ref.getChild(1).getChild(0).getChild(1).getChild(0);
    assertEquals(3, subp.getChildCount());
    assertEquals("TimeStep", subp.getChild(0).getText());
    assertEquals("=", subp.getChild(1).getText());
    assertEquals("'0.0016'", subp.getChild(2).getText());
  }

  @Test
  public void testQuery29()
      throws InterruptedException, SQLException, ConnectionException, QueryException {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.query29));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    System.out.println(sfq.toStringTree(parser));

    // 5 children: FIND, entity, WHITE_SPACE, filter, EOF
    assertEquals(5, sfq.getChildCount());
    assertEquals("FIND ", sfq.getChild(0).getText());
    assertEquals("ename", sfq.getChild(1).getText());
    assertEquals("WHICH pname IS NULL", sfq.getChild(3).getText());

    assertEquals(Pattern.TYPE_NORMAL, sfq.e.type);

    final ParseTree filter = sfq.getChild(3);
    // 2 children: WHICH , pov,
    assertEquals(2, filter.getChildCount());
    assertEquals("WHICH ", filter.getChild(0).getText());
    assertEquals("pname IS NULL", filter.getChild(1).getText());

    final ParseTree conjunction = filter.getChild(1).getChild(0);

    // 2 children: pname, IS_NOT_NULL
    assertEquals(2, conjunction.getChildCount());
    assertEquals("pname ", conjunction.getChild(0).getText());
    assertEquals("IS NULL", conjunction.getChild(1).getText());
    assertEquals("POV(pname,0,null)", sfq.filter.toString());
  }

  @Test
  public void testQuery29a()
      throws InterruptedException, SQLException, ConnectionException, QueryException {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.query29a));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    System.out.println(sfq.toStringTree(parser));

    // 5 children: FIND, entity, WHITE_SPACE, filter, EOF
    assertEquals(5, sfq.getChildCount());
    assertEquals("FIND ", sfq.getChild(0).getText());
    assertEquals("ename", sfq.getChild(1).getText());
    assertEquals("WHICH pname IS NOT NULL", sfq.getChild(3).getText());

    assertEquals(Pattern.TYPE_NORMAL, sfq.e.type);

    final ParseTree filter = sfq.getChild(3);
    // 2 children: WHICH , pov,
    assertEquals(2, filter.getChildCount());
    assertEquals("WHICH ", filter.getChild(0).getText());
    assertEquals("pname IS NOT NULL", filter.getChild(1).getText());

    final ParseTree conjunction = filter.getChild(1).getChild(0);

    // 2 children: pname, IS_NOT_NULL
    assertEquals(2, conjunction.getChildCount());
    assertEquals("pname ", conjunction.getChild(0).getText());
    assertEquals("IS NOT NULL", conjunction.getChild(1).getText());
    assertEquals("POV(pname,!0,null)", sfq.filter.toString());
  }

  private void runQueryFromTicket156(
      final String query,
      String sugar,
      final boolean neg,
      final String transaction,
      final String by,
      String someone_else,
      final String but,
      final String person,
      final String on,
      final String date) {

    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(query));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    // 5 children: FIND, ename, WHITE_SPACE, FILTER, EOF
    assertEquals(5, sfq.getChildCount());
    assertEquals("FIND ", sfq.getChild(0).getText());
    assertEquals("ename", sfq.getChild(1).getText());
    assertEquals(
        "WHICH "
            + sugar
            + " "
            + transaction
            + " "
            + (by != null ? by : "")
            + (someone_else != null ? " " + someone_else : "")
            + (but != null ? " " + but : "")
            + (person != null ? " " + person : "")
            + (on != null ? " " + on + " " + date : ""),
        sfq.getChild(3).getText());
  }

  @Test
  public void TestTicket156() {
    final String[] transactions = {"CREATED", "INSERTED", "UPDATED"};
    final String[] sugar = {"", "WAS", "IS", "HAS BEEN", "HAVE BEEN", "HAD BEEN", "WERE"};
    final String[] neg_sugar = {
      "HAS NOT BEEN",
      "HAVE NOT BEEN",
      "HAD NOT BEEN",
      "WEREN'T",
      "ISN'T",
      "WASN'T",
      "HASN'T BEEN",
      "HAVEN'T BEEN",
      "HADN'T BEEN",
      "WAS NOT",
      "WERE NOT",
      "IS NOT",
      "NOT"
    };
    final String someone_else = "SOMEONE ELSE";
    final String but = "BUT";
    final String[] person = {"ME", "person.name"};
    final String by = "BY";
    final String on = "ON";
    final String date[] = {"'2015-12-24'", "'2015-12-24 18:15:00'"};

    // sugar
    for (final String s : sugar) {
      final boolean neg_s = false;
      final StringBuffer query1 = new StringBuffer("FIND ename WHICH ");
      query1.append(s);
      query1.append(" ");

      // transaction w/o not
      for (final String t : transactions) {
        final StringBuffer query2 = new StringBuffer(query1);
        query2.append(t);
        query2.append(" ");

        // by person
        for (final String p : person) {
          final StringBuffer query3 = new StringBuffer(query2);
          query3.append(by);
          final String by_flag = by;
          query3.append(" ");
          query3.append(p);
          final String soe_flag = null;

          runQueryFromTicket156(
              query3.toString(), s, neg_s, t, by_flag, soe_flag, null, p, null, null);
        }

        // by someone else but person
        for (final String p : person) {
          final StringBuffer query3 = new StringBuffer(query2);
          query3.append(by);
          final String by_flag = by;
          query3.append(" ");
          query3.append(someone_else);
          final String soe_flag = someone_else;

          runQueryFromTicket156(
              query3.toString(), s, neg_s, t, by_flag, soe_flag, null, null, null, null);

          query3.append(" ");
          query3.append(but);
          query3.append(" ");
          query3.append(p);
          runQueryFromTicket156(
              query3.toString(), s, neg_s, t, by_flag, soe_flag, but, p, null, null);
        }

        // on date
        for (final String d : date) {
          final StringBuffer query3 = new StringBuffer(query2);
          query3.append(" ");
          query3.append(on);
          query3.append(" ");
          query3.append(d);

          runQueryFromTicket156(query3.toString(), s, neg_s, t, null, null, null, null, on, d);
        }
      }
    }

    // neg_sugar
    for (final String s : neg_sugar) {
      final boolean neg_s = true;
      final StringBuffer query1 = new StringBuffer("FIND ename WHICH ");
      query1.append(s);
      query1.append(" ");

      // transaction w/o not
      for (final String t : transactions) {
        final StringBuffer query2 = new StringBuffer(query1);
        query2.append(t);
        query2.append(" ");

        // by person
        for (final String p : person) {
          final StringBuffer query3 = new StringBuffer(query2);
          query3.append(by);
          final String by_flag = by;
          query3.append(" ");
          query3.append(p);

          final String soe_flag = null;
          runQueryFromTicket156(
              query3.toString(), s, neg_s, t, by_flag, soe_flag, null, p, null, null);
        }

        // by someone else but person
        for (final String p : person) {
          final StringBuffer query3 = new StringBuffer(query2);
          query3.append(by);
          final String by_flag = by;
          query3.append(" ");
          query3.append(someone_else);
          final String soe_flag = someone_else;

          runQueryFromTicket156(
              query3.toString(), s, neg_s, t, by_flag, soe_flag, null, null, null, null);

          query3.append(" ");
          query3.append(but);
          query3.append(" ");
          query3.append(p);
          runQueryFromTicket156(
              query3.toString(), s, neg_s, t, by_flag, soe_flag, but, p, null, null);
        }

        // on date
        for (final String d : date) {
          final StringBuffer query3 = new StringBuffer(query2);
          query3.append(" ");
          query3.append(on);
          query3.append(" ");
          query3.append(d);

          runQueryFromTicket156(query3.toString(), s, neg_s, t, null, null, null, null, on, d);
        }
      }
    }
  }

  /*
   * String query30 =
   * "FIND RECORD.CREATED BY 'henrik.tomwoerden' AND THE GREATEST ID";
   */
  @Test
  public void testQuery30()
      throws InterruptedException, SQLException, ConnectionException, QueryException {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.query30));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    for (final Token t : tokens.getTokens()) {
      System.out.println(t.toString());
    }
    System.out.println(sfq.toStringTree(parser));

    // 4 children: FIND, entity, filter, EOF
    assertEquals("FIND ", sfq.getChild(0).getText());
    assertEquals("RECORD", sfq.getChild(1).getText());
    assertEquals(".CREATED BY 'henrik.tomwoerden' AND THE GREATEST ID", sfq.getChild(2).getText());

    assertNull(sfq.e);
    assertEquals(4, sfq.getChildCount());

    final ParseTree filter = sfq.getChild(2);
    // 2 children: WHICH , conjunction,
    assertEquals(2, filter.getChildCount());
    assertEquals(".", filter.getChild(0).getText());
    assertEquals(
        "CREATED BY 'henrik.tomwoerden' AND THE GREATEST ID", filter.getChild(1).getText());

    final ParseTree conjunction = filter.getChild(1);

    // 4 children: transaction, WHITE_SPACE, AND, id_filter
    assertEquals(4, conjunction.getChildCount());
    assertEquals("CREATED BY 'henrik.tomwoerden'", conjunction.getChild(0).getText());
    assertEquals("AND ", conjunction.getChild(2).getText());
    assertEquals("THE GREATEST ID", conjunction.getChild(3).getText());
  }

  /** String query31 = "FIND PROPERTIES WHICH WERE INSERTED TODAY"; */
  @Test
  public void testQuery31() {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.query31));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    System.out.println(sfq.toStringTree(parser));

    // 4 children: FIND, role, WHICHCLAUSE, EOF
    assertEquals(4, sfq.getChildCount());
    assertEquals("FIND ", sfq.getChild(0).getText());
    assertEquals("PROPERTIES ", sfq.getChild(1).getText());
    assertEquals("WHICH WERE INSERTED TODAY", sfq.getChild(2).getText());
    assertEquals(null, sfq.e);
    assertEquals(Query.Role.PROPERTY, sfq.r);
    assertEquals("TransactionFilter", sfq.filter.getClass().getSimpleName());

    final ParseTree whichclause = sfq.getChild(2);
    // 2 children: WHICH , transaction
    assertEquals(2, whichclause.getChildCount());
    assertEquals("WHICH WERE ", whichclause.getChild(0).getText());
    assertEquals("INSERTED TODAY", whichclause.getChild(1).getText());

    final ParseTree transactionFilter = whichclause.getChild(1).getChild(0);
    assertEquals(2, transactionFilter.getChildCount());
    assertEquals("INSERTED ", transactionFilter.getChild(0).getText());
    assertEquals("TODAY", transactionFilter.getChild(1).getText());
  }

  /** String ticket173 = "COUNT ename"; */
  @Test
  public void TestTicket173()
      throws InterruptedException, SQLException, ConnectionException, QueryException {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.ticket173));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    System.out.println(sfq.toStringTree(parser));

    // 4 children: FIND, EMPTY_SPACE, entity, entity_filter
    assertEquals(4, sfq.getChildCount());
    assertEquals("COUNT ", sfq.getChild(0).getText());
    assertEquals("ename", sfq.getChild(1).getText());
    assertEquals(Pattern.TYPE_NORMAL, sfq.e.type);

    // entity_filter
    assertEquals(".pname1=val1", sfq.getChild(2).getText());
    final ParseTree entity_filter = sfq.getChild(2);

    // 2 children: WHICH_EXP, conjunction
    assertEquals(2, entity_filter.getChildCount());
    assertEquals(".", entity_filter.getChild(0).getText());

    // conjunction
    assertEquals("pname1=val1", entity_filter.getChild(1).getText());
    final ParseTree conjunction = entity_filter.getChild(1);

    // 1 child: pov
    assertEquals(1, conjunction.getChildCount());
    assertEquals("pname1=val1", conjunction.getChild(0).getText());

    // pov
    final ParseTree pov1 = conjunction.getChild(0);

    // 3 chidren: property, operator, value
    assertEquals(3, pov1.getChildCount());
    assertEquals("pname1", pov1.getChild(0).getText());
    assertEquals("=", pov1.getChild(1).getText());
    assertEquals("val1", pov1.getChild(2).getText());

    assertEquals("ename", sfq.e.toString());
    assertNotNull(sfq.filter);
    assertEquals(POV.class.getName(), sfq.filter.getClass().getName());
    final EntityFilterInterface f = sfq.filter;

    assertNotNull(f);
    assertEquals("POV(pname1,=,val1)", f.toString());
  }

  /** String ticket163 = "FIND Entity . pname=2.0"; */
  @Test
  public void TestTicket163()
      throws InterruptedException, SQLException, ConnectionException, QueryException {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.ticket163));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    System.out.println(sfq.toStringTree(parser));

    // 4 children: FIND, EMPTY_SPACE, entity, entity_filter
    assertEquals(4, sfq.getChildCount());
    assertEquals("FIND ", sfq.getChild(0).getText());
    assertEquals("Entity ", sfq.getChild(1).getText());

    // entity_filter
    assertEquals(". pname=2.0", sfq.getChild(2).getText());
    final ParseTree entity_filter = sfq.getChild(2);

    // 2 children: WHICH_EXP, conjunction
    assertEquals(2, entity_filter.getChildCount());
    assertEquals(". ", entity_filter.getChild(0).getText());

    // conjunction
    assertEquals("pname=2.0", entity_filter.getChild(1).getText());
    final ParseTree conjunction = entity_filter.getChild(1);

    // 1 child: pov
    assertEquals(1, conjunction.getChildCount());
    assertEquals("pname=2.0", conjunction.getChild(0).getText());

    // pov
    final ParseTree pov1 = conjunction.getChild(0);

    // 3 chidren: property, operator, value
    assertEquals(3, pov1.getChildCount());
    assertEquals("pname", pov1.getChild(0).getText());
    assertEquals("=", pov1.getChild(1).getText());
    assertEquals("2.0", pov1.getChild(2).getText());

    assertEquals(Query.Role.ENTITY, sfq.r);
    assertNotNull(sfq.filter);
    assertEquals(POV.class.getName(), sfq.filter.getClass().getName());
    final EntityFilterInterface f = sfq.filter;

    assertNotNull(f);
    assertEquals("POV(pname,=,2.0)", f.toString());
  }

  /**
   * String ticket165 = "FIND Record WHICH IS REFERENCED BY annotation WITH comment='blablabla'" ;
   */
  @Test
  public void TestTicket165()
      throws InterruptedException, SQLException, ConnectionException, QueryException {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.ticket165));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    System.out.println(sfq.toStringTree(parser));

    // 4 children: FIND, RECORD, entity_filter, EOF
    assertEquals(4, sfq.getChildCount());
    assertEquals("FIND ", sfq.getChild(0).getText());
    assertEquals("Record ", sfq.getChild(1).getText());

    // entity_filter
    assertEquals(
        "WHICH IS REFERENCED BY annotation WITH comment='blablabla'", sfq.getChild(2).getText());
    final ParseTree entity_filter = sfq.getChild(2);

    // 2 children: WHICH_EXP, filter
    assertEquals(2, entity_filter.getChildCount());
    assertEquals("WHICH ", entity_filter.getChild(0).getText());

    // filter
    assertEquals(
        "IS REFERENCED BY annotation WITH comment='blablabla'",
        entity_filter.getChild(1).getText());
    final ParseTree filter = entity_filter.getChild(1);

    // 2 children: backref, subproperty
    assertEquals(2, filter.getChildCount());
    assertEquals("IS REFERENCED BY annotation ", filter.getChild(0).getText());
    assertEquals("WITH comment='blablabla'", filter.getChild(1).getText());

    // backref
    final ParseTree backref = filter.getChild(0);

    // 3 children: IS_REFERENCED, BY, entity
    assertEquals(4, backref.getChildCount());
    assertEquals("IS REFERENCED ", backref.getChild(0).getText());
    assertEquals("BY ", backref.getChild(1).getText());
    assertEquals("annotation", backref.getChild(2).getText());

    // subproperty
    final ParseTree subp = filter.getChild(1);

    // 1 children: subquery
    assertEquals(1, subp.getChildCount());
    assertEquals("WITH comment='blablabla'", subp.getChild(0).getText());

    // subquery
    final ParseTree subquery = subp.getChild(0);

    // 2 children: WHICH_EXP filter
    assertEquals(2, subquery.getChildCount());
    assertEquals("WITH ", subquery.getChild(0).getText());
    assertEquals("comment='blablabla'", subquery.getChild(1).getText());

    assertEquals(Query.Role.RECORD, sfq.r);
    assertNotNull(sfq.filter);
    assertEquals(Backreference.class.getName(), sfq.filter.getClass().getName());
    final Backreference f = (Backreference) sfq.filter;

    assertNotNull(f);
    assertEquals("@(annotation,null)", f.toString());

    assertTrue(f.hasSubProperty());
  }

  /** String ticket187 = "FIND Entity WITH pname=-1"; */
  @Test
  public void TestTicket187()
      throws InterruptedException, SQLException, ConnectionException, QueryException {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.ticket187));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    System.out.println(sfq.toStringTree(parser));

    // 4 children: FIND, EMPTY_SPACE, entity, entity_filter
    assertEquals(4, sfq.getChildCount());
    assertEquals("FIND ", sfq.getChild(0).getText());
    assertEquals("Entity ", sfq.getChild(1).getText());

    // entity_filter
    assertEquals("WITH pname=-1", sfq.getChild(2).getText());
    final ParseTree entity_filter = sfq.getChild(2);

    // 2 children: WHICH_EXP, conjunction
    assertEquals(2, entity_filter.getChildCount());
    assertEquals("WITH ", entity_filter.getChild(0).getText());

    // conjunction
    assertEquals("pname=-1", entity_filter.getChild(1).getText());
    final ParseTree conjunction = entity_filter.getChild(1);

    // 1 child: pov
    assertEquals(1, conjunction.getChildCount());
    assertEquals("pname=-1", conjunction.getChild(0).getText());

    // pov
    final ParseTree pov1 = conjunction.getChild(0);

    // 3 chidren: property, operator, value
    assertEquals(3, pov1.getChildCount());
    assertEquals("pname", pov1.getChild(0).getText());
    assertEquals("=", pov1.getChild(1).getText());
    assertEquals("-1", pov1.getChild(2).getText());

    assertEquals(Query.Role.ENTITY, sfq.r);
    assertNotNull(sfq.filter);
    assertEquals(POV.class.getName(), sfq.filter.getClass().getName());
    final EntityFilterInterface f = sfq.filter;

    assertNotNull(f);
    assertEquals("POV(pname,=,-1)", f.toString());
  }

  /*
   * String ticket207 = "FIND RECORD WHICH REFERENCES 10594";
   */

  @Test
  public void TestTicket207()
      throws InterruptedException, SQLException, ConnectionException, QueryException {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.ticket207));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    // 4 children: FIND, EMPTY_SPACE, entity, entity_filter
    assertEquals(4, sfq.getChildCount());
    assertEquals("FIND ", sfq.getChild(0).getText());
    assertEquals("RECORD ", sfq.getChild(1).getText());

    // entity_filter
    assertEquals("WHICH -> 10594", sfq.getChild(2).getText());
    final ParseTree entity_filter = sfq.getChild(2);

    // 2 children: WHICH_EXP, conjunction
    assertEquals(2, entity_filter.getChildCount());
    assertEquals("WHICH ", entity_filter.getChild(0).getText());

    // conjunction
    assertEquals("-> 10594", entity_filter.getChild(1).getText());
    final ParseTree conjunction = entity_filter.getChild(1);

    // 1 child: pov
    assertEquals(1, conjunction.getChildCount());
    assertEquals("-> 10594", conjunction.getChild(0).getText());

    // pov
    final ParseTree pov1 = conjunction.getChild(0);

    // 3 children: (no property), operator, WHITE_SPACE, value
    assertEquals(3, pov1.getChildCount());
    assertEquals("->", pov1.getChild(0).getText());
    assertEquals("10594", pov1.getChild(2).getText());

    assertEquals(Query.Role.RECORD, sfq.r);
    assertNotNull(sfq.filter);
    assertEquals(POV.class.getName(), sfq.filter.getClass().getName());
    final EntityFilterInterface f = sfq.filter;

    assertNotNull(f);
    assertEquals("POV(null,->,10594)", f.toString());

    assertTrue(f instanceof POV);
  }

  /** String query33 = "FIND ename WITH a date IN 2015"; */
  @Test
  public void testQuery33() {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.query33));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    System.out.println(sfq.toStringTree(parser));

    // 5 children: FIND, entity, WHITE_SPACE, entity_filter, EOF
    assertEquals(5, sfq.getChildCount());
    assertEquals("FIND ", sfq.getChild(0).getText());
    assertEquals("ename", sfq.getChild(1).getText());
    assertEquals("WITH a date IN 2015", sfq.getChild(3).getText());
    assertEquals("ename", sfq.e.toString());
    assertNull(sfq.r);
    assertEquals("POV", sfq.filter.getClass().getSimpleName());

    final ParseTree whichclause = sfq.getChild(3);
    // 2 children: WHICH , transaction
    assertEquals(2, whichclause.getChildCount());
    assertEquals("WITH a ", whichclause.getChild(0).getText());
    assertEquals("date IN 2015", whichclause.getChild(1).getText());

    final ParseTree transactionFilter = whichclause.getChild(1).getChild(0);
    assertEquals(3, transactionFilter.getChildCount());
    assertEquals("date ", transactionFilter.getChild(0).getText());
    assertEquals("IN ", transactionFilter.getChild(1).getText());
    assertEquals("2015", transactionFilter.getChild(2).getText());

    assertTrue(sfq.filter instanceof POV);
    final POV pov = (POV) sfq.filter;
    assertEquals("(", pov.getOperator());
    assertEquals("2015", pov.getValue());
  }

  /** String query33a = "FIND ename WITH a date IN \"2015\""; */
  @Test
  public void testQuery33a() {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.query33a));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    System.out.println(sfq.toStringTree(parser));

    // 5 children: FIND, role, WHITE_SPACE, WHICHCLAUSE, EOF
    assertEquals(5, sfq.getChildCount());
    assertEquals("FIND ", sfq.getChild(0).getText());
    assertEquals("ename", sfq.getChild(1).getText());
    assertEquals("WITH a date IN \"2015\"", sfq.getChild(3).getText());
    assertEquals("ename", sfq.e.toString());
    assertNull(sfq.r);
    assertEquals("POV", sfq.filter.getClass().getSimpleName());

    final ParseTree whichclause = sfq.getChild(3);
    // 2 children; WHICH , transaction
    assertEquals(2, whichclause.getChildCount());
    assertEquals("WITH a ", whichclause.getChild(0).getText());
    assertEquals("date IN \"2015\"", whichclause.getChild(1).getText());

    final ParseTree transactionFilter = whichclause.getChild(1).getChild(0);
    assertEquals(3, transactionFilter.getChildCount());
    assertEquals("date ", transactionFilter.getChild(0).getText());
    assertEquals("IN ", transactionFilter.getChild(1).getText());
    assertEquals("\"2015\"", transactionFilter.getChild(2).getText());

    assertTrue(sfq.filter instanceof POV);
    final POV pov = (POV) sfq.filter;
    assertEquals("(", pov.getOperator());
    assertEquals("2015", pov.getValue());
  }

  /** String query34 = "FIND ename WITH a date NOT IN 2015"; */
  @Test
  public void testQuery34() {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.query34));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    System.out.println(sfq.toStringTree(parser));

    // 5 children: FIND, entity, WHITE_SPACE, entity_filter, EOF
    assertEquals(5, sfq.getChildCount());
    assertEquals("FIND ", sfq.getChild(0).getText());
    assertEquals("ename", sfq.getChild(1).getText());
    assertEquals("WITH a date NOT IN 2015", sfq.getChild(3).getText());
    assertEquals("ename", sfq.e.toString());
    assertNull(sfq.r);
    assertEquals("POV", sfq.filter.getClass().getSimpleName());

    final ParseTree whichclause = sfq.getChild(3);
    // 2 children: WHICH , POV
    assertEquals(2, whichclause.getChildCount());
    assertEquals("WITH a ", whichclause.getChild(0).getText());
    assertEquals("date NOT IN 2015", whichclause.getChild(1).getText());

    final ParseTree transactionFilter = whichclause.getChild(1).getChild(0);
    assertEquals(4, transactionFilter.getChildCount());
    assertEquals("date ", transactionFilter.getChild(0).getText());
    assertEquals("NOT ", transactionFilter.getChild(1).getText());
    assertEquals("IN ", transactionFilter.getChild(2).getText());
    assertEquals("2015", transactionFilter.getChild(3).getText());

    assertTrue(sfq.filter instanceof POV);
    final POV pov = (POV) sfq.filter;
    assertEquals("!(", pov.getOperator());
    assertEquals("2015", pov.getValue());
  }

  /** String query34a = "FIND ename WITH a date NOT IN \"2015\""; */
  @Test
  public void testQuery34a() {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.query34a));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    System.out.println(sfq.toStringTree(parser));

    // 5 children: FIND, role, WHITE_SPACE, WHICHCLAUSE, EOF
    assertEquals(5, sfq.getChildCount());
    assertEquals("FIND ", sfq.getChild(0).getText());
    assertEquals("ename", sfq.getChild(1).getText());
    assertEquals("WITH a date NOT IN \"2015\"", sfq.getChild(3).getText());
    assertEquals("ename", sfq.e.toString());
    assertNull(sfq.r);
    assertEquals("POV", sfq.filter.getClass().getSimpleName());

    final ParseTree whichclause = sfq.getChild(3);
    // 2 children: WHICH , POV
    assertEquals(2, whichclause.getChildCount());
    assertEquals("WITH a ", whichclause.getChild(0).getText());
    assertEquals("date NOT IN \"2015\"", whichclause.getChild(1).getText());

    final ParseTree transactionFilter = whichclause.getChild(1).getChild(0);
    assertEquals(4, transactionFilter.getChildCount());
    assertEquals("date ", transactionFilter.getChild(0).getText());
    assertEquals("NOT ", transactionFilter.getChild(1).getText());
    assertEquals("IN ", transactionFilter.getChild(2).getText());
    assertEquals("\"2015\"", transactionFilter.getChild(3).getText());

    assertTrue(sfq.filter instanceof POV);
    final POV pov = (POV) sfq.filter;
    assertEquals("!(", pov.getOperator());
    assertEquals("2015", pov.getValue());
  }

  /** String query35 = "FIND ename WITH a date IN 2015-01-01"; */
  @Test
  public void testQuery35() {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.query35));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    System.out.println(sfq.toStringTree(parser));

    // 5 children: FIND, entity, WHITE_SPACE, entity_filter, EOF
    assertEquals(5, sfq.getChildCount());
    assertEquals("FIND ", sfq.getChild(0).getText());
    assertEquals("ename", sfq.getChild(1).getText());
    assertEquals("WITH a date IN 2015-01-01", sfq.getChild(3).getText());
    assertEquals("ename", sfq.e.toString());
    assertNull(sfq.r);
    assertEquals("POV", sfq.filter.getClass().getSimpleName());

    final ParseTree whichclause = sfq.getChild(3);
    // 2 children: WHICH , POV
    assertEquals(2, whichclause.getChildCount());
    assertEquals("WITH a ", whichclause.getChild(0).getText());
    assertEquals("date IN 2015-01-01", whichclause.getChild(1).getText());

    final ParseTree transactionFilter = whichclause.getChild(1).getChild(0);
    assertEquals(3, transactionFilter.getChildCount());
    assertEquals("date ", transactionFilter.getChild(0).getText());
    assertEquals("IN ", transactionFilter.getChild(1).getText());
    assertEquals("2015-01-01", transactionFilter.getChild(2).getText());

    assertTrue(sfq.filter instanceof POV);
    final POV pov = (POV) sfq.filter;
    assertEquals("(", pov.getOperator());
    assertEquals("2015-01-01", pov.getValue());
  }

  /** String query35a = "FIND ename WITH a date IN \"2015-01-01\""; */
  @Test
  public void testQuery35a() {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.query35a));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    System.out.println(sfq.toStringTree(parser));

    // 5 children: FIND, role, WHITE_SPACE, WHICHCLAUSE, EOF
    assertEquals(5, sfq.getChildCount());
    assertEquals("FIND ", sfq.getChild(0).getText());
    assertEquals("ename", sfq.getChild(1).getText());
    assertEquals("WITH a date IN \"2015-01-01\"", sfq.getChild(3).getText());
    assertEquals("ename", sfq.e.toString());
    assertNull(sfq.r);
    assertEquals("POV", sfq.filter.getClass().getSimpleName());

    final ParseTree whichclause = sfq.getChild(3);
    // 2 children: WHICH , POV
    assertEquals(2, whichclause.getChildCount());
    assertEquals("WITH a ", whichclause.getChild(0).getText());
    assertEquals("date IN \"2015-01-01\"", whichclause.getChild(1).getText());

    final ParseTree transactionFilter = whichclause.getChild(1).getChild(0);
    assertEquals(3, transactionFilter.getChildCount());
    assertEquals("date ", transactionFilter.getChild(0).getText());
    assertEquals("IN ", transactionFilter.getChild(1).getText());
    assertEquals("\"2015-01-01\"", transactionFilter.getChild(2).getText());

    assertTrue(sfq.filter instanceof POV);
    final POV pov = (POV) sfq.filter;
    assertEquals("(", pov.getOperator());
    assertEquals("2015-01-01", pov.getValue());
  }

  /** String query36 = "FIND ename.pname LIKE \"wil*card\""; */
  @Test
  public void testQuery36() {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.query36));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    System.out.println(sfq.toStringTree(parser));

    // 4 children: FIND, role, WHICHCLAUSE, EOF
    assertEquals(4, sfq.getChildCount());
    assertEquals("FIND ", sfq.getChild(0).getText());
    assertEquals("ename", sfq.getChild(1).getText());
    assertEquals(".pname LIKE \"wil*card\"", sfq.getChild(2).getText());
    assertEquals("ename", sfq.e.toString());
    assertNull(sfq.r);
    assertEquals("POV", sfq.filter.getClass().getSimpleName());

    final ParseTree whichclause = sfq.getChild(2);
    // 2 children: WHICH , POV
    assertEquals(2, whichclause.getChildCount());
    assertEquals(".", whichclause.getChild(0).getText());
    assertEquals("pname LIKE \"wil*card\"", whichclause.getChild(1).getText());

    final ParseTree transactionFilter = whichclause.getChild(1).getChild(0);
    assertEquals(3, transactionFilter.getChildCount());
    assertEquals("pname ", transactionFilter.getChild(0).getText());
    assertEquals("LIKE ", transactionFilter.getChild(1).getText());
    assertEquals("\"wil*card\"", transactionFilter.getChild(2).getText());

    assertTrue(sfq.filter instanceof POV);
    final POV pov = (POV) sfq.filter;
    assertEquals("LIKE", pov.getOperator());
    assertEquals("wil%card", pov.getValue());
  }

  /** String query37 = "FIND ename.pname LIKE wil*card"; */
  @Test
  public void testQuery37() {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.query37));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    System.out.println(sfq.toStringTree(parser));

    // 4 children: FIND, role, WHICHCLAUSE, EOF
    assertEquals(4, sfq.getChildCount());
    assertEquals("FIND ", sfq.getChild(0).getText());
    assertEquals("ename", sfq.getChild(1).getText());
    assertEquals(".pname LIKE wil*card", sfq.getChild(2).getText());
    assertEquals("ename", sfq.e.toString());
    assertNull(sfq.r);
    assertEquals("POV", sfq.filter.getClass().getSimpleName());

    final ParseTree whichclause = sfq.getChild(2);
    // 2 children: WHICH , POV
    assertEquals(2, whichclause.getChildCount());
    assertEquals(".", whichclause.getChild(0).getText());
    assertEquals("pname LIKE wil*card", whichclause.getChild(1).getText());

    final ParseTree transactionFilter = whichclause.getChild(1).getChild(0);
    assertEquals(3, transactionFilter.getChildCount());
    assertEquals("pname ", transactionFilter.getChild(0).getText());
    assertEquals("LIKE ", transactionFilter.getChild(1).getText());
    assertEquals("wil*card", transactionFilter.getChild(2).getText());

    assertTrue(sfq.filter instanceof POV);
    final POV pov = (POV) sfq.filter;
    assertEquals("LIKE", pov.getOperator());
    assertEquals("wil%card", pov.getValue());
  }

  /** String query38 = "FIND ename WHICH HAS A pname LIKE \"wil*\""; */
  @Test
  public void testQuery38() {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.query38));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    System.out.println(sfq.toStringTree(parser));

    // 5 children: FIND, ename, WHITE_SPACE, WHICHCLAUSE, EOF
    assertEquals(5, sfq.getChildCount());
    assertEquals("FIND ", sfq.getChild(0).getText());
    assertEquals("ename", sfq.getChild(1).getText());
    assertEquals("WHICH HAS A pname LIKE \"wil*\"", sfq.getChild(3).getText());
    assertEquals("ename", sfq.e.toString());
    assertNull(sfq.r);
    assertEquals("POV", sfq.filter.getClass().getSimpleName());

    final ParseTree whichclause = sfq.getChild(3);
    // 2 children: WHICH , POV
    assertEquals(2, whichclause.getChildCount());
    assertEquals("WHICH HAS A ", whichclause.getChild(0).getText());
    assertEquals("pname LIKE \"wil*\"", whichclause.getChild(1).getText());

    final ParseTree transactionFilter = whichclause.getChild(1).getChild(0);
    assertEquals(3, transactionFilter.getChildCount());
    assertEquals("pname ", transactionFilter.getChild(0).getText());
    assertEquals("LIKE ", transactionFilter.getChild(1).getText());
    assertEquals("\"wil*\"", transactionFilter.getChild(2).getText());

    assertTrue(sfq.filter instanceof POV);
    final POV pov = (POV) sfq.filter;
    assertEquals("LIKE", pov.getOperator());
    assertEquals("wil%", pov.getValue());
  }

  /** String query39 = "FIND ename WHICH HAS A pname LIKE wil*"; */
  @Test
  public void testQuery39() {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.query39));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    System.out.println(sfq.toStringTree(parser));

    // 5 children: FIND, entity, WHITE_SPACE, entity_filter, EOF
    assertEquals(5, sfq.getChildCount());
    assertEquals("FIND ", sfq.getChild(0).getText());
    assertEquals("ename", sfq.getChild(1).getText());
    assertEquals("WHICH HAS A pname LIKE wil*", sfq.getChild(3).getText());
    assertEquals("ename", sfq.e.toString());
    assertNull(sfq.r);
    assertEquals("POV", sfq.filter.getClass().getSimpleName());

    final ParseTree whichclause = sfq.getChild(3);
    // 2 children: WHICH , POV
    assertEquals(2, whichclause.getChildCount());
    assertEquals("WHICH HAS A ", whichclause.getChild(0).getText());
    assertEquals("pname LIKE wil*", whichclause.getChild(1).getText());

    final ParseTree transactionFilter = whichclause.getChild(1).getChild(0);
    assertEquals(3, transactionFilter.getChildCount());
    assertEquals("pname ", transactionFilter.getChild(0).getText());
    assertEquals("LIKE ", transactionFilter.getChild(1).getText());
    assertEquals("wil*", transactionFilter.getChild(2).getText());

    assertTrue(sfq.filter instanceof POV);
    final POV pov = (POV) sfq.filter;
    assertEquals("LIKE", pov.getOperator());
    assertEquals("wil%", pov.getValue());
  }

  /** String query40 = "FIND ename WHICH HAS A pname = wil*"; */
  @Test
  public void testQuery40() {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.query40));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    System.out.println(sfq.toStringTree(parser));

    // 5 children: FIND, entity, WHITE_SPACE, entity_filter, EOF
    assertEquals(5, sfq.getChildCount());
    assertEquals("FIND ", sfq.getChild(0).getText());
    assertEquals("ename", sfq.getChild(1).getText());
    assertEquals("WHICH HAS A pname = wil*", sfq.getChild(3).getText());
    assertEquals("ename", sfq.e.toString());
    assertNull(sfq.r);
    assertEquals("POV", sfq.filter.getClass().getSimpleName());

    final ParseTree whichclause = sfq.getChild(3);
    // 2 children: WHICH , POV
    assertEquals(2, whichclause.getChildCount());
    assertEquals("WHICH HAS A ", whichclause.getChild(0).getText());
    assertEquals("pname = wil*", whichclause.getChild(1).getText());

    final ParseTree transactionFilter = whichclause.getChild(1).getChild(0);
    assertEquals(4, transactionFilter.getChildCount());
    assertEquals("pname ", transactionFilter.getChild(0).getText());
    assertEquals("=", transactionFilter.getChild(1).getText());
    assertEquals("wil*", transactionFilter.getChild(3).getText());

    assertTrue(sfq.filter instanceof POV);
    final POV pov = (POV) sfq.filter;
    assertEquals("=", pov.getOperator());
    assertEquals("wil*", pov.getValue());
  }

  /** String query41 = "FIND FILE WHICH IS STORED AT /data/bla.acq"; */
  @Test
  public void testQuery41() {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.query41));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    System.out.println(sfq.toStringTree(parser));

    // 4 children: FIND, role, WHICHCLAUSE, EOF
    assertEquals(4, sfq.getChildCount());
    assertEquals("FIND ", sfq.getChild(0).getText());
    assertEquals("FILE ", sfq.getChild(1).getText());
    assertEquals("WHICH IS STORED AT /data/bla.acq", sfq.getChild(2).getText());
    assertEquals("FILE", sfq.r.toString());
    assertNull(sfq.e);
    assertEquals("StoredAt", sfq.filter.getClass().getSimpleName());

    final ParseTree whichclause = sfq.getChild(2);
    // 2 children: WHICH , POV
    assertEquals(2, whichclause.getChildCount());
    assertEquals("WHICH ", whichclause.getChild(0).getText());
    assertEquals("IS STORED AT /data/bla.acq", whichclause.getChild(1).getText());

    final ParseTree transactionFilter = whichclause.getChild(1).getChild(0);
    assertEquals(2, transactionFilter.getChildCount());
    assertEquals("IS STORED AT ", transactionFilter.getChild(0).getText());
    assertEquals("/data/bla.acq", transactionFilter.getChild(1).getText());

    assertTrue(sfq.filter instanceof StoredAt);
    final StoredAt storedAt = (StoredAt) sfq.filter;
    assertEquals("SAT(/data/bla.acq)", storedAt.toString());
  }

  /** String query42 = "FIND FILE WHICH IS STORED AT /*"; */
  @Test
  public void testQuery42() {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.query42));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    System.out.println(sfq.toStringTree(parser));

    // 4 children: FIND, role, WHICHCLAUSE, EOF
    assertEquals(4, sfq.getChildCount());
    assertEquals("FIND ", sfq.getChild(0).getText());
    assertEquals("FILE ", sfq.getChild(1).getText());
    assertEquals("WHICH IS STORED AT /*", sfq.getChild(2).getText());
    assertEquals("FILE", sfq.r.toString());
    assertNull(sfq.e);
    assertEquals("StoredAt", sfq.filter.getClass().getSimpleName());

    final ParseTree whichclause = sfq.getChild(2);
    // 2 children: WHICH , POV
    assertEquals(2, whichclause.getChildCount());
    assertEquals("WHICH ", whichclause.getChild(0).getText());
    assertEquals("IS STORED AT /*", whichclause.getChild(1).getText());

    final ParseTree transactionFilter = whichclause.getChild(1).getChild(0);
    assertEquals(2, transactionFilter.getChildCount());
    assertEquals("IS STORED AT ", transactionFilter.getChild(0).getText());
    assertEquals("/*", transactionFilter.getChild(1).getText());

    assertTrue(sfq.filter instanceof StoredAt);
    final StoredAt storedAt = (StoredAt) sfq.filter;
    assertTrue(StoredAt.requiresPatternMatching(storedAt.location));
    assertEquals("SAT(%%)", storedAt.toString());
  }

  /** String query43 = "FIND FILE WHICH IS STORED AT /* /" (without the space); */
  @Test
  public void testQuery43() {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.query43));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    System.out.println(sfq.toStringTree(parser));

    // 4 children: FIND, role, WHICHCLAUSE, EOF
    assertEquals(4, sfq.getChildCount());
    assertEquals("FIND ", sfq.getChild(0).getText());
    assertEquals("FILE ", sfq.getChild(1).getText());
    assertEquals("WHICH IS STORED AT /*/", sfq.getChild(2).getText());
    assertEquals("FILE", sfq.r.toString());
    assertNull(sfq.e);
    assertEquals("StoredAt", sfq.filter.getClass().getSimpleName());

    final ParseTree whichclause = sfq.getChild(2);
    // 2 children: WHICH , POV
    assertEquals(2, whichclause.getChildCount());
    assertEquals("WHICH ", whichclause.getChild(0).getText());
    assertEquals("IS STORED AT /*/", whichclause.getChild(1).getText());

    final ParseTree transactionFilter = whichclause.getChild(1).getChild(0);
    assertEquals(2, transactionFilter.getChildCount());
    assertEquals("IS STORED AT ", transactionFilter.getChild(0).getText());
    assertEquals("/*/", transactionFilter.getChild(1).getText());

    assertTrue(sfq.filter instanceof StoredAt);
    final StoredAt storedAt = (StoredAt) sfq.filter;
    assertTrue(StoredAt.requiresPatternMatching(storedAt.location));
    assertEquals("SAT(%%/)", storedAt.toString());
  }

  /** String query44 = "FIND FILE WHICH IS STORED AT /** /"; */
  @Test
  public void testQuery44() {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.query44));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    System.out.println(sfq.toStringTree(parser));

    // 4 children: FIND, role, WHICHCLAUSE, EOF
    assertEquals(4, sfq.getChildCount());
    assertEquals("FIND ", sfq.getChild(0).getText());
    assertEquals("FILE ", sfq.getChild(1).getText());
    assertEquals("WHICH IS STORED AT /**/", sfq.getChild(2).getText());
    assertEquals("FILE", sfq.r.toString());
    assertNull(sfq.e);
    assertEquals("StoredAt", sfq.filter.getClass().getSimpleName());

    final ParseTree whichclause = sfq.getChild(2);
    // 2 children: WHICH , POV
    assertEquals(2, whichclause.getChildCount());
    assertEquals("WHICH ", whichclause.getChild(0).getText());
    assertEquals("IS STORED AT /**/", whichclause.getChild(1).getText());

    final ParseTree transactionFilter = whichclause.getChild(1).getChild(0);
    assertEquals(2, transactionFilter.getChildCount());
    assertEquals("IS STORED AT ", transactionFilter.getChild(0).getText());
    assertEquals("/**/", transactionFilter.getChild(1).getText());

    assertTrue(sfq.filter instanceof StoredAt);
    final StoredAt storedAt = (StoredAt) sfq.filter;
    assertEquals("SAT(%/)", storedAt.toString());
  }

  /** String query45 = "FIND FILE WHICH IS STORED AT /** /*"; */
  @Test
  public void testQuery45() {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.query45));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    System.out.println(sfq.toStringTree(parser));

    // 4 children: FIND, role, WHICHCLAUSE, EOF
    assertEquals(4, sfq.getChildCount());
    assertEquals("FIND ", sfq.getChild(0).getText());
    assertEquals("FILE ", sfq.getChild(1).getText());
    assertEquals("WHICH IS STORED AT /**/*", sfq.getChild(2).getText());
    assertEquals("FILE", sfq.r.toString());
    assertNull(sfq.e);
    assertEquals("StoredAt", sfq.filter.getClass().getSimpleName());

    final ParseTree whichclause = sfq.getChild(2);
    // 2 children: WHICH , POV
    assertEquals(2, whichclause.getChildCount());
    assertEquals("WHICH ", whichclause.getChild(0).getText());
    assertEquals("IS STORED AT /**/*", whichclause.getChild(1).getText());

    final ParseTree transactionFilter = whichclause.getChild(1).getChild(0);
    assertEquals(2, transactionFilter.getChildCount());
    assertEquals("IS STORED AT ", transactionFilter.getChild(0).getText());
    assertEquals("/**/*", transactionFilter.getChild(1).getText());

    assertTrue(sfq.filter instanceof StoredAt);
    final StoredAt storedAt = (StoredAt) sfq.filter;
    assertEquals("SAT(%/%%)", storedAt.toString());
  }

  /** String query46 = "FIND FILE WHICH IS STORED AT /* /*"; */
  @Test
  public void testQuery46() {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.query46));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    System.out.println(sfq.toStringTree(parser));

    // 4 children: FIND, role, WHICHCLAUSE, EOF
    assertEquals(4, sfq.getChildCount());
    assertEquals("FIND ", sfq.getChild(0).getText());
    assertEquals("FILE ", sfq.getChild(1).getText());
    assertEquals("WHICH IS STORED AT /*/*", sfq.getChild(2).getText());
    assertEquals("FILE", sfq.r.toString());
    assertNull(sfq.e);
    assertEquals("StoredAt", sfq.filter.getClass().getSimpleName());

    final ParseTree whichclause = sfq.getChild(2);
    // 2 children: WHICH , POV
    assertEquals(2, whichclause.getChildCount());
    assertEquals("WHICH ", whichclause.getChild(0).getText());
    assertEquals("IS STORED AT /*/*", whichclause.getChild(1).getText());

    final ParseTree transactionFilter = whichclause.getChild(1).getChild(0);
    assertEquals(2, transactionFilter.getChildCount());
    assertEquals("IS STORED AT ", transactionFilter.getChild(0).getText());
    assertEquals("/*/*", transactionFilter.getChild(1).getText());

    assertTrue(sfq.filter instanceof StoredAt);
    final StoredAt storedAt = (StoredAt) sfq.filter;
    assertEquals("SAT(%%/%%)", storedAt.toString());
  }

  /** String query47 = "FIND FILE WHICH IS STORED AT /* /*.acq"; */
  @Test
  public void testQuery47() {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.query47));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    System.out.println(sfq.toStringTree(parser));

    // 4 children: FIND, role, WHICHCLAUSE, EOF
    assertEquals(4, sfq.getChildCount());
    assertEquals("FIND ", sfq.getChild(0).getText());
    assertEquals("FILE ", sfq.getChild(1).getText());
    assertEquals("WHICH IS STORED AT /*/*.acq", sfq.getChild(2).getText());
    assertEquals("FILE", sfq.r.toString());
    assertNull(sfq.e);
    assertEquals("StoredAt", sfq.filter.getClass().getSimpleName());

    final ParseTree whichclause = sfq.getChild(2);
    // 2 children: WHICH , POV
    assertEquals(2, whichclause.getChildCount());
    assertEquals("WHICH ", whichclause.getChild(0).getText());
    assertEquals("IS STORED AT /*/*.acq", whichclause.getChild(1).getText());

    final ParseTree transactionFilter = whichclause.getChild(1).getChild(0);
    assertEquals(2, transactionFilter.getChildCount());
    assertEquals("IS STORED AT ", transactionFilter.getChild(0).getText());
    assertEquals("/*/*.acq", transactionFilter.getChild(1).getText());

    assertTrue(sfq.filter instanceof StoredAt);
    final StoredAt storedAt = (StoredAt) sfq.filter;
    assertEquals("SAT(%%/%%.acq)", storedAt.toString());
  }

  /** String query48 = "FIND FILE WHICH IS STORED AT /** /*.acq"; */
  @Test
  public void testQuery48() {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.query48));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    System.out.println(sfq.toStringTree(parser));

    // 4 children: FIND, role, WHICHCLAUSE, EOF
    assertEquals(4, sfq.getChildCount());
    assertEquals("FIND ", sfq.getChild(0).getText());
    assertEquals("FILE ", sfq.getChild(1).getText());
    assertEquals("WHICH IS STORED AT /**/*.acq", sfq.getChild(2).getText());
    assertEquals("FILE", sfq.r.toString());
    assertNull(sfq.e);
    assertEquals("StoredAt", sfq.filter.getClass().getSimpleName());

    final ParseTree whichclause = sfq.getChild(2);
    // 2 children: WHICH , POV
    assertEquals(2, whichclause.getChildCount());
    assertEquals("WHICH ", whichclause.getChild(0).getText());
    assertEquals("IS STORED AT /**/*.acq", whichclause.getChild(1).getText());

    final ParseTree transactionFilter = whichclause.getChild(1).getChild(0);
    assertEquals(2, transactionFilter.getChildCount());
    assertEquals("IS STORED AT ", transactionFilter.getChild(0).getText());
    assertEquals("/**/*.acq", transactionFilter.getChild(1).getText());

    assertTrue(sfq.filter instanceof StoredAt);
    final StoredAt storedAt = (StoredAt) sfq.filter;
    assertEquals("SAT(%/%%.acq)", storedAt.toString());
  }

  /** String query49 = "FIND FILE WHICH IS STORED AT /*.acq"; */
  @Test
  public void testQuery49() {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.query49));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    System.out.println(sfq.toStringTree(parser));

    // 4 children: FIND, role, WHICHCLAUSE, EOF
    assertEquals(4, sfq.getChildCount());
    assertEquals("FIND ", sfq.getChild(0).getText());
    assertEquals("FILE ", sfq.getChild(1).getText());
    assertEquals("WHICH IS STORED AT /*.acq", sfq.getChild(2).getText());
    assertEquals("FILE", sfq.r.toString());
    assertNull(sfq.e);
    assertEquals("StoredAt", sfq.filter.getClass().getSimpleName());

    final ParseTree whichclause = sfq.getChild(2);
    // 2 children: WHICH , POV
    assertEquals(2, whichclause.getChildCount());
    assertEquals("WHICH ", whichclause.getChild(0).getText());
    assertEquals("IS STORED AT /*.acq", whichclause.getChild(1).getText());

    final ParseTree transactionFilter = whichclause.getChild(1).getChild(0);
    assertEquals(2, transactionFilter.getChildCount());
    assertEquals("IS STORED AT ", transactionFilter.getChild(0).getText());
    assertEquals("/*.acq", transactionFilter.getChild(1).getText());

    assertTrue(sfq.filter instanceof StoredAt);
    final StoredAt storedAt = (StoredAt) sfq.filter;
    assertEquals("SAT(%%.acq)", storedAt.toString());
  }

  /** String query50 = "FIND FILE WHICH IS STORED AT *.acq"; */
  @Test
  public void testQuery50() {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.query50));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    System.out.println(sfq.toStringTree(parser));

    // 4 children: FIND, role, WHICHCLAUSE, EOF
    assertEquals(4, sfq.getChildCount());
    assertEquals("FIND ", sfq.getChild(0).getText());
    assertEquals("FILE ", sfq.getChild(1).getText());
    assertEquals("WHICH IS STORED AT *.acq", sfq.getChild(2).getText());
    assertEquals("FILE", sfq.r.toString());
    assertNull(sfq.e);
    assertEquals("StoredAt", sfq.filter.getClass().getSimpleName());

    final ParseTree whichclause = sfq.getChild(2);
    // 2 children: WHICH , POV
    assertEquals(2, whichclause.getChildCount());
    assertEquals("WHICH ", whichclause.getChild(0).getText());
    assertEquals("IS STORED AT *.acq", whichclause.getChild(1).getText());

    final ParseTree transactionFilter = whichclause.getChild(1).getChild(0);
    assertEquals(2, transactionFilter.getChildCount());
    assertEquals("IS STORED AT ", transactionFilter.getChild(0).getText());
    assertEquals("*.acq", transactionFilter.getChild(1).getText());

    assertTrue(sfq.filter instanceof StoredAt);
    final StoredAt storedAt = (StoredAt) sfq.filter;
    assertEquals("SAT(%.acq)", storedAt.toString());
  }

  /** String query51 = "FIND FILE WHICH IS STORED AT *"; */
  @Test
  public void testQuery51() {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.query51));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    System.out.println(sfq.toStringTree(parser));

    // 4 children: FIND, role, WHICHCLAUSE, EOF
    assertEquals(4, sfq.getChildCount());
    assertEquals("FIND ", sfq.getChild(0).getText());
    assertEquals("FILE ", sfq.getChild(1).getText());
    assertEquals("WHICH IS STORED AT *", sfq.getChild(2).getText());
    assertEquals("FILE", sfq.r.toString());
    assertNull(sfq.e);
    assertEquals("StoredAt", sfq.filter.getClass().getSimpleName());

    final ParseTree whichclause = sfq.getChild(2);
    // 2 children: WHICH , POV
    assertEquals(2, whichclause.getChildCount());
    assertEquals("WHICH ", whichclause.getChild(0).getText());
    assertEquals("IS STORED AT *", whichclause.getChild(1).getText());

    final ParseTree transactionFilter = whichclause.getChild(1).getChild(0);
    assertEquals(2, transactionFilter.getChildCount());
    assertEquals("IS STORED AT ", transactionFilter.getChild(0).getText());
    assertEquals("*", transactionFilter.getChild(1).getText());

    assertTrue(sfq.filter instanceof StoredAt);
    final StoredAt storedAt = (StoredAt) sfq.filter;
    assertEquals("SAT(%)", storedAt.toString());
  }

  /** test regexp pattern */
  @Test
  public void testRegexPattern() {
    final java.util.regex.Pattern p = StoredAt.PATTERN;
    Matcher matcher;

    matcher = p.matcher("*");
    assertTrue(matcher.matches());
    assertEquals(3, matcher.groupCount());
    assertEquals("*", matcher.group(0));
    assertEquals(null, matcher.group(1));
    assertEquals(null, matcher.group(2));
    assertEquals("*", matcher.group(3));

    matcher = p.matcher("leading*");
    assertTrue(matcher.find());
    // assertEquals(3, matcher.groupCount());
    assertEquals("leading*", matcher.group(0));
    assertEquals("leading", matcher.group(1));
    assertEquals(null, matcher.group(2));
    assertEquals("*", matcher.group(3));

    matcher = p.matcher("leading**");
    assertTrue(matcher.matches());
    // assertEquals(3, matcher.groupCount());
    assertEquals("leading", matcher.group(1));
    assertEquals("**", matcher.group(2));
    assertEquals(null, matcher.group(3));

    matcher = p.matcher("leading**mid**");
    assertTrue(matcher.find());
    // assertEquals(3, matcher.groupCount());
    assertEquals("leading**", matcher.group(0));
    assertEquals("leading", matcher.group(1));
    assertEquals("**", matcher.group(2));
    assertEquals(null, matcher.group(3));

    assertTrue(matcher.find());
    // assertEquals(3, matcher.groupCount());
    assertEquals("mid**", matcher.group(0));
    assertEquals("mid", matcher.group(1));
    assertEquals("**", matcher.group(2));
    assertEquals(null, matcher.group(3));

    matcher = p.matcher("leading**mid*");
    assertTrue(matcher.find());
    // assertEquals(3, matcher.groupCount());
    assertEquals("leading**", matcher.group(0));
    assertEquals("leading", matcher.group(1));
    assertEquals("**", matcher.group(2));
    assertEquals(null, matcher.group(3));

    assertTrue(matcher.find());
    // assertEquals(3, matcher.groupCount());
    assertEquals("mid*", matcher.group(0));
    assertEquals("mid", matcher.group(1));
    assertEquals(null, matcher.group(2));
    assertEquals("*", matcher.group(3));

    matcher = p.matcher("\\*");
    assertTrue(matcher.find());
    // assertEquals(3, matcher.groupCount());
    assertEquals("\\*", matcher.group(0));
    assertEquals("\\*", matcher.group(1));
    assertEquals(null, matcher.group(2));
    assertEquals(null, matcher.group(3));

    matcher = p.matcher("\\*\\*");
    assertTrue(matcher.find());
    assertEquals(3, matcher.groupCount());
    assertEquals("\\*\\*", matcher.group(0));
    assertEquals("\\*\\*", matcher.group(1));
    assertEquals(null, matcher.group(2));
    assertEquals(null, matcher.group(3));

    matcher = p.matcher("\\*\\*asdf");
    assertTrue(matcher.find());
    assertEquals(3, matcher.groupCount());
    assertEquals("\\*\\*asdf", matcher.group(0));
    assertEquals("\\*\\*asdf", matcher.group(1));
    assertEquals(null, matcher.group(2));
    assertEquals(null, matcher.group(3));

    matcher = p.matcher("\\*\\*asdf\\\\");
    assertTrue(matcher.find());
    assertEquals(3, matcher.groupCount());
    assertEquals("\\*\\*asdf\\\\", matcher.group(0));
    assertEquals("\\*\\*asdf\\\\", matcher.group(1));
    assertEquals(null, matcher.group(2));
    assertEquals(null, matcher.group(3));

    matcher = p.matcher("\\**");
    assertTrue(matcher.find());
    assertEquals(3, matcher.groupCount());
    assertEquals("\\**", matcher.group(0));
    assertEquals("\\*", matcher.group(1));
    assertEquals(null, matcher.group(2));
    assertEquals("*", matcher.group(3));

    matcher = p.matcher("\\\\**");
    assertTrue(matcher.find());
    assertEquals(3, matcher.groupCount());
    assertEquals("\\\\**", matcher.group(0));
    assertEquals("\\\\", matcher.group(1));
    assertEquals("**", matcher.group(2));
    assertEquals(null, matcher.group(3));
  }

  /** String query52 = "FIND FILE WHICH IS STORED AT *%.acq"; */
  @Test
  public void testQuery52() {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.query52));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    System.out.println(sfq.toStringTree(parser));

    // 4 children: FIND, role, WHICHCLAUSE, EOF
    assertEquals(4, sfq.getChildCount());
    assertEquals("FIND ", sfq.getChild(0).getText());
    assertEquals("FILE ", sfq.getChild(1).getText());
    assertEquals("WHICH IS STORED AT *%.acq", sfq.getChild(2).getText());
    assertEquals("FILE", sfq.r.toString());
    assertNull(sfq.e);
    assertEquals("StoredAt", sfq.filter.getClass().getSimpleName());

    final ParseTree whichclause = sfq.getChild(2);
    // 2 children: WHICH , POV
    assertEquals(2, whichclause.getChildCount());
    assertEquals("WHICH ", whichclause.getChild(0).getText());
    assertEquals("IS STORED AT *%.acq", whichclause.getChild(1).getText());

    final ParseTree transactionFilter = whichclause.getChild(1).getChild(0);
    assertEquals(2, transactionFilter.getChildCount());
    assertEquals("IS STORED AT ", transactionFilter.getChild(0).getText());
    assertEquals("*%.acq", transactionFilter.getChild(1).getText());

    assertTrue(sfq.filter instanceof StoredAt);
    final StoredAt storedAt = (StoredAt) sfq.filter;
    assertEquals("SAT(%\\%.acq)", storedAt.toString());
  }

  /** String query53 = "FIND FILE WHICH IS STORED AT *\\*.acq"; */
  @Test
  public void testQuery53() {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.query53));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    System.out.println(sfq.toStringTree(parser));

    // 4 children: FIND, role, WHICHCLAUSE, EOF
    assertEquals(4, sfq.getChildCount());
    assertEquals("FIND ", sfq.getChild(0).getText());
    assertEquals("FILE ", sfq.getChild(1).getText());
    assertEquals("WHICH IS STORED AT *\\*.acq", sfq.getChild(2).getText());
    assertEquals("FILE", sfq.r.toString());
    assertNull(sfq.e);
    assertEquals("StoredAt", sfq.filter.getClass().getSimpleName());

    final ParseTree whichclause = sfq.getChild(2);
    // 2 children: WHICH , POV
    assertEquals(2, whichclause.getChildCount());
    assertEquals("WHICH ", whichclause.getChild(0).getText());
    assertEquals("IS STORED AT *\\*.acq", whichclause.getChild(1).getText());

    final ParseTree transactionFilter = whichclause.getChild(1).getChild(0);
    assertEquals(2, transactionFilter.getChildCount());
    assertEquals("IS STORED AT ", transactionFilter.getChild(0).getText());
    assertEquals("*\\*.acq", transactionFilter.getChild(1).getText());

    assertTrue(sfq.filter instanceof StoredAt);
    final StoredAt storedAt = (StoredAt) sfq.filter;
    assertEquals("SAT(%*.acq)", storedAt.toString());
  }

  /** String ticket241 = "FIND RECORD WHICH HAS been created by some*"; */
  @Test
  public void testTicket241() {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.ticket241));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    System.out.println(sfq.toStringTree(parser));

    assertTrue(sfq.filter instanceof TransactionFilter);
    assertEquals("TRANS(Insert,null,null,Transactor(some%,=))", sfq.filter.toString());
  }

  /** String ticket242 = "FIND RECORD WHICH HAS been created by some.user"; */
  @Test
  public void testTicket242() {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.ticket242));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    System.out.println(sfq.toStringTree(parser));

    assertEquals("TRANS(Insert,null,null,Transactor(some.user,=))", sfq.filter.toString());
    assertTrue(sfq.filter instanceof TransactionFilter);
  }

  /** String ticket262a = "COUNT FILE which is not referenced"; */
  @Test
  public void testTicket262a() {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.ticket262a));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    System.out.println(sfq.toStringTree(parser));

    assertTrue(sfq.filter instanceof Negation);
    final EntityFilterInterface f2 = ((Negation) sfq.filter).getFilter();
    assertTrue(f2 instanceof Backreference);
    assertEquals("@(null,null)", f2.toString());
  }

  /** String ticket262b = "COUNT FILE WHICH IS NOT REFERENCED BY"; */
  @Test
  public void testTicket262b() {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.ticket262b));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    System.out.println(sfq.toStringTree(parser));
    assertNull(sfq.filter);
  }

  /** String ticket262c = "COUNT FILE WHICH IS NOT REFERENCED BY entity"; */
  @Test
  public void testTicket262c() {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.ticket262c));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    System.out.println(sfq.toStringTree(parser));

    assertTrue(sfq.filter instanceof Negation);
    final EntityFilterInterface f2 = ((Negation) sfq.filter).getFilter();
    assertTrue(f2 instanceof Backreference);
    assertEquals("@(entity,null)", f2.toString());
  }

  /** String ticket262d = "COUNT FILE WHICH IS NOT REFERENCED AND WHICH WAS created by me"; */
  @Test
  public void testTicket262d() {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.ticket262d));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    System.out.println(sfq.toStringTree(parser));

    assertTrue(sfq.filter instanceof Conjunction);
    final LinkedList<EntityFilterInterface> f2 = ((Conjunction) sfq.filter).getFilters();

    assertTrue(f2.getFirst() instanceof Negation);
    final Negation n = (Negation) f2.getFirst();
    assertEquals("@(null,null)", n.getFilter().toString());

    assertTrue(f2.getLast() instanceof TransactionFilter);
    assertEquals("TRANS(Insert,null,null,Transactor(null,=))", f2.getLast().toString());
  }

  /** String ticket262e = "COUNT FILE WHICH IS NOT REFERENCED AND WAS created by me"; */
  @Test
  public void testTicket262e() {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.ticket262e));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    System.out.println(sfq.toStringTree(parser));

    assertTrue(sfq.filter instanceof Conjunction);
    final LinkedList<EntityFilterInterface> f2 = ((Conjunction) sfq.filter).getFilters();

    assertTrue(f2.getFirst() instanceof Negation);
    final Negation n = (Negation) f2.getFirst();
    assertEquals("@(null,null)", n.getFilter().toString());

    assertTrue(f2.getLast() instanceof TransactionFilter);
    assertEquals("TRANS(Insert,null,null,Transactor(null,=))", f2.getLast().toString());
  }

  /** String ticket262f = "COUNT FILE WHICH IS NOT REFERENCED BY entity AND WAS created by me"; */
  @Test
  public void testTicket262f() {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.ticket262f));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    System.out.println(sfq.toStringTree(parser));

    assertTrue(sfq.filter instanceof Conjunction);
    final LinkedList<EntityFilterInterface> f2 = ((Conjunction) sfq.filter).getFilters();

    assertTrue(f2.getFirst() instanceof Negation);
    final Negation n = (Negation) f2.getFirst();
    assertEquals("@(entity,null)", n.getFilter().toString());

    assertTrue(f2.getLast() instanceof TransactionFilter);
    assertEquals("TRANS(Insert,null,null,Transactor(null,=))", f2.getLast().toString());
  }

  /**
   * String ticket262g = "COUNT FILE WHICH IS NOT REFERENCED BY entity AND WHICH WAS created by me"
   * ;
   */
  @Test
  public void testTicket262g() {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.ticket262g));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    System.out.println(sfq.toStringTree(parser));

    assertTrue(sfq.filter instanceof Conjunction);
    final LinkedList<EntityFilterInterface> f2 = ((Conjunction) sfq.filter).getFilters();

    assertTrue(f2.getFirst() instanceof Negation);
    final Negation n = (Negation) f2.getFirst();
    assertEquals("@(entity,null)", n.getFilter().toString());

    assertTrue(f2.getLast() instanceof TransactionFilter);
    assertEquals("TRANS(Insert,null,null,Transactor(null,=))", f2.getLast().toString());
  }

  /** String ticket262h = "COUNT FILE WHICH IS NOT REFERENCED BY entity WHICH WAS created by me"; */
  @Test
  public void testTicket262h() {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.ticket262h));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    System.out.println(sfq.toStringTree(parser));

    assertTrue(sfq.filter instanceof Negation);
    final EntityFilterInterface backref = ((Negation) sfq.filter).getFilter();

    assertTrue(backref instanceof Backreference);
    assertEquals("@(entity,null)", backref.toString());

    assertNotNull(((Backreference) backref).getSubProperty());
    assertEquals(
        "TRANS(Insert,null,null,Transactor(null,=))",
        ((Backreference) backref).getSubProperty().getFilter().toString());
  }

  /**
   * String ticket262i = "COUNT FILE WHICH IS NOT REFERENCED BY AN entity WHICH WAS created by me" ;
   */
  @Test
  public void testTicket262i() {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.ticket262i));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    System.out.println(sfq.toStringTree(parser));
    assertEquals(4, sfq.getChildCount());
    assertEquals(
        "WHICH IS NOT REFERENCED BY AN entity WHICH WAS created by me", sfq.getChild(2).getText());

    ParseTree filter = sfq.getChild(2).getChild(1);
    assertEquals(1, filter.getChildCount());
    ParseTree negation = filter.getChild(0);
    assertEquals(2, negation.getChildCount());
    assertEquals("NOT ", negation.getChild(0).getText());
    assertEquals("REFERENCED BY AN entity WHICH WAS created by me", negation.getChild(1).getText());
    ParseTree refereced = negation.getChild(1).getChild(0);
    assertEquals("REFERENCED ", refereced.getChild(0).getText());
    assertEquals("BY ", refereced.getChild(1).getText());
    assertEquals("AN ", refereced.getChild(2).getText());
    assertEquals("entity ", refereced.getChild(3).getText());

    assertTrue(sfq.filter instanceof Negation);
    final EntityFilterInterface backref = ((Negation) sfq.filter).getFilter();

    assertTrue(backref instanceof Backreference);
    assertEquals("@(entity,null)", backref.toString());

    assertNotNull(((Backreference) backref).getSubProperty());
    assertEquals(
        "TRANS(Insert,null,null,Transactor(null,=))",
        ((Backreference) backref).getSubProperty().getFilter().toString());
  }

  /**
   * String ticket262j = "COUNT FILE WHICH IS REFERENCED BY A e1 AS A e2 AND IS STORED AT *config.p"
   * ;
   */
  @Test
  public void testTicket262j() {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.ticket262j));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    System.out.println(sfq.toStringTree(parser));

    assertTrue(sfq.filter instanceof Conjunction);
    final LinkedList<EntityFilterInterface> con = ((Conjunction) sfq.filter).getFilters();
    final Backreference backref = (Backreference) con.getFirst();
    final StoredAt storedat = (StoredAt) con.getLast();

    assertEquals("SAT(%config.p)", storedat.toString());
    assertEquals("@(e1,e2)", backref.toString());
  }

  /**
   * String ticket262k = "COUNT FILE WHICH IS STORED AT *config.p AND IS REFERENCED BY A e1 AS A e2"
   * ;
   */
  @Test
  public void testTicket262k() {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.ticket262k));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    System.out.println(sfq.toStringTree(parser));

    assertTrue(sfq.filter instanceof Conjunction);
    final LinkedList<EntityFilterInterface> con = ((Conjunction) sfq.filter).getFilters();
    final Backreference backref = (Backreference) con.getLast();
    final StoredAt storedat = (StoredAt) con.getFirst();

    assertEquals("SAT(%config.p)", storedat.toString());
    assertEquals("@(e1,e2)", backref.toString());
  }

  /**
   * String ticket262l = "COUNT FILE WHICH IS REFERENCED BY A e1 AS A e2 AND WHICH IS STORED AT
   * *config.p" ;
   */
  @Test
  public void testTicket262l() {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.ticket262l));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    System.out.println(sfq.toStringTree(parser));

    assertTrue(sfq.filter instanceof Conjunction);
    final LinkedList<EntityFilterInterface> con = ((Conjunction) sfq.filter).getFilters();
    final Backreference backref = (Backreference) con.getFirst();
    final StoredAt storedat = (StoredAt) con.getLast();

    assertEquals("SAT(%config.p)", storedat.toString());
    assertEquals("@(e1,e2)", backref.toString());
  }

  /**
   * String ticket262m = "COUNT FILE WHICH IS STORED AT *config.p AND WHICH IS REFERENCED BY A e1 AS
   * A e2" ;
   */
  @Test
  public void testTicket262m() {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.ticket262m));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    System.out.println(sfq.toStringTree(parser));

    assertTrue(sfq.filter instanceof Conjunction);
    final LinkedList<EntityFilterInterface> con = ((Conjunction) sfq.filter).getFilters();
    final Backreference backref = (Backreference) con.getLast();
    final StoredAt storedat = (StoredAt) con.getFirst();

    assertEquals("SAT(%config.p)", storedat.toString());
    assertEquals("@(e1,e2)", backref.toString());
  }

  /** String ticket228a = "COUNT *_*"; */
  @Test
  public void testTicket228a() {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.ticket228a));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    System.out.println(sfq.toStringTree(parser));

    assertEquals(Query.Pattern.TYPE_LIKE, sfq.e.type);
    assertEquals("%\\_%", sfq.e.toString());
  }

  /** String ticket228b = "COUNT '*.*'"; */
  @Test
  public void testTicket228b() {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.ticket228b));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    System.out.println(sfq.toStringTree(parser));

    assertEquals(Query.Pattern.TYPE_LIKE, sfq.e.type);
    assertEquals("%.%", sfq.e.toString());
  }

  /** String query54a = "SELECT field FROM RECORD ename"; */
  @Test
  public void testQuery54a() {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.query54a));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    System.out.println(tokens.getTokens());
    System.out.println(sfq.toStringTree(parser));

    assertEquals(Query.Pattern.TYPE_NORMAL, sfq.e.type);
    assertEquals("ename", sfq.e.toString());
    assertEquals(Query.Role.RECORD, sfq.r);
    assertEquals(Query.Type.SELECT, sfq.t);
    assertNotNull(sfq.s);
    assertFalse(sfq.s.isEmpty());
    assertEquals(1, sfq.s.size());
    assertEquals("field", sfq.s.get(0).toString());
  }

  /** String query54b = "SELECT field with spaces FROM RECORD ename"; */
  @Test
  public void testQuery54b() {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.query54b));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    System.out.println(tokens.getTokens());
    System.out.println(sfq.toStringTree(parser));

    assertEquals(Query.Pattern.TYPE_NORMAL, sfq.e.type);
    assertEquals("ename", sfq.e.toString());
    assertEquals(Query.Role.RECORD, sfq.r);
    assertEquals(Query.Type.SELECT, sfq.t);
    assertNotNull(sfq.s);
    assertFalse(sfq.s.isEmpty());
    assertEquals(1, sfq.s.size());
    assertEquals("field with spaces", sfq.s.get(0).toString());
  }

  /** String query54c = "SELECT field1, field2, field3 FROM RECORD ename"; */
  @Test
  public void testQuery54c() {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.query54c));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    System.out.println(tokens.getTokens());
    System.out.println(sfq.toStringTree(parser));

    assertEquals(Query.Pattern.TYPE_NORMAL, sfq.e.type);
    assertEquals("ename", sfq.e.toString());
    assertEquals(Query.Role.RECORD, sfq.r);
    assertEquals(Query.Type.SELECT, sfq.t);
    assertNotNull(sfq.s);
    assertFalse(sfq.s.isEmpty());
    assertEquals(3, sfq.s.size());
    assertEquals("field1", sfq.s.get(0).toString());
    assertEquals("field2", sfq.s.get(1).toString());
    assertEquals("field3", sfq.s.get(2).toString());
  }

  /**
   * String query54d = "SELECT field1.subfield1, field1.subfield2, field2.*, field3 FROM RECORD
   * ename" ;
   */
  @Test
  public void testQuery54d() {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.query54d));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    System.out.println(tokens.getTokens());
    System.out.println(sfq.toStringTree(parser));

    assertEquals(Query.Pattern.TYPE_NORMAL, sfq.e.type);
    assertEquals("ename", sfq.e.toString());
    assertEquals(Query.Role.RECORD, sfq.r);
    assertEquals(Query.Type.SELECT, sfq.t);
    assertNotNull(sfq.s);
    assertFalse(sfq.s.isEmpty());
    assertEquals(4, sfq.s.size());
    assertEquals("field1.subfield1", sfq.s.get(0).toString());
    assertEquals("field1.subfield2", sfq.s.get(1).toString());
    assertEquals("field2.*", sfq.s.get(2).toString());
    assertEquals("field3", sfq.s.get(3).toString());
  }

  /** String query55a = "FIND ename WHICH IS STORED AT /dir/with/date/2016-05-15"; */
  @Test
  public void testQuery55a() {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.query55a));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    // 4 children: FIND, role, WHICHCLAUSE, EOF
    assertEquals(4, sfq.getChildCount());
    assertEquals("FIND ", sfq.getChild(0).getText());
    assertEquals("FILE ", sfq.getChild(1).getText());
    assertEquals("WHICH IS STORED AT /dir/with/date/2016-05-15", sfq.getChild(2).getText());
    assertEquals("FILE", sfq.r.toString());
    assertNull(sfq.e);
    assertEquals("StoredAt", sfq.filter.getClass().getSimpleName());

    final ParseTree whichclause = sfq.getChild(2);
    // 2 children: WHICH , POV
    assertEquals(2, whichclause.getChildCount());
    assertEquals("WHICH ", whichclause.getChild(0).getText());
    assertEquals("IS STORED AT /dir/with/date/2016-05-15", whichclause.getChild(1).getText());

    final ParseTree transactionFilter = whichclause.getChild(1).getChild(0);
    assertEquals(2, transactionFilter.getChildCount());
    assertEquals("IS STORED AT ", transactionFilter.getChild(0).getText());
    assertEquals("/dir/with/date/2016-05-15", transactionFilter.getChild(1).getText());

    assertTrue(sfq.filter instanceof StoredAt);
    final StoredAt storedAt = (StoredAt) sfq.filter;
    assertEquals("SAT(/dir/with/date/2016-05-15)", storedAt.toString());
  }

  /** String query55b = "FIND ename WHICH IS STORED AT /dir/with/date/2016-05-15/**"; */
  @Test
  public void testQuery55b() {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.query55b));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    // 4 children: FIND, role, WHICHCLAUSE, EOF
    assertEquals(4, sfq.getChildCount());
    assertEquals("FIND ", sfq.getChild(0).getText());
    assertEquals("FILE ", sfq.getChild(1).getText());
    assertEquals("WHICH IS STORED AT /dir/with/date/2016-05-15/**", sfq.getChild(2).getText());
    assertEquals("FILE", sfq.r.toString());
    assertNull(sfq.e);
    assertEquals("StoredAt", sfq.filter.getClass().getSimpleName());

    final ParseTree whichclause = sfq.getChild(2);
    // 2 children: WHICH , POV
    assertEquals(2, whichclause.getChildCount());
    assertEquals("WHICH ", whichclause.getChild(0).getText());
    assertEquals("IS STORED AT /dir/with/date/2016-05-15/**", whichclause.getChild(1).getText());

    final ParseTree transactionFilter = whichclause.getChild(1).getChild(0);
    assertEquals(2, transactionFilter.getChildCount());
    assertEquals("IS STORED AT ", transactionFilter.getChild(0).getText());
    assertEquals("/dir/with/date/2016-05-15/**", transactionFilter.getChild(1).getText());

    assertTrue(sfq.filter instanceof StoredAt);
    final StoredAt storedAt = (StoredAt) sfq.filter;
    assertEquals("SAT(dir/with/date/2016-05-15/%)", storedAt.toString());
  }

  /** String query56a = "FIND RECORD WHICH REFERENCES anna"; */
  @Test
  public void testQuery56a() {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.query56a));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    assertEquals(Query.Role.RECORD, sfq.r);
    assertNotNull(sfq.filter);
    assertEquals(POV.class.getName(), sfq.filter.getClass().getName());
    final EntityFilterInterface f = sfq.filter;

    assertNotNull(f);
    assertEquals("POV(null,->,anna)", f.toString());

    assertTrue(f instanceof POV);
  }

  /** String query56b = "FIND RECORD WHICH REFERENCES AN ename2"; */
  @Test
  public void testQuery56b() {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.query56b));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    assertEquals(Query.Role.RECORD, sfq.r);
    assertNotNull(sfq.filter);
    assertEquals(POV.class.getName(), sfq.filter.getClass().getName());
    final EntityFilterInterface f = sfq.filter;

    assertNotNull(f);
    assertEquals("POV(null,->,ename2)", f.toString());

    assertTrue(f instanceof POV);
  }

  /** String query56c = "FIND RECORD WHICH REFERENCES atom"; */
  @Test
  public void testQuery56c() {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.query56c));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    assertEquals(Query.Role.RECORD, sfq.r);
    assertNotNull(sfq.filter);
    assertEquals(POV.class.getName(), sfq.filter.getClass().getName());
    final EntityFilterInterface f = sfq.filter;

    assertNotNull(f);
    assertEquals("POV(null,->,atom)", f.toString());

    assertTrue(f instanceof POV);
  }

  /** String query56d = "FIND RECORD WHICH REFERENCES A tom"; */
  @Test
  public void testQuery56d() {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.query56d));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    assertEquals(Query.Role.RECORD, sfq.r);
    assertNotNull(sfq.filter);
    assertEquals(POV.class.getName(), sfq.filter.getClass().getName());
    final EntityFilterInterface f = sfq.filter;

    assertNotNull(f);
    assertEquals("POV(null,->,tom)", f.toString());

    assertTrue(f instanceof POV);
  }

  /** Test for several path strings if they would require pattern matching or not. */
  @Test
  public void testFilePath() {
    // These do not require complicated pattern.
    assertFalse(StoredAt.requiresPatternMatching(filepath_verb01));
    assertFalse(StoredAt.requiresPatternMatching(filepath_verb02));
    assertFalse(StoredAt.requiresPatternMatching(filepath_verb03));
    assertFalse(StoredAt.requiresPatternMatching(filepath_verb04));
    assertFalse(StoredAt.requiresPatternMatching(filepath_verb05));
    assertFalse(StoredAt.requiresPatternMatching(filepath_verb06));
    assertFalse(StoredAt.requiresPatternMatching(filepath_verb07));

    // These need special handling in order to be used in LIKE queries.
    assertTrue(StoredAt.requiresPatternMatching(filepath_pat01));
    assertTrue(StoredAt.requiresPatternMatching(filepath_pat02));
    assertTrue(StoredAt.requiresPatternMatching(filepath_pat03));
    assertTrue(StoredAt.requiresPatternMatching(filepath_pat04));
  }

  /** String referenceByLikePattern = "FIND ENTITY WHICH IS REFERENCED BY *name*"; */
  @Test
  public void testReferencedByLike() {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.referenceByLikePattern));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    assertEquals(Query.Role.ENTITY, sfq.r);
    assertNotNull(sfq.filter);
    assertEquals(Backreference.class.getName(), sfq.filter.getClass().getName());
    final EntityFilterInterface f = sfq.filter;

    assertNotNull(f);
    assertEquals("@(%name%,null)", f.toString());

    assertTrue(f instanceof Backreference);
    assertEquals("%name%", ((Backreference) f).getEntity());
  }

  @Test
  public void testFilePathInQuotes() {
    CQLLexer lexer = new CQLLexer(CharStreams.fromString(query_filepath_quotes));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    // 4 children: FIND, role, WHICHCLAUSE, EOF
    assertEquals(4, sfq.getChildCount());
    assertEquals("FIND ", sfq.getChild(0).getText());
    assertEquals("FILE ", sfq.getChild(1).getText());
    assertEquals(
        "WHICH IS STORED AT '/SimulationData/2016_single/2018-01-10/**'",
        sfq.getChild(2).getText());
    assertEquals("FILE", sfq.r.toString());
    assertNull(sfq.e);
    assertEquals("StoredAt", sfq.filter.getClass().getSimpleName());

    final ParseTree whichclause = sfq.getChild(2);
    // 2 children: WHICH , POV
    assertEquals(2, whichclause.getChildCount());
    assertEquals("WHICH ", whichclause.getChild(0).getText());
    assertEquals(
        "IS STORED AT '/SimulationData/2016_single/2018-01-10/**'",
        whichclause.getChild(1).getText());

    final ParseTree satFilter = whichclause.getChild(1).getChild(0);
    assertEquals(2, satFilter.getChildCount());
    assertEquals("IS STORED AT ", satFilter.getChild(0).getText());
    assertEquals("'/SimulationData/2016_single/2018-01-10/**'", satFilter.getChild(1).getText());

    assertTrue(sfq.filter instanceof StoredAt);
    final StoredAt storedAt = (StoredAt) sfq.filter;
    assertTrue(storedAt.pattern_matching);
    assertEquals("SAT(SimulationData/2016\\_single/2018-01-10/%)", storedAt.toString());
  }

  @Test
  public void testFilePathInQuotes2() {
    CQLLexer lexer = new CQLLexer(CharStreams.fromString(query_filepath_quotes_2));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    // 4 children: FIND, role, WHICHCLAUSE, EOF
    assertEquals(4, sfq.getChildCount());
    assertEquals("FIND ", sfq.getChild(0).getText());
    assertEquals("FILE ", sfq.getChild(1).getText());
    assertEquals(
        "WHICH IS STORED AT /SimulationData/2016_single/2018-01-10/**", sfq.getChild(2).getText());
    assertEquals("FILE", sfq.r.toString());
    assertNull(sfq.e);
    assertEquals("StoredAt", sfq.filter.getClass().getSimpleName());

    final ParseTree whichclause = sfq.getChild(2);
    // 2 children: WHICH , POV
    assertEquals(2, whichclause.getChildCount());
    assertEquals("WHICH ", whichclause.getChild(0).getText());
    assertEquals(
        "IS STORED AT /SimulationData/2016_single/2018-01-10/**",
        whichclause.getChild(1).getText());

    final ParseTree satFilter = whichclause.getChild(1).getChild(0);
    assertEquals(2, satFilter.getChildCount());
    assertEquals("IS STORED AT ", satFilter.getChild(0).getText());
    assertEquals("/SimulationData/2016_single/2018-01-10/**", satFilter.getChild(1).getText());

    assertTrue(sfq.filter instanceof StoredAt);
    final StoredAt storedAt = (StoredAt) sfq.filter;
    assertTrue(storedAt.pattern_matching);
    assertEquals("SAT(SimulationData/2016\\_single/2018-01-10/%)", storedAt.toString());
  }

  @Test
  public void testEmptyTextValue() {
    CQLLexer lexer = new CQLLexer(CharStreams.fromString(emptyTextValue));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    EntityFilterInterface pov = sfq.filter;
    assertEquals("POV(prop,=,)", pov.toString());
  }

  /** String queryIssue31 = "FIND FILE WHICH IS STORED AT /data/in0.foo"; */
  @Test
  public void testIssue31() {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.queryIssue31));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    System.out.println(sfq.toStringTree(parser));

    // 4 children: FIND, role, WHICHCLAUSE, EOF
    assertEquals(4, sfq.getChildCount());
    assertEquals("WHICH IS STORED AT /data/in0.foo", sfq.getChild(2).getText());
    assertEquals("FILE", sfq.r.toString());
    assertNull(sfq.e);
    assertEquals("StoredAt", sfq.filter.getClass().getSimpleName());
    final ParseTree whichclause = sfq.getChild(2);
    final ParseTree transactionFilter = whichclause.getChild(1).getChild(0);
    assertEquals("/data/in0.foo", transactionFilter.getChild(1).getText());
    final StoredAt storedAt = (StoredAt) sfq.filter;
    assertEquals("SAT(/data/in0.foo)", storedAt.toString());
  }

  /**
   * Testing a conjunction which begins with a nested disjunction. The bug was a parsing error which
   * was caused by a missing option for a disjunction/conjunction, wrapped into parenthesis as first
   * element of a conjunction/disjunction
   *
   * <p>String queryMR56 = "FIND ENTITY WITH ((p0 = v0 OR p1=v1) AND p2=v2)";
   */
  @Test
  public void testMR56() {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.queryMR56));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    System.out.println(sfq.toStringTree(parser));

    // 4 children: FIND, role, WHICHCLAUSE, EOF
    assertEquals(4, sfq.getChildCount());
    assertEquals("WITH ((p0 = v0 OR p1=v1) AND p2=v2)", sfq.getChild(2).getText());
    assertEquals("ENTITY", sfq.r.toString());
    assertEquals("Conjunction", sfq.filter.getClass().getSimpleName());
    final ParseTree whichclause = sfq.getChild(2);
    final ParseTree conjunction = whichclause.getChild(2);
    assertEquals("(p0 = v0 OR p1=v1) AND p2=v2", conjunction.getText());
    final ParseTree disjunction = conjunction.getChild(1);
    assertEquals("p0 = v0 OR p1=v1", disjunction.getText());
    final ParseTree pov = conjunction.getChild(4);
    assertEquals("p2=v2", pov.getText());
  }

  /** String versionedQuery1 = "FIND ANY VERSION OF ENTITY e1"; */
  @Test
  public void testVersionedQuery1() {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.versionedQuery1));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    System.out.println(sfq.toStringTree(parser));

    // 5 children: FIND, version, role, entity,  EOF
    assertEquals(5, sfq.getChildCount());
    assertEquals(VersionFilter.ANY_VERSION, sfq.v);
    assertEquals(Query.Role.ENTITY, sfq.r);
    assertEquals("e1", sfq.e.toString());
  }

  /** String query57a = "FIND ENTITY"; */
  @Test
  public void testQuery57a() {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.query57a));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    System.out.println(sfq.toStringTree(parser));

    // 3 children: FIND, ENTITY, EOF
    assertEquals(3, sfq.getChildCount());
    assertEquals(Query.Role.ENTITY, sfq.r);
    assertNull(sfq.e);
  }

  /** String query57b = "FIND ENTITY WITH ID"; */
  @Test
  public void testQuery57b() {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.query57b));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    System.out.println(sfq.toStringTree(parser));

    // 4 children: FIND, ENTITY, WHICHCLAUSE, EOF
    assertEquals(4, sfq.getChildCount());
    assertEquals(Query.Role.ENTITY, sfq.r);
    assertNull(sfq.e);
    assertEquals("WITH ID", sfq.getChild(2).getText());
  }

  /** String query57c = "FIND ENTITY WITH ID = 123"; */
  @Test
  public void testQuery57c() {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.query57c));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    System.out.println(sfq.toStringTree(parser));

    // 4 children: FIND, ENTITY, WHICHCLAUSE, EOF
    assertEquals(4, sfq.getChildCount());
    assertEquals(Query.Role.ENTITY, sfq.r);
    assertNull(sfq.e);
    assertEquals("WITH ID = 123", sfq.getChild(2).getText());
  }

  /** String queryIssue116 = "FIND *"; */
  @Test
  public void testIssue116() {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.queryIssue116));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    System.out.println(sfq.toStringTree(parser));

    // 3 children: FIND, ENTITY, EOF
    assertEquals(3, sfq.getChildCount());
    assertNull(sfq.r);
    assertTrue(sfq.e.type == Query.Pattern.TYPE_LIKE);
    assertEquals("%", sfq.e.toString());
  }

  /** String query54e = "SELECT id FROM ename"; */
  @Test
  public void testQuery54e() {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.query54e));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    System.out.println(sfq.toStringTree(parser));

    assertEquals(5, sfq.getChildCount());
    assertEquals("SELECT ", sfq.getChild(0).getText());
    assertEquals("id ", sfq.getChild(1).getText());
    assertEquals("FROM ", sfq.getChild(2).getText());
    assertEquals("ename", sfq.getChild(3).getText());
    assertEquals(Query.Pattern.TYPE_NORMAL, sfq.e.type);
    assertEquals("ename", sfq.e.toString());
    assertNull(sfq.r);
    assertEquals(Query.Type.SELECT, sfq.t);
    assertNotNull(sfq.s);
    assertFalse(sfq.s.isEmpty());
    assertEquals(1, sfq.s.size());
    assertEquals("id", sfq.s.get(0).toString());
  }

  /** String query58a = "FIND ENTITY WITH endswith"; */
  @Test
  public void testQuery58a() {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.query58a));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    System.out.println(sfq.toStringTree(parser));

    assertEquals("POV(endswith,null,null)", sfq.filter.toString());
  }

  /** String query58b = "FIND ENTITY WITH endswith = val1"; */
  @Test
  public void testQuery58b() {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.query58b));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    System.out.println(sfq.toStringTree(parser));

    assertEquals("POV(endswith,=,val1)", sfq.filter.toString());
  }

  /** String query58c = "FIND ENTITY WITH 0with = val1"; */
  @Test
  public void testQuery58c() {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.query58c));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    System.out.println(sfq.toStringTree(parser));

    assertNull(((POV) sfq.filter).getSubProperty());
    assertEquals("POV(0with,=,val1)", sfq.filter.toString());
  }

  /** String query58d = "FIND ENTITY WITH WITH"; */
  @Test
  public void testQuery58d() {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.query58d));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    System.out.println(sfq.toStringTree(parser));

    assertEquals("POV(withdrawn,=,TRUE)", sfq.filter.toString());
    assertNull(((POV) sfq.filter).getSubProperty());
  }

  /** String query58e = "FIND ENTITY WITH pname=with"; */
  @Test
  public void testQuery58e() {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.query58e));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    System.out.println(sfq.toStringTree(parser));

    assertEquals("POV(pname,=,with)", sfq.filter.toString());
    assertNull(((POV) sfq.filter).getSubProperty());
  }

  @Test
  /** String queryIssue132a = "FIND ENTITY WHICH HAS BEEN INSERTED AFTER TODAY"; */
  public void testIssue132a() {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.queryIssue132a));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    System.out.println(sfq.toStringTree(parser));

    assertEquals("TRANS(Insert,>,Today,null)", sfq.filter.toString());
  }

  @Test
  /** String queryIssue132b = "FIND ENTITY WHICH HAS BEEN CREATED TODAY BY ME"; */
  public void testIssue132b() {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.queryIssue132b));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    System.out.println(sfq.toStringTree(parser));

    assertEquals("TRANS(Insert,(,Today,Transactor(null,=))", sfq.filter.toString());
  }

  /**
   * Multiple white space chars after `FROM`.
   *
   * <p>String queryIssue134 = "SELECT pname FROM ename";
   */
  @Test
  public void testIssue134() {
    // must not throw ParsingException
    new Query(this.queryIssue134).parse();
  }

  /**
   * Space before special character unit
   *
   * <p>String queryIssue131= "FIND ENTITY WITH pname = 13 €";
   */
  @Test
  public void testIssue131() {
    // must not throw ParsingException
    new Query(this.queryIssue131).parse();
  }

  /** String issue131a = "FIND ename WITH pname1.x AND pname2"; */
  @Test
  public void testIssue131a() {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.issue131a));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    System.out.println(sfq.toStringTree(parser));

    assertTrue(sfq.filter instanceof Conjunction);
    LinkedList<EntityFilterInterface> filters = ((Conjunction) sfq.filter).getFilters();
    assertEquals(filters.size(), 2);
    assertTrue(filters.get(0) instanceof POV);
    assertTrue(filters.get(1) instanceof POV);
    POV pov1 = ((POV) filters.get(0));
    POV pov2 = ((POV) filters.get(1));
    assertEquals("POV(pname1,null,null)", pov1.toString());
    assertEquals("POV(pname2,null,null)", pov2.toString());
    assertTrue(pov1.hasSubProperty());
    assertFalse(pov2.hasSubProperty());
    assertEquals("POV(x,null,null)", pov1.getSubProperty().getFilter().toString());
  }

  /** String issue131b = "FIND ename WITH (pname1.x < 10) AND (pname1.x)"; */
  @Test
  public void testIssue131b() {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.issue131b));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    System.out.println(sfq.toStringTree(parser));

    assertTrue(sfq.filter instanceof Conjunction);
    LinkedList<EntityFilterInterface> filters = ((Conjunction) sfq.filter).getFilters();
    assertEquals(filters.size(), 2);
    assertTrue(filters.get(0) instanceof POV);
    assertTrue(filters.get(1) instanceof POV);
    POV pov1 = ((POV) filters.get(0));
    POV pov2 = ((POV) filters.get(1));
    assertEquals("POV(pname1,null,null)", pov1.toString());
    assertEquals("POV(pname1,null,null)", pov2.toString());
    assertTrue(pov1.hasSubProperty());
    assertTrue(pov2.hasSubProperty());
    assertEquals("POV(x,<,10)", pov1.getSubProperty().getFilter().toString());
    assertEquals("POV(x,null,null)", pov2.getSubProperty().getFilter().toString());
  }

  /** String issue131c = "FIND ename WITH pname2 AND pname1.x "; */
  @Test
  public void testIssue131c() {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.issue131c));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    System.out.println(sfq.toStringTree(parser));

    assertTrue(sfq.filter instanceof Conjunction);
    LinkedList<EntityFilterInterface> filters = ((Conjunction) sfq.filter).getFilters();
    assertEquals(filters.size(), 2);
    assertTrue(filters.get(0) instanceof POV);
    assertTrue(filters.get(1) instanceof POV);
    POV pov1 = ((POV) filters.get(0));
    POV pov2 = ((POV) filters.get(1));
    assertEquals("POV(pname2,null,null)", pov1.toString());
    assertEquals("POV(pname1,null,null)", pov2.toString());
    assertFalse(pov1.hasSubProperty());
    assertTrue(pov2.hasSubProperty());
    assertEquals("POV(x,null,null)", pov2.getSubProperty().getFilter().toString());
  }

  /** String issue131d = "FIND ename WITH (pname1.x) AND pname2"; */
  @Test
  public void testIssue131d() {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.issue131d));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    System.out.println(sfq.toStringTree(parser));

    assertTrue(sfq.filter instanceof Conjunction);
    LinkedList<EntityFilterInterface> filters = ((Conjunction) sfq.filter).getFilters();
    assertEquals(filters.size(), 2);
    assertTrue(filters.get(0) instanceof POV);
    assertTrue(filters.get(1) instanceof POV);
    POV pov1 = ((POV) filters.get(0));
    POV pov2 = ((POV) filters.get(1));
    assertEquals("POV(pname1,null,null)", pov1.toString());
    assertEquals("POV(pname2,null,null)", pov2.toString());
    assertTrue(pov1.hasSubProperty());
    assertFalse(pov2.hasSubProperty());
    assertEquals("POV(x,null,null)", pov1.getSubProperty().getFilter().toString());
  }

  /** String issue131e = "FIND ename WITH (pname1.pname2 > 30) AND (pname1.pname2 < 40)"; */
  @Test
  public void testIssue131e() {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.issue131e));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    System.out.println(sfq.toStringTree(parser));

    assertTrue(sfq.filter instanceof Conjunction);
    LinkedList<EntityFilterInterface> filters = ((Conjunction) sfq.filter).getFilters();
    assertEquals(filters.size(), 2);
    assertTrue(filters.get(0) instanceof POV);
    assertTrue(filters.get(1) instanceof POV);
    POV pov1 = ((POV) filters.get(0));
    POV pov2 = ((POV) filters.get(1));
    assertEquals("POV(pname1,null,null)", pov1.toString());
    assertEquals("POV(pname1,null,null)", pov2.toString());
    assertTrue(pov1.hasSubProperty());
    assertTrue(pov2.hasSubProperty());
    assertEquals("POV(pname2,>,30)", pov1.getSubProperty().getFilter().toString());
    assertEquals("POV(pname2,<,40)", pov2.getSubProperty().getFilter().toString());
  }

  /** String issue131f = "FIND ename WITH (pname1.pname2 > 30) AND pname1.pname2 < 40"; */
  @Test
  public void testIssue131f() {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.issue131f));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    System.out.println(sfq.toStringTree(parser));

    assertTrue(sfq.filter instanceof Conjunction);
    LinkedList<EntityFilterInterface> filters = ((Conjunction) sfq.filter).getFilters();
    assertEquals(filters.size(), 2);
    assertTrue(filters.get(0) instanceof POV);
    assertTrue(filters.get(1) instanceof POV);
    POV pov1 = ((POV) filters.get(0));
    POV pov2 = ((POV) filters.get(1));
    assertEquals("POV(pname1,null,null)", pov1.toString());
    assertEquals("POV(pname1,null,null)", pov2.toString());
    assertTrue(pov1.hasSubProperty());
    assertTrue(pov2.hasSubProperty());
    assertEquals("POV(pname2,>,30)", pov1.getSubProperty().getFilter().toString());
    assertEquals("POV(pname2,<,40)", pov2.getSubProperty().getFilter().toString());
  }

  /**
   * Integer values which are too large for Int32
   *
   * <p>String queryIssue145= "FIND ENTITY WITH pname145 = 10000000000";
   */
  @Test
  public void testIssue145() {
    // must yield a valid value
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.queryIssue145));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    System.out.println(sfq.toStringTree(parser));
    assertEquals("POV(pname145,=,10000000000)", sfq.filter.toString());

    // assert value
    POV pov = ((POV) sfq.filter);
    System.out.println(pov.getValue());
    assertEquals("10000000000", pov.getValue());
    assertNotNull(pov.getVDouble());
    assertNull(pov.getVInt());
    assertEquals(1e10, pov.getVDouble().doubleValue(), 0.0);
  }

  /** Spaces and escaped dots in selects */
  @Test
  public void testIssue130a() {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.issue130a));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    System.out.println(sfq.toStringTree(parser));

    assertNotNull(sfq.s);
    assertFalse(sfq.s.isEmpty());
    assertEquals(4, sfq.s.size());
    assertEquals("name with spaces.and dot", sfq.s.get(0).toString());
    assertEquals("name with spaces.and dot", sfq.s.get(0).getSelector());
    assertNull(sfq.s.get(0).getSubselection());
    assertEquals("name with spaces.name", sfq.s.get(1).toString());
    assertEquals("name with spaces", sfq.s.get(1).getSelector());
    assertEquals("name", sfq.s.get(1).getSubselection().toString());
    assertEquals("name with spaces.name", sfq.s.get(2).toString());
    assertEquals("name with spaces", sfq.s.get(2).getSelector());
    assertEquals("name", sfq.s.get(2).getSubselection().toString());
    assertEquals("name with,comma and.dot and 'single_quote.sub", sfq.s.get(3).toString());
    assertEquals("name with,comma and.dot and 'single_quote", sfq.s.get(3).getSelector());
    assertEquals("sub", sfq.s.get(3).getSubselection().toString());
  }

  /** Single quotes around the selector */
  @Test
  public void testIssue130b() {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.issue130b));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    System.out.println(sfq.toStringTree(parser));

    assertNotNull(sfq.s);
    assertFalse(sfq.s.isEmpty());
    assertEquals(1, sfq.s.size());
    assertEquals("Wrapper", sfq.s.get(0).toString());
    assertEquals("Wrapper", sfq.s.get(0).getSelector());
    assertNull(sfq.s.get(0).getSubselection());
  }

  @Test
  public void testQuotation1() {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.quotation1));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    assertTrue(sfq.filter instanceof Conjunction);
    Conjunction conj = (Conjunction) sfq.filter;
    assertEquals(6, conj.getFilters().size());
    assertEquals("POV(null,LIKE ,%with double%)", conj.getFilters().get(0).toString());
    assertEquals("POV(null,LIKE ,%and single%)", conj.getFilters().get(1).toString());
    assertEquals("POV(null,LIKE ,%what's wrong?%)", conj.getFilters().get(2).toString());
    assertEquals("POV(null,LIKE ,%'%)", conj.getFilters().get(3).toString());
    assertEquals("POV(null,LIKE ,%nothin'%)", conj.getFilters().get(4).toString());
    assertEquals("POV(null,LIKE ,%\"'bla%)", conj.getFilters().get(5).toString());
  }

  @ParameterizedTest
  @ValueSource(
      strings = {
        "1e+23",
        "1E+23",
        "5e22",
        "5E22",
        "2e-323",
        "2E-323",
        "-123",
        "-1e23",
        "3E15m^2",
        "-3e15m",
        "-3e15 1/s",
        "3e15 m^2",
        "+1",
        "+2.234",
        "+2.234e+23",
        "+3.324E-23"
      })
  public void testIssue144(String scientific_notation) {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.issue144 + scientific_notation));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    final CqContext sfq = parser.cq();

    for (final Token t : tokens.getTokens()) {
      System.out.println(t.toString());
    }

    System.out.println(sfq.toStringTree(parser));

    assertTrue(sfq.filter instanceof POV);
    POV pov = (POV) sfq.filter;
    assertEquals("POV(pname,=," + scientific_notation + ")", pov.toString());
    assertTrue(pov.getVDouble() != null || pov.getVInt() != null);
  }

  @ParameterizedTest
  @ValueSource(strings = {"- 123", "- 1e23", "2 e -23", "2E- 323", "+ 1"})
  public void testIssue144WhiteSpaceInNumber(String number) {
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.issue144 + number));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    assertThrowsExactly(Query.ParsingException.class, () -> parser.cq());
  }

  /**
   * Test that brackets around 'has pname' do not cause filter to become subproperty filter.
   *
   * <p>https://gitlab.com/caosdb/caosdb-server/-/issues/203
   */
  @Test
  public void testIssue203() {
    // setup 203a
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.issue203a));
    CommonTokenStream tokens = new CommonTokenStream(lexer);
    CQLParser parser = new CQLParser(tokens);
    CqContext sfq = parser.cq();

    // here we test that the filter stays a POV and is not falsly interpreted
    // as subproperty filter
    assertTrue(sfq.filter instanceof POV);

    // setup 203b (with conjunction)
    lexer = new CQLLexer(CharStreams.fromString(this.issue203b));
    tokens = new CommonTokenStream(lexer);
    parser = new CQLParser(tokens);
    sfq = parser.cq();
    Conjunction conj = (Conjunction) sfq.filter;

    // the outer filter should be conjuction
    assertTrue(sfq.filter instanceof Conjunction);
    // here we test that the filters stays a POV and is not falsly interpreted
    // as subproperty filters
    assertTrue(conj.getFilters().get(0) instanceof POV);
    assertTrue(conj.getFilters().get(1) instanceof POV);
  }

  @Test
  public void testDecimalNumber() {
    // This should always be DEC, WHITESPACE, AND  repeat
    final String text =
        "1.2123e+3 AND 1.21234E+3 AND 1.21234E-3 AND 1.21234E3 AND 16.0 AND 1.2 AND -1.2 AND +1.2 AND 1.2 AND - 1.2 AND + 1.2 AND 2e-323 AND 2E-323 AND 2E- 323 AND 2 e -323 AND + 1.2123e+323";
    CQLLexer lexer = new CQLLexer(CharStreams.fromString(text));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);
    Vocabulary vocab = lexer.getVocabulary();

    int no = 0;
    for (final Token t : tokens.getTokens()) {
      if (no % 3 == 0) {
        assertEquals(vocab.getSymbolicName(t.getType()), "DECIMAL_NUMBER");
      }
      if (no % 3 == 1) {
        if (vocab.getSymbolicName(t.getType()) != "EOF") {
          assertEquals(vocab.getSymbolicName(t.getType()), "WHITE_SPACE");
        }
      }
      if (no % 3 == 2) {
        assertEquals(vocab.getSymbolicName(t.getType()), "AND");
      }
      no = no + 1;
    }
  }
}
