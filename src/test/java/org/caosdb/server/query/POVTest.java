package org.caosdb.server.query;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.regex.Matcher;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

class POVTest {

  public static final String MAX_INT = "2147483647";
  public static final String MIN_INT = "-2147483648";
  public static final String MAX_DOUBLE = "1.7976931348623157E308";
  public static final String MIN_DOUBLE = "4.9E-324";

  @ParameterizedTest
  @ValueSource(strings = {"16", MAX_INT, MIN_INT, "0", "-0", "1", "- 1", "-1", "+1"})
  void testNumberPatternMatchInteger(String intValue) {

    Matcher matcher = POV.NUMBER_PATTERN.matcher(intValue);
    assertTrue(matcher.matches());
    assertEquals(intValue, matcher.group(1).toString());

    Integer.valueOf(intValue.replaceAll("\\s", ""));

    Matcher matcherWithUnit = POV.NUMBER_PATTERN.matcher(intValue + " m^2");
    assertTrue(matcherWithUnit.matches());
    assertEquals(intValue, matcherWithUnit.group(1).toString());
    assertEquals("m^2", matcherWithUnit.group(2).toString());

    Matcher matcherWithStrangeUnit = POV.NUMBER_PATTERN.matcher(intValue + " e");
    assertTrue(matcherWithStrangeUnit.matches());
    assertEquals(intValue, matcherWithStrangeUnit.group(1).toString());
    assertEquals("e", matcherWithStrangeUnit.group(2).toString());
  }

  @ParameterizedTest
  @ValueSource(
      strings = {
        "1.2123e+3",
        "1.21234E+3",
        "5.213e2",
        "5.2234E2",
        "16.0",
        MAX_DOUBLE,
        MIN_DOUBLE,
        "0.0",
        "-0.0",
        "1.2",
        "- 1.2",
        "-1.2",
        "2e-323",
        "2E-323",
        "2E- 323",
        "2 e -323",
        "+ 2.2132e+23"
      })
  void testNumberPatternMatchDouble(String doubleValue) {

    Matcher matcher = POV.NUMBER_PATTERN.matcher(doubleValue);
    assertTrue(matcher.matches());
    assertEquals(doubleValue, matcher.group(1).toString());

    Double.valueOf(doubleValue.replaceAll("\\s", ""));

    Matcher matcherWithUnit = POV.NUMBER_PATTERN.matcher(doubleValue + " m^2");
    assertTrue(matcherWithUnit.matches());
    assertEquals(doubleValue, matcherWithUnit.group(1).toString());
    assertEquals("m^2", matcherWithUnit.group(2).toString());

    Matcher matcherWithStrangeUnit = POV.NUMBER_PATTERN.matcher(doubleValue + " e");
    assertTrue(matcherWithStrangeUnit.matches());
    assertEquals(doubleValue, matcherWithStrangeUnit.group(1).toString());
    assertEquals("e", matcherWithStrangeUnit.group(2).toString());
  }
}
