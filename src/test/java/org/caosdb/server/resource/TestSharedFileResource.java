/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2019 IndiScale GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */

package org.caosdb.server.resource;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.concurrent.ConcurrentMap;
import net.jcip.annotations.NotThreadSafe;
import org.apache.shiro.mgt.DefaultSecurityManager;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.subject.support.DelegatingSubject;
import org.caosdb.server.CaosDBServer;
import org.caosdb.server.FileSystem;
import org.caosdb.server.ServerProperties;
import org.caosdb.server.accessControl.AnonymousAuthenticationToken;
import org.caosdb.server.accessControl.AnonymousRealm;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;
import org.restlet.Request;
import org.restlet.Response;
import org.restlet.data.Disposition;
import org.restlet.data.MediaType;
import org.restlet.data.Method;
import org.restlet.data.Reference;
import org.restlet.representation.FileRepresentation;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;

@NotThreadSafe
public class TestSharedFileResource {

  static String illegalTopLevelString = "22334455-HGFEDCBA";
  static String illegalFolderString = "11001100-00110011";
  static String legalFolderString = "22334455-ABCDEFGH";
  static String legalFileString = "somefile.dat";
  static String legalFileStringComplete = (new File(legalFolderString, legalFileString)).toString();
  static String fileInIllegalFolderStringComplete =
      (new File(illegalFolderString, legalFileString)).toString();

  static File illegalTopLevelFile;
  static File illegalFolder;
  static File fileInIllegalFolder;
  static File legalFolder;
  static File legalFile;

  @BeforeAll
  public static void setup() throws Exception {
    CaosDBServer.initServerProperties();
    String tmpStr = FileSystem.getTmp();

    illegalTopLevelFile = new File(tmpStr, illegalTopLevelString);
    illegalTopLevelFile.createNewFile();
    illegalFolder = new File(tmpStr, illegalFolderString);
    illegalFolder.mkdir();
    fileInIllegalFolder = new File(illegalFolder, legalFileString);
    fileInIllegalFolder.createNewFile();

    legalFolder = new File(tmpStr, legalFolderString);
    legalFolder.mkdir();

    legalFile = new File(legalFolder, legalFileString);
    legalFile.createNewFile();
  }

  // Some tests for illegal requests.
  @Test
  public void testValidity() throws Exception {
    SharedFileResource res = new SharedFileResource();
    assertNull(res.getFile(illegalTopLevelString));
    assertNull(res.getFile(illegalFolderString));
    assertNull(res.getFile(illegalFolderString));
    assertNull(res.getFile(fileInIllegalFolderStringComplete));
    assertNotNull(res.getFile(legalFileStringComplete));
  }

  // Trying to obtain files via `path` attribute.
  @Test
  public void testFileUrl(@TempDir Path tmpDir) throws Exception {

    provideUserSourcesFile(tmpDir);
    final Subject user = new DelegatingSubject(new DefaultSecurityManager(new AnonymousRealm()));
    CaosDBServer.setProperty(ServerProperties.KEY_AUTH_OPTIONAL, "true");
    user.login(AnonymousAuthenticationToken.getInstance());
    SharedFileResource resource =
        new SharedFileResource() {

          @Override
          public String getSRID() {
            return "TEST-SRID";
          }

          @Override
          public String getCRID() {
            return "TEST-CRID";
          }

          @Override
          public Long getTimestamp() {
            return 0L;
          }

          @Override
          public Reference getRootRef() {
            return new Reference("https://example.com/root/");
          }

          @Override
          public Reference getReference() {
            return new Reference("https://example.com/");
          }

          @Override
          public Subject getUser() {
            // TODO Auto-generated method stub
            return user;
          }
        };

    Representation entity = new StringRepresentation("lalala");
    entity.setMediaType(MediaType.TEXT_ALL);
    Request req = new Request(Method.GET, "../Shared/", entity);
    ConcurrentMap<String, Object> attrs = req.getAttributes();
    attrs.put("path", legalFileStringComplete);
    req.setAttributes(attrs);
    resource.init(null, req, new Response(null));
    Representation repr = resource.handle();
    FileRepresentation frep;
    try {
      frep = (FileRepresentation) repr;
    } catch (Exception e) {
      fail("Rsssource did not produce a FileRepresentation.");
      // This line won't be reached, but is necessary for the compiler.
      frep = (FileRepresentation) repr;
    }
    assertTrue(frep.getFile().toString().endsWith(legalFileStringComplete));
    assertEquals(Disposition.TYPE_ATTACHMENT, frep.getDisposition().getType());
  }

  /** Creates a dummy usersources.ini and injects it into the server properties. */
  private void provideUserSourcesFile(Path tempFolder) throws IOException {
    File usersourcesini = tempFolder.resolve("usersources.ini").toFile();
    usersourcesini.createNewFile();
    String usersourcesFileName = usersourcesini.getAbsolutePath();
    String usersourcesContent =
        "realms = PAM\n"
            + "defaultRealm = PAM\n"
            + "\n"
            + "[PAM]\n"
            + "class = org.caosdb.server.accessControl.Pam\n"
            + "default_status = ACTIVE\n"
            + "include.user = admin\n"
            + ";include.group = [uncomment and put your groups here]\n"
            + ";exclude.user = [uncomment and put excluded users here]\n"
            + ";exclude.group = [uncomment and put excluded groups here]\n"
            + "\n"
            + ";it is necessary to add at least one admin\n"
            + "user.admin.roles = administration";

    Files.write(Paths.get(usersourcesFileName), usersourcesContent.getBytes());
    CaosDBServer.setProperty(ServerProperties.KEY_USER_SOURCES_INI_FILE, usersourcesFileName);
  }
}
