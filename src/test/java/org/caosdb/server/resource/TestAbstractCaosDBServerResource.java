package org.caosdb.server.resource;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.HashSet;
import org.apache.shiro.mgt.DefaultSecurityManager;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.subject.support.DelegatingSubject;
import org.caosdb.server.CaosDBException;
import org.caosdb.server.CaosDBServer;
import org.caosdb.server.ServerProperties;
import org.caosdb.server.accessControl.AnonymousAuthenticationToken;
import org.caosdb.server.accessControl.AnonymousRealm;
import org.caosdb.server.accessControl.Role;
import org.caosdb.server.database.BackendTransaction;
import org.caosdb.server.database.access.Access;
import org.caosdb.server.database.backend.implementation.MySQL.ConnectionException;
import org.caosdb.server.database.backend.interfaces.RetrievePermissionRulesImpl;
import org.caosdb.server.database.backend.interfaces.RetrieveRoleImpl;
import org.caosdb.server.database.exceptions.TransactionException;
import org.caosdb.server.database.misc.TransactionBenchmark;
import org.caosdb.server.permissions.PermissionRule;
import org.caosdb.server.utils.WebinterfaceUtils;
import org.jdom2.Element;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;
import org.restlet.data.Reference;
import org.restlet.representation.Representation;

public class TestAbstractCaosDBServerResource {

  /** a no-op mock-up which resolved all rules to an empty set of permissions. */
  public static class RetrievePermissionRulesMockup implements RetrievePermissionRulesImpl {

    public RetrievePermissionRulesMockup(Access a) {}

    @Override
    public void setTransactionBenchmark(TransactionBenchmark b) {}

    @Override
    public TransactionBenchmark getBenchmark() {
      return null;
    }

    @Override
    public HashSet<PermissionRule> retrievePermissionRule(String role) throws TransactionException {
      return new HashSet<>();
    }
  }

  /** a no-op mock-up which returns null */
  public static class RetrieveRoleMockup implements RetrieveRoleImpl {

    public RetrieveRoleMockup(Access a) {}

    @Override
    public void setTransactionBenchmark(TransactionBenchmark b) {}

    @Override
    public TransactionBenchmark getBenchmark() {
      return null;
    }

    @Override
    public Role retrieve(String role) throws TransactionException {
      return null;
    }
  }

  @BeforeAll
  public static void initServerProperties() throws IOException {
    CaosDBServer.initServerProperties();

    BackendTransaction.setImpl(
        RetrievePermissionRulesImpl.class, RetrievePermissionRulesMockup.class);
    BackendTransaction.setImpl(RetrieveRoleImpl.class, RetrieveRoleMockup.class);
  }

  @Test
  public void testReponseRootElement(@TempDir Path tmpDir) throws IOException {
    final Subject user = new DelegatingSubject(new DefaultSecurityManager(new AnonymousRealm()));
    CaosDBServer.setProperty(ServerProperties.KEY_AUTH_OPTIONAL, "true");
    user.login(AnonymousAuthenticationToken.getInstance());
    AbstractCaosDBServerResource s =
        new AbstractCaosDBServerResource() {

          @Override
          protected Representation httpGetInChildClass()
              throws ConnectionException,
                  IOException,
                  SQLException,
                  CaosDBException,
                  NoSuchAlgorithmException,
                  Exception {
            // TODO Auto-generated method stub
            return null;
          }

          @Override
          public String getSRID() {
            return "TEST-SRID";
          }

          @Override
          public String getCRID() {
            return "TEST-CRID";
          }

          @Override
          public Long getTimestamp() {
            return 0L;
          }

          @Override
          public Reference getRootRef() {
            return new Reference("https://example.com/root/");
          }

          @Override
          public Subject getUser() {
            // TODO Auto-generated method stub
            return user;
          }

          @Override
          public WebinterfaceUtils getUtils() {
            return WebinterfaceUtils.getInstance(getRootRef().toString());
          }
        };
    provideUserSourcesFile(tmpDir);
    Element response = s.generateRootElement();
    assertNotNull(response);
    assertEquals("TEST-SRID", response.getAttribute("srid").getValue());
    assertEquals("TEST-CRID", response.getAttribute("crid").getValue());
    assertEquals("0", response.getAttribute("timestamp").getValue());
    assertEquals("https://example.com/root/", response.getAttributeValue("baseuri"));
    Element userInfo = response.getChild("UserInfo");
    assertNotNull(userInfo);
  }

  /**
   * Creates a dummy usersources.ini and injects it into the server properties.
   *
   * @param tmpDir
   */
  private void provideUserSourcesFile(Path tempFolder) throws IOException {
    File usersourcesini = tempFolder.resolve("usersources.ini").toFile();
    usersourcesini.createNewFile();
    String usersourcesFileName = usersourcesini.getAbsolutePath();

    String usersourcesContent =
        "realms = PAM\n"
            + "defaultRealm = PAM\n"
            + "\n"
            + "[PAM]\n"
            + "class = org.caosdb.server.accessControl.Pam\n"
            + "default_status = ACTIVE\n"
            + "include.user = admin\n"
            + ";include.group = [uncomment and put your groups here]\n"
            + ";exclude.user = [uncomment and put excluded users here]\n"
            + ";exclude.group = [uncomment and put excluded groups here]\n"
            + "\n"
            + ";it is necessary to add at least one admin\n"
            + "user.admin.roles = administration";

    Files.write(Paths.get(usersourcesFileName), usersourcesContent.getBytes());
    CaosDBServer.setProperty(ServerProperties.KEY_USER_SOURCES_INI_FILE, usersourcesFileName);
  }
}
