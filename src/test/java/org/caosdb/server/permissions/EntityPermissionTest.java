package org.caosdb.server.permissions;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class EntityPermissionTest {

  @Test
  public void testGRPCMapping() {
    assertEquals(
        EntityPermission.EDIT_ACL.getMapping(),
        org.caosdb.api.entity.v1.EntityPermission.ENTITY_PERMISSION_EDIT_ACL);

    assertEquals(
        EntityPermission.DELETE.getMapping(),
        org.caosdb.api.entity.v1.EntityPermission.ENTITY_PERMISSION_DELETE);

    assertEquals(
        EntityPermission.USE_AS_DATA_TYPE.getMapping(),
        org.caosdb.api.entity.v1.EntityPermission.ENTITY_PERMISSION_USE_AS_DATA_TYPE);
    assertEquals(
        EntityPermission.USE_AS_PARENT.getMapping(),
        org.caosdb.api.entity.v1.EntityPermission.ENTITY_PERMISSION_USE_AS_PARENT);
    assertEquals(
        EntityPermission.USE_AS_PROPERTY.getMapping(),
        org.caosdb.api.entity.v1.EntityPermission.ENTITY_PERMISSION_USE_AS_PROPERTY);
    assertEquals(
        EntityPermission.USE_AS_REFERENCE.getMapping(),
        org.caosdb.api.entity.v1.EntityPermission.ENTITY_PERMISSION_USE_AS_REFERENCE);

    assertEquals(
        EntityPermission.RETRIEVE_ENTITY.getMapping(),
        org.caosdb.api.entity.v1.EntityPermission.ENTITY_PERMISSION_RETRIEVE_ENTITY);
    assertEquals(
        EntityPermission.RETRIEVE_ACL.getMapping(),
        org.caosdb.api.entity.v1.EntityPermission.ENTITY_PERMISSION_RETRIEVE_ACL);
    assertEquals(
        EntityPermission.RETRIEVE_OWNER.getMapping(),
        org.caosdb.api.entity.v1.EntityPermission.ENTITY_PERMISSION_RETRIEVE_OWNER);
    assertEquals(
        EntityPermission.RETRIEVE_HISTORY.getMapping(),
        org.caosdb.api.entity.v1.EntityPermission.ENTITY_PERMISSION_RETRIEVE_HISTORY);
    assertEquals(
        EntityPermission.RETRIEVE_FILE.getMapping(),
        org.caosdb.api.entity.v1.EntityPermission.ENTITY_PERMISSION_RETRIEVE_FILE);

    assertEquals(
        EntityPermission.UPDATE_VALUE.getMapping(),
        org.caosdb.api.entity.v1.EntityPermission.ENTITY_PERMISSION_UPDATE_VALUE);
    assertEquals(
        EntityPermission.UPDATE_DESCRIPTION.getMapping(),
        org.caosdb.api.entity.v1.EntityPermission.ENTITY_PERMISSION_UPDATE_DESCRIPTION);
    assertEquals(
        EntityPermission.UPDATE_DATA_TYPE.getMapping(),
        org.caosdb.api.entity.v1.EntityPermission.ENTITY_PERMISSION_UPDATE_DATA_TYPE);
    assertEquals(
        EntityPermission.UPDATE_NAME.getMapping(),
        org.caosdb.api.entity.v1.EntityPermission.ENTITY_PERMISSION_UPDATE_NAME);
    assertEquals(
        EntityPermission.UPDATE_QUERY_TEMPLATE_DEFINITION.getMapping(),
        org.caosdb.api.entity.v1.EntityPermission
            .ENTITY_PERMISSION_UPDATE_QUERY_TEMPLATE_DEFINITION);
    assertEquals(
        EntityPermission.UPDATE_ROLE.getMapping(),
        org.caosdb.api.entity.v1.EntityPermission.ENTITY_PERMISSION_UPDATE_ROLE);

    assertEquals(
        EntityPermission.UPDATE_ADD_PARENT.getMapping(),
        org.caosdb.api.entity.v1.EntityPermission.ENTITY_PERMISSION_UPDATE_ADD_PARENT);
    assertEquals(
        EntityPermission.UPDATE_REMOVE_PARENT.getMapping(),
        org.caosdb.api.entity.v1.EntityPermission.ENTITY_PERMISSION_UPDATE_REMOVE_PARENT);

    assertEquals(
        EntityPermission.UPDATE_ADD_PROPERTY.getMapping(),
        org.caosdb.api.entity.v1.EntityPermission.ENTITY_PERMISSION_UPDATE_ADD_PROPERTY);
    assertEquals(
        EntityPermission.UPDATE_REMOVE_PROPERTY.getMapping(),
        org.caosdb.api.entity.v1.EntityPermission.ENTITY_PERMISSION_UPDATE_REMOVE_PROPERTY);

    assertEquals(
        EntityPermission.UPDATE_ADD_FILE.getMapping(),
        org.caosdb.api.entity.v1.EntityPermission.ENTITY_PERMISSION_UPDATE_ADD_FILE);
    assertEquals(
        EntityPermission.UPDATE_REMOVE_FILE.getMapping(),
        org.caosdb.api.entity.v1.EntityPermission.ENTITY_PERMISSION_UPDATE_REMOVE_FILE);
    assertEquals(
        EntityPermission.UPDATE_MOVE_FILE.getMapping(),
        org.caosdb.api.entity.v1.EntityPermission.ENTITY_PERMISSION_UPDATE_MOVE_FILE);
  }
}
