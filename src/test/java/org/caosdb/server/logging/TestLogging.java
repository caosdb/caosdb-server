package org.caosdb.server.logging;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.io.IOException;
import org.apache.commons.io.FileUtils;
import org.caosdb.server.CaosDBServer;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.restlet.Request;
import org.restlet.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TestLogging {

  private Logger logger = LoggerFactory.getLogger(getClass());
  private Logger request_errors_logger =
      LoggerFactory.getLogger(CaosDBServer.REQUEST_ERRORS_LOGGER);
  private Logger request_time_logger = LoggerFactory.getLogger(CaosDBServer.REQUEST_TIME_LOGGER);

  @BeforeAll
  public static void setup() throws IOException {
    File f = new File("testlog/");
    FileUtils.forceDeleteOnExit(f);
  }

  @Test
  public void testLogger() {
    logger.error("error");
    logger.warn("warn");
    logger.info("info");
    logger.debug("debug");
    logger.trace("trace");
    assertEquals(logger.getName(), "org.caosdb.server.logging.TestLogging");
    assertTrue(logger.isErrorEnabled());
    assertTrue(logger.isWarnEnabled());
    assertTrue(logger.isInfoEnabled());
    assertTrue(logger.isDebugEnabled());
    assertTrue(logger.isTraceEnabled());
  }

  @Test
  public void testRequestErrorsLogger() {
    assertTrue(request_errors_logger.isErrorEnabled());
    request_errors_logger.error(
        "ERROR:REQUEST:" + "ABC0123",
        new RequestErrorLogMessage(new Request(), new Response(null)),
        new Exception("exc"));
  }

  @Test
  public void testRequestTimeLogger() {
    assertTrue(CaosDBServer.isDebugMode());
    assertFalse(request_time_logger.isErrorEnabled());
    assertFalse(request_time_logger.isTraceEnabled());
  }
}
