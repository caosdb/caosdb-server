/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.utils.mail;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import org.apache.commons.io.FileUtils;
import org.caosdb.server.CaosDBServer;
import org.caosdb.server.ServerProperties;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class TestMail {

  public static File testDir = null;

  @BeforeAll
  public static void setupUp() throws IOException {
    testDir = Files.createTempDirectory(".TestDir_Mail").toFile();

    CaosDBServer.initServerProperties();
    // output mails to the test dir
    CaosDBServer.setProperty(
        ServerProperties.KEY_MAIL_TO_FILE_HANDLER_LOC, testDir.getAbsolutePath());
  }

  @AfterAll
  public static void tearDown() throws IOException {
    FileUtils.deleteDirectory(testDir);
  }

  @Test
  public void testMail() {
    final Mail mail =
        new Mail(
            "The Admin",
            "test@example.com",
            "The User",
            "test2@example.com",
            "Test",
            "This is a Test");
    mail.send();
  }
}
