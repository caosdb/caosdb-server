/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.jobs;

import static org.junit.jupiter.api.Assertions.fail;

import java.io.IOException;
import org.caosdb.server.CaosDBServer;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class ScheduleTest {

  @BeforeAll
  public static void setup() throws IOException {
    CaosDBServer.initServerProperties();
  }

  /** The problem of ticket #297 was that a CHECK job was executed during the ROLL_BACK stage. */
  @Test
  public void testTicket297() {
    final Schedule schedule = new Schedule();
    schedule.add(
        new Job() {

          @Override
          protected void run() {
            fail("this job should not run");
          }

          @Override
          public JobTarget getTarget() {
            return null;
          }
        });

    schedule.runJobs(TransactionStage.ROLL_BACK);
  }
}
