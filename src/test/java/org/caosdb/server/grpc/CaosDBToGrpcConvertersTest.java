/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2023 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2023 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.caosdb.server.grpc;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.TimeZone;
import org.caosdb.api.entity.v1.Value.Builder;
import org.caosdb.datetime.DateTimeFactory2;
import org.caosdb.server.datatype.CollectionValue;
import org.caosdb.server.datatype.GenericValue;
import org.caosdb.server.datatype.ReferenceValue;
import org.caosdb.server.datatype.Value;
import org.caosdb.server.entity.EntityID;
import org.caosdb.server.entity.FileProperties;
import org.caosdb.server.entity.Message;
import org.caosdb.server.entity.RetrieveEntity;
import org.caosdb.server.entity.Role;
import org.caosdb.server.entity.StatementStatus;
import org.caosdb.server.entity.wrapper.Parent;
import org.caosdb.server.entity.wrapper.Property;
import org.caosdb.server.entity.xml.IdAndServerMessagesOnlyStrategy;
import org.caosdb.server.query.Query.Selection;
import org.junit.jupiter.api.Test;

public class CaosDBToGrpcConvertersTest {

  @Test
  public void testConvertScalarValue_Datetime() {
    TimeZone timeZone = TimeZone.getTimeZone("UTC");
    DateTimeFactory2 factory = new DateTimeFactory2(timeZone);
    CaosDBToGrpcConverters converters = new CaosDBToGrpcConverters(timeZone);
    Value value = null;
    assertEquals(
        converters.convertScalarValue(value).toString(),
        "special_value: SPECIAL_VALUE_UNSPECIFIED\n");
    value = factory.parse("2022");
    assertEquals(converters.convertScalarValue(value).toString(), "string_value: \"2022\"\n");
    value = factory.parse("2022-12");
    assertEquals(converters.convertScalarValue(value).toString(), "string_value: \"2022-12\"\n");
    value = factory.parse("2022-12-24");
    assertEquals(converters.convertScalarValue(value).toString(), "string_value: \"2022-12-24\"\n");
    value = factory.parse("2022-12-24T18:15:00");
    assertEquals(
        converters.convertScalarValue(value).toString(),
        "string_value: \"2022-12-24T18:15:00+0000\"\n");
    value = factory.parse("2022-12-24T18:15:00.999999");
    assertEquals(
        converters.convertScalarValue(value).toString(),
        "string_value: \"2022-12-24T18:15:00.999999+0000\"\n");
    value = factory.parse("2022-12-24T18:15:00.999999UTC");
    assertEquals(
        converters.convertScalarValue(value).toString(),
        "string_value: \"2022-12-24T18:15:00.999999+0000\"\n");
    value = factory.parse("2022-12-24T18:15:00.999999+0200");
    assertEquals(
        converters.convertScalarValue(value).toString(),
        "string_value: \"2022-12-24T16:15:00.999999+0000\"\n");
  }

  @Test
  public void testConvertEntity_FileDescriptor() {
    RetrieveEntity entity = new RetrieveEntity();
    CaosDBToGrpcConverters converters = new CaosDBToGrpcConverters(null);
    assertEquals(converters.convert(entity).toString(), "entity {\n}\n");
    entity.setFileProperties(new FileProperties("checksum1234", "the/path", 1024L));
    assertEquals(
        converters.convert(entity).toString(),
        "entity {\n  file_descriptor {\n    path: \"the/path\"\n    size: 1024\n  }\n}\n");
  }

  @Test
  public void testIdServerMessagesOnlyStrategy() {
    // @review Florian Spreckelsen 2022-03-22
    RetrieveEntity entity = new RetrieveEntity();

    // must be printed
    entity.setId(new EntityID("1234"));
    entity.addInfo("info");
    entity.addWarning(new Message("warning"));
    entity.addError(new Message("error"));

    // must not be printed
    Parent par = new Parent(new RetrieveEntity());
    par.setName("dont print parent");
    entity.addParent(par);
    entity.setName("dont print");
    entity.setDescription("dont print");
    entity.setRole(Role.File);
    entity.setFileProperties(new FileProperties("dont print checksum", "dont print path", 1234L));
    Property p = new Property(new RetrieveEntity());
    p.setStatementStatus(StatementStatus.FIX);
    p.setName("dont print property");
    p.setDatatype("TEXT");
    p.setValue(new GenericValue("don print"));
    entity.addProperty(p);

    CaosDBToGrpcConverters converters = new CaosDBToGrpcConverters(null);

    // first test the normal SerializeFieldStrategy instead
    entity.setSerializeFieldStrategy(null);
    assertTrue(converters.convert(entity).toString().contains("dont print"));

    // now suppress all fields but id and server messages.
    entity.setSerializeFieldStrategy(new IdAndServerMessagesOnlyStrategy());
    assertEquals(
        converters.convert(entity).toString(),
        "entity {\n  id: \"1234\"\n}\nerrors {\n  code: 1\n  description: \"error\"\n}\nwarnings {\n  code: 1\n  description: \"warning\"\n}\ninfos {\n  code: 1\n  description: \"info\"\n}\n");
  }

  @Test
  public void testGetSelectedValueWithNullValue() {
    Property p = new Property(new RetrieveEntity());
    p.setName("p0");
    p.setDatatype("DOUBLE");
    RetrieveEntity entity = new RetrieveEntity();
    entity.addProperty(p);

    CaosDBToGrpcConverters converters = new CaosDBToGrpcConverters(null);
    Builder value = converters.getSelectedValue(new Selection("p0"), entity);
    assertEquals(
        "scalar_value {\n" + "  special_value: SPECIAL_VALUE_UNSPECIFIED\n" + "}\n",
        value.toString());
  }

  @Test
  public void testGetSelectedValueWithListOfReferenceValue() {
    CollectionValue col = new CollectionValue();
    Property p = new Property(new RetrieveEntity());
    p.setName("Person");
    p.setDatatype("List<Person>");

    Property fullName1 = new Property(new RetrieveEntity());
    fullName1.setName("full name");
    fullName1.setDatatype("TEXT");
    fullName1.setValue(new GenericValue("Harry Belafonte"));

    RetrieveEntity person1 = new RetrieveEntity();
    person1.addProperty(fullName1);
    ReferenceValue val1 = new ReferenceValue(new EntityID("1234"));
    val1.setEntity(person1, false);
    col.add(val1);

    Property fullName2 = new Property(new RetrieveEntity());
    fullName2.setName("full name");
    fullName2.setDatatype("TEXT");
    fullName2.setValue(new GenericValue("Louis Prima"));

    RetrieveEntity person2 = new RetrieveEntity();
    person2.addProperty(fullName2);
    ReferenceValue val2 = new ReferenceValue(new EntityID("1234"));
    val2.setEntity(person2, false);
    col.add(val2);
    p.setValue(col);

    RetrieveEntity entity = new RetrieveEntity();
    entity.addProperty(p);

    CaosDBToGrpcConverters converters = new CaosDBToGrpcConverters(null);
    Builder value =
        converters.getSelectedValue(
            new Selection("Person").setSubSelection(new Selection("full name")), entity);
    assertEquals(
        "list_values {\n"
            + "  values {\n"
            + "    string_value: \"Harry Belafonte\"\n"
            + "  }\n"
            + "  values {\n"
            + "    string_value: \"Louis Prima\"\n"
            + "  }\n"
            + "}\n",
        value.toString());
  }
}
