/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2020,2023 Timm Fitschen <t.fitschen@indiscale.com>
 * Copyright (C) 2020,2023 IndiScale GmbH <info@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.caosdb.server.entity.container;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.caosdb.server.datatype.GenericValue;
import org.caosdb.server.datatype.ReferenceValue;
import org.caosdb.server.entity.Entity;
import org.caosdb.server.entity.EntityID;
import org.caosdb.server.entity.RetrieveEntity;
import org.caosdb.server.entity.Role;
import org.caosdb.server.entity.wrapper.Property;
import org.caosdb.server.entity.xml.PropertyToElementStrategyTest;
import org.caosdb.server.entity.xml.SerializeFieldStrategy;
import org.jdom2.Element;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class PropertyContainerTest {

  public static Entity house = null;
  public static Property houseHeight = null;
  public static Entity window = null;
  public static Property windowHeight = null;
  public static Entity houseOwner = null;
  public static Property windowProperty = null;

  @BeforeAll
  public static void setup() {
    window = new RetrieveEntity(new EntityID("1234"));
    windowHeight = new Property(new RetrieveEntity("window.height", Role.Property));
    window.addProperty(windowHeight);
    windowHeight.setValue(new GenericValue("windowHeight"));

    houseOwner = new RetrieveEntity("The Queen", Role.Record);

    house = new RetrieveEntity("Buckingham Palace", Role.Record);
    houseHeight = new Property(new RetrieveEntity("height", Role.Property));
    houseHeight.setValue(new GenericValue("houseHeight"));
    house.addProperty(houseHeight);
    windowProperty = new Property(new RetrieveEntity(new EntityID("2345")));
    windowProperty.setName("window");
    windowProperty.setValue(new ReferenceValue(window.getId()));
    house.addProperty(windowProperty);

    house.addProperty(new Property(new RetrieveEntity()));
    house.addProperty(new Property(houseHeight));
  }

  @Test
  public void test() {
    final PropertyContainer container = new PropertyContainer(new RetrieveEntity());
    final Element element = new Element("Record");
    final SerializeFieldStrategy setFieldStrategy =
        new SerializeFieldStrategy()
            .addSelection(PropertyToElementStrategyTest.parse("window.height"));

    container.addToElement(windowProperty, element, setFieldStrategy);

    assertEquals(1, element.getChildren().size());
  }
}
