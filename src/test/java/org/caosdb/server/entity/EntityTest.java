/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2021 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2021 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.entity;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.caosdb.server.datatype.IntegerDatatype;
import org.junit.jupiter.api.Test;

public class EntityTest {

  @Test
  public void testIsReference() {
    EntityInterface entity = new RetrieveEntity("test");
    assertFalse(entity.isReference());

    entity.setDatatype(new IntegerDatatype());
    assertFalse(entity.isReference());

    entity.setDatatype("Person");
    assertTrue(entity.isReference());
  }

  @Test
  public void testIsReferenceList() {
    EntityInterface entity = new RetrieveEntity("test");
    assertFalse(entity.isReferenceList());

    entity.setDatatype(new IntegerDatatype());
    assertFalse(entity.isReferenceList());

    entity.setDatatype("Person");
    assertFalse(entity.isReferenceList());

    entity.setDatatype("LIST<Person>");
    assertTrue(entity.isReferenceList());
  }
}
