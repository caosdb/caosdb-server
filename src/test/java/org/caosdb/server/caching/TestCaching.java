/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2023 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2023 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.caosdb.server.caching;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;

import java.io.IOException;
import org.apache.commons.jcs.JCS;
import org.apache.commons.jcs.access.CacheAccess;
import org.caosdb.server.CaosDBServer;
import org.caosdb.server.ServerProperties;
import org.caosdb.server.accessControl.Pam;
import org.caosdb.server.database.backend.transaction.RetrieveProperties;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class TestCaching {

  @BeforeAll
  public static void init() throws IOException {
    CaosDBServer.initServerProperties();
    CaosDBServer.getServerProperties().setProperty(ServerProperties.KEY_CACHE_DISABLE, "FALSE");
    CaosDBServer.initCaching();
    JCSCacheHelper.init();
  }

  @Test
  public void testCacheConfig() {
    CacheAccess<String, String> retrieve_properties_cache =
        JCS.getInstance(RetrieveProperties.CACHE_REGION);
    assertEquals(1003, retrieve_properties_cache.getCacheAttributes().getMaxObjects());

    CacheAccess<String, String> retrieve_entities_cache = JCS.getInstance("BACKEND_SparseEntities");
    assertEquals(1002, retrieve_entities_cache.getCacheAttributes().getMaxObjects());

    CacheAccess<String, String> pam_groups_cache = JCS.getInstance(Pam.CACHE_REGION_GROUPS);
    assertEquals(1000, pam_groups_cache.getCacheAttributes().getMaxObjects());
    assertEquals(false, pam_groups_cache.getCacheAttributes().isUseMemoryShrinker());
    assertEquals(false, pam_groups_cache.getDefaultElementAttributes().getIsEternal());
    assertEquals(61, pam_groups_cache.getDefaultElementAttributes().getIdleTime());
    assertEquals(601, pam_groups_cache.getDefaultElementAttributes().getMaxLife());
  }

  @Test
  public void testCacheElements() throws IOException {
    final CacheAccess<Object, Object> cache = JCS.getInstance("default");

    final String key = "KEY";
    final String value = "VALUE";

    cache.put(key, value);

    assertEquals(value, cache.get(key));
    assertSame(value, cache.get(key));
  }
}
