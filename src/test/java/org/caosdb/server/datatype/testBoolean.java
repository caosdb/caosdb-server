/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.datatype;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.caosdb.server.datatype.AbstractDatatype.Table;
import org.caosdb.server.entity.Message;
import org.caosdb.server.utils.ServerMessages;
import org.junit.jupiter.api.Test;

public class testBoolean {

  @Test
  public void testCaosEnumCI() {
    CaosEnum ce = new CaosEnum("TRUE", "FALSE", "NEITHER");
    assertNotNull(ce.valueOf("TRUE"));
    assertNotNull(ce.valueOf("true"));
    assertNotNull(ce.valueOf("TRue"));
    assertNotNull(ce.valueOf("False"));
    assertNotNull(ce.valueOf("Neither"));
    try {
      ce.valueOf("bla");
    } catch (IllegalArgumentException e) {
      assertEquals("No such element: bla", e.getMessage());
    }
    assertTrue(ce.valueOf("true").equals(ce.valueOf("True")));
    assertTrue(ce.valueOf("true") == ce.valueOf("TRUE"));

    assertEquals("TRUE", ce.valueOf("True").toString());
    assertEquals("TRUE", ce.valueOf("TRUE").toString());
    assertEquals("TRUE", ce.valueOf("tRUe").toString());
  }

  @Test
  public void testCaosEnumCS() {
    CaosEnum ce = new CaosEnum(true, "TRUE", "FALSE", "NEITHER", "neither");
    assertNotNull(ce.valueOf("TRUE"));
    assertNotNull(ce.valueOf("FALSE"));
    assertNotNull(ce.valueOf("NEITHER"));
    assertNotNull(ce.valueOf("neither"));
    assertTrue(ce.valueOf("neither").equals(ce.valueOf("neither")));
    assertTrue(ce.valueOf("neither") == ce.valueOf("neither"));
    assertFalse(ce.valueOf("neither").equals(ce.valueOf("NEITHER")));
    try {
      ce.valueOf("true");
    } catch (IllegalArgumentException e) {
      assertEquals("No such element: true", e.getMessage());
    }

    assertEquals("TRUE", ce.valueOf("TRUE").toString());
    assertEquals("neither", ce.valueOf("neither").toString());
    assertEquals("NEITHER", ce.valueOf("NEITHER").toString());
  }

  @Test
  public void testBooleanValue() {
    assertNotNull(BooleanValue.valueOf("true"));
    assertNotNull(BooleanValue.valueOf("TRUE"));
    assertNotNull(BooleanValue.valueOf("TRUe"));
    assertNotNull(BooleanValue.valueOf("FALSE"));
    try {
      BooleanValue.valueOf("neither");
    } catch (IllegalArgumentException e) {
      assertEquals("No such element: neither", e.getMessage());
    }
  }

  @Test
  public void testBooleanDatatype() throws Message {
    BooleanDatatype d = new BooleanDatatype();
    assertEquals("TRUE", d.parseValue("TRUE").toDatabaseString());
    assertEquals("TRUE", d.parseValue("True").toDatabaseString());
    assertEquals("FALSE", d.parseValue("fALSE").toDatabaseString());

    try {
      d.parseValue("fALSE");
    } catch (Message m) {
      assertEquals(ServerMessages.CANNOT_PARSE_BOOL_VALUE, m);
    }

    SingleValue v = d.parseValue("fALSE");
    assertEquals(Table.enum_data, v.getTable());
  }
}
