package org.caosdb.datetime;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.TimeZone;
import org.junit.jupiter.api.Test;

class UTCTimeZoneShiftTest {

  @Test
  void test() {
    TimeZone timeZone = TimeZone.getTimeZone("CST");
    assertEquals("-0500", UTCTimeZoneShift.getISO8601Offset(timeZone, 1665133324000L));
  }
}
