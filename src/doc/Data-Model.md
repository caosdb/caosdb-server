
# CaosDB Data Model

The data structure is built from some basic building blocks which are
shown in the following picture:

![entities](entities.png)

It has a base object called **Entity**. Entities are either **Record
Types**, **Records**, or **Abstract Properties** (and Record like
objects for files). What *kind of data* is stored is defined by the
Record Types. Actual data is then stored in CaosDB as Records which
are of some Record Type. Those Records can have Properties that
contain information about the Record. The following is a more detailed
explanation (also see this
[paper](https://www.mdpi.com/2306-5729/4/2/83)).

> Record Types and Abstract Properties are used to define the ontology for a particular domain in
> which the RDMS (research data management) is used. Records are used to store the actual data and
> therefore represent individuals or particular things, e.g. a particular experiment, a particular
> time series, etc.

## Record Types

**Record Types** define classes or types of things, e.g. persons,
experiments, timeseries, etc. Records can be viewed as members of the
class defined by its Record Type. These classes can contain Abstract
Properties which define key-value relationships for properties of the
things along with the expected data type and possibly the default
unit, a default value, or a range of permitted values. As files on the
backend file system are a major focus of this database management
system, there is a special File entity that encapsulates typical file
properties like path, size and checksum.

## Entities

**Entities** can be related via binary, directed, transitive is-a
relations which model both subtyping and instantiation, depending on
the relata. These relations construct a directed graph of the
Entities. If A is-a B we call A the child of B and B the parent of
A. No adamant restrictions are imposed on the relate of the is-a
relation and thus, Entities can be children of multiple Entities.

## Properties

Each Entity has a list of Entity Properties, or in short just
**Properties**. An Entity Property is not an Entity of its own, but a
triple of an Abstract Property, a value or Null, and an
Importance. The values can be numerals, strings, dates, any other
valid value that fits into one of several builtin data types, or, most
notably, references to other Entities. The Importance is either
`obligatory`, `recommended`, `suggested`, or `fix`. A valid child of
an Entity implicitly inherits its parent’s Properties according to
their Importance, which means that it is obliged, recommended or only
suggested to have a Property with the same Abstract Property (or any
subtype thereof). As opposed to Properties with other priorities,
**Fixed Properties** have no effect on the Entity’s children.

During the creation or update of Entities, the importances of the
parents are checked by the server. Missing obligatory Properties
invalidate the transaction and result in an error, by default. Missing
Properties, when they are recommended, result in a warning, but the
transaction is considered valid. Entities with missing suggested
Properties are silently accepted as valid. See [the detailed
specification of RecordTypes](specification/RecordType) for more
information on importances and inheritance.
