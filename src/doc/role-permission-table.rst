.. _Role Permission Table:

Role Permissions
================

Roles in CaosDB can have the following permissions

.. list-table:: Role permissions
   :header-rows: 1
   :widths: 33 67

   * - Permission
     - Description
   * - ``ACCESS_SERVER_PROPERTIES``
     - Permission to read the server properties.
   * - ``ACM:*``
     - Permissions to administrate the access controll management system. That
       includes managing users, roles, and assigning permissions to roles and
       roles to users.
   * - ``ACM:ROLE:*``
     - Permissions to manage roles, i.e. create, retrieve, update and delete
       roles and assign them to users.
   * - ``ACM:ROLE:ASSIGN:?ROLE?``
     - Permission to assign a role (to a user).
   * - ``ACM:ROLE:DELETE:?ROLE?``
     - Permission to delete a role.
   * - ``ACM:ROLE:INSERT``
     - Permission to create a new role.
   * - ``ACM:ROLE:RETRIEVE:DESCRIPTION:?ROLE?``
     - Permission to retrieve the description of a role.
   * - ``ACM:ROLE:RETRIEVE:PERMISSIONS:?ROLE?``
     - Permission to read the permissions of a role.
   * - ``ACM:ROLE:UPDATE:DESCRIPTION:?ROLE?``
     - Permission to update the description of a role.
   * - ``ACM:ROLE:UPDATE:PERMISSIONS:?ROLE?``
     - Permission to set the permissions of a role.
   * - ``ACM:USER:*``
     - Permissions to manage users, i.e. create, retrieve, update and delete
       users.
   * - ``ACM:USER:DELETE:?REALM?:?USER?``
     - Permission to delete a user
   * - ``ACM:USER:INSERT:?REALM?``
     - Permission to create a user in the given realm
   * - ``ACM:USER:RETRIEVE:INFO:?REALM?:?USER?``
     - Permission to retrieve the user info (email, entity, status)
   * - ``ACM:USER:RETRIEVE:ROLES:?REALM?:?USER?``
     - Permission to retrieve the roles of a user
   * - ``ACM:USER:UPDATE:EMAIL:?REALM?:?USER?``
     - Permission to update the email address of a user.
   * - ``ACM:USER:UPDATE:ENTITY:?REALM?:?USER?``
     - Permission to set the entity which is associated with a user.
   * - ``ACM:USER:UPDATE:ROLES:?REALM?:?USER?``
     - Permission to change the roles of a user.
   * - ``ACM:USER:UPDATE:STATUS:?REALM?:?USER?``
     - Permission to update the status of a user, i.e. marking them as
       ``ACTIVE`` or ``INACTIVE``.
   * - ``ACM:USER:UPDATE_PASSWORD:?REALM?:?USER?``
     - Permission to set the password of a user.
   * - ``SCRIPTING:EXECUTE:?PATH?``
     - Permission to execute a server-side script under the given path. Note
       that, for utilizing the wild cards feature, you have to use ``':'`` as
       path separator. E.g. ``'SCRIPTING:EXECUTE:my_scripts:*'`` would be the
       permission to execute all executables below the ``my_scripts`` directory.
   * - ``SERVERLOGS:RETRIEVE``
     - Permission to read the server logs. (DEPRECATED)
   * - ``STATE:*``
     - Permissions to manage state models and the states of entities.
   * - ``STATE:ASSIGN:?STATE_MODEL?``
     - Permission to assign a state model.
   * - ``STATE:FORCE:FINAL``
     - Permission to force to leave a state models specified life-cycle even
       though the currrent state isn't a final state in the that model.
   * - ``STATE:TRANSITION:?TRANSITION?``
     - Permission to initiate a transition.
   * - ``STATE:UNASSIGN:?STATE_MODEL?``
     - Permission to unassign a state model.
   * - ``TRANSACTION:*``
     - Permission to execute any writable transaction. This permission only
       allows to execute these transactions in general. The necessary entities
       permissions are not implied.
   * - ``TRANSACTION:DELETE:?ENTITY_ROLE?``
     - Permission to delete entities of a given role (e.g. Record, File,
       RecordType, or Property).
   * - ``TRANSACTION:INSERT:?ENTITY_ROLE?``
     - Permission to insert entities of a given role (e.g. Record, File,
       RecordType, or Property).
   * - ``TRANSACTION:UPDATE:?ENTITY_ROLE?``
     - Permission to update entities of a given role (e.g. Record, File,
       RecordType, or Property).
