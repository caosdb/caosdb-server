Permissions
===========

CaosDB has a fine grained role-based permission system. Each interaction with
the server is governed by the current role(s) of the user, by default this is
the ``anonymous`` role. The permissions for an action which involves one or more
objects are set either manually or via default permissions which can be
configured.

Permissions are needed to perform particular elementary *actions* during any
interaction with the the server. E.g. retrieving an Entity requires the
requesting user to have the ``RETRIEVE:ENTITY`` permission for that entity, and
``DELETE:ENTITY`` to delete the entity.

The permissions of every user are calculated from a set of *Permission
Rules*. These rules have several sources. Some are global defaults, some are
defined by administrators or the owners of an entity.

.. note::

   As a side note on the implementation: The server uses `Apache Shiro
   <https://shiro.apache.org/documentation.html>`__ for its permission system.

What is a Permission Rule?
--------------------------

A Permission Rule consists of:

-  A type: Permission Rules can be of ``Grant`` or ``Deny`` type, either
   granting or denying specific permissions.
-  A :doc:`role <roles>` (or user): For which users the
   permission shall be granted or denied.
-  A permission action: Which action shall be permitted or forbidden,
   for example all retrieval, or modifications of a specific entity.
-  An optional priority: May be ``true`` or ``false``. Permissions with
   priority = ``true`` override those without, see the calculation rules
   below.

There are two complementing types of permission rules that will be explained
below: role permissions and entity permissions.

.. _role-permissions:

Role permissions
***********************

As the name suggests, role permissions are assigned to a specific role and
define whether, in general, a particular role is allowed, e.g., to perform
specific transactions, update roles or users, or execute server-side
scripts. See the :ref:`role-permissions table<Role Permission Table>` for a full list of role
permissions. The most common are

- ``TRANSACTiON:*``: Allows to perform any write transaction (in general). Note
  that this is the necessary but not sufficient condition for
  deleting/inserting/updating an entity and the corresponding :ref:`entity
  permissions<entity-permissions>` are required, too.
- ``SCRIPTING:EXECUTE:?PATH?``: Permission to execute a server-side script under
  the given path. Note that, for utilizing the wild cards feature, you have to
  use ``':'`` as path separator. E.g. ``'SCRIPTING:EXECUTE:my_scripts:*'`` would
  be the permission to execute all executables below the ``my_scripts``
  directory.

.. _entity-permissions:

Entity permissions
******************

As the name suggests, entity permissions define what a certain user or role is
allowed to do with a specific entity. Thus, entity permissions can be used to,
e.g., deny everone but administration users to update or delete a specific
record types, or to allow everyone to retrieve a specific record. See the
:ref:`entity-permissions table<Entity permissions table>` for a full list of
possible entity permissions. Typical permissions are:

-  ``RETRIEVE:ENTITY``: To retrieve the full entity (name,
   description, data type, …) with all parents and properties (unless
   prohibited by another rule on the property level).
-  ``RETRIEVE:ACL``: To retrieve the full and final ACL of this
   entity.
-  ``RETRIEVE:ENTITY:1234``: To retrieve the entity ``1234``.
-  ``RETRIEVE:*:1234``: For all “retrieve” actions concerning the
   entity ``1234``.




.. _Calculation:

Permission calculation
----------------------

For each action a user tries to perform, the server tests, in the
following order, which rules apply:

1. *Grant* rules, without priority.
2. *Deny* rules, without priority.
3. *Grant* rules, with priority.
4. *Deny* rules, with priority.

If at the end the user’s permission is *granted*, the action may take
place. Otherwise (the result is *denied* or the permission still
undefined), the action can not take place. In other words, if you have
not been given the permission explicitly at some point, you don’t have
it.

Administration permissions
**************************

There is one special permission rule that grants global administration
permissions to a user or a role: ``Grant(*)P``, i.e., grant everything with
priority. This promotes a user with this role to administration level, very
similar to the ``root`` user on POSIX systems. As with any other rule, the
effect of this rule can be overriden by *Deny* rules with priority. The
``Grant(*)P`` permission should really only be used for administration users
and in no other case.

.. warning::

   ``Grant(*)P`` is a powerful administration permission rule. Be cautious
   when assigning administration privileges to users and roles.


How to set permissions
----------------------

There are multiple ways to set role and entity permissions. The most
common and best tested way currently is to set global default *entity* permissions
in the ``global_entity_permissions.xml`` config file, and role-based *role*
permissions with the ``caosdb_admin.py`` `utility script
<https://gitlab.com/caosdb/caosdb-pylib/-/blob/main/src/caosdb/utils/caosdb_admin.py>`__
of CaosDB's Python library which is also used to `manage users and
roles <https://docs.indiscale.com/caosdb-pylib/administration.html>`__. Below you
find a more detailed description of the possible ways of setting permissions.

-  **Config file:** Some default permissions are typically set in the
   ``global_entity_permissions.xml`` file, see also the `default file
   <https://gitlab.com/caosdb/caosdb-server/-/blob/main/conf/core/global_entity_permissions.xml>`__. Here,
   you can set the default permissions that every entity on the server has. The
   global default permissions can **only** be set in this file; all other ways
   below can only change the permissions of individual entities. Note that you
   can add more rules in the ``global_entity_permissions.xml``, but you can not remove rules by
   writing to this file. Thus, it might not be possible to overrule
   permissions defined here (see :ref:`Permission
   calculation<Calculation>`). Note also that, as the name suggests, only
   :ref:`entity permissions<entity-permissions>` can be set this way. The
   role-based :ref:`role-permissions<role-permissions>` have to be
   set with one of the other ways explained below.
-  **API:** Both REST and GRPC API allow to set the permissions. This hasn't been
   documented properly yet, but for the GRPC API, `the specification
   <https://gitlab.com/caosdb/caosdb-proto/-/blob/main/proto/caosdb/acm/v1alpha1/main.proto>`__
   may give some insight.
-  **The Python library:** The permissions can also conveniently be set via the
   Python library. Currently the best documentation is inside various files
   which use the permission API:

   - The `example file
     <https://gitlab.com/caosdb/caosdb-pylib/-/blob/main/examples/set_permissions.py>`__
   - The ``caosdb_admin.py`` `utility script
     <https://gitlab.com/caosdb/caosdb-pylib/-/blob/main/src/caosdb/utils/caosdb_admin.py>`__
   - There is a comprehensive `example <https://docs.indiscale.com/caosdb-pylib/gallery/curator-permissions.html>`_
     in PyCaosDB's gode gallery.
   - The `integration tests
     <https://gitlab.com/caosdb/caosdb-pyinttest/-/blob/main/tests/test_permissions.py>`__
     also cover quite a bit of the permission API.

-  **WebUI:** This is currently work in progress. A WebUI ACM module which uses the
   GRPC interface is under `active development
   <https://gitlab.indiscale.com/caosdb/src/caosdb-webui2>`__.
