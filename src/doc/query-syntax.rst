CaosDB Query Language Syntax
============================

This is the documentation of the CaosDB Query Language Syntax. The
authoritative specification of the syntax is the ANTLR4 grammar you can find in
the source code:
`CQLParser.g4 <https://gitlab.com/caosdb/caosdb-server/-/blob/main/src/main/java/org/caosdb/server/query/CQLParser.g4>`__
and
`CQLLexer.g4 <https://https://gitlab.com/caosdb/caosdb-server/-/blob/main/src/main/java/org/caosdb/server/query/CQLLexer.g4>`__

See examples in :doc:`Query Language<CaosDB-Query-Language>`.

.. a4:autogrammar:: CQLParser.g4

.. a4:autogrammar:: CQLLexer.g4
