# Paging
The Paging flag splits the retrieval of a (possibly huge) number entities into pages.

## Syntax


          flag   = name, [":", value];
          name   = "P";
          value  = [ index ], ["L", length]];
          index  =  ? any positive integer ?;
          length =  ? any positive integer ?;

## Semantics

The `index` (starting with zero) denotes the index of the first entity to be retrieved. The `length` is the number of entities on that page. If `length` is omitted, the default number of entities is returned (as configured by a server constant called ...). If only the `name` is given the paging behaves as if the `index` has been zero.

## Examples

`http://localhost:10080/Entities/all?flags=P:24L50` returns 50 entities starting with the 25th entity which would be retrieved without paging.

`http://localhost:10080/Entities/all?flags=P:24` returns the default number of entities starting with the 25th entity which would be retrieved without paging.

`http://localhost:10080/Entities/all?flags=P:L50` returns 50 entities starting with the first entity which would be retrieved without paging.

`http://localhost:10080/Entities/all?flags=P` returns the default number of entities starting with the first entity which would be retrieved without paging.
