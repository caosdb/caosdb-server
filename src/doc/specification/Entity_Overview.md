# Overview of the CaosDB Entities' structure

**Warning:** This overview may be outdated.

Version: 0.1.0r1

Author: Timm Fitschen

Email: timm.fitschen@ds.mpg.de

Date: 2017-12-17

## Introduction

CaosDB is a database management system that stores it's data into `Entities`. An `Entity` can be thought of as the equivalent to tables, rows, columns and the tuples that fill the tables of a traditional RDBMS. Entities are not only used to store the data they also define the structure of the data.

## Formal Definition

An `Entity` may have 

* a `domain`
* an `id`
* a `role`
* a `name`
* a `data type`
* a `Set of Values`
* a `Set of Properties`
* a `Set of Parents`

A `domain` contains an `Entity`.

An `id` is an arbitrary string. 

A `role` is an arbitrary string. Especially, it may be one of the following strings:

* `RecordType`
* `Record`
* `Relation`
* `Property`
* `File`
* `QueryTemplate`
* `Domain`
* `Unit`
* `Rule`
* `DataType`
* `Remote`

A `name` is an arbitrary string.

A `data type` contains an `Entity`. Note: this is not necessarily a `Data Type`.

### Set of Values

A `Set of Values` is a mapping from a `indices` to a finite set of `Values`.

An `index` is an interval of non-negative integers starting with zero.

#### Value

A `Value` may have a `data type` and/or a `unit`.

A `data type` is an `Entity`. Note: this is not necessarily a `Data Type`.

A `unit` is an arbitrary string.

### Data Type

A `Data Type` is an `Entity` with role `DataType`.

#### Reference Data Type

A `Reference Data Type` is a `Data Type`. It may have a `scope`. 

A `scope` contains an `Entity`.

#### Collection Data Type

A `Collection Data Type` is a `Data Type`. It may have an ordered set of `elements`.

### Record Type

A `Record Type` is an `Entity` with role `RecordType`.

### Record

A `Record` is an `Entity` with role `Record`.

### Relation

A `Relation` is an `Entity` with role `Relation`.

### Property

A `Property` is an `Entity` with role `Property`. It is also refered to as `Abstract Property`.

### File

A `File` is an `Entity` with role `File`.

A `File` may have 

* a `path`
* a `size`
* a `checksum`

A `path` is an arbitrary string.

A `size` is a non-negative integer.

A `checksum` is an ordered pair (`method`,`result`).

A `method` is an arbitrary string. 

A `result` is an arbitrary string.

### QueryTemplate

A `QueryTemplate` is an `Entity` with role `QueryTemplate`.

### Domain

A `Domain` is an `Entity` with role `Domain`.

### Unit

A `Unit` is an `Entity` with role `Unit`.

### Rule

A `Rule` is an `Entity` with role `Rule`.

### Remote

A `Remote` is an `Entity` with role `Remote`.

### Set of Parents

A `Set of Parents` is a set of `Parents`.

#### Parent

A `Parent` may contain another `Entity`. 

A `Parent` may have an `affiliation`.

An `affiliation` may contain of the following strings:

* `subtyping`
* `instantiation`
* `membership`
* `parthood`
* `realization`

### Set of Properties

A `Set of Properties` is a tripple (`index`, set of `Implemented Properties`, `Phrases`).

An `index` is a bijective mapping from an interval of non-negative integer numbers starting with zero to the set of `Implemented Properties`.

#### Implemented Property

An `Implemented Property` contains another `Entity`.

An `Implemented Property` may have an `importance`.

An `Implemented Property` may have a `maximum cardinality`.

An `Implemented Property` may have a `minimum cardinality`.

An `Implemented Property` may have an `import`.

An `importance` is an arbitrary string. It may contain of the following strings:

* `obligatory`
* `recommended`
* `suggested`
* `fix`

A `maximum cardinality` is a non-negative integer.

A `minimum cardinality` is a non-negative integer.

An `import` is an arbitrary string. It may contain of the following strings:

* `fix`
* `none`

#### Phrases

`Phrases` are a mapping from the cartesian product of the `index` with itself to a `predicate`.

A `predicate` is an arbitrary string.


