Developing CaosDB
=================

.. toctree::
   :glob:
   :maxdepth: 2

   Structure of the Java code <structure>
   Testing the server code <testing>
   Logging server output <logging>
   Benchmarking CaosDB <benchmarking>

CaosDB is an Open-Source project, so anyone may modify the source as they like. These pages aim to
provide some help for all CaosDB developers.

More generally, these are the most common ways to contribute to CaosDB:

- You found a bug, have a question, or want to request a feature? Please `create an issue
  <https://gitlab.com/caosdb/caosdb-server/-/issues>`_.
- You want to contribute code? Please fork the repository and create a merge request in GitLab and
  choose this repository as target. Make sure to select "Allow commits from members who can merge
  the target branch" under Contribution when creating the merge request. This allows our team to
  work with you on your request.
- If you have a suggestion for this `documentation <https://docs.indiscale.com/caosdb-server/>`_,
  the preferred way is also a merge request as describe above (the documentation resides in
  ``src/doc``).  However, you can also create an issue for it.
- You can also contact the developers at *info (AT) caosdb.de*.
