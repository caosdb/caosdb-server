
State Machine
=============

Prerequisites
-------------

In order to use the state machine functionality you have to set the
corresponding server setting: ``EXT_ENTITY_STATE=ENABLED``.

Also, a few RecordTypes and Properties are required. You can use the
script `setup_state_model.py <https://gitlab.com/caosdb/caosdb-server/-/blob/dev/src/doc/tutorials/setup_state_model.py>`_
to create those or you may have a look at it to see what is needed (``setup_state_data_model`` function).

Defining the State Machine
--------------------------

Now you are setup to create your own state machine. You can define States and Transitions
and bundle it all to a StateModel. The above mentioned ``setup_state_model.py`` script defines
a publication cycle with the state "Unpublished", "UnderReview" and "Published".
Again, the ``setup_state_model.py`` script provides orientation on how this
can be setup (``setup_model_publication_cycle`` function).

Note, that you can provide ACL to the state definition which will be applied to an entity once
the state is reached. This is for example useful to change the visibility depending on a state change.

If you assign a state to a RecordType, this state will be the initial state
of Records that have that parent. For example by executing:

.. code-block:: Python

    rt = db.RecordType("Article").retrieve()
    rt.state = db.State(name="UnPublished", model="Publish Life-cycle")
    rt.update()
