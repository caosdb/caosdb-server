
Welcome to caosdb-server's documentation!
=========================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   :hidden:
   :glob:

   Getting started <README_SETUP>
   Concepts <concepts>
   tutorials
   FAQ
   Query Language <CaosDB-Query-Language>
   administration
   Development <development/devel>
   Dependencies <DEPENDENCIES>
   Changelog <CHANGELOG>
   specification/index.rst
   Glossary
   Server Internals <_apidoc/packages>
   Related Projects <related_projects/index>
   Back to overview <https://docs.indiscale.com/>

Welcome to the CaosDB, the flexible semantic data management toolkit!

This documentation helps you to :doc:`get started<README_SETUP>`,
explains the most important :doc:`concepts<concepts>` and has
information if you want to :doc:`develop<development/devel>` CaosDB
yourself.


Indices and tables
==================

* :ref:`genindex`
