====
FAQs
====

These FAQs (frequently asked questions) can be extended, if *you* help us.  Please `submit an issue
<https://gitlab.com/caosdb/caosdb-server/issues/new>`__ if you have a question that should be
answered here.

.. contents:: Select your question:
   :local:

How do I declare a LIST property?
=================================

Use the datatype parameter (available with Property constructors and
with the ``add_property`` method and the ``LIST`` function.

.. code:: python

   # with constructor
   p = caosdb.Property(name="ListOfDoubles", datatype=caosdb.LIST(caosdb.DOUBLE))

   # with add_property method
   my_entity.add_property(name="ListOfIntegers", datatype=caosdb.LIST(caosdb.INTEGER))
   my_entity.add_property(name="ListOfThings", datatype=caosdb.LIST("Thing"))
   my_entity.add_property(name="ListOfThings", datatype=caosdb.LIST(caosdb.RecordType('Thing'))

Which data types are there?
===========================

There are 7 basic data types:

-  ``INTEGER``
-  ``DOUBLE``
-  ``DATETIME``
-  ``TEXT``
-  ``BOOLEAN``
-  ``FILE``
-  ``REFERENCE``

There is (so far) 1 data type for collections:

-  ``LIST`` (Actually, LIST-of-another-data-type, e.g. ``LIST(INTEGER)``)

And furthermore,…

-  Any RecordType can be used as a ``REFERENCE`` data type with a
   limited scope. That is, a property

   .. code:: python

      p = caosdb.Property(name="Conductor", datatype="Person")

   will only accept those Entities as value which have a “Person”
   RecordType as a direct or indirect parent.

See also: :any:`Datatype<specification/Datatype>`.
