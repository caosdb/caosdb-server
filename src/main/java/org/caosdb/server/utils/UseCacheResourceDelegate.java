/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2023 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2023 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.caosdb.server.utils;

import org.caosdb.server.CaosDBServer;

/**
 * Basic implementation of the {@link UseCacheResource} interface.
 *
 * <p>This implementation uses a Delegator Pattern to link different UseCacheResources together.
 * This is uses to delegate the responsibility to the transaction as the final Object of the
 * delegation.
 *
 * @author Timm Fitschen <t.fitschen@indiscale.com>
 */
public class UseCacheResourceDelegate implements UseCacheResource {
  private boolean useCache = CaosDBServer.useCache();
  private UseCacheResource delegate = null;

  @Override
  public void setUseCache(boolean useCache) {
    if (this.delegate != null) {
      this.delegate.setUseCache(useCache);
    } else {
      this.useCache = useCache;
    }
  }

  @Override
  public boolean useCache() {
    if (delegate != null) {
      return delegate.useCache();
    }
    return CaosDBServer.useCache() && useCache;
  }

  @Override
  public void setUseCacheResourceDelegate(UseCacheResource delegate) {
    if (delegate == this) {
      this.delegate = null;
    } else {
      this.delegate = delegate;
    }
  }

  @Override
  public UseCacheResource getUseCacheResourceDelegate() {
    if (this.delegate != null) {
      return this.delegate;
    }
    return this;
  }
}
