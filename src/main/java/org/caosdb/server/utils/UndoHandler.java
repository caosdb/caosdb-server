/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.utils;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Iterator;

public class UndoHandler implements Undoable {
  private final Deque<Undoable> us = new ArrayDeque<Undoable>();

  @Override
  public void undo() {
    final Iterator<Undoable> descendingIterator = this.us.descendingIterator();
    while (descendingIterator.hasNext()) {
      descendingIterator.next().undo();
    }
  }

  public void append(final Undoable u) {
    if (u != null) {
      this.us.push(u);
    }
  }

  @Override
  public void cleanUp() {
    final Iterator<Undoable> descendingIterator = this.us.descendingIterator();
    while (descendingIterator.hasNext()) {
      final Undoable next = descendingIterator.next();
      next.cleanUp();
      descendingIterator.remove();
    }
    this.us.clear();
  }
}
