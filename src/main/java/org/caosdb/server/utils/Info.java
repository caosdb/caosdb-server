/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.utils;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.LinkedList;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.caosdb.datetime.UTCDateTime;
import org.caosdb.server.CaosDBServer;
import org.caosdb.server.FileSystem;
import org.caosdb.server.database.DatabaseAccessManager;
import org.caosdb.server.database.access.Access;
import org.caosdb.server.database.backend.transaction.GetInfo;
import org.caosdb.server.database.backend.transaction.SyncStats;
import org.caosdb.server.database.exceptions.TransactionException;
import org.caosdb.server.database.proto.ProtoInfo;
import org.caosdb.server.entity.Message;
import org.caosdb.server.transaction.TransactionInterface;
import org.jdom2.Element;

public class Info extends AbstractObservable implements Observer, TransactionInterface {

  private static Info instance = new Info();
  public static final String SYNC_DATABASE_EVENT = "SyncDatabaseEvent";
  private final Access access;
  public Logger logger = LogManager.getLogger(getClass());
  private UTCDateTime timestamp;

  @Override
  public boolean notifyObserver(final String e, final Observable o) {
    try {
      syncDatabase();
    } catch (final Exception exc) {
      logger.error(exc);
    }
    return true;
  }

  private Info() {
    this.timestamp = UTCDateTime.SystemMillisToUTCDateTime(System.currentTimeMillis());
    this.access = DatabaseAccessManager.getInfoAccess(this);
    try {
      syncDatabase();
    } catch (final Exception exc) {
      logger.error(exc);
    }
  }

  public static Info getInstance() {
    return instance;
  }

  private Long lastSync = 0L;
  private static File dropOffBox = null;
  private static int filesCount = -1;
  private static int propertiesCount = -1;
  private static int recordsCount = -1;
  private static int recordTypesCount = -1;
  private static int tmpFilesCount;
  private static String fssize = "";

  public static Integer getRecordsCount() throws Exception {
    return recordsCount;
  }

  public static final Integer getFilesCount() {
    return filesCount;
  }

  public static final Integer getPropertiesCount() {
    return propertiesCount;
  }

  public static final Integer getRecordTypesCount() {
    return recordTypesCount;
  }

  /**
   * Utility function that takes an array of file objects and returns a linked list containing XML
   * representations of all files within each contained directory. All files in the array that are
   * directories are recursively traversed. This function is used for files within the drop off box
   * only.
   *
   * @param files An array of file objects.
   * @return A linked list containing XML elements. The path attribute of these elements is set to
   *     the absolute path of the filenames where the path to the drop off box is removed at the
   *     beginning.
   * @fixme Should check if the files are inside the DropOffBox path.
   */
  private static LinkedList<Element> getFlatList(final File[] files) {
    try {
      final LinkedList<Element> ret = new LinkedList<Element>();
      for (final File file : files) {
        if (file.isDirectory()) {
          ret.addAll(getFlatList(file.listFiles()));
        } else {
          final Element element = new Element("file");
          final String tempPath =
              file.getCanonicalPath().substring(dropOffBox.getCanonicalPath().length() + 1);
          element.setAttribute("path", tempPath);
          ret.add(element);
        }
      }
      return ret;
    } catch (final IOException e) {
      e.printStackTrace();
    }
    return null;
  }

  private static LinkedList<Element> getTree(final File[] files) {
    final LinkedList<Element> ret = new LinkedList<Element>();
    for (final File file : files) {
      if (file.isDirectory()) {
        final Element element = new Element("dir");
        element.setAttribute("path", file.getName());
        element.addContent(getTree(file.listFiles()));
        ret.add(element);
      } else {
        final Element element = new Element("file");
        element.setAttribute("path", file.getName());
        ret.add(element);
      }
    }
    return ret;
  }

  public long timeSinceSync() {
    long time = System.currentTimeMillis();
    return time - lastSync;
  }

  public void syncDatabase() throws Exception {
    synchronized (lastSync) {
      if (timeSinceSync() < 10000) {
        return;
      }
      lastSync = System.currentTimeMillis();
    }

    final ProtoInfo i = new ProtoInfo();
    final GetInfo t = new GetInfo(i);
    execute(t, access);
    filesCount = i.filesCount;
    recordsCount = i.recordsCount;
    recordTypesCount = i.recordTypesCount;
    propertiesCount = i.propertiesCount;
    fssize = Utils.getReadableByteSize(i.fssize);
    tmpFilesCount = countTmpFiles();

    notifyObservers(SYNC_DATABASE_EVENT);
  }

  private static int countTmpFiles() throws IOException {
    final File tmpdir = new File(FileSystem.getTmp());
    return tmpdir.list().length;
  }

  public static Element toElement() throws Exception {
    return toElement(false);
  }

  /**
   * Generates an XML element showing the information for the /Info resource in CaosDB.
   *
   * @return An XML element containing:
   *     <ul>
   *       <li>The number of records
   *       <li>The number of properties
   *       <li>The number of record types
   *       <li>The number of files
   *       <li>The total file size
   *       <li>The number of temp files
   *       <li>The path of the DropOffBox
   *       <li>A tree of files in the DropOffBox
   *     </ul>
   *     TODO: The error format for missing or not readable drop off box has to be specified.
   */
  public static Element toElement(final boolean tree) throws Exception, SQLException {
    if (filesCount == -1 && recordsCount == -1) {
      getInstance().syncDatabase();
    }
    dropOffBox = new File(FileSystem.getDropOffBox());
    final Element info = new Element("Stats");
    final Element counts = new Element("counts");
    counts.setAttribute("records", Integer.toString(recordsCount));
    counts.setAttribute("properties", Integer.toString(propertiesCount));
    counts.setAttribute("recordTypes", Integer.toString(recordTypesCount));
    counts.setAttribute("files", Integer.toString(filesCount));
    counts.setAttribute("fssize", fssize);
    counts.setAttribute("tmpfiles", Integer.toString(tmpFilesCount));
    if (CaosDBServer.isDebugMode()) {
      counts.setAttribute("debug", "true");
    }
    final Element e = new Element("dropOffBox");
    if (dropOffBox.isDirectory()) {
      if (dropOffBox.canRead()) {
        if (tree) {
          e.setAttribute("path", dropOffBox.getAbsolutePath());
          e.addContent(getTree(dropOffBox.listFiles()));
        } else {
          e.setAttribute("path", dropOffBox.getAbsolutePath());
          e.addContent(getFlatList(dropOffBox.listFiles()));
        }
      } else {
        // TODO: return a message that the DropOffBox is not readable.
      }
    } else {
      // TODO: This function should at least return a message that the DropOffBox is disabled or not
      // present.
    }
    info.addContent(counts);
    info.addContent(e);
    return info;
  }

  public void syncDatabase(final ServerStat stat) throws TransactionException, Message {

    final Access access = getInstance().access;

    final SyncStats t = new SyncStats(stat);
    execute(t, access);
  }

  @Override
  public void execute() throws Exception {
    syncDatabase();
  }

  @Override
  public UTCDateTime getTimestamp() {
    return timestamp;
  }
}
