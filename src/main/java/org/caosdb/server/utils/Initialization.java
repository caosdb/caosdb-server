/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2020 Indiscale GmbH <info@indiscale.com>
 * Copyright (C) 2020 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.utils;

import org.caosdb.datetime.UTCDateTime;
import org.caosdb.server.database.DatabaseAccessManager;
import org.caosdb.server.database.access.Access;
import org.caosdb.server.transaction.TransactionInterface;

public final class Initialization implements TransactionInterface, AutoCloseable {

  private Access access;
  private UTCDateTime timestamp;
  private static final Initialization instance = new Initialization();

  private Initialization() {
    this.timestamp = UTCDateTime.SystemMillisToUTCDateTime(System.currentTimeMillis());
    this.access = DatabaseAccessManager.getInitAccess(this);
  }

  public static final Initialization setUp() {
    return instance;
  }

  public final Access getAccess() {
    return this.access;
  }

  @Override
  public void execute() throws Exception {}

  @Override
  public void close() throws Exception {
    if (this.access != null) {
      this.access.release();
      this.access = null;
    }
  }

  @Override
  public UTCDateTime getTimestamp() {
    return timestamp;
  }
}
