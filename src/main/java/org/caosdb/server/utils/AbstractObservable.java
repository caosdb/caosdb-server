/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.utils;

import java.util.Iterator;
import org.eclipse.jetty.util.ConcurrentHashSet;

public abstract class AbstractObservable implements Observable {

  private ConcurrentHashSet<Observer> observers = null;

  @Override
  public boolean acceptObserver(final Observer o) {
    if (this.observers == null) {
      this.observers = new ConcurrentHashSet<Observer>();
    }
    return this.observers.add(o);
  }

  /**
   * @param e A String denoting the notification event.
   */
  @Override
  public void notifyObservers(final String e) {
    if (this.observers != null) {
      final Iterator<Observer> it = this.observers.iterator();
      while (it.hasNext()) {
        final Observer o = it.next();
        if (!o.notifyObserver(e, this)) {
          it.remove();
        }
      }
    }
  }

  public void removeAllObservers() {
    this.observers = null;
  }

  public boolean removeObserver(final Observer observer) {
    return this.observers.remove(observer);
  }
}
