/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2021 Indiscale GmbH <info@indiscale.com>
 * Copyright (C) 2021 Daniel Hornung <d.hornung@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.utils;

/**
 * The order of the EntityStatus values matters, in that everything later than "QUALIFIED" also
 * counts as qualified.
 *
 * <p>
 *
 * <ul>
 *   <li>IGNORE - This instance must be ignored during further processing.
 *   <li>UNQUALIFIED - This instance has been checked and is not qualified to be processed further
 *       (i.e. to be inserted, updated, deleted)
 *   <li>DELETED - This instance has been deleted recently.
 *   <li>NONEXISTENT - This instance has been called (via id or something) but it doesn't exist.
 *   <li>QUALIFIED - This instance has a provisional or final ID, represents a well-formed entity,
 *       and any referenced entities (by a reference property) have status QUALIFIED or VALID. Every
 *       status after this one also counts as QUALIFIED.
 *   <li>VALID - This instance has a final ID and has been synchronized with the database.
 * </ul>
 *
 * @author Timm Fitschen <t.fitschen@indiscale.com>
 */
public enum EntityStatus {
  IGNORE,
  UNQUALIFIED,
  DELETED,
  NONEXISTENT,
  QUALIFIED,
  VALID
}
