/**
 * ** header v3.0 This file is a part of the CaosDB Project.
 *
 * <p>Copyright (c) 2019 IndiScale GmbH Copyright (c) 2019 Daniel Hornung <d.hornung@indiscale.com>
 *
 * <p>This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * <p>This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * <p>You should have received a copy of the GNU Affero General Public License along with this
 * program. If not, see <https://www.gnu.org/licenses/>.
 *
 * <p>** end header
 */
package org.caosdb.server.utils;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.caosdb.server.CaosDBServer;
import org.caosdb.server.ServerProperties;
import org.caosdb.server.resource.AbstractCaosDBServerResource;
import org.caosdb.server.resource.Webinterface;
import org.restlet.Request;
import org.restlet.data.Reference;

/**
 * This class is responsible for two things:
 * <li>Resolving relative paths of files of the web interface to {@link File} objects.
 * <li>Generating URIs to public files of the web interface from relative string paths.
 *
 *     <p>Especially the {@link AbstractCaosDBServerResource} and the {@link Webinterface} use this
 *     class for these tasks.
 *
 * @author Timm Fitschen (t.fitschen@indiscale.com)
 */
public class WebinterfaceUtils {

  private Logger logger = LogManager.getLogger(this.getClass());
  public static final String DEFAULT_WEBUI_PUBLIC = "caosdb-webui/public";
  public static final String DEFAULT_BUILD_NUMBER_FILE = ".build_number";
  public static final String DEFAULT_ROUTE = "webinterface";
  private final String host;
  private final String publicDir;
  private final String route;
  private final Path buildNumberFile;
  private final String contextRoot;
  private String buildNumber = null;
  private long buildNumberDate;
  private static final Map<String, WebinterfaceUtils> instances = new HashMap<>();

  public static String getForwardedProto(Request request) {
    String scheme = null;
    String forwarded = request.getHeaders().getFirstValue("Forwarded", true);
    if (forwarded != null) {
      for (String directive : forwarded.split(";")) {
        String[] s = directive.split("=", 2);
        if (s.length == 2 && "proto".equalsIgnoreCase(s[0])) {
          scheme = s[1];
        }
      }
    }
    if (scheme == null) {
      scheme = request.getHeaders().getFirstValue("X-Forwarded-Proto", true);
    }
    return scheme;
  }

  /**
   * Retrieve an instance of {@link WebinterfaceUtils} for the request. The instance can be shared
   * with other callers.
   *
   * @param request
   * @return a shared instance of {@link WebinterfaceUtils}.
   */
  public static WebinterfaceUtils getInstance(Request request) {
    String hostStr = request.getHostRef().getHostIdentifier();
    String scheme = getForwardedProto(request);
    if (scheme != null) {
      hostStr = hostStr.replaceFirst("^" + request.getHostRef().getScheme(), scheme);
    }
    return getInstance(hostStr);
  }

  /**
   * Retrieve an instance of {@link WebinterfaceUtils} for the host. The instance can be shared with
   * other callers.
   *
   * @param host
   * @return a shared instance of {@link WebinterfaceUtils}.
   */
  public static WebinterfaceUtils getInstance(String host) {
    synchronized (instances) {
      WebinterfaceUtils ret = instances.get(host);
      if (ret == null) {
        ret = new WebinterfaceUtils(host);
        instances.put(host, ret);
      }
      return ret;
    }
  }

  /**
   * Create a utility class for creating references to resources of the web interface.
   *
   * @param host
   * @param contextRoot
   * @param publicDir
   * @param route
   * @param buildNumberFile
   */
  WebinterfaceUtils(
      String host, String contextRoot, String publicDir, String route, String buildNumberFile) {
    this.host = host;
    this.contextRoot =
        contextRoot != null ? contextRoot.replaceFirst("^/", "").replaceFirst("/$", "") : null;
    this.publicDir = publicDir;
    this.route = route;
    this.buildNumberFile = getPublicRootPath().resolve(buildNumberFile);
  }

  /**
   * Create a utility class for creating references to resources of the web interface.
   *
   * <p>The route to the web interface, the build number file and the public folder of the web
   * interface build directory are set to default values ({@link #DEFAULT_BUILD_NUMBER_FILE}, {@link
   * #DEFAULT_ROUTE}, and {@link #DEFAULT_WEBUI_PUBLIC}).
   *
   * <p>The contextRoute defaults to the server property {@link ServerProperties.KEY_CONTEXT_ROOT}.
   *
   * @param host - the host of the web interface.
   * @see {@link WebinterfaceUtils#WebinterfaceUtils(String, String, String, String)}.
   */
  WebinterfaceUtils(Reference host) {
    this(host.getHostIdentifier());
  }

  /**
   * Create a utility class for creating references to resources of the web interface.
   *
   * <p>The route to the web interface, the build number file and the public folder of the web
   * interface build directory are set to default values ({@link #DEFAULT_BUILD_NUMBER_FILE}, {@link
   * #DEFAULT_ROUTE}, and {@link #DEFAULT_WEBUI_PUBLIC}).
   *
   * <p>The contextRoute defaults to the server property {@link ServerProperties.KEY_CONTEXT_ROOT}.
   *
   * @param host - the host of the web interface.
   * @see {@link WebinterfaceUtils#WebinterfaceUtils(String, String, String, String)}.
   */
  WebinterfaceUtils(String host) {
    this(
        host,
        CaosDBServer.getServerProperty(ServerProperties.KEY_CONTEXT_ROOT),
        DEFAULT_WEBUI_PUBLIC,
        DEFAULT_ROUTE,
        DEFAULT_BUILD_NUMBER_FILE);
  }

  /**
   * Return the server root reference.
   *
   * <p>The server root is composed of the scheme (https://) the host name (example.com) the port if
   * necessary (:10443) and the context root of the application (beta).
   *
   * @return the server root reference e.g. https://example.com:10443/beta.
   */
  public String getServerRootURI() {
    if (contextRoot == null || contextRoot.length() == 0) {
      return host;
    }
    return host + "/" + contextRoot;
  }

  /**
   * Return the root reference for the web interface.
   *
   * <p>The root reference for the web interface is the server root reference ({@link
   * #getServerRootURI()}) plus the route to the resource which routes to the individual resources
   * of the web interface ({@link Webinterface}).
   *
   * @return root reference for the web interface.
   */
  public String getWebinterfaceRootURI() {
    return getServerRootURI() + "/" + this.route + "/" + getBuildNumber();
  }

  /**
   * Return the absolute path of the root of the public directory of the webui.
   *
   * @return Public directory of webui.
   */
  public Path getPublicRootPath() {
    return Paths.get(this.publicDir).toAbsolutePath();
  }

  /**
   * return the Path of the build number file.
   *
   * @return
   */
  public Path getBuildNumberFile() {
    return this.buildNumberFile;
  }

  /**
   * Determine the build number of the webui. Returns null if the build number could not be
   * determined.
   *
   * <p>Because opening and reading the {@link #buildNumberFile} content is expensive, the
   * buildNumber is cached.
   *
   * <p>The cached buildNumber invalidates when the files modified time stamp is younger than the
   * {@link #buildNumberDate}. The validity of the cache is checked once every minute with a {@link
   * CronJob}.
   *
   * @return Build number or null.
   */
  public String getBuildNumber() {
    validateBuildNumber();
    if (this.buildNumber == null) {
      this.buildNumberDate = System.currentTimeMillis();
      this.buildNumber = readBuildNumber();
    }
    return this.buildNumber;
  }

  /**
   * Return the latest buildNumber, freshly read from the buildNumberfile.
   *
   * @return fresh build number.
   */
  public String readBuildNumber() {
    Path buildNumberFile = getBuildNumberFile();
    StringBuilder contentBuilder = new StringBuilder();
    try (Stream<String> stream = Files.lines(buildNumberFile, StandardCharsets.UTF_8)) {
      stream.forEach(s -> contentBuilder.append(s));
    } catch (IOException e) {
      logger.error(e);
      return null;
    }
    return contentBuilder.toString();
  }

  /**
   * Return the public file or null if the file does not exists or is not in the public directory of
   * the web interface.
   *
   * <p>This method is used to resolve requests for files of the web interface.
   *
   * @param path
   * @return The file or null
   */
  public File getPublicFile(String path) {
    Path publicFilePath = getPublicFilePath(path);
    if (publicFilePath != null) {
      File publicFile = publicFilePath.toFile();
      if (publicFile.exists() && !publicFile.isDirectory() && publicFile.canRead())
        return publicFile;
    }
    return null;
  }

  /**
   * Return the absolute path to the public file denoted by the path.
   *
   * <p>This method is used to resolve requests for files of the web interface.
   *
   * @param path - path to the file of the web interface, relative to the public directory of the
   *     web interface.
   * @return The absolute path of the web interface file.
   */
  public Path getPublicFilePath(String path) {
    Path root = getPublicRootPath();
    Path resolved = root.resolve(path).toAbsolutePath().normalize();
    if (resolved.startsWith(root)) {
      return resolved;
    }
    return null;
  }

  /**
   * Return a URI to a public file of the web interface.
   *
   * <p>This method is used to generate references for *.xsl files or other resources of the web
   * interface.
   *
   * @param publicFile - the path of the public file, relative to the public directory of the web
   *     interface.
   * @return a URI for the public file of the web interface.
   */
  public String getWebinterfaceURI(String publicFile) {
    return getWebinterfaceRootURI() + "/" + publicFile;
  }

  /**
   * Check the validity of the buildNumber and invalidate the buildNumber if the buildNumberDate is
   * older than the {@link File#lastModified} time stamp of the buildNumberFile.
   */
  public void validateBuildNumber() {
    if (buildNumber != null
        && getBuildNumberFile().toFile().lastModified() > this.buildNumberDate) {
      this.buildNumber = null;
    }
  }
}
