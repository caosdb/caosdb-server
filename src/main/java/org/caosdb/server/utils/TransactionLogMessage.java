/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.utils;

import java.util.TimeZone;
import org.caosdb.datetime.UTCDateTime;
import org.caosdb.server.entity.EntityInterface;
import org.jdom2.Element;

public class TransactionLogMessage {

  private final String transaction;
  private final String username;
  private final UTCDateTime dt;
  private final EntityInterface entity;

  @Override
  public String toString() {
    return this.transaction
        + " of "
        + this.entity.toString()
        + ":"
        + this.username
        + " on "
        + this.dt.toDateTimeString(TimeZone.getDefault());
  }

  public TransactionLogMessage(
      final String transaction,
      final EntityInterface entity,
      final String username,
      final UTCDateTime dateTime) {
    this.entity = entity;
    this.transaction = transaction;
    this.username = username;
    this.dt = dateTime;
  }

  public TransactionLogMessage xmlAppendTo(final Element root) {
    final Element e = new Element("History");
    if (this.transaction != null) {
      e.setAttribute("transaction", this.transaction);
    }
    if (this.username != null) {
      e.setAttribute("username", this.username);
    }
    if (this.dt != null) {
      e.setAttribute("datetime", this.dt.toDateTimeString(TimeZone.getDefault()));
    }
    root.addContent(e);
    return this;
  }
}
