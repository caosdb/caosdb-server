/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.utils;

import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.Set;
import org.caosdb.server.jobs.FlagJob;
import org.caosdb.server.jobs.Job;
import org.caosdb.server.jobs.JobAnnotation;
import org.caosdb.server.transaction.TransactionInterface;
import org.jdom2.Element;
import org.reflections.Reflections;

public class FlagInfo {

  public static ArrayList<JobAnnotation> as = scan();

  public static ArrayList<JobAnnotation> scan() {
    final ArrayList<JobAnnotation> as = new ArrayList<JobAnnotation>();
    final Reflections jobPackage = new Reflections("org.caosdb.server.jobs.core");
    final Set<Class<? extends FlagJob>> allClassesSet = jobPackage.getSubTypesOf(FlagJob.class);
    for (final Class<? extends Job> c : allClassesSet) {
      for (final Annotation a : c.getAnnotations()) {
        if (a instanceof JobAnnotation) {
          as.add((JobAnnotation) a);
        }
      }
    }
    return as;
  }

  public static Element toElement() {
    final Element ret = new Element("Flags");
    for (final JobAnnotation a : as) {
      final Element aElem = new Element(a.flag());
      aElem.setAttribute("description", a.description());
      aElem.setAttribute("stage", a.stage().toString());
      if (!a.transaction().equals(TransactionInterface.class)) {
        aElem.setAttribute("transaction", a.transaction().getSimpleName());
      }
      final String[] array = a.values();
      if (array.length > 0) {
        final StringBuilder values = new StringBuilder();
        values.append("[").append(array[0]);
        for (int i = 1; i < array.length; i++) {
          values.append(',').append(array[i]);
        }
        values.append("]");
        aElem.setAttribute("values", values.toString());
      }
      if (!a.defaultValue().equals("")) {
        aElem.setAttribute("defaultValue", a.defaultValue());
      }
      ret.addContent(aElem);
    }
    return ret;
  }
}
