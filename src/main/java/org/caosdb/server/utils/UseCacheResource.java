/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2023 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2023 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.caosdb.server.utils;

/**
 * Interface which indicates that a class can turn on and off the caching of the current transaction
 * and inform the caller about the state of the transactions's caching.
 *
 * @author Timm Fitschen <t.fitschen@indiscale.com>
 */
public interface UseCacheResource {

  public default boolean useCache() {
    return getUseCacheResourceDelegate().useCache();
  }

  public default void setUseCache(boolean useCache) {
    getUseCacheResourceDelegate().setUseCache(useCache);
  }

  public void setUseCacheResourceDelegate(UseCacheResource delegate);

  public UseCacheResource getUseCacheResourceDelegate();
}
