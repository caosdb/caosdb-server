package org.caosdb.server.scripting;

import org.caosdb.server.accessControl.ACMPermissions;

public class ScriptingPermissions extends ACMPermissions {

  public static final String PATH_PARAMETER = "?PATH?";

  public ScriptingPermissions(String permission, String description) {
    super(permission, description);
  }

  public String toString(String path) {
    return toString().replace(PATH_PARAMETER, path.replace("/", ":"));
  }

  private static final ScriptingPermissions execution =
      new ScriptingPermissions(
          "SCRIPTING:EXECUTE:" + PATH_PARAMETER,
          "Permission to execute a server-side script under the given path. Note that, for utilizing the wild cards feature, you have to use ':' as path separator. E.g. 'SCRIPTING:EXECUTE:my_scripts:*' would be the permission to execute all executables below the my_scripts directory.");

  public static final String PERMISSION_EXECUTION(final String call) {
    return execution.toString(call);
  }

  public static String init() {
    return ScriptingPermissions.class.getSimpleName();
  }
}
