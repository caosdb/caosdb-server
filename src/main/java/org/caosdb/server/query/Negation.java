/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.query;

import static org.caosdb.server.database.backend.implementation.MySQL.DatabaseUtils.bytes2UTF8;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.apache.shiro.subject.Subject;
import org.caosdb.server.database.access.Access;
import org.caosdb.server.query.Query.QueryException;
import org.jdom2.Element;

public class Negation implements EntityFilterInterface, QueryInterface {

  /**
   * Allows to create a Negation with: EntityFilterInterface filter = ... NegationFactory nf = new
   * NegationFactory(); nf.add(filter); EntityFilter negation = nf.getNegation(); Now, negation
   * negates filter.
   *
   * @author tf
   */
  public static class NegationFactory extends EntityFilterContainer {

    private Negation neg = null;

    public NegationFactory() {}

    public Negation getNegation() {
      if (this.neg == null) {
        this.neg = new Negation(getFilters().poll());
      }
      return this.neg;
    }
  }

  private String targetSet = null;
  private final int targetSetCount = -1;
  private QueryInterface query = null;

  /*
   * The filter which is to be negated.
   */
  private final EntityFilterInterface filter;

  public Negation(final EntityFilterInterface f) {
    this.filter = f;
  }

  public EntityFilterInterface getFilter() {
    return this.filter;
  }

  @Override
  public void apply(final QueryInterface query) throws QueryException {
    this.query = query;
    final long t1 = System.currentTimeMillis();
    try {
      // generate empty temporary targetSet if query.getTargetSet() is not
      // empty anyways.
      final PreparedStatement callInitEmptyTarget =
          getConnection().prepareStatement("call initDisjunctionFilter(?)");
      callInitEmptyTarget.setBoolean(1, query.isVersioned());
      final ResultSet initEmptyTargetResultSet = callInitEmptyTarget.executeQuery();
      if (initEmptyTargetResultSet.next()) {
        this.targetSet = bytes2UTF8(initEmptyTargetResultSet.getBytes("newTableName"));
      }
      initEmptyTargetResultSet.close();

      this.filter.apply(this);

      if (query.getTargetSet() == null) {
        // intersect temporary targetSet with query.getSourceSet()
        final CallableStatement callFinishConjunctionFilter =
            query.getConnection().prepareCall("call calcDifference(?,?,?)");
        callFinishConjunctionFilter.setString(1, query.getSourceSet());
        callFinishConjunctionFilter.setString(2, this.targetSet);
        callFinishConjunctionFilter.setBoolean(3, this.isVersioned());
        callFinishConjunctionFilter.execute();
      } else {
        // merge temporary targetSet with query.getTargetSet()
        final CallableStatement callFinishConjunctionFilter =
            query.getConnection().prepareCall("call calcComplementUnion(?,?,?,?)");
        callFinishConjunctionFilter.setString(1, query.getTargetSet());
        callFinishConjunctionFilter.setString(2, this.targetSet);
        callFinishConjunctionFilter.setString(3, query.getSourceSet());
        callFinishConjunctionFilter.setBoolean(4, this.isVersioned());
        callFinishConjunctionFilter.execute();
      }
    } catch (final SQLException e) {
      throw new QueryException(e);
    }
    query.addBenchmark(this.getClass().getSimpleName(), System.currentTimeMillis() - t1);
  }

  @Override
  public String getSourceSet() {
    return this.query.getSourceSet();
  }

  @Override
  public Connection getConnection() {
    return this.query.getConnection();
  }

  @Override
  public int getTargetSetCount() {
    return this.targetSetCount;
  }

  @Override
  public Element toElement() {
    final Element ret = new Element("Negation");
    ret.addContent(this.filter.toElement());
    return ret;
  }

  @Override
  public Query getQuery() {
    return this.query.getQuery();
  }

  @Override
  public String getTargetSet() {
    return this.targetSet;
  }

  @Override
  public Access getAccess() {
    return this.query.getAccess();
  }

  @Override
  public Subject getUser() {
    return this.query.getUser();
  }

  @Override
  public void addBenchmark(final String str, final long time) {
    this.query.addBenchmark(this.getClass().getSimpleName() + "." + str, time);
  }

  @Override
  public boolean isVersioned() {
    return this.query.isVersioned();
  }

  @Override
  public String getCacheKey() {
    StringBuilder sb = new StringBuilder();
    sb.append("NEG(");
    if (this.filter != null) sb.append(this.filter.getCacheKey());
    sb.append(")");
    return sb.toString();
  }
}
