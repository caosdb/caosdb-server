/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.query;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;
import org.caosdb.datetime.Date;
import org.caosdb.datetime.DateTimeFactory2;
import org.caosdb.datetime.Interval;
import org.caosdb.datetime.UTCDateTime;
import org.caosdb.server.accessControl.Principal;
import org.caosdb.server.accessControl.UserSources;
import org.caosdb.server.query.Query.QueryException;
import org.jdom2.Element;

public class TransactionFilter implements EntityFilterInterface {

  public static final String INSERTION = "Insert";
  public static final String UPDATE = "Update";
  public static final String TODAY = "Today";

  public static final Transactor eq_currentTransactor() {
    return new Transactor("=", Transactor.ME);
  }

  public static final Transactor neq_currentTransactor() {
    return new Transactor("!=", Transactor.ME);
  }

  public static final Transactor eq_foreignTransactor(final Query.Pattern user) {
    return new Transactor("=", user);
  }

  public static final Transactor neq_foreignTransactor(final Query.Pattern user) {
    return new Transactor("!=", user);
  }

  public static final Transactor eq_foreignTransactor(final String user) {
    return eq_foreignTransactor(new Query.Pattern(user, Query.Pattern.TYPE_NORMAL));
  }

  public static final Transactor neq_foreignTransactor(final String user) {
    return neq_foreignTransactor(new Query.Pattern(user, Query.Pattern.TYPE_NORMAL));
  }

  public static final class Transactor {

    private static final Query.Pattern ME = new Query.Pattern(null, 0);

    private final String operator;
    private final Query.Pattern user;

    private Transactor(final String operator, final Query.Pattern user) {
      this.operator = operator;
      this.user = user;
    }

    String getOperator() {
      return this.operator;
    }

    boolean isMe() {
      return this.user == ME;
    }

    Query.Pattern getPattern() {
      return this.user;
    }

    @Override
    public String toString() {
      return "Transactor(" + this.user + "," + this.operator + ")";
    }
  }

  public TransactionFilter(
      final String type,
      final Transactor transactor,
      final String time,
      final String timeOperator) {
    this.transactor = transactor;
    this.transactionTime = time;
    this.transactionType = type;
    this.transactionTimeOperator = timeOperator;
  }

  private final Transactor transactor;
  private final String transactionTime;
  private final String transactionTimeOperator;
  private final String transactionType;

  @Override
  public void apply(final QueryInterface query) throws QueryException {
    final long t1 = System.currentTimeMillis();

    final Connection connection = query.getConnection();

    String realm = null;
    String userName = null;
    final Interval dt;

    if (this.transactionTime != null) {
      if (this.transactionTime.equalsIgnoreCase(TODAY)) {

        dt = Date.today();

      } else {
        try {
          dt = (Interval) DateTimeFactory2.valueOf(this.transactionTime);
        } catch (final ClassCastException e) {
          throw new QueryException("Transaction time must be a SemiCompleteDateTime.");
        } catch (final IllegalArgumentException e) {
          throw new QueryException(e.getMessage());
        }
      }
    } else {
      dt = null;
    }

    if (this.transactor != null) {
      if (this.transactor.isMe()) {
        // set name
        userName = ((Principal) query.getUser().getPrincipal()).getUsername();
        realm = ((Principal) query.getUser().getPrincipal()).getRealm();
      } else {
        if (this.transactor != null
            && this.transactor.getPattern() != null
            && this.transactor.getPattern().type != Query.Pattern.TYPE_NORMAL) {
          throw new UnsupportedOperationException(
              "Regular expression and like patterns are not implemented for transactor names yet.");
        }

        // set name
        userName = this.transactor.getPattern().toString();
        realm = UserSources.guessRealm(userName);
      }
    }

    // applyTransactionFilter(resultSet,transaction,operator_u,userid,username,date1,time1,date2,time2,operator_t)
    try {
      final CallableStatement prepareCall =
          connection.prepareCall("call applyTransactionFilter(?,?,?,?,?,?,?,?,?,?,?)");

      // set result set
      prepareCall.setString(1, query.getSourceSet());
      prepareCall.setString(2, query.getTargetSet());

      // set transaction type
      prepareCall.setString(3, this.transactionType);

      // set operator_u
      if (this.transactor != null) {
        prepareCall.setString(4, this.transactor.getOperator());
      } else {
        prepareCall.setNull(4, Types.CHAR);
      }

      // user
      if (userName != null) {
        // set realm+userName
        prepareCall.setString(5, realm);
        prepareCall.setString(6, userName);
      } else {
        // set userName to null
        prepareCall.setNull(5, Types.VARCHAR);
        prepareCall.setNull(6, Types.VARCHAR);
      }

      // set interval
      if (dt != null) {
        final UTCDateTime ilb = dt.getInclusiveLowerBound();
        final UTCDateTime eub = dt.getExclusiveUpperBound();

        prepareCall.setLong(7, ilb.getUTCSeconds());
        if (ilb.hasNanoseconds()) {
          prepareCall.setInt(8, ilb.getNanoseconds());
        } else {
          prepareCall.setNull(8, Types.INTEGER);
        }
        if (eub != null) {
          prepareCall.setLong(9, eub.getUTCSeconds());
          if (eub.hasNanoseconds()) {
            prepareCall.setInt(10, eub.getNanoseconds());
          } else {
            prepareCall.setNull(10, Types.INTEGER);
          }
          // interval'
        } else {
          prepareCall.setNull(9, Types.BIGINT);
          prepareCall.setNull(10, Types.INTEGER);
        }

        prepareCall.setString(11, transactionTimeOperator);
      } else {
        // ilb_sec, ilb_nanos, eub_sec, eub_nanos, operator_t
        prepareCall.setNull(7, Types.BIGINT);
        prepareCall.setNull(8, Types.INTEGER);
        prepareCall.setNull(9, Types.BIGINT);
        prepareCall.setNull(10, Types.INTEGER);
        prepareCall.setNull(11, Types.CHAR);
      }

      prepareCall.execute();
      prepareCall.close();
    } catch (final SQLException e) {
      throw new QueryException(e);
    }
    query.addBenchmark(this.getClass().getSimpleName(), System.currentTimeMillis() - t1);
  }

  @Override
  public Element toElement() {
    final Element ret = new Element("TransactionFilter");
    if (this.transactor != null) {
      if (this.transactor.isMe()) {
        ret.setAttribute("transactor", "me");
      } else {
        ret.setAttribute("transactor", this.transactor.getPattern().toString());
      }
      ret.setAttribute("operator", this.transactor.getOperator());
    }
    if (this.transactionTime != null) {
      ret.setAttribute("transactionTime", this.transactionTime);
    }
    if (this.transactionType != null) {
      ret.setAttribute("transactionType", this.transactionType);
    }
    return ret;
  }

  @Override
  public String toString() {
    return "TRANS("
        + this.transactionType
        + ","
        + this.transactionTimeOperator
        + ","
        + this.transactionTime
        + ","
        + this.transactor
        + ")";
  }

  @Override
  public String getCacheKey() {
    return toString();
  }
}
