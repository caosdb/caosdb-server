/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.query;

import static java.sql.Types.VARCHAR;
import static org.caosdb.server.database.backend.implementation.MySQL.DatabaseUtils.bytes2UTF8;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.apache.shiro.subject.Subject;
import org.caosdb.server.database.access.Access;
import org.caosdb.server.query.Query.QueryException;
import org.jdom2.Element;

public class SubProperty implements QueryInterface, EntityFilterInterface {
  private final EntityFilterInterface filter;
  private final int targetSetCount = -1;
  private String sourceSet = null;
  private final String targetSet = null;
  private QueryInterface query = null;

  public SubProperty(final EntityFilterInterface f) {
    this.filter = f;
  }

  public EntityFilterInterface getFilter() {
    return this.filter;
  }

  @Override
  public void apply(final QueryInterface query) throws QueryException {
    apply(query, query.getTargetSet(), null, null);
  }

  public void apply(
      final QueryInterface query,
      final String sourceSet,
      final String properties,
      final String refids)
      throws QueryException {
    final long t1 = System.currentTimeMillis();
    this.query = query;
    try {
      // initSubProperty(sourceSet, properties, refids)
      final CallableStatement callInitSubProperty =
          getConnection().prepareCall("call initSubProperty(?,?,?)");
      if (sourceSet != null) {
        callInitSubProperty.setString(1, sourceSet); // sourceSet
      } else {
        callInitSubProperty.setString(1, query.getSourceSet()); // sourceSet
      }
      if (properties != null) {
        callInitSubProperty.setString(2, properties);
      } else {
        callInitSubProperty.setNull(2, VARCHAR);
      }
      if (refids != null) {
        callInitSubProperty.setString(3, refids);
      } else {
        callInitSubProperty.setNull(3, VARCHAR);
      }
      final ResultSet rs = callInitSubProperty.executeQuery();
      if (rs.next()) {
        this.sourceSet = bytes2UTF8(rs.getBytes("list"));

        this.filter.apply(this);

        getQuery().filterIntermediateResult(this.sourceSet);

        final CallableStatement callFinishSubProperty =
            getConnection().prepareCall("call finishSubProperty(?,?,?,?)");
        callFinishSubProperty.setString(1, query.getSourceSet()); // sourceSet of parent query
        if (query.getTargetSet() != null) { // targetSet
          callFinishSubProperty.setString(2, query.getTargetSet());
        } else {
          callFinishSubProperty.setNull(2, VARCHAR);
        }
        callFinishSubProperty.setString(3, this.sourceSet); // sub query sourceSet
        callFinishSubProperty.setBoolean(4, this.isVersioned());
        callFinishSubProperty.execute();
        callFinishSubProperty.close();
      } else {
        callInitSubProperty.close();
        throw new QueryException("initSubProperty should return a string!");
      }
    } catch (final SQLException e) {
      throw new QueryException(e);
    }
    query.addBenchmark(this.getClass().getSimpleName(), System.currentTimeMillis() - t1);
  }

  @Override
  public Element toElement() {
    final Element ret = new Element("SubQuery");
    ret.addContent(this.filter.toElement());
    return ret;
  }

  @Override
  public String toString() {
    return "SUBPROPERTY()";
  }

  @Override
  public String getSourceSet() {
    return this.sourceSet;
  }

  @Override
  public Connection getConnection() {
    return this.query.getConnection();
  }

  @Override
  public int getTargetSetCount() {
    return this.targetSetCount;
  }

  @Override
  public Query getQuery() {
    return this.query.getQuery();
  }

  @Override
  public String getTargetSet() {
    return this.targetSet;
  }

  @Override
  public Access getAccess() {
    return this.query.getAccess();
  }

  @Override
  public Subject getUser() {
    return this.query.getUser();
  }

  @Override
  public void addBenchmark(final String str, final long time) {
    this.query.addBenchmark(this.getClass().getSimpleName() + "." + str, time);
  }

  @Override
  public boolean isVersioned() {
    return this.query.isVersioned();
  }

  @Override
  public String getCacheKey() {
    StringBuilder sb = new StringBuilder();
    sb.append("SUB(");
    if (this.filter != null) sb.append(this.filter.getCacheKey());
    return sb.toString();
  }
}
