/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2019-2021 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2019-2021 Timm Fitschen <t.fitschen@indiscale.com>
 * Copyright (C) 2020 Florian Spreckelsen <f.spreckelsen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
parser grammar CQLParser;

options { tokenVocab = CQLLexer; }

@header {
    import java.util.LinkedList;
    import java.util.List;
}

/**
 * This is the root of the CQL grammar.
 */
cq returns [Query.Type t, List<Query.Selection> s, Query.Pattern e, Query.Role r, EntityFilterInterface filter, VersionFilter v]
    @init{
        $s = null;
        $e = null;
        $r = null;
        $v = VersionFilter.UNVERSIONED;
        $filter = null;
    }
:

    (
       SELECT prop_sel {$s = $prop_sel.s;} FROM {$t = Query.Type.SELECT;}
       | FIND {$t = Query.Type.FIND;}
       | COUNT {$t = Query.Type.COUNT;})
    (version {$v = $version.v;})?
    ( role {$r = $role.r;} )?
    (
        entity_filter {$filter = $entity_filter.filter;}
        |
        entity WHITE_SPACE?{$e = $entity.ep;}
        (
            entity_filter {$filter = $entity_filter.filter;}
        )?
    )?
    EOF
;

/**
 * For versioned queries.
 */
version returns [VersionFilter v]
    @init{
        $v = null;
    }
:
    ANY_VERSION_OF {$v = VersionFilter.ANY_VERSION;}
;

/**
 * The comma-separated list of selected (sub)properties of a SELECT query.
 */
prop_sel returns [List<Query.Selection> s]
    @init{
        $s = new LinkedList<Query.Selection>();
    }
:
    prop_subsel {$s.add($prop_subsel.sub);}
    (SELECT_COMMA prop_subsel {$s.add($prop_subsel.sub);})*
;

/**
 * The (sub-)selections (e.g. geolocation.longitude) of a SELECT query.
 */
prop_subsel returns [Query.Selection sub]:
    selector_txt {$sub = new Query.Selection($selector_txt.text);}
    (
        SELECT_DOT s=prop_subsel {$sub.setSubSelection($s.sub);}
    )?
;

/**
 * Rule for the allowed characters of a prop_subsel.
 */
selector_txt:
    (
        SELECT_DOUBLE_QUOTE
        ( SELECT_DOUBLE_QUOTE_TXT | SELECT_DOUBLE_QUOTE_ESCAPED )*
        SELECT_DOUBLE_QUOTE_END
    ) | (
        SELECT_SINGLE_QUOTE
        ( SELECT_SINGLE_QUOTE_TXT | SELECT_SINGLE_QUOTE_ESCAPED )*
        SELECT_SINGLE_QUOTE_END
    )
    |
    ( SELECTOR_TXT | SELECT_ESCAPED )+
;

/**
 * The entity role.
 */
role returns [Query.Role r]:
    RECORDTYPE {$r = Query.Role.RECORDTYPE;}
    | RECORD {$r = Query.Role.RECORD;}
    | PROPERTY {$r = Query.Role.PROPERTY;}
    | FILE {$r = Query.Role.FILE;}
    | QUERYTEMPLATE {$r = Query.Role.QUERYTEMPLATE;}
    | ENTITY {$r = Query.Role.ENTITY;}
;

/**
 * The filters of a FIND, SELECT, or COUNT query.
 */
entity_filter returns [EntityFilterInterface filter]
    @init{
        $filter = null;
    }
:
    which_exp
    (
        (
            LPAREN WHITE_SPACE?
            (HAS_A (PROPERTY)?)?
            (
                filter_expression {$filter = $filter_expression.efi;}
                | conjunction {$filter = $conjunction.c;}
                | disjunction {$filter = $disjunction.d;}
            )
            RPAREN
        ) | (
            filter_expression {$filter = $filter_expression.efi;}
            | conjunction {$filter = $conjunction.c;}
            | disjunction {$filter = $disjunction.d;}
        )
    )?
;

/**
 * WHICH keyword and syntactic sugar.
 */
which_exp:
    WHICH (HAS_A (PROPERTY)?)?
    | HAS_A (PROPERTY)?
    | WITH_A (PROPERTY)?
    | WHERE
    | DOT WHITE_SPACE?
;

/**
 * Collection of different filters.
 */
filter_expression returns [EntityFilterInterface efi]
:
    backreference (subproperty {((Backreference) $backreference.ref).setSubProperty($subproperty.subp);})? {$efi = $backreference.ref;}
    | idfilter {$efi=$idfilter.filter;}
    | storedat {$efi=$storedat.filter;}
    | transaction {$efi=$transaction.filter;}
    | ( pov (subproperty {((POV) $pov.filter).setSubProperty($subproperty.subp);})? {$efi = $pov.filter;} )
    | subproperty {$efi=$subproperty.subp;}
    | negation {$efi=$negation.n;}
;

/**
 * ID Filter (e.g. id > 10123).
 */
idfilter returns [IDFilter filter] locals [String o, String v, String a]
@init{
    $a = null;
    $o = null;
    $v = null;
}
@after{
    $filter = new IDFilter($o,$v,$a);
}
:
    (minmax {$a=$minmax.agg;})??
    ID
    (
        OPERATOR {$o = $OPERATOR.text;}
        WHITE_SPACE?
        value {$v = $value.str;}
    )?
;

/**
 * Transaction filter (e.g INSERTED BY ME).
 */
transaction returns [TransactionFilter filter] locals [String type, TransactionFilter.Transactor user, String time, String time_op]
@init{
    $time = null;
    $user = null;
    $type = null;
    $time_op = null;
}
@after{
    $filter = new TransactionFilter($type,$user,$time,$time_op);
}
:
    (
        ( INSERTED | CREATED ) {$type = TransactionFilter.INSERTION;}
        | ( UPDATED ) {$type = TransactionFilter.UPDATE;}
    )

    (
        transactor (transaction_time {$time = $transaction_time.tqp; $time_op = $transaction_time.op;})? {$user = $transactor.t;}
        | transaction_time (transactor {$user = $transactor.t;})? {$time = $transaction_time.tqp; $time_op = $transaction_time.op;}
    )
;

/**
 * The transactor (e.g. "user1", or "ME").
 */
transactor returns [TransactionFilter.Transactor t]
:
    BY
    (
        SOMEONE ELSE BUT ME {$t = TransactionFilter.neq_currentTransactor();}
        | SOMEONE ELSE BUT entity {$t = TransactionFilter.neq_foreignTransactor($entity.ep);}
        | SOMEONE ELSE BUT username {$t = TransactionFilter.neq_foreignTransactor($username.ep);}
        | SOMEONE ELSE {$t = TransactionFilter.neq_currentTransactor();}
        | ME {$t = TransactionFilter.eq_currentTransactor();}
        | entity {$t = TransactionFilter.eq_foreignTransactor($entity.ep);}
        | username {$t = TransactionFilter.eq_foreignTransactor($username.ep);}
    )
;

/**
 * A user name or a user name pattern.
 */
username returns [Query.Pattern ep] locals [int type]
@init{
    $type = Query.Pattern.TYPE_NORMAL;
}
@after{
    $ep = new Query.Pattern($text, Query.Pattern.TYPE_NORMAL);
}
:
    ( STAR {$type = Query.Pattern.TYPE_LIKE;} | ~(STAR | WHITE_SPACE) )+
;

/**
 * Time or timeframe of a transaction (for the transaction filter).
 */
transaction_time returns [String tqp, String op]
@init {
     $op = "(";
}
:
    (
        AT  {$op = "=";}
        | (ON | IN)
        | (
            BEFORE {$op = "<";}
              | UNTIL {$op = "<=";}
              | AFTER {$op = ">";}
              | SINCE {$op = ">=";}
        )
    )?
    (
        TODAY {$tqp = TransactionFilter.TODAY;}
        | value {$tqp = $value.text;}
    )
;

/**
 * A date time or a fragment of a date time.
 *
 * Not fully compliant with iso 8601 (TODO).
 */
datetime
:
    UNSIGNED_INT // year
    (
        HYPHEN UNSIGNED_INT // mon
        (
            HYPHEN UNSIGNED_INT // day of mon
            (
                (m=TXT {$m.text.equals("T")}?)?// compliance with iso datetime
                UNSIGNED_INT // hour
                (
                    COLON UNSIGNED_INT // minut
                    (
                        COLON UNSIGNED_INT // sec
                        (
                            DOT UNSIGNED_INT // millisec
                        )?
                    )?
                )?
            )?
        )?
    )?
;

/**
 * The property-operator-value filter (e.g. temperator > 240°C) and
 * (optionally) subfilters.
 */
pov returns [POV filter] locals [Query.Pattern p, String o, String v, String a]
    @init{
        $p = null;
        $o = null;
        $v = null;
        $a = null;
    }
    @after{
        $filter = new POV($p,$o,$v,$a);
    }
:
    (
        property {$p = $property.pp; $a=$property.agg;}
        (
            (
              LIKE {$o = $LIKE.text.trim();}
              ( like_pattern {$v = $like_pattern.ep.toString();}
                | value {$v = $value.str;} )
              | OPERATOR {$o = $OPERATOR.text;} WHITE_SPACE? value {$v = $value.str;}
            )
            | IS_NULL {$o = "0";}
            | IS_NOT_NULL {$o = "!0";}
            | IN value {$o = "("; $v=$value.str;}
            | NEGATION IN value {$o = "!("; $v=$value.str;}
        )?
    )
    |
    (
        ( LIKE {$o = $LIKE.text;}
          ( like_pattern {$v = $like_pattern.ep.toString();}
            | value {$v = $value.str;} )
        )
        | ( OPERATOR {$o = $OPERATOR.text;} WHITE_SPACE? value {$v = $value.str;}
          ( AS_A
            property {$p = $property.pp;} )?
        )
    )
    WHITE_SPACE?
;


/**
 * Wrapper for a filter on referenced entities.
 */
subproperty returns [SubProperty subp]
@init{
    $subp = null;
}
:
    subproperty_filter {$subp = new SubProperty($subproperty_filter.filter);}
;

/**
 * The actual filter on referenced entities.
 */
subproperty_filter returns [EntityFilterInterface filter]
    @init{
        $filter = null;
    }
:
    which_exp
    (
        (
            LPAREN WHITE_SPACE?
            (
                filter_expression {$filter = $filter_expression.efi;}
                | conjunction {$filter = $conjunction.c;}
                | disjunction {$filter = $disjunction.d;}
            )
            RPAREN
        ) | (
            filter_expression {$filter = $filter_expression.efi;}
        )
    )?
;

/**
 * The backreference filter (e.g. REFERENCED BY ...).
 */
backreference returns [Backreference ref] locals [Query.Pattern e, Query.Pattern p]
    @init{
        $e = null;
        $p = null;
    }
    @after{
        $ref = new Backreference($e, $p);
    }
:
    IS_REFERENCED
    (BY A? entity {$e=$entity.ep;})?
    (
        WHITE_SPACE?
        AS_A
        property {$p=$property.pp;}
    )?
    WHITE_SPACE?
;

/**
 * Stored-at filter for the path of files.
 */
storedat returns [StoredAt filter] locals [String loc]
    @init{
        $loc = null;
    }
    @after{
        $filter = new StoredAt($loc);
    }
:
    IS_STORED_AT
    location {$loc = $location.str;}
    WHITE_SPACE?
;

/**
 * Combine other filters with a logical AND.
 */
conjunction returns [Conjunction c]
    @init{
        $c = new Conjunction();
    }
:
    (
        f1 = filter_expression {$c.add($f1.efi);}
        |
        LPAREN WHITE_SPACE?
        (HAS_A (PROPERTY)?)?
        (
            f4 = filter_expression {$c.add($f4.efi);}
            | disjunction {$c.add($disjunction.d);}
            | c3=conjunction {$c.addAll($c3.c);}
        )
        RPAREN
    )
    (
        WHITE_SPACE?
        AND
        (
            ( which_exp | A (PROPERTY)?? )
        )?
        (
            f2 = filter_expression {$c.add($f2.efi);}
            | (
                LPAREN WHITE_SPACE?
                (
                    f3 = filter_expression {$c.add($f3.efi);}
                    | disjunction {$c.add($disjunction.d);}
                    | c2=conjunction {$c.addAll($c2.c);}
                )
                RPAREN
            )
        )
    )+
;

/**
 * Combine other filter with a logical OR.
 */
disjunction returns [Disjunction d]
    @init{
        $d = new Disjunction();
    }
:
    (
        f1 = filter_expression {$d.add($f1.efi);}
        |
        LPAREN WHITE_SPACE?
        (HAS_A (PROPERTY)?)?
        (
            f4 = filter_expression {$d.add($f4.efi);}
            | conjunction {$d.add($conjunction.c);}
            | d3 = disjunction {$d.addAll($d3.d);}
        )
        RPAREN
    )
    (
        OR
        (
            ( which_exp | A (PROPERTY)? )
        )?
        (
            f2 = filter_expression {$d.add($f2.efi);}
            | (
                LPAREN WHITE_SPACE?
                (
                    f3 = filter_expression {$d.add($f3.efi);}
                    | conjunction {$d.add($conjunction.c);}
                    | d2 = disjunction {$d.addAll($d2.d);}
                )
                RPAREN
            )
        )
    )+
;

/**
 * Negation of another filter.
 */
negation returns [Negation n]
    @init{
    }
:
    NEGATION
    (
        f1 = filter_expression {$n = new Negation($f1.efi);}
        | (
            LPAREN WHITE_SPACE?
            (
                f2 = filter_expression {$n = new Negation($f2.efi);}
                | disjunction {$n = new Negation($disjunction.d);}
                | conjunction {$n = new Negation($conjunction.c);}
            )
            RPAREN
        )
    )
;

/**
 * An entity's full name, id, or a pattern for a name.
 */
entity returns [Query.Pattern ep]
:
    regexp_pattern {$ep = $regexp_pattern.ep;}
    | like_pattern {$ep = $like_pattern.ep;}
    | ( double_quoted {$ep = $double_quoted.ep;} )
    | ( single_quoted {$ep = $single_quoted.ep;} )
    | ENTITY {$ep = new Query.Pattern((String) $text.trim(), Query.Pattern.TYPE_NORMAL);}
    | ( ~(ENTITY |WHITE_SPACE | DOT) )+ {$ep = new Query.Pattern((String) $text.trim(), Query.Pattern.TYPE_NORMAL);}
;


/**
 * A regexp pattern.
 */
regexp_pattern returns [Query.Pattern ep] locals [StringBuffer sb]
    @init{
        $sb = new StringBuffer();
    }
:
    REGEXP_BEGIN
    (ESC_REGEXP_END {$sb.append(">>");} |m=. {$sb.append($m.text);})*?
    REGEXP_END {$ep = new Query.Pattern((String) $sb.toString(), Query.Pattern.TYPE_REGEXP);}
;

/**
 * A like pattern (e.g. "*oxide").
 */
like_pattern returns [Query.Pattern ep] locals [StringBuffer sb]
    @init{
        $sb = new StringBuffer();
    }
:
    ~(WHITE_SPACE|DOT|LIKE|OPERATOR|AS_A|AND|OR|IS_STORED_AT|IS_REFERENCED)*?
    STAR
    ~(WHITE_SPACE|DOT|STAR)*?
    {$ep = new Query.Pattern((String) $text, Query.Pattern.TYPE_LIKE);}
;

/**
 * A Property's name, id or a pattern of a name.
 */
property returns [Query.Pattern pp, String agg]locals [StringBuffer sb]
    @init{
        $sb = new StringBuffer();
        $agg = null;
    }
:
    (minmax {$agg=$minmax.agg;})??
    (
        regexp_pattern {$pp = $regexp_pattern.ep;}
        | like_pattern {$pp = $like_pattern.ep;}
        | ( double_quoted {$pp = $double_quoted.ep;} )
        | ( single_quoted {$pp = $single_quoted.ep;} )
        | ((m=TXT | m=UNSIGNED_INT | m=UNSIGNED_DECIMAL_NUMBER | m=REGEXP_MARKER | m=ENTITY){$sb.append($m.text);})+  {$pp = new Query.Pattern($sb.toString(), Query.Pattern.TYPE_NORMAL);}
    )
    WHITE_SPACE?
;

/**
 * Expression for minumum or maximum.
 */
minmax returns [String agg]
:
    (THE?? (
        GREATEST {$agg="max";}
        | SMALLEST {$agg="min";}
    ))
;

/**
 * A property value.
 */
value returns [String str]
:
    number_with_unit {$str = $number_with_unit.text;}
    | datetime {$str = $datetime.text;}
    | atom {$str = $atom.ep.toString();}
    WHITE_SPACE?
;

/**
 * A number with a unit (e.g. 20m).
 */
number_with_unit
:
    ( UNSIGNED_INT | DECIMAL_NUMBER | ( HYPHEN | PLUS ) WHITE_SPACE? UNSIGNED_INT)
    (WHITE_SPACE? unit)?
;

/**
 * A unit from a physical system of units or any other kind of system.
 */
unit
:
    (~(WHITE_SPACE | WHICH | HAS_A | WITH_A | WHERE | DOT | AND | OR | RPAREN ))
    (~(WHITE_SPACE))*
    |
    UNSIGNED_INT SLASH (~(WHITE_SPACE))+
;

/**
 * A files path.
 */
location returns [String str]
:
    atom {$str = $atom.ep.str;}
    |
    (~WHITE_SPACE)+ {$str = $text; }
;

/**
 * An atomic string or pattern.
 */
atom returns [Query.Pattern ep]
:
    double_quoted {$ep = $double_quoted.ep;}
    | single_quoted {$ep = $single_quoted.ep;}
    | (~(WHITE_SPACE | DOT | RPAREN | LPAREN ))+ {$ep = new Query.Pattern($text, Query.Pattern.TYPE_NORMAL);}
;

/**
 * A single-quoted string or pattern.
 */
single_quoted returns [Query.Pattern ep] locals [StringBuffer sb, int patternType]
    @init{
        $sb = new StringBuffer();
        $patternType = Query.Pattern.TYPE_NORMAL;
    }
    @after{
        $ep = new Query.Pattern($sb.toString(),$patternType);
    }
:
    SINGLE_QUOTE_START
    (
        t = SINGLE_QUOTE_ESCAPED_CHAR {$sb.append($t.text.substring(1,$t.text.length()));}
    |
        r = SINGLE_QUOTE_STAR {$sb.append($r.text); $patternType = Query.Pattern.TYPE_LIKE;}
    |
        s = ~SINGLE_QUOTE_END {$sb.append($s.text);}
    )*?
    SINGLE_QUOTE_END
;

/**
 * A double-quoted string or pattern.
 */
double_quoted returns [Query.Pattern ep] locals [StringBuffer sb, int patternType]
    @init{
        $sb = new StringBuffer();
        $patternType = Query.Pattern.TYPE_NORMAL;
    }
    @after{
        $ep = new Query.Pattern($sb.toString(),$patternType);
    }
:
    DOUBLE_QUOTE_START
    (
        t = DOUBLE_QUOTE_ESCAPED_CHAR {$sb.append($t.text.substring(1,$t.text.length()));}
    |
        r = DOUBLE_QUOTE_STAR {$sb.append($r.text); $patternType = Query.Pattern.TYPE_LIKE;}
    |
        s = ~DOUBLE_QUOTE_END {$sb.append($s.text);}
    )*?
    DOUBLE_QUOTE_END
;
