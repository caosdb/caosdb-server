/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.query;

import static org.caosdb.server.database.backend.implementation.MySQL.DatabaseUtils.bytes2UTF8;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.apache.shiro.subject.Subject;
import org.caosdb.server.database.access.Access;
import org.caosdb.server.query.Query.QueryException;
import org.jdom2.Element;

public class Conjunction extends EntityFilterContainer
    implements QueryInterface, EntityFilterInterface {

  private String sourceSet = null;
  private String targetSet = null;
  private QueryInterface query = null;

  @Override
  public void apply(final QueryInterface query) throws QueryException {
    final long t1 = System.currentTimeMillis();
    this.query = query;
    if (query.getTargetSet() != null) {
      applyOnCopyToTarget(query);
    } else {
      // apply filter on source
      for (final EntityFilterInterface filter : getFilters()) {
        filter.apply(query);
        if (query.getTargetSetCount() == 0) {
          break;
        }
      }
    }
    query.addBenchmark(this.getClass().getSimpleName(), System.currentTimeMillis() - t1);
  }

  private void applyOnCopyToTarget(final QueryInterface query) throws QueryException {
    try {
      // generate empty temporary targetSet if query.getTargetSet() is not
      // empty anyways.
      final PreparedStatement callInitEmptyTarget =
          getConnection().prepareStatement("call initEmptyTargetSet(?,?)");
      callInitEmptyTarget.setString(1, query.getTargetSet());
      callInitEmptyTarget.setBoolean(2, query.isVersioned());
      final ResultSet initEmptyTargetResultSet = callInitEmptyTarget.executeQuery();
      if (initEmptyTargetResultSet.next()) {
        this.targetSet = bytes2UTF8(initEmptyTargetResultSet.getBytes("newTableName"));
      }
      initEmptyTargetResultSet.close();

      // apply the first filter on copy from source to target...
      boolean first = true;
      for (final EntityFilterInterface filter : getFilters()) {
        filter.apply(this);
        if (first) {
          // then apply every other filter on the new sourceSet
          this.sourceSet = this.targetSet;
          this.targetSet = null;
          first = false;
        }
      }

      if (!this.sourceSet.equalsIgnoreCase(query.getTargetSet())) {
        // merge the sourceSet (former temporary targetSet) with
        // query.getTargetSet()
        final CallableStatement callFinishConjunctionFilter =
            query.getConnection().prepareCall("call calcUnion(?,?)");
        callFinishConjunctionFilter.setString(1, query.getTargetSet());
        callFinishConjunctionFilter.setString(2, this.sourceSet);
        callFinishConjunctionFilter.execute();
      }
    } catch (final SQLException e) {
      throw new QueryException(e);
    }
  }

  @Override
  public Element toElement() {
    final Element ret = new Element("Conjunction");
    for (final EntityFilterInterface filter : getFilters()) {
      ret.addContent(filter.toElement());
    }
    return ret;
  }

  @Override
  public Query getQuery() {
    return this.query.getQuery();
  }

  @Override
  public String getSourceSet() {
    if (this.sourceSet != null) {
      return this.sourceSet;
    } else {
      return this.query.getSourceSet();
    }
  }

  @Override
  public Connection getConnection() {
    return this.query.getConnection();
  }

  @Override
  public int getTargetSetCount() {
    return -1;
  }

  @Override
  public String getTargetSet() {
    return this.targetSet;
  }

  @Override
  public Access getAccess() {
    return this.query.getAccess();
  }

  @Override
  public Subject getUser() {
    return this.query.getUser();
  }

  @Override
  public void addBenchmark(final String str, final long time) {
    this.query.addBenchmark(this.getClass().getSimpleName() + "." + str, time);
  }

  @Override
  public boolean isVersioned() {
    return this.query.isVersioned();
  }

  @Override
  public String getCacheKey() {
    StringBuilder sb = new StringBuilder();
    sb.append("Conj(");
    for (EntityFilterInterface filter : getFilters()) {
      sb.append(filter.getCacheKey());
    }
    sb.append(")");
    return sb.toString();
  }
}
