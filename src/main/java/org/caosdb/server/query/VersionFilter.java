package org.caosdb.server.query;

public class VersionFilter {

  public static final VersionFilter ANY_VERSION = new VersionFilter();
  public static final VersionFilter UNVERSIONED = new VersionFilter();
}
