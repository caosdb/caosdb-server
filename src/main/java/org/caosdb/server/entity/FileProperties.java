/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.entity;

import java.io.File;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import org.caosdb.server.CaosDBException;
import org.caosdb.server.FileSystem;
import org.caosdb.server.utils.FileUtils;
import org.caosdb.server.utils.ServerMessages;
import org.caosdb.server.utils.UndoHandler;
import org.caosdb.server.utils.Undoable;

public class FileProperties {
  private File thumbnail = null;
  private File file = null;
  private String checksum = null;
  private String path = null;
  private Long size = null;
  private String tmpIdentifier = null;

  public FileProperties setChecksum(final String checksum) {
    this.checksum = checksum;
    return this;
  }

  public String getChecksum() {
    return this.checksum;
  }

  public boolean hasChecksum() {
    if (this.checksum == null || this.checksum.equals("")) {
      return false;
    }
    return true;
  }

  public FileProperties setPath(final String path) {
    // remove leading slash
    this.path = (path == null ? null : path.replaceFirst("^/", ""));
    return this;
  }

  public String getPath() {
    return this.path;
  }

  public boolean hasPath() {
    if (this.path == null || this.path.equals("")) {
      return false;
    }
    return true;
  }

  public FileProperties setSize(final Long size) {
    this.size = size;
    return this;
  }

  public Long getSize() {
    return this.size;
  }

  public boolean hasSize() {
    if (this.size == null) {
      return false;
    }
    return true;
  }

  public FileProperties(final String checksum, final String path, final Long size) {
    this.checksum = checksum;
    this.path = (path == null ? null : path.replaceFirst("^/", ""));
    this.size = size;
  }

  public FileProperties(
      final String checksum, final String path, final Long size, final String tmpIdentifier) {
    this.checksum = checksum;
    this.path = (path == null ? null : path.replaceFirst("^/", ""));
    this.size = size;
    this.tmpIdentifier = tmpIdentifier;
  }

  public FileProperties setFile(final File file) {
    this.file = file;
    return this;
  }

  public File getFile() {
    return this.file;
  }

  public FileProperties setTmpIdentifier(final String tmpIdentifier) {
    this.tmpIdentifier = tmpIdentifier;
    return this;
  }

  public String getTmpIdentifier() {
    return this.tmpIdentifier;
  }

  private String getThumbnailPath(final File target) {
    // path string of the parent folder
    final String parentPathString = target.getParentFile().getAbsolutePath();

    // $parentPathString/.thumbnails/ is the directory for
    // thumbnails
    return parentPathString + "/.thumbnails/" + target.getName();
  }

  public Undoable storeFile() throws IOException, Message {
    final UndoHandler ret = new UndoHandler();
    try {

      final File target = new File(FileSystem.getPath(this.path));

      // create parent folder if necessary
      if (!target.getParentFile().exists()) {
        ret.append(FileUtils.createFolders(target.getParentFile()));
      }

      // process thumbnail
      File thumbnailTarget = null;
      if (this.thumbnail != null) {
        final String thumbnailPath = getThumbnailPath(target);

        // create thumbnail dir if necessary
        thumbnailTarget = new File(thumbnailPath);
        if (!thumbnailTarget.getParentFile().exists()) {
          ret.append(FileUtils.createFolders(thumbnailTarget.getParentFile()));
        }
      }

      if (this.file == null) {
        throw ServerMessages.FILE_HAS_NOT_BEEN_UPLOAED;
      }

      // move file from tmp dir to file system
      ret.append(FileUtils.rename(this.file, target));
      this.file = target;

      // move thumbnail if exists
      if (thumbnailTarget != null) {
        ret.append(FileUtils.rename(this.thumbnail, thumbnailTarget));
        this.thumbnail = thumbnailTarget;
      }
    } catch (final Message e) {
      ret.undo();
      throw e;
    } catch (final IOException e) {
      ret.undo();
      throw e;
    }
    return ret;
  }

  public Undoable deleteFile() throws Message, IOException, InterruptedException, CaosDBException {
    final File file = new File(FileSystem.getPath(this.path));

    final File thumbnail = new File(getThumbnailPath(file));
    if (!thumbnail.exists()) {
      return delete(file);
    }
    final Undoable deleteFile = delete(file);
    final Undoable deleteThumbnail = delete(thumbnail);
    return new Undoable() {

      @Override
      public void undo() {
        deleteFile.undo();
        deleteThumbnail.undo();
      }

      @Override
      public void cleanUp() {
        deleteFile.cleanUp();
        deleteThumbnail.cleanUp();
      }
    };
  }

  private static Undoable delete(final File file) throws IOException, InterruptedException {
    if (file.getAbsolutePath().startsWith(FileSystem.getBasepath())) {
      final Undoable d;
      final File parent = file.getParentFile();
      if (FileUtils.isSymlink(file)) {
        d = FileUtils.unlink(file);
      } else if (file.getCanonicalPath().startsWith(FileSystem.getBasepath())) {
        d = FileUtils.delete(file, file.isDirectory());
      } else {
        throw new CaosDBException(
            "File is in Filesystem, but it is neither a normal file nor a symlink.");
      }
      if (parent != null && parent.listFiles() != null && parent.listFiles().length == 0) {
        final UndoHandler ret = new UndoHandler();
        ret.append(d);
        ret.append(delete(parent));
        return ret;
      } else {
        return d;
      }
    }
    return null;
  }

  public File retrieveFromFileSystem()
      throws IOException, CaosDBException, NoSuchAlgorithmException {
    if (this.file != null) {
      return this.file;
    }
    if (hasPath()) {
      final File f = FileSystem.getFromFileSystem(getPath());
      if (f == null) {
        throw new CaosDBException(
            "File not in file system. File system is not consistent with the data base!!! "
                + getPath());
      }
      this.file = f;
      return f;
    }

    return null;
  }

  public FileProperties setThumbnail(final File thumbnail) {
    this.thumbnail = thumbnail;
    return this;
  }

  public File getThumbnail() {
    return this.thumbnail;
  }

  private boolean pickupable = false;
  private String tempPath = null;

  public FileProperties setPickupable(final boolean b) {
    this.pickupable = b;
    return this;
  }

  public boolean isPickupable() {
    return this.pickupable;
  }

  public void cleanUpTmpDir() {
    if (this.tempPath != null) {
      new File(this.tempPath).delete();
    }
  }

  public void removeOnCleanUp(final String tempPath) {
    this.tempPath = tempPath;
  }

  public boolean hasTmpIdentifier() {
    return this.tmpIdentifier != null;
  }
}
