/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2022 - 2023 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2022 - 2023 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.caosdb.server.entity;

import java.io.Serializable;
import java.util.Objects;

/**
 * EntityID
 *
 * <p>This class is an abstraction layer for the entity id.
 *
 * <p>It also allows to link entities together, e.g. an Entity's Property with the corresponding
 * abstract Property, even when the abstract Property does not have a valid ID yet (during bulk
 * inserts).
 *
 * @author Timm Fitschen <t.fitschen@indiscale.com>
 */
public class EntityID implements Serializable {

  private static final long serialVersionUID = -9062285447437546105L;

  public static final EntityID DEFAULT_DOMAIN = new EntityID("0");
  private String id = null;
  private EntityID link = null;

  public EntityID() {}

  public EntityID(final String id) {
    this.id = id;
  }

  public void setId(final String id) {
    this.id = id;
  }

  @Override
  public String toString() {
    if (this.link != null) {
      return this.link.toString();
    }
    return this.id;
  }

  public boolean hasId() {
    return this.id != null || this.link != null && this.link.hasId();
  }

  public void link(final EntityInterface entity) {
    this.link = entity.getId();
  }

  public void link(final EntityID entityId) {
    this.link = entityId;
  }

  @Override
  public boolean equals(final Object obj) {
    if (obj instanceof EntityID) {
      return Objects.equals(((EntityID) obj).toString(), this.toString());
    }
    return false;
  }

  @Override
  public int hashCode() {
    if (this.hasId()) {
      return toString().hashCode();
    }
    return super.hashCode();
  }

  public boolean isTemporary() {
    return toString().startsWith("-");
  }

  public static boolean isReserved(final EntityID id) {
    try {
      return id.hasId() && Integer.parseInt(id.toString()) < 100;
    } catch (NumberFormatException e) {
      return false;
    }
  }
}
