/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.entity.xml;

import org.caosdb.server.datatype.SingleValue;
import org.caosdb.server.datatype.Value;
import org.caosdb.server.entity.EntityInterface;
import org.caosdb.server.entity.MagicTypes;
import org.jdom2.Attribute;
import org.jdom2.Element;

public class PropertyToElementStrategy extends EntityToElementStrategy {

  public PropertyToElementStrategy() {
    super("Property");
  }

  @Override
  protected void addPermissions(EntityInterface entity, Element element) {
    // don't
  }

  @Override
  public Element addToElement(
      final EntityInterface entity,
      final Element element,
      final SerializeFieldStrategy setFieldStrategy) {
    try {
      final Value v = entity.getValue();
      if (entity.hasId()) {
        if (MagicTypes.getType(entity.getId()) != null) {
          switch (MagicTypes.getType(entity.getId())) {
            case UNIT:
              if (setFieldStrategy.isToBeSet("unit") && v != null) {
                final Attribute a = new Attribute("unit", ((SingleValue) v).toDatabaseString());
                element.setAttribute(a);
              }
              return element;
            default:
              break;
          }
        }
      }
    } catch (final NumberFormatException e) {
      e.printStackTrace();
    }

    element.addContent(toElement(entity, setFieldStrategy));

    return element;
  }
}
