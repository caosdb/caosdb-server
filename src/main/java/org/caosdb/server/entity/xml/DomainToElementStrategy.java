/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2020 Timm Fitschen <t.fitschen@indiscale.com>
 * Copyright (C) 2020 IndiScale GmbH <info@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.entity.xml;

import org.caosdb.server.entity.EntityInterface;
import org.jdom2.Element;

/**
 * Generates a JDOM (XML) representation of an entity with role "Domain".
 *
 * @author Timm Fitschen <t.fitschen@indiscale.com>
 */
public class DomainToElementStrategy extends EntityToElementStrategy {

  public DomainToElementStrategy() {
    super("Domain");
  }

  @Override
  public Element toElement(
      final EntityInterface entity, final SerializeFieldStrategy serializeFieldStrategy) {
    Element element = new Element(tagName);
    sparseEntityToElement(element, entity, serializeFieldStrategy);
    return element;
  }

  @Override
  public Element addToElement(
      final EntityInterface entity,
      final Element element,
      final SerializeFieldStrategy serializeFieldStrategy) {
    element.addContent(toElement(entity, serializeFieldStrategy));
    return element;
  }
}
