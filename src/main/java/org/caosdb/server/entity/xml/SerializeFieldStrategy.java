/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2020 Timm Fitschen <t.fitschen@indiscale.com>
 * Copyright (C) 2020 IndiScale GmbH <info@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.entity.xml;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import org.caosdb.server.entity.EntityInterface;
import org.caosdb.server.query.Query;
import org.caosdb.server.query.Query.Selection;

/**
 * A class which decides whether the properties, parents, name, etc. of an entity are to be included
 * into the serialization or not.
 *
 * <p>The decision is based on a list of {@link Query.Selection} or smart defaults.
 *
 * @author Timm Fitschen <t.fitschen@indiscale.com>
 */
public class SerializeFieldStrategy {

  private final List<Selection> selections = new LinkedList<Selection>();
  private HashMap<String, Boolean> cache = null;

  /**
   * The default is: Any field should be included into the serialization, unless it is a referenced
   * entity.
   */
  private static final SerializeFieldStrategy defaultSelections =
      new SerializeFieldStrategy(null) {
        @Override
        public boolean isToBeSet(final String field) {
          return field == null || !field.equalsIgnoreCase("_referenced");
        }
      };

  public SerializeFieldStrategy(final List<Selection> selections) {
    if (selections != null) {
      this.selections.addAll(selections);
    }
  }

  public SerializeFieldStrategy() {
    this(null);
  }

  public SerializeFieldStrategy addSelection(final Selection selection) {
    // ignore null
    if (selection == null) {
      return this;
    }

    if (this.cache != null) {
      this.cache.clear();
      this.cache = null;
    }
    this.selections.add(selection);
    return this;
  }

  public SerializeFieldStrategy forProperty(final EntityInterface property) {
    return forProperty(property.getName());
  }

  /** Return the strategy for a property. */
  public SerializeFieldStrategy forProperty(final String name) {
    // if property is to be omitted: always-false-strategy
    if (!isToBeSet(name)) {
      return new SerializeFieldStrategy() {
        @Override
        public boolean isToBeSet(final String field) {
          return false;
        }
      };
    }

    final LinkedList<Selection> subselections = new LinkedList<Selection>();
    for (final Selection s : this.selections) {
      if (s.getSelector().equalsIgnoreCase(name) && s.getSubselection() != null) {
        subselections.add(s.getSubselection());
      }
    }
    if (subselections.isEmpty() && this.isToBeSet("_referenced")) {

      /**
       * If the super selection decided that the referenced entity is to be included into the
       * serialization, while it doesn't specify the subselects, every field is to be included.
       *
       * <p>This is the case when the selections are deeply nested but only the very last segments
       * are actually used, e.g ["a.b.c.d1", "a.b.c.d2"].
       */
      return new SerializeFieldStrategy() {
        // Return true for everything except version fields.
        @Override
        public boolean isToBeSet(String field) {
          return field == null || !field.equalsIgnoreCase("version");
        }
      };
    }
    return new SerializeFieldStrategy(subselections);
  }

  public boolean isToBeSet(final String field) {
    // default values
    if (this.selections == null || this.selections.isEmpty()) {
      return defaultSelections.isToBeSet(field);
    }

    // There must be a least one selection defined, a null field won't match anything.
    if (field == null) {
      return false;
    }

    if (this.cache == null) {
      this.cache = new HashMap<String, Boolean>();
      // always include the id, version, role and the name
      this.cache.put("id", true);
      this.cache.put("version", true);
      this.cache.put("name", true);
      this.cache.put("role", true);

      // ... and the referenced entity.
      this.cache.put("_referenced", true);
      for (final Selection selection : this.selections) {
        if (selection.getSelector().equals("value")) {
          // if the value is present, the data type is needed as well
          this.cache.put("datatype", true);
        }
        this.cache.put(selection.getSelector().toLowerCase(), true);
      }
    }

    // return from cache or false
    return (this.cache.containsKey(field.toLowerCase())
        ? this.cache.get(field.toLowerCase())
        : false);
  }
}
