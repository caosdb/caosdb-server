package org.caosdb.server.entity.xml;

/**
 * Special purpose subclass of {@link SerializeFieldStrategy} which only serializes the id and the
 * server messages (error, warning, info).
 *
 * <p>This strategy is used when the client doesn't have the permission to retrieve and entity.
 *
 * @author Timm Fitschen <t.fitschen@indiscale.com>
 */
public class IdAndServerMessagesOnlyStrategy extends SerializeFieldStrategy {

  // @review Florian Spreckelsen 2022-03-22
  @Override
  public boolean isToBeSet(String field) {
    return "id".equals(field)
        || "error".equals(field)
        || "warning".equals(field)
        || "info".equals(field);
  }
}
