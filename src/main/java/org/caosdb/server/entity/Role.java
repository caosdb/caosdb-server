/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.entity;

import java.util.HashMap;
import org.caosdb.server.database.access.Access;
import org.caosdb.server.database.backend.transaction.GetIDByName;
import org.caosdb.server.entity.xml.EntityToElementStrategy;
import org.caosdb.server.entity.xml.FileToElementStrategy;
import org.caosdb.server.entity.xml.ToElementStrategy;

public enum Role {
  RecordType,
  Record,
  File,
  Property,
  DataType,
  QueryTemplate;
  private static HashMap<Role, EntityID> ids = null;

  public static void init(final Access access) throws Exception {
    ids = new HashMap<>();

    for (final Role r : Role.values()) {
      ids.put(r, execute(new GetIDByName(r.name(), "ROLE"), access).getId());
    }
  }

  private static GetIDByName execute(final GetIDByName getIDByName, final Access access) {
    getIDByName.setAccess(access);
    getIDByName.executeTransaction();
    return getIDByName;
  }

  public static Role parse(final String str) {
    for (final Role r : Role.values()) {
      if (r.toString().equalsIgnoreCase(str)) {
        return r;
      }
    }
    throw new NoSuchRoleException("No such role '" + str + "'.");
  }

  public EntityID getId() {
    return ids.get(this);
  }

  public ToElementStrategy getToElementStrategy() {
    switch (this) {
      case File:
        return new FileToElementStrategy();
      default:
        return new EntityToElementStrategy(toString());
    }
  }
}
