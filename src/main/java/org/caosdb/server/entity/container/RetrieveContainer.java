/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2023 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2023 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.caosdb.server.entity.container;

import java.util.HashMap;
import org.apache.shiro.subject.Subject;
import org.caosdb.server.entity.EntityID;
import org.caosdb.server.entity.RetrieveEntity;

/**
 * A container of entities which are about to be retrieved.
 *
 * <p>A suitable name would also be `ReadOnlyEntitiesContainer` because entities in this container
 * will only be retrieved, not changed, inserted or deleted.
 *
 * <p>Because all entities are expected to be existing, all entities can theoretically be identified
 * via their (valid) entity id (plus version id, if applicable).
 *
 * <p>However, sometimes, entities are being retrieved via the name instead of the id. Then the id
 * has to be resolved first. This happens during the init stage of the owning transaction.
 *
 * <p>The container also holds the flags of the current transaction (e.g. cache=false, ACL=true,
 * ...).
 *
 * @author Timm Fitschen <t.fitschen@indiscale.com>
 */
public class RetrieveContainer extends TransactionContainer {

  private static final long serialVersionUID = 4816050921531043503L;

  public RetrieveContainer(
      final Subject user,
      final Long timestamp,
      final String srid,
      final HashMap<String, String> flags) {
    super(user, timestamp, srid, flags);
  }

  public void add(final EntityID id, final String version) {
    add(new RetrieveEntity(id, version));
  }

  public void add(final EntityID id, final String name, final String version) {
    add(new RetrieveEntity(id, name, version));
  }
}
