/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2020 Timm Fitschen <t.fitschen@indiscale.com>
 * Copyright (C) 2020 IndiScale GmbH <info@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.entity.container;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import org.caosdb.server.entity.Entity;
import org.caosdb.server.entity.EntityInterface;
import org.caosdb.server.entity.wrapper.Property;
import org.caosdb.server.entity.xml.PropertyToElementStrategy;
import org.caosdb.server.entity.xml.SerializeFieldStrategy;
import org.caosdb.server.entity.xml.ToElementStrategy;
import org.caosdb.server.utils.EntityStatus;
import org.caosdb.server.utils.Observable;
import org.caosdb.server.utils.Observer;
import org.caosdb.server.utils.ServerMessages;
import org.jdom2.Element;

public class PropertyContainer extends Container<Property> {

  private final ToElementStrategy s;

  private static final long serialVersionUID = -8452118156168954326L;

  private final EntityInterface domain;

  public PropertyContainer(final EntityInterface domain) {
    this.domain = domain;
    this.s = new PropertyToElementStrategy();
  }

  /** Sort the properties by their pidx (Property Index). */
  public void sort() {
    Collections.sort(
        this,
        new Comparator<EntityInterface>() {
          @Override
          public int compare(final EntityInterface o1, final EntityInterface o2) {
            if (o1 instanceof Property && o2 instanceof Property) {
              return ((Property) o1).getPIdx() - ((Property) o2).getPIdx();
            }
            return 0;
          }
        });
  }

  /**
   * Add a single property to the element using the given setFieldStrategy.
   *
   * @param property
   * @param element
   * @param serializeFieldStrategy
   */
  public void addToElement(
      EntityInterface property, Element element, SerializeFieldStrategy serializeFieldStrategy) {
    if (serializeFieldStrategy.isToBeSet(property.getName())) {
      SerializeFieldStrategy strategy = serializeFieldStrategy.forProperty(property.getName());
      this.s.addToElement(property, element, strategy);
    }
  }

  /**
   * Add all properties to the element using the given setFieldStrategy.
   *
   * @param element
   * @param serializeFieldStrategy
   */
  public void addToElement(
      final Element element, final SerializeFieldStrategy serializeFieldStrategy) {
    sort();
    for (final EntityInterface property : this) {
      addToElement(property, element, serializeFieldStrategy);
    }
  }

  @Override
  public boolean addAll(final Collection<? extends Property> c) {
    for (final Property p : c) {
      addStatusObserver(this.domain, p);
      p.setDomain(this.domain);
    }
    return super.addAll(c);
  }

  @Override
  public boolean add(final Property p) {
    p.setDomain(this.domain);
    addStatusObserver(this.domain, p);
    return super.add(p);
  }

  private static boolean doCheck(final EntityInterface entity, final Property prop) {
    if (entity.getEntityStatus() == EntityStatus.UNQUALIFIED) {
      return false;
    }
    switch (prop.getEntityStatus()) {
      case UNQUALIFIED:
      case DELETED:
      case NONEXISTENT:
        entity.addError(ServerMessages.ENTITY_HAS_UNQUALIFIED_PROPERTIES);
        entity.setEntityStatus(EntityStatus.UNQUALIFIED);
        return false;
      case VALID:
        return false;
      default:
        return true;
    }
  }

  private static void addStatusObserver(final EntityInterface entity, final Property property) {
    if (doCheck(entity, property)) {
      property.acceptObserver(
          new Observer() {

            @Override
            public boolean notifyObserver(final String e, final Observable sender) {
              if (e == Entity.ENTITY_STATUS_CHANGED_EVENT) {
                if (entity.getEntityStatus() == EntityStatus.QUALIFIED) {
                  return doCheck(entity, property);
                }
              }
              return true;
            }
          });
    }
  }
}
