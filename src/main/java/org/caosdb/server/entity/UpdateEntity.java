/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2019-2021 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2019-2021 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.caosdb.server.entity;

import org.caosdb.server.transaction.WriteTransaction;
import org.caosdb.server.utils.EntityStatus;
import org.jdom2.Element;

/**
 * UpdateEntity class represents entities which are to be updated. The previous version is appeded
 * during the {@link WriteTransaction} transactions initialization.
 *
 * @author Timm Fitschen (t.fitschen@indiscale.com)
 */
public class UpdateEntity extends WritableEntity {

  public UpdateEntity(final Element element, Role role) {
    super(element, role);
  }

  public UpdateEntity(Element e) {
    this(e, Role.parse(e.getName()));
  }

  /** The previous version of this entity. */
  private EntityInterface original = null;

  public UpdateEntity(final EntityID id, final Role role) {
    super(id, role);
  }

  public UpdateEntity(final EntityID id) {
    this(id, null);
  }

  public UpdateEntity() {
    this((EntityID) null);
  }

  @Override
  public boolean skipJob() {
    return getEntityStatus() != EntityStatus.QUALIFIED;
  }

  public void setOriginal(final EntityInterface original) {
    this.original = original;
  }

  public EntityInterface getOriginal() {
    return this.original;
  }
}
