/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 *   Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2020 - 2023 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2020 - 2023 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.caosdb.server.entity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.AuthorizationException;
import org.apache.shiro.authz.Permission;
import org.apache.shiro.subject.Subject;
import org.caosdb.server.CaosDBException;
import org.caosdb.server.accessControl.Principal;
import org.caosdb.server.database.proto.SparseEntity;
import org.caosdb.server.database.proto.VerySparseEntity;
import org.caosdb.server.datatype.AbstractCollectionDatatype;
import org.caosdb.server.datatype.AbstractDatatype;
import org.caosdb.server.datatype.CollectionValue;
import org.caosdb.server.datatype.GenericValue;
import org.caosdb.server.datatype.ReferenceDatatype;
import org.caosdb.server.datatype.Value;
import org.caosdb.server.entity.Message.MessageType;
import org.caosdb.server.entity.container.ParentContainer;
import org.caosdb.server.entity.container.PropertyContainer;
import org.caosdb.server.entity.wrapper.Parent;
import org.caosdb.server.entity.wrapper.Property;
import org.caosdb.server.entity.xml.EntityToElementStrategy;
import org.caosdb.server.entity.xml.SerializeFieldStrategy;
import org.caosdb.server.entity.xml.ToElementStrategy;
import org.caosdb.server.entity.xml.ToElementable;
import org.caosdb.server.permissions.EntityACL;
import org.caosdb.server.query.Query.Selection;
import org.caosdb.server.utils.AbstractObservable;
import org.caosdb.server.utils.EntityStatus;
import org.caosdb.server.utils.ServerMessages;
import org.caosdb.server.utils.TransactionLogMessage;
import org.caosdb.unit.Unit;
import org.jdom2.Element;

/**
 * Abstract base class for all the important entity classes.
 *
 * <p>This is the central data class of the server. It represents Records, RecordTypes, Properties,
 * and Files.
 *
 * <p>This class holds the messages and other transient information as well as the actual data that
 * is being retrieved from or written to the back-end during a transaction.
 *
 * @author Timm Fitschen <t.fitschen@indiscale.com>
 */
public abstract class Entity extends AbstractObservable implements EntityInterface {

  public static final String DATATYPE_CHANGED_EVENT = "DatatypeChangedEvent";
  public static final String ENTITY_STATUS_CHANGED_EVENT = "EntityStatusChangedEvent";
  private Unit unit = null;
  private Role role = null;
  private EntityStatus entityStatus = EntityStatus.QUALIFIED;
  private FileProperties fileProperties = null;
  private String name = null;
  private String description = null;
  private Value value = null;
  private final EntityID domain = new EntityID("0");
  private final EntityID id = new EntityID();
  private AbstractDatatype datatype = null;
  private final ParentContainer parents = new ParentContainer(this);
  private final PropertyContainer properties = new PropertyContainer(this);
  private final List<TransactionLogMessage> history = new ArrayList<>();
  private List<Selection> selections = null;
  private EntityACL entityACL = null;

  @Override
  public boolean hasEntityACL() {
    return this.entityACL != null;
  }

  @Override
  public EntityACL getEntityACL() {
    return this.entityACL;
  }

  @Override
  public void setEntityACL(final EntityACL acl) {
    this.entityACL = acl;
  }

  public void setEntityACL(final String acl) {
    setEntityACL(EntityACL.deserialize(acl));
  }

  @Override
  public void checkPermission(final Subject subject, final Permission permission) {
    try {
      if (!this.hasPermission(subject, permission)) {
        String user = "The current user ";
        if (subject.getPrincipal() instanceof Principal) {
          user = ((Principal) subject.getPrincipal()).getUsername();
        }
        throw new AuthorizationException(
            user + " doesn't have permission " + permission.toString());
      }
    } catch (final NullPointerException e) {
      throw new AuthorizationException("This entity doesn't have an ACL!");
    }
  }

  @Override
  public void checkPermission(final Permission permission) {
    final Subject subject = SecurityUtils.getSubject();
    checkPermission(subject, permission);
  }

  @Override
  public boolean hasPermission(final Permission permission) {
    final Subject subject = SecurityUtils.getSubject();
    return hasPermission(subject, permission);
  }

  @Override
  public boolean hasPermission(final Subject subject, final Permission permission) {
    return this.entityACL.isPermitted(subject, permission);
  }

  @Override
  public final boolean hasProperties() {
    return !this.properties.isEmpty();
  }

  @Override
  public final PropertyContainer getProperties() {
    return this.properties;
  }

  @Override
  public void addProperty(final Property property) {
    this.properties.add(property);
  }

  @Override
  public void addProperty(final List<Property> properties) {
    this.properties.addAll(properties);
  }

  @Override
  public void setProperties(final PropertyContainer properties) {
    this.properties.addAll(properties);
  }

  @Override
  public final ParentContainer getParents() {
    return this.parents;
  }

  @Override
  public final boolean hasParents() {
    return !this.parents.isEmpty();
  }

  @Override
  public void addParent(final Parent parent) {
    this.parents.add(parent);
  }

  @Override
  public final AbstractDatatype getDatatype() {
    return this.datatype;
  }

  @Override
  public final void setDatatype(final AbstractDatatype datatype) {
    this.datatype = datatype;
    notifyObservers(DATATYPE_CHANGED_EVENT);
  }

  @Override
  public void setDatatype(final String datatype) {
    if (datatype == null) {
      setDatatype((AbstractDatatype) null);
    } else {
      setDatatype(AbstractDatatype.datatypeFactory(datatype));
    }
  }

  @Override
  public boolean hasDatatype() {
    return this.datatype != null;
  }

  @Override
  public final EntityID getId() {
    return this.id;
  }

  @Override
  public final boolean hasId() {
    return this.id.hasId();
  }

  @Override
  public EntityInterface linkIdToEntity(final EntityInterface link) {
    this.id.link(link);
    return this;
  }

  @Override
  public final Value getValue() {
    return this.value;
  }

  @Override
  public final void setValue(final Value value) {
    this.value = value;
  }

  @Override
  public final boolean hasValue() {
    return this.value != null;
  }

  @Override
  public final String getDescription() {
    return this.description;
  }

  @Override
  public final void setDescription(final String description) {
    this.description = description;
  }

  @Override
  public final boolean hasDescription() {
    return !(this.description == null || this.description.isEmpty() || this.description.equals(""));
  }

  @Override
  public final void setName(final String name) {
    this.name = name;
  }

  @Override
  public final String getName() {
    return this.name;
  }

  @Override
  public final boolean hasName() {
    if (this.name == null || this.name.isEmpty() || this.name.equals("")) {
      return false;
    }
    return true;
  }

  @Override
  public final FileProperties getFileProperties() {
    return this.fileProperties;
  }

  @Override
  public final void setFileProperties(final FileProperties fileProperties) {
    this.fileProperties = fileProperties;
  }

  @Override
  public final boolean hasFileProperties() {
    return this.fileProperties != null;
  }

  @Override
  public final EntityStatus getEntityStatus() {
    return this.entityStatus;
  }

  @Override
  public final void setEntityStatus(final EntityStatus entityStatus) {
    if (getEntityStatus() == entityStatus) {
      return;
    }
    if (this.entityStatus == EntityStatus.UNQUALIFIED) {
      throw new CaosDBException(
          "It is not allowed to change the state again, once an UNQUALIFIED state has been reached.");
    }
    this.entityStatus = entityStatus;
    notifyObservers(ENTITY_STATUS_CHANGED_EVENT);
  }

  @Override
  public final boolean hasEntityStatus() {
    return this.entityStatus != null;
  }

  @Override
  public final Role getRole() {
    return this.role;
  }

  @Override
  public final void setRole(final Role role) {
    this.role = role;
  }

  @Override
  public final void setRole(final String role) {
    if (role != null) {
      this.role = Role.parse(role);
    } else {
      this.role = null;
    }
  }

  @Override
  public final boolean hasRole() {
    return this.role != null;
  }

  @Override
  public void setUnit(final Unit unit) {
    this.unit = unit;
  }

  @Override
  public boolean hasUnit() {
    return this.unit != null;
  }

  @Override
  public Unit getUnit() {
    return this.unit;
  }

  @Override
  public VerySparseEntity getVerySparseEntity() {
    return getSparseEntity();
  }

  @Override
  public SparseEntity getSparseEntity() {
    final SparseEntity ret = new SparseEntity();
    ret.id = getId().toString();
    ret.name = getName();
    ret.description = getDescription();
    ret.acl = getEntityACL().serialize();
    if (hasRole()) {
      ret.role = getRole().toString();
    }
    if (hasDatatype()) {
      if (getDatatype() instanceof AbstractCollectionDatatype) {
        ret.collection = ((AbstractCollectionDatatype) getDatatype()).getCollectionName();
        ret.datatype_id =
            ((AbstractCollectionDatatype) getDatatype()).getDatatype().getId().toString();
        ret.datatype_name = ((AbstractCollectionDatatype) getDatatype()).getDatatype().getName();
      } else {
        ret.datatype_id = getDatatype().getId().toString();
        ret.datatype_name = getDatatype().getName();
      }
    }
    if (hasFileProperties()) {
      ret.fileHash = getFileProperties().getChecksum();
      ret.filePath = getFileProperties().getPath();
      ret.fileSize = getFileProperties().getSize();
    }
    return ret;
  }

  @Override
  public void setId(final EntityID id) {
    this.id.link(id);
  }

  public Entity() {}

  public Entity(EntityID id) {
    setId(id);
  }

  public Entity(final EntityID id, final Role role) {
    this(id);
    if (role != null) {
      setRole(role);
      setToElementStragegy(role.getToElementStrategy());
    }
  }

  public Entity(final String name, final Role role) {
    this(name);
    if (role != null) {
      setRole(role);
      setToElementStragegy(role.getToElementStrategy());
    }
  }

  public Entity(final Element e, final Role role) throws Exception {
    parseFromElement(e);
    if (role != null) {
      setRole(role);
      setToElementStragegy(role.getToElementStrategy());
    }
  }

  public Entity(final String name) {
    setName(name);
  }

  public Entity(final Element e) {
    try {
      setRole(e.getName().toUpperCase());
      parseFromElement(e);
    } catch (final NoSuchRoleException exc) {
      parseFromElement(e);
      addError(ServerMessages.NO_SUCH_ENTITY_ROLE(e.getName()));
    }
  }

  private StatementStatusInterface statementStatus = null;

  /**
   * statementStatus getter.
   *
   * @return
   */
  @Override
  public StatementStatusInterface getStatementStatus() {
    return this.statementStatus;
  }

  /**
   * statementStatus setter.
   *
   * @return
   */
  @Override
  public void setStatementStatus(final StatementStatusInterface statementStatus) {
    this.statementStatus = statementStatus;
  }

  @Override
  public void setStatementStatus(final String statementStatus) {
    this.statementStatus = StatementStatus.valueOf(statementStatus.toUpperCase());
  }

  /**
   * statementStatus hasser.
   *
   * @return true id statementStatus!= null, false otherwise.
   */
  @Override
  public final boolean hasStatementStatus() {
    return this.statementStatus != null;
  }

  /**
   * cuid - a client unique identifier which is specified by the client. It MUST NOT be changed on
   * server-side. Thus, it can be used by the client to identify objects uniquely and quickly.
   */
  private String cuid = null;

  /**
   * cuid hasser. Does this wrapped entity have a cuid?
   *
   * @return true if cuid != null, false otherwise.
   */
  @Override
  public final boolean hasCuid() {
    return this.cuid != null;
  }

  /**
   * cuid getter.
   *
   * @return cuid.
   */
  @Override
  public final String getCuid() {
    return this.cuid;
  }

  /**
   * cuid setter.
   *
   * @param cuid
   * @throws CaosDBException if one tried to set it once again.
   */
  @Override
  public final void setCuid(final String cuid) {
    if (!hasCuid()) {
      this.cuid = cuid;
    }
  }

  // Strategy to convert this Entity to an XML element.
  private ToElementStrategy toElementStrategy = null;

  @Override
  public void setToElementStragegy(final ToElementStrategy s) {
    this.toElementStrategy = s;
  }

  @Override
  public final Element toElement() {
    return getToElementStrategy().toElement(this, getSerializeFieldStrategy());
  }

  @Override
  public SerializeFieldStrategy getSerializeFieldStrategy() {
    // @review Florian Spreckelsen 2022-03-22
    if (this.serializeFieldStrategy == null) {
      this.serializeFieldStrategy = new SerializeFieldStrategy(getSelections());
    }
    return this.serializeFieldStrategy;
  }

  @Override
  public final void addToElement(final Element element) {
    addToElement(element, getSerializeFieldStrategy());
  }

  @Override
  public void addToElement(final Element element, final SerializeFieldStrategy strategy) {
    getToElementStrategy().addToElement(this, element, strategy);
  }

  @Override
  public EntityID getDomain() {
    return domain;
  }

  /** Errors, Warnings and Info messages for this entity. */
  private final Set<ToElementable> messages = new HashSet<>();

  @Override
  public final Set<ToElementable> getMessages() {
    return this.messages;
  }

  @Override
  public boolean hasMessages() {
    return !this.messages.isEmpty();
  }

  @Override
  public final boolean hasMessage(final String type) {
    for (final ToElementable m : this.messages) {
      if (m instanceof Message && ((Message) m).getType().equalsIgnoreCase(type)) {
        return true;
      }
    }
    return false;
  }

  @Override
  public void removeMessage(final Message m) {
    this.messages.remove(m);
  }

  @Override
  public final List<Message> getMessages(final String type) {
    final LinkedList<Message> ret = new LinkedList<>();
    for (final ToElementable m : this.messages) {
      if (m instanceof Message && ((Message) m).getType().equalsIgnoreCase(type)) {
        ret.add((Message) m);
      }
    }
    return ret;
  }

  @Override
  public final void addMessage(final ToElementable m) {
    this.messages.add(m);
  }

  @Override
  public final void addError(final Message m) {
    setEntityStatus(EntityStatus.UNQUALIFIED);
    if (m.getType().equalsIgnoreCase(MessageType.Error.toString())) {
      addMessage(m);
    } else {
      addMessage(new Message(MessageType.Error, m.getCode(), m.getDescription(), m.getBody()));
    }
  }

  @Override
  public void addInfo(final String description) {
    final Message m = new Message(description);
    addMessage(m);
  }

  @Override
  public void addInfo(final Message m) {
    if (m.getType().equalsIgnoreCase(MessageType.Info.toString())) {
      addMessage(m);
    } else {
      addMessage(new Message(MessageType.Info, m.getCode(), m.getDescription(), m.getBody()));
    }
  }

  boolean isParsed = false;
  private String queryTemplateDefinition = null;

  @Override
  public void parseValue() throws Message {
    if (this.isParsed) {
      return;
    }
    this.isParsed = true;
    if (getValue() != null) setValue(getDatatype().parseValue(getValue()));
  }

  @Override
  public void setQueryTemplateDefinition(final String query) {
    this.queryTemplateDefinition = query;
  }

  @Override
  public String getQueryTemplateDefinition() {
    return this.queryTemplateDefinition;
  }

  @Override
  public void parseFromElement(final Element element) {

    // Check if this element has any attributes at all.
    if (!element.hasAttributes() && element.getChildren().isEmpty()) {
      setEntityStatus(EntityStatus.IGNORE);
      addError(ServerMessages.ENTITY_IS_EMPTY);
      return;
    }

    // This should be the status after parsing. It will be redefined if
    // any error occurs.
    setEntityStatus(EntityStatus.QUALIFIED);

    // Parse CUID.
    if (element.getAttribute("cuid") != null) {
      setCuid(element.getAttributeValue("cuid"));
    }

    // Parse ID. Generate error if it isn't an integer.
    if (element.getAttribute("id") != null && !element.getAttributeValue("id").equals("")) {
      this.setId(element.getAttributeValue("id"));
    }

    // Parse NAME.
    if (element.getAttribute("name") != null && !element.getAttributeValue("name").equals("")) {
      setName(element.getAttributeValue("name"));
    }

    // Parse DESCRIPTION.
    if (element.getAttribute("description") != null
        && !element.getAttributeValue("description").equals("")) {
      setDescription(element.getAttributeValue("description"));
    }

    // Parse DATA TYPE. Generate error if it doesn't fit the Datatype enum.
    if (element.getAttribute("datatype") != null
        && !element.getAttributeValue("datatype").equals("")) {
      try {
        this.setDatatype(element.getAttributeValue("datatype"));
      } catch (final IllegalArgumentException e) {
        addError(ServerMessages.UNKNOWN_DATATYPE);
      }
    }

    // Parse UNIT.
    if (element.getAttribute("unit") != null && !element.getAttributeValue("unit").equals("")) {
      final EntityInterface magicUnit = MagicTypes.UNIT.getEntity();
      final Property unit = new Property(new RetrieveEntity());
      unit.setDescription(magicUnit.getDescription());
      unit.setName(magicUnit.getName());
      unit.setId(magicUnit.getId());
      unit.setDatatype(magicUnit.getDatatype());
      unit.setStatementStatus(StatementStatus.FIX);
      unit.setValue(new GenericValue(element.getAttribute("unit").getValue()));
      unit.setEntityStatus(EntityStatus.QUALIFIED);
      addProperty(unit);
    }

    // parse IMPORTANCE. Generate error if it doesn't fit the importance
    // Enum.
    if (element.getAttribute("importance") != null
        && !element.getAttributeValue("importance").equals("")) {
      try {
        setStatementStatus(element.getAttributeValue("importance"));
      } catch (final IllegalArgumentException e) {
        addError(ServerMessages.UNKNOWN_IMPORTANCE);
        setEntityStatus(EntityStatus.UNQUALIFIED);
      }
    }

    final CollectionValue vals = new CollectionValue();
    int pidx = 0;
    for (final Element pe : element.getChildren()) {
      if (pe.getName().equalsIgnoreCase("Version")) {
        // IGNORE: Once it becomes allowed for clients to set a version id, parsing
        // the Version element would be done here. Until this is the case, the
        // Version tag is ignored.
      } else if (pe.getName().equalsIgnoreCase("EmptyString")) {
        // special case: empty string which cannot be distinguished from null
        // values otherwise.
        setValue(new GenericValue(""));
      } else if (pe.getName().equalsIgnoreCase("Value")) {
        // Parse sub-elements which represent VALUES of this entity.
        if (pe.getChild("EmptyString") != null) {
          vals.add(new GenericValue(""));
        } else if (pe.getText() != null && pe.getTextTrim() != "") {
          vals.add(new GenericValue(pe.getTextTrim()));
        } else {
          vals.add(null);
        }
      } else if (pe.getName().equalsIgnoreCase("Property")) {
        // Parse sub elements which represent PROPERTIES of this
        // record.

        WritableEntity wrapped;
        if (this instanceof InsertEntity) {
          wrapped = new InsertEntity(pe, Role.Property);
        } else {
          wrapped = new UpdateEntity(pe, Role.Property);
        }
        final Property property = new Property(wrapped);

        // Ignore parents of properties
        property.getParents().clear();
        property.setPIdx(pidx++);
        addProperty(property);
      } else if (pe.getName().equalsIgnoreCase("Parent")) {
        // Parse sub elements which represent PARENTS of this
        // record.
        WritableEntity wrapped;
        if (this instanceof InsertEntity) {
          wrapped = new InsertEntity(pe, null);
        } else {
          wrapped = new UpdateEntity(pe, null);
        }
        final Parent parent = new Parent(wrapped);
        addParent(parent);
      } else if (pe.getName().equalsIgnoreCase("EntityACL")) {
        // Parse and concatenate EntityACL
        try {
          final EntityACL newACL = EntityACL.parseFromElement(pe);
          if (hasEntityACL()) {
            setEntityACL(EntityACL.combine(getEntityACL(), newACL));
          } else {
            this.setEntityACL(newACL);
          }
        } catch (final IllegalArgumentException exc) {
          setEntityStatus(EntityStatus.UNQUALIFIED);
          addError(ServerMessages.CANNOT_PARSE_ENTITY_ACL);
        }
      } else if (getRole() == Role.QueryTemplate && pe.getName().equalsIgnoreCase("Query")) {
        setQueryTemplateDefinition(pe.getTextNormalize());
      } else {
        addMessage(ClientMessage.fromXML(pe));
      }
    }

    // Parse VALUE.
    if (vals.size() != 0) {
      setValue(vals);
    } else if (element.getTextTrim() != null && !element.getTextTrim().equals("")) {
      setValue(new GenericValue(element.getTextTrim()));
    }

    // Parse PATH.
    String path = null;
    if (element.getAttribute("path") != null && !element.getAttributeValue("path").equals("")) {
      path = element.getAttributeValue("path");
    }
    if (element.getAttribute("destination") != null
        && !element.getAttributeValue("destination").equals("")) {
      path = element.getAttributeValue("destination");
    }

    // Parse CHECKSUM
    String checksum = null;
    if (element.getAttribute("checksum") != null
        && !element.getAttributeValue("checksum").equals("")) {
      checksum = element.getAttributeValue("checksum");
    }

    // Parse SIZE
    Long size = null;
    if (element.getAttribute("size") != null && !element.getAttributeValue("size").equals("")) {
      size = Long.parseLong(element.getAttributeValue("size"));
    }

    // Parse TMPIDENTIFYER.
    String tmpIdentifier = null;
    boolean pickup = false;
    if (element.getAttribute("pickup") != null && !element.getAttributeValue("pickup").equals("")) {
      tmpIdentifier = element.getAttributeValue("pickup");
      pickup = true;
    } else if (element.getAttribute("upload") != null
        && !element.getAttributeValue("upload").equals("")) {
      tmpIdentifier = element.getAttributeValue("upload");
    }
    if (tmpIdentifier != null && tmpIdentifier.endsWith("/")) {
      tmpIdentifier = tmpIdentifier.substring(0, tmpIdentifier.length() - 1);
    }

    // Store PATH, HASH, SIZE, TMPIDENTIFYER
    if (tmpIdentifier != null || checksum != null || path != null || size != null) {
      setFileProperties(
          new FileProperties(checksum, path, size, tmpIdentifier).setPickupable(pickup));
    }

    // Parse flags
    if (element.getAttribute("flag") != null && !element.getAttributeValue("flag").equals("")) {
      if (element.getAttributeValue("flag").contains(",")) {
        final String[] flags = element.getAttributeValue("flag").split(",");
        for (final String f : flags) {
          if (f.length() > 0) {
            if (f.contains(":")) {
              final String[] kv = f.split(":", 2);
              this.flags.put(kv[0].trim(), kv[1].trim());
            } else {
              this.flags.put(f.trim(), null);
            }
          }
        }
      } else {
        final String f = element.getAttributeValue("flag");
        if (f.contains(":")) {
          final String[] kv = f.split(":", 2);
          this.flags.put(kv[0].trim(), kv[1].trim());
        } else {
          this.flags.put(f.trim(), null);
        }
      }
    }
  }

  @Override
  public String toString() {
    return getRole().toString()
        + (hasId() ? "(" + getId().toString() + ")" : "()")
        + (hasCuid() ? "[" + getCuid() + "]" : "[]")
        + (hasName() ? "(" + getName() + ")" : "()")
        + (hasDatatype() ? "{" + getDatatype().toString() + "}" : "{}");
  }

  @Override
  public void addWarning(final Message m) {
    if (m.getType().equalsIgnoreCase(MessageType.Warning.toString())) {
      addMessage(m);
    } else {
      addMessage(new Message(MessageType.Warning, m.getCode(), m.getDescription(), m.getBody()));
    }
  }

  private final HashMap<String, String> flags = new HashMap<String, String>();

  @Override
  public HashMap<String, String> getFlags() {
    return this.flags;
  }

  @Override
  public void setFlag(final String key, final String value) {
    this.flags.put(key, value);
  }

  @Override
  public String getFlag(final String key) {
    return getFlags().get(key);
  }

  private boolean descOverride = false;

  @Override
  public EntityInterface setDescOverride(final boolean b) {
    this.descOverride = b;
    return this;
  }

  @Override
  public boolean isDescOverride() {
    return this.descOverride;
  }

  private boolean nameOverride = false;

  @Override
  public EntityInterface setNameOverride(final boolean b) {
    this.nameOverride = b;
    return this;
  }

  @Override
  public boolean isNameOverride() {
    return this.nameOverride;
  }

  private boolean datatypeOverride = false;
  private Version version = new Version();
  private SerializeFieldStrategy serializeFieldStrategy = null;

  @Override
  public EntityInterface setDatatypeOverride(final boolean b) {
    this.datatypeOverride = b;
    return this;
  }

  @Override
  public boolean isDatatypeOverride() {
    return this.datatypeOverride;
  }

  @Override
  public void addTransactionLog(final TransactionLogMessage transactionLogMessage) {
    this.history.add(transactionLogMessage);
  }

  @Override
  public List<TransactionLogMessage> getTransactionLogMessages() {
    return this.history;
  }

  @Override
  public boolean hasTransactionLogMessages() {
    return !this.history.isEmpty();
  }

  @Override
  public ToElementStrategy getToElementStrategy() {
    if (this.toElementStrategy == null) {
      if (hasRole()) {
        return getRole().getToElementStrategy();
      } else {
        return new EntityToElementStrategy("Entity");
      }
    }
    return this.toElementStrategy;
  }

  @Override
  public final EntityInterface parseSparseEntity(final SparseEntity spe) {
    setId(new EntityID(spe.id));
    this.setRole(spe.role);
    setEntityACL(spe.acl);
    if (spe.versionId != null) {
      this.version = new Version(spe.versionId);
    }

    if (!isNameOverride()) {
      setName(spe.name);
    }
    if (!isDescOverride()) {
      setDescription(spe.description);
    }
    if (!isDatatypeOverride() && spe.datatype_id != null) {
      if (spe.collection != null) {
        this.setDatatype(
            AbstractCollectionDatatype.collectionDatatypeFactory(
                spe.collection, spe.datatype_id, spe.datatype_name));
      } else {
        this.setDatatype(
            AbstractDatatype.datatypeFactory(new EntityID(spe.datatype_id), spe.datatype_name));
      }
    }

    if (spe.filePath != null) {
      setFileProperties(new FileProperties(spe.fileHash, spe.filePath, spe.fileSize));
    } else {
      setFileProperties(null);
    }

    return this;
  }

  public EntityInterface addSelections(final List<Selection> selections) {
    if (this.selections == null) {
      this.selections = new LinkedList<>();
    }
    this.selections.addAll(selections);
    return this;
  }

  @Override
  public List<Selection> getSelections() {
    return this.selections;
  }

  @Override
  public boolean skipJob() {
    return this.entityStatus == EntityStatus.IGNORE;
  }

  @Override
  public Version getVersion() {
    return this.version;
  }

  @Override
  public boolean hasVersion() {
    return this.version.getId() != null;
  }

  @Override
  public void setVersion(final Version version) {
    this.version = version;
  }

  /** Return "id@version" if there is versioning information, else only "id". */
  @Override
  public String getIdVersion() {
    if (!this.hasId()) {
      return null;
    } else if (this.hasVersion()) {
      return new StringBuilder()
          .append(getId())
          .append("@")
          .append(getVersion().getId())
          .toString();
    }
    return getId().toString();
  }

  @Override
  public boolean isReference() {
    return this.hasDatatype() && this.getDatatype() instanceof ReferenceDatatype;
  }

  @Override
  public boolean isReferenceList() {
    return this.hasDatatype()
        && this.getDatatype() instanceof AbstractCollectionDatatype
        && ((AbstractCollectionDatatype) getDatatype()).getDatatype() instanceof ReferenceDatatype;
  }

  @Override
  public void setSerializeFieldStrategy(SerializeFieldStrategy s) {
    // @review Florian Spreckelsen 2022-03-22
    this.serializeFieldStrategy = s;
  }
}
