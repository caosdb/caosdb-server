/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2023 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2023 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.caosdb.server.entity;

import java.util.UUID;
import org.caosdb.server.transaction.Transaction;

/**
 * Example implementation for string ids based on UUID.
 *
 * @see {@link EntityIdRegistryStrategy}
 * @author Timm Fitschen <t.fitschen@indiscale.com>
 */
public class StringIds extends EntityIdRegistryStrategy {

  public StringIds(Transaction<?> t) {
    super(t);
  }

  @Override
  public String generate() {
    return UUID.randomUUID().toString();
  }

  @Override
  public boolean matchIdPattern(String id) {
    try {
      UUID.fromString(id);
      return true;
    } catch (IllegalArgumentException e) {
      return false;
    }
  }
}
