/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.entity;

import java.util.List;
import org.apache.shiro.authz.Permission;
import org.apache.shiro.subject.Subject;
import org.caosdb.server.database.proto.SparseEntity;
import org.caosdb.server.database.proto.VerySparseEntity;
import org.caosdb.server.datatype.AbstractDatatype;
import org.caosdb.server.datatype.Value;
import org.caosdb.server.entity.container.ParentContainer;
import org.caosdb.server.entity.container.PropertyContainer;
import org.caosdb.server.entity.wrapper.Parent;
import org.caosdb.server.entity.wrapper.Property;
import org.caosdb.server.entity.xml.SerializeFieldStrategy;
import org.caosdb.server.entity.xml.ToElementable;
import org.caosdb.server.jobs.JobTarget;
import org.caosdb.server.permissions.EntityACL;
import org.caosdb.server.utils.Observable;
import org.caosdb.server.utils.TransactionLogMessage;
import org.caosdb.unit.Unit;
import org.jdom2.Element;

public interface EntityInterface
    extends JobTarget, Observable, ToElementable, WriteEntity, TransactionEntity {

  public abstract VerySparseEntity getVerySparseEntity();

  public abstract SparseEntity getSparseEntity();

  public abstract EntityID getId();

  public abstract String getIdVersion();

  public abstract void setId(EntityID id);

  public abstract boolean hasId();

  public abstract StatementStatusInterface getStatementStatus();

  public abstract void setStatementStatus(StatementStatusInterface statementStatus);

  public abstract void setStatementStatus(String statementStatus);

  public abstract boolean hasStatementStatus();

  public abstract void addProperty(Property property);

  public abstract boolean hasProperties();

  public abstract PropertyContainer getProperties();

  public abstract void addParent(Parent parent);

  public abstract boolean hasParents();

  public abstract ParentContainer getParents();

  public abstract void setName(String name);

  public abstract String getName();

  public abstract boolean hasName();

  public abstract String getDescription();

  public abstract void setDescription(String description);

  public abstract boolean hasDescription();

  public abstract Role getRole();

  public abstract void setRole(Role role);

  public abstract void setRole(String role) throws IllegalArgumentException;

  public abstract boolean hasRole();

  public abstract Value getValue();

  public abstract void setValue(Value value);

  public abstract boolean hasValue();

  public abstract AbstractDatatype getDatatype();

  public abstract void setDatatype(AbstractDatatype datatype);

  public abstract void setDatatype(String datatype);

  public abstract boolean hasDatatype();

  public abstract FileProperties getFileProperties();

  public abstract void setFileProperties(FileProperties fileProperties);

  public abstract boolean hasFileProperties();

  public abstract void parseValue() throws Message;

  @Override
  public abstract String toString();

  public abstract void addProperty(List<Property> properties);

  public abstract void setProperties(PropertyContainer properties);

  public abstract EntityInterface setDescOverride(boolean b);

  public abstract boolean isDescOverride();

  public abstract EntityInterface setNameOverride(boolean b);

  public abstract boolean isNameOverride();

  public abstract EntityInterface setDatatypeOverride(boolean b);

  public abstract boolean isDatatypeOverride();

  public abstract void addTransactionLog(TransactionLogMessage transactionLogMessage);

  public abstract List<TransactionLogMessage> getTransactionLogMessages();

  public abstract boolean hasTransactionLogMessages();

  public abstract void setUnit(Unit unit);

  public abstract Unit getUnit();

  public abstract boolean hasUnit();

  public abstract EntityID getDomain();

  public abstract EntityInterface parseSparseEntity(SparseEntity spe);

  public abstract EntityInterface linkIdToEntity(EntityInterface link);

  public abstract EntityACL getEntityACL();

  public abstract void setEntityACL(EntityACL acl);

  public abstract void checkPermission(Subject subject, Permission permission);

  public abstract void checkPermission(Permission permission);

  /** Return true iff the current thread's subject has a permission. */
  public abstract boolean hasPermission(Permission permission);

  /** Return true iff the given subject has a permission. */
  public abstract boolean hasPermission(Subject subject, Permission permission);

  public abstract boolean hasEntityACL();

  public abstract String getQueryTemplateDefinition();

  public abstract void setQueryTemplateDefinition(String query);

  public abstract Version getVersion();

  public abstract boolean hasVersion();

  public abstract void setVersion(Version version);

  public abstract void addToElement(Element element, SerializeFieldStrategy strategy);

  /** Return true iff the data type is present and is an instance of ReferenceDatatype. */
  public abstract boolean isReference();

  /**
   * Return true iff the data type is present, an instance of AbstractCollectionDatatype and the
   * AbstractCollectionDatatype's elements' data type is an instance of ReferenceDatatype.
   */
  public abstract boolean isReferenceList();

  public abstract SerializeFieldStrategy getSerializeFieldStrategy();

  public default void setId(String id) {
    getId().setId(id);
  }
}
