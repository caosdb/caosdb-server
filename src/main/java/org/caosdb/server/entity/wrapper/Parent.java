/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.entity.wrapper;

import org.caosdb.server.entity.Affiliation;
import org.caosdb.server.entity.EntityInterface;
import org.caosdb.server.entity.xml.ParentToElementStrategy;

public class Parent extends EntityWrapper {

  private Affiliation affiliation;

  public Parent(final EntityInterface p) {
    super(p);
    setToElementStragegy(new ParentToElementStrategy());
  }

  public void setEntity(final EntityInterface entity) {
    super.entity = entity;
  }

  public void setAffiliation(final Affiliation affiliation) {
    this.affiliation = affiliation;
  }

  public Affiliation getAffiliation() {
    return this.affiliation;
  }

  @Override
  public boolean hasVersion() {
    // parents are not versioned (yet).
    return false;
  }
}
