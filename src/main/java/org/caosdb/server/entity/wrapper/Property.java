/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.entity.wrapper;

import org.apache.shiro.authz.AuthorizationException;
import org.apache.shiro.authz.Permission;
import org.apache.shiro.subject.Subject;
import org.caosdb.server.database.proto.ProtoProperty;
import org.caosdb.server.datatype.AbstractCollectionDatatype;
import org.caosdb.server.datatype.AbstractDatatype;
import org.caosdb.server.datatype.CollectionValue;
import org.caosdb.server.datatype.GenericValue;
import org.caosdb.server.entity.EntityID;
import org.caosdb.server.entity.EntityInterface;
import org.caosdb.server.entity.RetrieveEntity;
import org.caosdb.server.entity.xml.PropertyToElementStrategy;

public class Property extends EntityWrapper {

  public Property(final EntityInterface prop) {
    super(prop);
    setToElementStragegy(new PropertyToElementStrategy());
  }

  /** Return the Property Index, the index of a property with respect to a containing Entity. */
  public int getPIdx() {
    return this.pIdx;
  }

  private int pIdx = 0;
  private EntityInterface domain = null;
  private boolean isName;

  /** Set the Property Index. */
  public void setPIdx(final int i) {
    this.pIdx = i;
  }

  public Property setDomain(final EntityInterface domain) {
    this.domain = domain;
    return this;
  }

  @Override
  public EntityID getDomain() {
    if (this.domain != null) {
      return this.domain.getId();
    }
    return null;
  }

  @Override
  public Property linkIdToEntity(final EntityInterface link) {
    this.entity.linkIdToEntity(link);
    return this;
  }

  public Property parseProtoProperty(final ProtoProperty pp) {
    setId(new EntityID(pp.id));
    setStatementStatus(pp.status);
    setPIdx(pp.idx);
    if (pp.name != null) {
      setName(pp.name);
      setNameOverride(true);
    }
    if (pp.desc != null) {
      setDescription(pp.desc);
      setDescOverride(true);
    }
    if (pp.type_id != null) {
      if (pp.collection != null) {
        this.setDatatype(
            AbstractCollectionDatatype.collectionDatatypeFactory(
                pp.collection, pp.type_id, pp.type_name));
      } else {
        this.setDatatype(AbstractDatatype.datatypeFactory(new EntityID(pp.type_id), pp.type_name));
      }
      setDatatypeOverride(true);
    }
    if (pp.value != null) {
      setValue(new GenericValue(pp.value));
    }
    if (pp.collValues != null) {
      CollectionValue value = new CollectionValue();
      for (Object next : pp.collValues) {
        if (next == null) {
          value.add(null);
        } else {
          value.add(new GenericValue(next.toString()));
        }
      }
      setValue(value);
    }
    if (pp.subProperties != null) {
      for (ProtoProperty subP : pp.subProperties) {
        this.addProperty(new Property(new RetrieveEntity()).parseProtoProperty(subP));
      }
    }
    return this;
  }

  @Override
  public void checkPermission(final Permission permission) {
    throw new AuthorizationException("This code should never be reached");
  }

  @Override
  public void checkPermission(final Subject subject, final Permission permission) {
    throw new AuthorizationException("This code should never be reached");
  }

  @Override
  public String toString() {
    return "IMPLPROPERTY (" + this.getStatementStatus() + ")" + this.entity.toString();
  }

  public void setIsName(final boolean b) {
    this.isName = b;
  }

  public boolean isName() {
    return this.isName;
  }

  public EntityInterface getDomainEntity() {
    return this.domain;
  }

  @Override
  public boolean hasVersion() {
    // properties are not versioned (yet).
    return false;
  }
}
