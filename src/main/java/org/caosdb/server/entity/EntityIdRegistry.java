/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2023 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2023 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.caosdb.server.entity;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import org.caosdb.server.database.exceptions.TransactionException;
import org.caosdb.server.transaction.Transaction;

/**
 * An abstract layer for managing entity ids.
 *
 * <p>That is, checking whether something is a well-formed id, generating well-formed ids and more.
 *
 * <p>This class follows a Strategy Pattern. The actual implemenation can be found in one of the
 * implementations of the {@link EntityIdRegistryStrategy} interface.
 *
 * @author Timm Fitschen <t.fitschen@indiscale.com>
 */
public class EntityIdRegistry {

  private EntityIdRegistryStrategy strategy;

  public EntityIdRegistry(Transaction<?> t) {
    // This is the place where we will inject the new string id behavior in the
    // future:
    //
    // this("org.caosdb.server.entity.StringIds", t);
    this("org.caosdb.server.entity.LegacyIds", t);
  }

  public EntityIdRegistry(EntityIdRegistryStrategy strategy) {
    this.strategy = strategy;
  }

  /** Return a well-formed, new and unused entity id. */
  public String generate() {
    return this.strategy.generate();
  }

  public EntityIdRegistry(String strategy, Transaction<?> t) {
    try {
      @SuppressWarnings("unchecked")
      Class<? extends EntityIdRegistry> clazz =
          (Class<? extends EntityIdRegistry>) Class.forName(strategy);
      Constructor<?> constructor = clazz.getConstructor(Transaction.class);
      this.strategy = (EntityIdRegistryStrategy) constructor.newInstance(t);
    } catch (ClassNotFoundException
        | NoSuchMethodException
        | SecurityException
        | InstantiationException
        | IllegalAccessException
        | IllegalArgumentException
        | InvocationTargetException e) {
      throw new TransactionException(e);
    }
  }

  /** Return true if the id is a well-formed id. */
  public boolean matchIdPattern(String id) {
    return this.strategy.matchIdPattern(id);
  }
}
