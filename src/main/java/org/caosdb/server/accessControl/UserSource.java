/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2019 IndiScale GmbH
 * Copyright (C) 2019 Timm Fitschen (t.fitschen@indiscale.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.accessControl;

import java.util.Map;
import java.util.Set;

/**
 * UserSources are sources for users - i.e. they authenticate users and contain basic information
 * about users.
 *
 * <p>UserSources let you
 * <li>check if a user exists - {@link #isUserExisting(String)}
 * <li>authenticate a user via a password - {@link #isValid(String, String)}
 * <li>get the default {@link UserStatus} - {@link #getDefaultUserStatus(String)}
 * <li>get the default email address - {@link #getDefaultUserEmail(String)}
 * <li>retrieve a users roles - {@link #resolveRolesForUsername(String)} The default email and
 *     default {@link UserStatus} might be overridden by other settings in CaosDB - that's why they
 *     are called "default".
 *
 *     <p>Also, the user's roles might be overridden by the internal user source {@link
 *     InternalUserSource}.
 *
 *     <p>A UserSource is configured via {@link #setMap(Map)}.
 *
 * @author Timm Fitschen (t.fitschen@indiscale.com)
 */
public interface UserSource {

  /**
   * Every UserSource has a unique name, e.g. PAM, CaosDB (which is default name of the internal
   * user source {@link InternalUserSource}).
   *
   * @return name
   */
  public String getName();

  /**
   * Check if a user exists.
   *
   * @param username
   * @return true iff this user source knows a user with that name
   */
  public boolean isUserExisting(String username);

  /**
   * Return all roles that a user is associated with.
   *
   * @param username
   * @return a user's roles
   */
  public Set<String> resolveRolesForUsername(final String username);

  /**
   * Configure this user source. The needed parameters are to be defined and documented by the
   * implementations.
   *
   * @param map the configuration
   */
  public void setMap(Map<String, String> map);

  /**
   * Return the {@link UserStatus} of that user.
   *
   * @param username
   * @return The user status of that user
   */
  public UserStatus getDefaultUserStatus(String username);

  /**
   * Return the email address of that user, or null if none is available as per this user source.
   *
   * <p>This method does not check if a user exists. So it will return null for unknown users.
   *
   * @param username
   * @return The email address or null
   */
  public String getDefaultUserEmail(String username);

  /**
   * Check if the user can be authenticated by the given password.
   *
   * @param username
   * @param password
   * @return true iff the password was correct.
   */
  public boolean isValid(String username, String password);
}
