package org.caosdb.server.accessControl;

public class Config {
  private String[] permissions = {};
  private String[] roles = {};
  private String purpose = null;
  private OneTimeTokenToFile output = null;
  private int maxReplays = 1;
  private long expiresAfter = OneTimeAuthenticationToken.DEFAULT_TIMEOUT_MS;
  private long replayTimeout = OneTimeAuthenticationToken.DEFAULT_REPLAYS_TIMEOUT_MS;
  private String name = AnonymousAuthenticationToken.PRINCIPAL.getUsername();

  public Config() {}

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public long getExpiresAfter() {
    return expiresAfter;
  }

  public void setExpiresAfter(long timeout) {
    this.expiresAfter = timeout;
  }

  public void setReplayTimeoutSeconds(long seconds) {
    this.setReplayTimeout(seconds * 1000);
  }

  public void setExpiresAfterSeconds(long seconds) {
    this.setExpiresAfter(seconds * 1000);
  }

  public void setMaxReplays(int maxReplays) {
    this.maxReplays = maxReplays;
  }

  public int getMaxReplays() {
    return maxReplays;
  }

  public String[] getPermissions() {
    return permissions;
  }

  public String getPurpose() {
    return purpose;
  }

  public void setPermissions(String[] permissions) {
    this.permissions = permissions;
  }

  public String[] getRoles() {
    return roles;
  }

  public void setRoles(String[] roles) {
    this.roles = roles;
  }

  public void setPurpose(String purpose) {
    this.purpose = purpose;
  }

  public OneTimeTokenToFile getOutput() {
    return output;
  }

  public void setOutput(OneTimeTokenToFile output) {
    this.output = output;
  }

  public long getReplayTimeout() {
    return replayTimeout;
  }

  public void setReplayTimeout(long replaysTimeout) {
    this.replayTimeout = replaysTimeout;
  }
}
