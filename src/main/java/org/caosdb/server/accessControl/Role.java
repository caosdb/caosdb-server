/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2021 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2021 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.caosdb.server.accessControl;

import java.io.Serializable;
import java.util.LinkedList;
import org.caosdb.server.database.proto.ProtoUser;
import org.caosdb.server.permissions.PermissionRule;
import org.jdom2.Element;

public class Role implements Serializable {

  private static final long serialVersionUID = -243913823L;
  public String name = null;
  public String description = null;
  public LinkedList<PermissionRule> permission_rules = null;
  public LinkedList<ProtoUser> users = null;

  public Element toElement() {
    final Element ret = new Element("Role");
    ret.setAttribute("name", this.name);
    ret.setAttribute("description", this.description);
    return ret;
  }
}
