/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2020-2021 Indiscale GmbH <info@indiscale.com>
 * Copyright (C) 2020-2021 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.caosdb.server.accessControl;

import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authz.AuthorizationException;
import org.apache.shiro.config.Ini;
import org.apache.shiro.subject.Subject;
import org.caosdb.server.CaosDBServer;
import org.caosdb.server.ServerProperties;
import org.caosdb.server.entity.Message;
import org.caosdb.server.permissions.Role;
import org.caosdb.server.transaction.LogUserVisitTransaction;
import org.caosdb.server.transaction.RetrieveRoleTransaction;
import org.caosdb.server.transaction.RetrieveUserTransaction;
import org.caosdb.server.utils.ServerMessages;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This singleton class is the primary resource for authenticating users and resolving principals to
 * roles.
 *
 * <p>Key concepts:
 *
 * <ul>
 *   <li>User name: A string which identifies a user uniquely across one realm. Why is this so?
 *       Because it is possible, that two different people from collaborating work groups with
 *       similar names have the same user name in their group e.g. "mueller@uni1.de" and
 *       "mueller@uni2.de" or two people from different user groups use the name "admin". In the
 *       "mueller" example the domain name of the email is the realm of authentication.
 *   <li>Realm: A string which uniquely identifies "where a user comes from". It guarantees the
 *       authentication of a user with a particular user name. Currently the possible realms are
 *       quite limited. Only "CaosDB" (which is controlled by the internal user source) and "PAM"
 *       which delegates authentication to the host system via PAM (Pluggable Authentication Module)
 *       are known and extension is not too easy.
 *   <li>User Source: An instance which provides the access to a realm where users can be
 *       authenticated.
 *   <li>Principal: The combination of realm and user name - hence a system-wide unique identifier
 *       for users and the primary key to identifying who did what and who is allowed to to do what.
 * </ul>
 *
 * @author Timm Fitschen (t.fitschen@indiscale.com)
 */
public class UserSources extends HashMap<String, UserSource> {

  public static final String USERNAME_PASSWORD_AUTHENTICATION = "USERNAME_PASSWORD_AUTHENTICATION";
  private static final Subject transactor = new SinglePermissionSubject("ACM:*:RETRIEVE:*");
  private static final Logger logger = LoggerFactory.getLogger(UserSources.class);
  public static final String KEY_DEFAULT_REALM = "defaultRealm";
  public static final String KEY_REALMS = "realms";
  public static final String KEY_REALM_CLASS = "class";

  private static final long serialVersionUID = 256585362760388255L;
  private static final UserSource internalRealm = new InternalUserSource();

  private static UserSources instance = new UserSources();

  /**
   * Check whether a user exists.
   *
   * @param principal - principal of the user.
   * @return true iff the user identified by the given {@link Principal} exists.
   */
  public static boolean isUserExisting(final Principal principal) {
    if (principal.getRealm().equals(OneTimeAuthenticationToken.REALM_NAME)) {
      return true;
    }
    if (principal.toString().equals(AnonymousAuthenticationToken.PRINCIPAL.toString())
        && CaosDBServer.getServerProperty(ServerProperties.KEY_AUTH_OPTIONAL)
            .equalsIgnoreCase("true")) {
      return true;
    }
    UserSource userSource = instance.get(principal.getRealm());
    if (userSource != null) {
      return userSource.isUserExisting(principal.getUsername());
    }
    return false;
  }

  private UserSources() {
    initMap();
    this.put(getInternalRealm());
    if (this.map.getSection(Ini.DEFAULT_SECTION_NAME) == null
        || !this.map.getSection(Ini.DEFAULT_SECTION_NAME).containsKey(KEY_REALMS)) {
      // no realms defined
      return;
    }
    final String[] realms =
        this.map
            .getSectionProperty(Ini.DEFAULT_SECTION_NAME, KEY_REALMS)
            .split("(\\s*,\\s*)|(\\s+)");
    for (final String realm : realms) {
      if (realm != null && !realm.isEmpty()) {
        final String className = this.map.getSectionProperty(realm, KEY_REALM_CLASS);
        if (className != null && !className.isEmpty()) {
          try {
            @SuppressWarnings("unchecked")
            final Class<? extends UserSource> clazz =
                (Class<? extends UserSource>) Class.forName(className);
            this.put(clazz.getDeclaredConstructor().newInstance());
          } catch (IllegalArgumentException
              | InvocationTargetException
              | NoSuchMethodException
              | SecurityException
              | ClassNotFoundException
              | InstantiationException
              | IllegalAccessException e) {
            logger.error("LOAD_USER_SOURCE", e);
          }
        }
      }
    }
  }

  private Ini map = null;

  public UserSource put(final UserSource src) {
    if (src.getName() == null) {
      throw new IllegalArgumentException("A user source's name must not be null.");
    }
    if (containsKey(src.getName())) {
      throw new IllegalArgumentException("This user source's name is already in use.");
    }
    src.setMap(this.map.getSection(src.getName()));
    return put(src.getName(), src);
  }

  public static UserSource add(final UserSource src) {
    return instance.put(src);
  }

  public void initMap() {
    this.map = new Ini();
    try (final FileInputStream f =
        new FileInputStream(
            CaosDBServer.getServerProperty(ServerProperties.KEY_USER_SOURCES_INI_FILE))) {
      this.map.load(f);
    } catch (final IOException e) {
      logger.debug("could not load usersources.ini", e);
    }
  }

  /**
   * Return the roles of a given user.
   *
   * @param realm
   * @param username
   * @return A set of user roles.
   */
  public static Set<String> resolveRoles(String realm, final String username) {
    if (AnonymousAuthenticationToken.PRINCIPAL.getRealm().equals(realm)
        && AnonymousAuthenticationToken.PRINCIPAL.getUsername().equals(username)) {
      return Collections.singleton(Role.ANONYMOUS_ROLE.toString());
    }
    if (realm == null) {
      realm = guessRealm(username);
    }

    RetrieveUserTransaction t = new RetrieveUserTransaction(realm, username, transactor);
    try {
      t.execute();
      if (t.getUser() != null) return t.getRoles();
    } catch (Exception e) {
      throw new AuthorizationException("Could not resolve roles for " + username + "@" + realm);
    }

    return null;
  }

  public static String guessRealm(final String username) {
    String found = null;
    for (final UserSource u : instance.values()) {
      if (u.isUserExisting(username)) {
        if (found == null) {
          found = u.getName();
        } else {
          throw new AuthenticationException(
              "This user cannot be uniquely identified. This name is used in several realms. ");
        }
      }
    }
    return found;
  }

  public static String guessRealm(final String username, final String defaultRealm) {
    final String found = guessRealm(username);
    if (found != null) {
      return found;
    }

    return defaultRealm;
  }

  public static String getDefaultRealm() {
    return instance.map.getSectionProperty(Ini.DEFAULT_SECTION_NAME, KEY_DEFAULT_REALM, "CaosDB");
  }

  /**
   * Return the roles of a given user.
   *
   * @param principal
   * @return A set of role names.
   */
  public static Set<String> resolveRoles(final Principal principal) {
    if (AnonymousAuthenticationToken.PRINCIPAL == principal) {
      // anymous has one role
      Set<String> roles = new HashSet<>();
      roles.add(Role.ANONYMOUS_ROLE.toString());
      return roles;
    }
    if (principal instanceof OneTimeAuthenticationToken) {
      return new HashSet<>(((OneTimeAuthenticationToken) principal).getRoles());
    }

    return resolveRoles(principal.getRealm(), principal.getUsername());
  }

  public static boolean isRoleExisting(final String role) {
    final RetrieveRoleTransaction t = new RetrieveRoleTransaction(role, transactor);
    try {
      t.execute();
      return true;
    } catch (final Message m) {
      if (m != ServerMessages.ROLE_DOES_NOT_EXIST) {
        throw new AuthenticationException(m);
      }
      return false;
    } catch (final Exception e) {
      throw new AuthenticationException(e);
    }
  }

  public static UserStatus getDefaultUserStatus(final String realm, String username) {
    return instance.get(realm).getDefaultUserStatus(username);
  }

  public static UserStatus getDefaultUserStatus(final Principal p) {
    return getDefaultUserStatus(p.getRealm(), p.getUsername());
  }

  public static String getDefaultUserEmail(final Principal p) {
    return getDefaultUserEmail(p.getRealm(), p.getUsername());
  }

  public static String getDefaultUserEmail(String realm, String username) {
    return instance.get(realm).getDefaultUserEmail(username);
  }

  public static UserSource getInternalRealm() {
    return internalRealm;
  }

  public static boolean isValid(String realm, final String username, final String password) {
    if (realm == null) {
      realm = guessRealm(username);
    }

    final boolean isValid = instance.get(realm).isValid(username, password);

    if (isValid && isActive(realm, username)) {
      logUserVisit(realm, username, USERNAME_PASSWORD_AUTHENTICATION);
      return true;
    }
    return false;
  }

  /** Log the current time as the user's last visit. */
  public static void logUserVisit(String realm, String username, String type) {
    try {
      LogUserVisitTransaction t =
          new LogUserVisitTransaction(System.currentTimeMillis(), realm, username, type);
      t.execute();
    } catch (final Exception e) {
      throw new AuthenticationException(e);
    }
  }

  private static boolean isActive(final String realm, final String username) {
    final RetrieveUserTransaction t = new RetrieveUserTransaction(realm, username, transactor);
    try {
      t.execute();
      if (t.getUser() != null) {
        return t.getUser().status == UserStatus.ACTIVE;
      } else {
        return instance.get(realm).getDefaultUserStatus(username) == UserStatus.ACTIVE;
      }
    } catch (final Exception e) {
      throw new AuthenticationException(e);
    }
  }

  public static boolean isActive(Principal principal) {
    if (principal.getRealm().equals(OneTimeAuthenticationToken.REALM_NAME)) {
      return true;
    }
    if (principal.getUsername().equals(AnonymousAuthenticationToken.PRINCIPAL.getUsername())
        && principal.getRealm().equals(AnonymousAuthenticationToken.PRINCIPAL.getRealm())
        && CaosDBServer.getServerProperty(ServerProperties.KEY_AUTH_OPTIONAL)
            .equalsIgnoreCase("true")) {
      return true;
    }
    return isActive(principal.getRealm(), principal.getUsername());
  }

  public static Set<String> getDefaultRoles(String realm, String username) {
    UserSource userSource = instance.get(realm);
    if (userSource == null) {
      return null;
    }
    // find all roles that are associated with this principal in this realm
    final Set<String> ret = userSource.resolveRolesForUsername(username);

    return ret;
  }
}
