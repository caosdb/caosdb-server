/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.accessControl;

import java.util.HashMap;
import org.caosdb.server.permissions.ResponsibleAgent;
import org.jdom2.Element;

public class Principal implements ResponsibleAgent {

  private final String username;
  private final String realm;
  public static final String REALM_SEPARATOR = "@";

  public Principal(final String realm, final String username) {
    this.username = username;
    this.realm = parseRealm(realm, username);
  }

  private static String parseRealm(final String realm, final String username) {
    return (realm == null
        ? UserSources.guessRealm(username, UserSources.getDefaultRealm())
        : realm);
  }

  public Principal(final String[] split) {
    this(split[0], split[1]);
  }

  public Principal(Principal principal) {
    this.username = principal.username;
    this.realm = principal.realm;
  }

  public String getRealm() {
    return this.realm;
  }

  public String getUsername() {
    return this.username;
  }

  @Override
  public void addToElement(final Element e) {
    e.setAttribute("username", this.username);
    e.setAttribute("realm", this.realm);
  }

  @Override
  public void addToMap(final HashMap<String, Object> map) {
    map.put("username", this.username);
    map.put("realm", this.realm);
  }

  @Override
  public int hashCode() {
    return this.username.hashCode() + this.realm.hashCode() * 7;
  }

  @Override
  public boolean equals(final Object obj) {
    if (obj instanceof Principal) {
      final Principal that = (Principal) obj;
      return this.username.equals(that.username) && this.realm.equals(that.realm);
    }
    return false;
  }

  @Override
  public String toString() {
    return this.username + REALM_SEPARATOR + this.realm;
  }
}
