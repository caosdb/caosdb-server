package org.caosdb.server.accessControl;

import com.google.common.io.Files;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import org.caosdb.server.CaosDBServer;
import org.quartz.CronScheduleBuilder;
import org.quartz.Job;
import org.quartz.JobBuilder;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;

public class OneTimeTokenToFile implements Job {
  private String file = null;
  private String schedule = null;

  public OneTimeTokenToFile() {}

  public static void output(OneTimeAuthenticationToken t, String file) throws IOException {
    output(t, new File(file));
  }

  public static void output(OneTimeAuthenticationToken t, File file) throws IOException {
    Files.createParentDirs(file);
    try (PrintWriter writer = new PrintWriter(file, "utf-8")) {
      writer.print(t.toString());
    }
  }

  public String getFile() {
    return file;
  }

  public void setFile(String file) {
    this.file = file;
  }

  public String getSchedule() {
    return schedule;
  }

  public void setSchedule(String schedule) {
    this.schedule = schedule;
  }

  /** If no schedule was set, immediately write the config to file, else schedule the job. */
  public void init(Config config) throws IOException, SchedulerException {

    if (this.schedule != null) {
      OneTimeAuthenticationToken.generate(config); // test config, throw away token
      JobDataMap map = new JobDataMap();
      map.put("config", config);
      map.put("file", file);
      JobDetail outputJob = JobBuilder.newJob(OneTimeTokenToFile.class).setJobData(map).build();
      Trigger trigger =
          TriggerBuilder.newTrigger()
              .withIdentity(config.toString())
              .withSchedule(CronScheduleBuilder.cronSchedule(this.schedule))
              .build();
      CaosDBServer.scheduleJob(outputJob, trigger);
    } else {
      output(OneTimeAuthenticationToken.generate(config), file);
    }
  }

  @Override
  public void execute(JobExecutionContext context) throws JobExecutionException {
    Config config = (Config) context.getMergedJobDataMap().get("config");
    String file = context.getMergedJobDataMap().getString("file");
    try {
      output(OneTimeAuthenticationToken.generate(config), file);
    } catch (IOException e) {
      // TODO log
      e.printStackTrace();
    }
  }
}
