package org.caosdb.server.accessControl;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.apache.shiro.authc.AuthenticationException;

/**
 * Utility class to manage OTTs: mark as consumed, removed expired OTTs, manage maximum number of
 * replays and replay timeout of tokens.
 */
class OneTimeTokenConsumedInfo {

  private static Map<String, OneTimeTokenConsumedInfo> consumedOneTimeTokens = new HashMap<>();

  public static void cleanupConsumedInfo() {
    synchronized (consumedOneTimeTokens) {
      for (Iterator<Map.Entry<String, OneTimeTokenConsumedInfo>> it =
              consumedOneTimeTokens.entrySet().iterator();
          it.hasNext(); ) {
        Map.Entry<String, OneTimeTokenConsumedInfo> next = it.next();
        if (next.getValue().isExpired()) {
          it.remove();
        }
      }
    }
  }

  /** If the token is valid, consume it once and store this information. */
  public static void consume(OneTimeAuthenticationToken oneTimeAuthenticationToken) {
    if (oneTimeAuthenticationToken.isValid()) {
      String key = OneTimeTokenConsumedInfo.getKey(oneTimeAuthenticationToken);
      OneTimeTokenConsumedInfo consumedInfo = null;
      synchronized (consumedOneTimeTokens) {
        consumedInfo = consumedOneTimeTokens.get(key);
        if (consumedInfo == null) {
          consumedInfo = new OneTimeTokenConsumedInfo(oneTimeAuthenticationToken);
          consumedOneTimeTokens.put(key, consumedInfo);
        }
      }
      consumedInfo.consume();
    }
  }

  private OneTimeAuthenticationToken oneTimeAuthenticationToken;
  private List<Long> replays = new LinkedList<>();

  public OneTimeTokenConsumedInfo(OneTimeAuthenticationToken oneTimeAuthenticationToken) {
    this.oneTimeAuthenticationToken = oneTimeAuthenticationToken;
  }

  public static String getKey(OneTimeAuthenticationToken token) {
    return token.checksum;
  }

  private int getNoOfReplays() {
    return replays.size();
  }

  private long getMaxReplays() {
    return oneTimeAuthenticationToken.getMaxReplays();
  }

  private long getReplayTimeout() {
    if (replays.size() == 0) {
      return Long.MAX_VALUE;
    }
    long firstReplayTime = replays.get(0);
    return firstReplayTime + oneTimeAuthenticationToken.getReplaysTimeout();
  }

  /** If there are still replays and time left, increase the replay counter by one. */
  public void consume() {
    synchronized (replays) {
      if (getNoOfReplays() >= getMaxReplays()) {
        throw new AuthenticationException("One-time token was consumed too often.");
      } else if (getReplayTimeout() < System.currentTimeMillis()) {
        throw new AuthenticationException("One-time token replays timeout expired.");
      }
      replays.add(System.currentTimeMillis());
    }
  }

  public boolean isExpired() {
    return oneTimeAuthenticationToken.isExpired();
  }
}
