/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2021 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2021 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.caosdb.server.accessControl;

import java.util.Collection;
import java.util.Set;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;

public class CaosDBAuthorizingRealm extends AuthorizingRealm {

  private static final CaosDBRolePermissionResolver role_permission_resolver =
      new CaosDBRolePermissionResolver();

  public Collection<String> getSessionRoles(SelfValidatingAuthenticationToken token) {
    return token.getRoles();
  }

  public Collection<String> getSessionPermissions(SelfValidatingAuthenticationToken token) {
    return token.getPermissions();
  }

  @Override
  protected AuthorizationInfo doGetAuthorizationInfo(final PrincipalCollection principals) {
    final SimpleAuthorizationInfo authzInfo = new SimpleAuthorizationInfo();
    Object principal = principals.getPrimaryPrincipal();

    // Add explicitly given roles and permissions.
    if (principal instanceof SelfValidatingAuthenticationToken) {
      Collection<String> sessionPermissions =
          getSessionPermissions((SelfValidatingAuthenticationToken) principal);

      Collection<String> sessionRoles =
          getSessionRoles((SelfValidatingAuthenticationToken) principal);

      authzInfo.addRoles(sessionRoles);
      authzInfo.addStringPermissions(sessionPermissions);
    }

    // Find all roles which are associated with this principal in this realm.
    final Set<String> principalRoles =
        UserSources.resolveRoles((Principal) principals.getPrimaryPrincipal());
    if (principalRoles != null) {
      authzInfo.addRoles(principalRoles);
    }

    if (authzInfo.getRoles() != null && !authzInfo.getRoles().isEmpty()) {
      authzInfo.addObjectPermission(
          role_permission_resolver.resolvePermissionsInRole(authzInfo.getRoles()));
    }

    return authzInfo;
  }

  public CaosDBAuthorizingRealm() {
    setCachingEnabled(false);
    setAuthorizationCachingEnabled(false);
  }

  @Override
  public boolean supports(final AuthenticationToken token) {
    // this is not an authenticating realm - just for authorizing.
    return false;
  }

  @Override
  protected AuthenticationInfo doGetAuthenticationInfo(final AuthenticationToken token) {
    return null;
  }
}
