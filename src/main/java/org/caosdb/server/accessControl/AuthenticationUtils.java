/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2020 Indiscale GmbH <info@indiscale.com>
 * Copyright (C) 2020 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.accessControl;

import static org.caosdb.server.utils.Utils.URLDecodeWithUTF8;

import java.sql.Timestamp;
import java.util.Collection;
import java.util.LinkedList;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.subject.Subject;
import org.caosdb.server.CaosDBServer;
import org.caosdb.server.ServerProperties;
import org.caosdb.server.permissions.ResponsibleAgent;
import org.caosdb.server.permissions.Role;
import org.caosdb.server.utils.Utils;
import org.restlet.data.Cookie;
import org.restlet.data.CookieSetting;

/**
 * Useful static methods, mainly for parsing and serializing SessionTokens by the means of web
 * cookies and producing session timeout cookies.
 *
 * @author tf
 */
public class AuthenticationUtils {

  public static final String SESSION_TOKEN_COOKIE = "SessionToken";
  public static final String SESSION_TIMEOUT_COOKIE = "SessionTimeOut";

  public static boolean isAnonymous(Subject user) {
    return AnonymousAuthenticationToken.PRINCIPAL.equals(user.getPrincipal());
  }

  public static boolean isAnonymous(Principal principal) {
    return AnonymousAuthenticationToken.PRINCIPAL.equals(principal);
  }

  /**
   * Create a cookie for a {@link SelfValidatingAuthenticationToken}. Returns null if the parameter
   * is null or the token is invalid. The cookie will have the httpOnly and secure flags enabled.
   *
   * @param token
   * @return A Cookie for the token.
   * @see {@link AuthenticationUtils#parseSessionTokenCookie(Cookie, String)}, {@link
   *     SelfValidatingAuthenticationToken}
   */
  private static CookieSetting createTokenCookie(
      final String cookieName, final SelfValidatingAuthenticationToken token) {
    if (token != null && token.isValid()) {
      // new expiration time.
      final int exp_in_sec = (int) Math.ceil(token.getTimeout() / 1000.0);
      return new CookieSetting(
          0,
          cookieName,
          Utils.URLEncodeWithUTF8(token.toString()),
          CaosDBServer.getServerProperty(ServerProperties.KEY_CONTEXT_ROOT) + "/",
          null,
          null,
          exp_in_sec,
          true,
          true);
    }
    return null;
  }

  public static CookieSetting createSessionTokenCookie(
      final SelfValidatingAuthenticationToken token) {
    return createTokenCookie(AuthenticationUtils.SESSION_TOKEN_COOKIE, token);
  }

  /**
   * Parse a SessionToken from a cookie with optional cryptographic curry.
   *
   * @param cookie
   * @param curry
   * @return A new SessionToken
   * @see {@link AuthenticationUtils#createSessionTokenCookie(SessionToken)}, {@link SessionToken}
   */
  public static SelfValidatingAuthenticationToken parseSessionTokenCookie(final Cookie cookie) {
    if (cookie != null) {
      final String tokenString = URLDecodeWithUTF8(cookie.getValue());
      if (tokenString != null && !tokenString.equals("")) {
        return SelfValidatingAuthenticationToken.parse(tokenString);
      }
    }
    return null;
  }

  /**
   * Create a session timeout cookie. The value is a plain UTC timestamp which tells the user how
   * long his session will stay active. This cookie will be ignored by the server and carries only
   * additional information for the user interfaces (E.g. they can remind the user before her
   * session is expiring or do an auto-logout based on this timestamp). Of course, the cookie will
   * be flagged with httpOnly:false (but secure:true).
   *
   * @param token a SessionToken
   * @return
   */
  public static CookieSetting createSessionTimeoutCookie(final SessionToken token) {

    // replace empty space with "T" for iso 8601 compliance
    String t = new Timestamp(0).toString().replaceFirst(" ", "T");
    int exp_in_sec = 0; // expires in zero seconds.

    if (token != null && token.isValid()) {
      t = new Timestamp(token.getExpires()).toString().replaceFirst(" ", "T");
      exp_in_sec = (int) Math.ceil(token.getTimeout() / 1000.0); // new expiration time
      return new CookieSetting(
          0,
          AuthenticationUtils.SESSION_TIMEOUT_COOKIE,
          Utils.URLEncodeWithUTF8(t),
          CaosDBServer.getServerProperty(ServerProperties.KEY_CONTEXT_ROOT) + "/",
          null,
          null,
          exp_in_sec,
          false,
          false);
    }
    return null;
  }

  // TODO move
  public static boolean isResponsibleAgentExistent(final ResponsibleAgent agent) {
    // 1) check OWNER, OTHER
    if (Role.OTHER_ROLE.equals(agent) || Role.OWNER_ROLE.equals(agent)) {
      return true;
    }

    // 2) check role exists in UserSources
    if (UserSources.isRoleExisting(agent.toString())) {
      return true;
    }

    // 3) check user exists in UserSources
    return agent instanceof Principal && UserSources.isUserExisting((Principal) agent);
  }

  public static CookieSetting getLogoutSessionTokenCookie() {
    return new CookieSetting(
        0,
        AuthenticationUtils.SESSION_TOKEN_COOKIE,
        "invalid",
        CaosDBServer.getServerProperty(ServerProperties.KEY_CONTEXT_ROOT) + "/",
        null,
        null,
        0,
        true,
        true);
  }

  public static Collection<? extends CookieSetting> getLogoutCookies() {
    final LinkedList<CookieSetting> ret = new LinkedList<CookieSetting>();
    ret.add(getLogoutSessionTokenCookie());
    ret.add(getLogoutSessionTimeoutCookie());
    return ret;
  }

  private static CookieSetting getLogoutSessionTimeoutCookie() {
    return new CookieSetting(
        0,
        AuthenticationUtils.SESSION_TIMEOUT_COOKIE,
        "invalid",
        CaosDBServer.getServerProperty(ServerProperties.KEY_CONTEXT_ROOT) + "/",
        null,
        null,
        0,
        false,
        false);
  }

  public static Collection<String> getRoles(Subject user) {
    return new CaosDBAuthorizingRealm().doGetAuthorizationInfo(user.getPrincipals()).getRoles();
  }

  public static boolean isFromOneTimeTokenRealm(Subject subject) {
    return ((Principal) subject.getPrincipal())
        .getRealm()
        .equals(OneTimeAuthenticationToken.REALM_NAME);
  }

  public static AuthorizationInfo getAuthorizationInfo(Subject user) {
    return new CaosDBAuthorizingRealm().doGetAuthorizationInfo(user.getPrincipals());
  }
}
