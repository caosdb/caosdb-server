/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.accessControl;

import java.util.HashSet;
import java.util.Set;
import org.apache.shiro.authc.AuthenticationException;
import org.caosdb.server.entity.Message;
import org.caosdb.server.permissions.CaosPermission;
import org.caosdb.server.permissions.PermissionRule;
import org.caosdb.server.transaction.RetrievePermissionRulesTransaction;
import org.caosdb.server.utils.ServerMessages;

public class CaosDBRolePermissionResolver {

  /** Return CaosPermission with the rules which are associated with the roles. */
  public CaosPermission resolvePermissionsInRole(final Set<String> roles) {
    final HashSet<PermissionRule> rules = new HashSet<PermissionRule>();
    for (final String role : roles) {
      final RetrievePermissionRulesTransaction t = new RetrievePermissionRulesTransaction(role);
      try {
        t.execute();
        rules.addAll(t.getRules());
      } catch (final Message m) {
        if (m != ServerMessages.ROLE_DOES_NOT_EXIST) {
          throw new AuthenticationException(m);
        }
      } catch (final Exception e) {
        throw new AuthenticationException(e);
      }
    }

    return new CaosPermission(rules);
  }
}
