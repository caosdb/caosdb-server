/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.accessControl;

import org.apache.shiro.authc.AuthenticationToken;

public class AnonymousAuthenticationToken implements AuthenticationToken {

  private static final long serialVersionUID = 3573372964529451961L;
  private static final AnonymousAuthenticationToken INSTANCE = new AnonymousAuthenticationToken();
  public static final Principal PRINCIPAL = new Principal("anonymous", "anonymous");

  private AnonymousAuthenticationToken() {}

  public static AnonymousAuthenticationToken getInstance() {
    return INSTANCE;
  }

  @Override
  public Principal getPrincipal() {
    return PRINCIPAL;
  }

  @Override
  public Object getCredentials() {
    return null;
  }

  @Override
  public boolean equals(Object obj) {
    return obj == this;
  }
}
