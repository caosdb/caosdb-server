package org.caosdb.server.accessControl;

import org.caosdb.server.CaosDBServer;
import org.quartz.CronScheduleBuilder;
import org.quartz.Job;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;

public class ConsumedInfoCleanupJob implements Job {

  public static void scheduleDaily() throws SchedulerException {
    JobDetail job = JobBuilder.newJob(ConsumedInfoCleanupJob.class).build();
    Trigger trigger =
        TriggerBuilder.newTrigger()
            .withSchedule(CronScheduleBuilder.dailyAtHourAndMinute(0, 0))
            .build();
    CaosDBServer.scheduleJob(job, trigger);
  }

  @Override
  public void execute(JobExecutionContext context) throws JobExecutionException {
    OneTimeTokenConsumedInfo.cleanupConsumedInfo();
  }
}
