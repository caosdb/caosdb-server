/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */

/**
 * @review Daniel Hornung 2022-03-04
 */
package org.caosdb.server.datatype;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import org.jdom2.Element;

public class CollectionValue implements Value, Iterable<IndexedSingleValue> {

  private List<IndexedSingleValue> list = new ArrayList<>();

  @Override
  public void addToElement(final Element e) {
    for (final SingleValue v : this) {
      final Element valueElem = new Element("Value");
      v.addToElement(valueElem);
      e.addContent(valueElem);
    }
  }

  public boolean add(final IndexedSingleValue e) {
    if (e == null) {
      return list.add(new IndexedSingleValue(null));
    }
    return list.add(e);
  }

  public void add(final SingleValue v) {
    this.add(new IndexedSingleValue(v));
  }

  public void add(final int i, final SingleValue v) {
    this.add(new IndexedSingleValue(i, v));
  }

  public void add(final Value v) {
    add(0, v);
  }

  public void add(final int i, final Value v) {
    if (v == null || v instanceof SingleValue) {
      add(i, (SingleValue) v);
    } else {
      throw new IllegalArgumentException("Expected a SingleValue.");
    }
  }

  @Override
  public Iterator<IndexedSingleValue> iterator() {
    return list.iterator();
  }

  public void sort() {
    Collections.sort(list);
  }

  public int size() {
    return list.size();
  }

  /** Compares if the content is equal, regardless of the order. */
  @Override
  public boolean equals(Value val) {
    if (val instanceof CollectionValue) {
      CollectionValue that = (CollectionValue) val;
      sort();
      that.sort();
      return this.list.equals(that.list);
    }
    return false;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj instanceof Value) {
      return this.equals((Value) obj);
    }
    return false;
  }
}
