/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * @review Daniel Hornung 2022-03-04
 */
package org.caosdb.server.datatype;

import com.google.common.base.Objects;
import org.caosdb.server.datatype.AbstractDatatype.Table;
import org.jdom2.Element;

public abstract class AbstractEnumValue implements SingleValue {

  private final EnumElement value;

  public AbstractEnumValue(final EnumElement e) {
    this.value = e;
  }

  @Override
  public final void addToElement(final Element e) {
    e.addContent(this.value.toString());
  }

  @Override
  public final Table getTable() {
    return Table.enum_data;
  }

  @Override
  public final String toDatabaseString() {
    return this.value.toString();
  }

  @Override
  public boolean equals(final Object obj) {
    if (obj instanceof AbstractEnumValue) {
      return equals((AbstractEnumValue) obj);
    }
    return false;
  }

  @Override
  public boolean equals(Value val) {
    if (val instanceof AbstractEnumValue) {
      final AbstractEnumValue that = (AbstractEnumValue) val;
      return Objects.equal(that.value, this.value);
    }
    return false;
  }
}
