/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.datatype;

import com.google.common.base.Objects;
import org.caosdb.server.datatype.AbstractDatatype.Table;
import org.jdom2.Element;

public class GenericValue implements SingleValue {
  Object value;
  Table table;

  public GenericValue(final Integer i) {
    if (i == null) {
      throw new NullPointerException();
    }
    this.value = i;
    this.table = Table.integer_data;
  }

  public GenericValue(final Double d) {
    if (d == null) {
      throw new NullPointerException();
    }
    this.value = d;
    if (d.isNaN()) {
      this.table = Table.text_data;
    } else {
      this.table = Table.double_data;
    }
  }

  public GenericValue(final String s) {
    if (s == null) {
      throw new NullPointerException();
    }
    this.value = s;
    this.table = Table.text_data;
  }

  public GenericValue(final long value) {
    this((Integer) Math.toIntExact(value));
  }

  @Override
  public void addToElement(final Element e) {
    if (this.value instanceof String && ((String) this.value).isEmpty()) {
      final Element empty = new Element("EmptyString");
      e.addContent(empty);
    } else {
      e.addContent(this.value.toString());
    }
  }

  @Override
  public Table getTable() {
    return this.table;
  }

  @Override
  public String toDatabaseString() {
    return this.value.toString();
  }

  @Override
  public String toString() {
    return toDatabaseString();
  }

  @Override
  public boolean equals(final Object obj) {
    if (obj instanceof Value) {
      return equals((Value) obj);
    }
    return false;
  }

  @Override
  public boolean equals(Value val) {
    if (val instanceof GenericValue) {
      final GenericValue that = (GenericValue) val;
      return Objects.equal(that.value, this.value);
    }
    return false;
  }

  public Object getValue() {
    return this.value;
  }
}
