/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.datatype;

import org.caosdb.server.entity.Message;

public class ListDatatype extends AbstractCollectionDatatype {

  private final AbstractDatatype datatype;

  public ListDatatype(final AbstractDatatype datatype) {
    this.datatype = datatype;
  }

  @Override
  public Value parseValue(final Object value) throws Message {
    final CollectionValue ret = new CollectionValue();
    if (value instanceof CollectionValue) {
      final CollectionValue colValue = (CollectionValue) value;
      colValue.sort();
      for (final IndexedSingleValue singleValue : colValue) {
        if (singleValue.getWrapped() == null) {
          ret.add(singleValue.getIndex(), null);
        } else {
          ret.add(singleValue.getIndex(), getDatatype().parseValue(singleValue.getWrapped()));
        }
      }
    } else {
      ret.add(getDatatype().parseValue(value));
    }
    return ret;
  }

  public String getDatatypeName() {
    return this.datatype.getName();
  }

  @Override
  public String getCollectionName() {
    return "LIST";
  }

  @Override
  public AbstractDatatype getDatatype() {
    return this.datatype;
  }
}
