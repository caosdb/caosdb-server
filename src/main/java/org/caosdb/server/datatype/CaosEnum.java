/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.datatype;

import com.google.common.base.Objects;
import java.util.TreeSet;

class EnumElement implements Comparable<EnumElement> {
  public final int index;
  public final String name;

  @Override
  public int hashCode() {
    return this.name.hashCode();
  }

  public EnumElement(final int index, final String name) {
    this.index = index;
    this.name = name;
  }

  @Override
  public int compareTo(final EnumElement o) {
    return this.index - o.index;
  }

  @Override
  public String toString() {
    return this.name;
  }

  @Override
  public boolean equals(final Object obj) {
    if (obj instanceof EnumElement) {
      final EnumElement that = (EnumElement) obj;
      return Objects.equal(this.name, that.name);
    }
    return false;
  }
}

public class CaosEnum {

  final boolean case_sensitive; // for string operations

  public CaosEnum(final String... values) {
    this(false, values);
  }

  public CaosEnum(final boolean case_sensitive, final String... values) {
    this.case_sensitive = case_sensitive;
    int index = 0;
    for (String v : values) {
      if (!case_sensitive) {
        v = v.toUpperCase();
      }
      this.values.add(new EnumElement(index++, v));
    }
  }

  private final TreeSet<EnumElement> values = new TreeSet<EnumElement>();

  public EnumElement valueOf(final String s) {
    int hash;
    if (!this.case_sensitive) {
      hash = s.toUpperCase().hashCode();
    } else {
      hash = s.hashCode();
    }
    for (final EnumElement e : this.values) {
      if (e.hashCode() == hash) {
        return e;
      }
    }
    throw new IllegalArgumentException("No such element: " + s);
  }
}
