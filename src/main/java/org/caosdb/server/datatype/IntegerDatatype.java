/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.datatype;

import org.caosdb.server.entity.Message;
import org.caosdb.server.utils.ServerMessages;

@DatatypeDefinition(name = "Integer")
public class IntegerDatatype extends AbstractDatatype {

  @Override
  public SingleValue parseValue(final Object value) throws Message {
    try {
      if (value instanceof GenericValue) {
        return new GenericValue(Long.parseLong(((GenericValue) value).toDatabaseString()));
      } else if (value instanceof CollectionValue) {
        throw ServerMessages.DATA_TYPE_DOES_NOT_ACCEPT_COLLECTION_VALUES;
      } else {
        return new GenericValue(Long.parseLong(value.toString()));
      }

    } catch (final ArithmeticException e) {
      throw ServerMessages.INTEGER_OUT_OF_RANGE;
    } catch (final NumberFormatException e) {
      throw ServerMessages.CANNOT_PARSE_INT_VALUE;
    }
  }
}
