/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2021 Indiscale GmbH <info@indiscale.com>
 * Copyright (C) 2021 Timm Fitschen <t.fitschen@indiscale.com>
 * Copyright (C) 2021 Daniel Hornung <d.hornung@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.database;

import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReentrantLock;
import org.caosdb.server.database.access.Access;
import org.caosdb.server.database.access.AccessControlAccess;
import org.caosdb.server.database.access.InfoAccess;
import org.caosdb.server.database.access.InitAccess;
import org.caosdb.server.database.access.TransactionAccess;
import org.caosdb.server.transaction.AccessControlTransaction;
import org.caosdb.server.transaction.TransactionInterface;
import org.caosdb.server.transaction.WriteTransactionInterface;
import org.caosdb.server.utils.Info;
import org.caosdb.server.utils.Initialization;
import org.caosdb.server.utils.Releasable;

/**
 * Acquire and release read access. {@link DatabaseAccessManager} uses this class for managing
 * access to the back-end during processing updates, inserts, deletions and retrievals. Read access
 * will be granted immediately for every thread that requests it, unless a thread requests that read
 * access be blocked (usually by requesting write access). If this happens, all threads that already
 * have read access will proceed and release their read access as usual but no NEW read permits will
 * be granted.
 *
 * <p>This is a blockable {@link Semaphore}. Any number of threads may acquire a permit unless it is
 * blocked. The blocking thread (requesting a {@link WriteAccessLock}) waits until all threads have
 * released their permits.
 *
 * @author Timm Fitschen <t.fitschen@indiscale.com>
 */
class ReadAccessSemaphore extends Semaphore implements Releasable {

  private static final long serialVersionUID = 4384921156838881337L;
  private AtomicInteger acquired = new AtomicInteger(0); // how many threads have read access
  Semaphore writersBlock =
      new Semaphore(1, true); // This semaphore is blocked as long as there are any

  // unreleased read permits.

  public ReadAccessSemaphore() {
    // this is a fair semaphore with no initial permit.
    super(1, true);
  }

  /**
   * Acquire a read access permit if and only if it has not been blocked via block(). If read access
   * is currently blocked, the thread waits until the unblock() method is invoked by any thread.
   */
  @Override
  public void acquire() throws InterruptedException {
    super.acquire(); // Protect the next few lines
    if (this.acquired.getAndIncrement() == 0) {
      this.writersBlock.acquire();
    }
    super.release();
  }

  /**
   * Release a read access permit.
   *
   * <p>If this is the last remaining acquired permit, also release the general writersBlock.
   */
  @Override
  public void release() {
    if (this.acquired.decrementAndGet() == 0) { // Last permit: release
      if (this.writersBlock.availablePermits() <= 0) {
        this.writersBlock.release();
      }
    }
  }

  /**
   * Acquire the permit to block further read access, if no thread currently has acquired read
   * access and no thread currently has a block.
   *
   * <p>Consequences of calling this method are:
   *
   * <ul>
   *   <li>Further read access permits are blocked for any thread.
   *   <li>Every thread that has a read access already can proceed. The current thread waits until
   *       all threads have released their read access.
   * </ul>
   *
   * <p>If another thread has invoked this method before, the current thread waits until the
   * unblock() method is called.
   *
   * @throws InterruptedException
   */
  public void block() throws InterruptedException {
    super.reducePermits(1);
    this.writersBlock.acquire();
  }

  /**
   * Unblock read access.
   *
   * <p>This method releases the writersBlock introduced by calling the block() method.
   */
  public void unblock() {
    if (this.writersBlock.availablePermits() <= 0) {
      this.writersBlock.release();
    }
    super.release();
  }

  public int waitingAquireAccess() {
    return getQueueLength();
  }
}

/**
 * Acquire and release write access. DatabaseAccessManager uses this class for managing access to
 * entities while processing updates, inserts, deletions and retrievals. Write access will be
 * granted to one and only one thread if no other thread already holds read or write access permits.
 * The write access seat has to be reserved before acquiring the lock.
 *
 * <p>The flow is as follows:
 *
 * <p>
 *
 * <pre>
 *                                        No new read
 *                                      access possible,
 *    Read access      Read access    wait for running read
 *      possible        possible       threads to fininish
 *
 *      +------+       +----------+       +----------+
 * +--> |  no  | ----> | reserved | ----> | acquired | --+
 * |    | Lock |       +----------+       +----------+   |
 * |    +------+                 \          /            |
 * |                           (1 seat together)         |
 * +-----------------------------------------------------+
 *             release()
 * </pre>
 *
 * @author Timm Fitschen
 */
class WriteAccessLock extends ReentrantLock implements Releasable {

  private static final long serialVersionUID = 833147084787201103L;
  private ReadAccessSemaphore readSem = null;
  private Thread reservedBy = null;

  public WriteAccessLock(final ReadAccessSemaphore readSem) {
    super();
    this.readSem = readSem;
  }

  /**
   * Reserve the seat for the next write access. While a write access is reserved but not yet
   * acquired, all read access may still be granted. When a write access has been already granted or
   * another reservation is active, the thread waits until the write access has been released.
   *
   * @throws InterruptedException
   */
  public void reserve() throws InterruptedException {
    super.lock();
    this.reservedBy = Thread.currentThread();
  }

  /**
   * Lock the write access seat. This method returns once all current read permits have been
   * released.
   */
  @Override
  public void lockInterruptibly() throws InterruptedException {
    if (!super.isHeldByCurrentThread()) {
      super.lock();
    }
    this.readSem.block(); // Wait until all current read permits have been released.
  }

  @Override
  public void unlock() {
    if (super.isHeldByCurrentThread()) {
      this.readSem.unblock();
      this.reservedBy = null;
      super.unlock();
    }
  }

  @Override
  public void release() {
    unlock();
  }

  public Thread whoHasReservedAccess() {
    return this.reservedBy;
  }
}

/**
 * Manages the read and write access to the database.
 *
 * @author tf
 */
public class DatabaseAccessManager {

  private DatabaseAccessManager() {}

  private static final DatabaseAccessManager instance = new DatabaseAccessManager();
  private final ReadAccessSemaphore readAccess = new ReadAccessSemaphore();
  private final WriteAccessLock writeAccess = new WriteAccessLock(this.readAccess);

  public static DatabaseAccessManager getInstance() {
    return instance;
  }

  /**
   * Return the thread which has successfully reserved write access. This is the thread which will
   * be the next to actually acquire write access.
   *
   * @return
   */
  public static Thread whoHasReservedWriteAccess() {
    return instance.writeAccess.whoHasReservedAccess();
  }

  /**
   * Acquire read access. This method returns the Access object as soon as there are no active write
   * permits.
   *
   * <p>The returned Access object can be used to read in the data base back-end.
   *
   * <p>Read access can be acquired parallel to other threads having read access or while another
   * thread has <em>reserved</em> write access. As soon as any thread has requested to
   * <em>acquire</em> write access, all other threads have to wait.
   *
   * @param t the {@link TransactionInterface} which requests the read access
   * @return {@link Access} object which holds and abstract away all connection details.
   * @throws InterruptedException
   */
  public Access acquireReadAccess(final TransactionInterface t) throws InterruptedException {
    this.readAccess.acquire();
    return new TransactionAccess(t, this.readAccess);
  }

  /**
   * Reserve write access. This method returns the Access object as soon as there is no other
   * reserved or acquired write access.
   *
   * <p>The returned Access object can be used to read in the data base back-end.
   *
   * <p>The reservation has no effect on granting of read access permits, but only one thread may at
   * any time reserve or acquire write access.
   *
   * @param wt - the {@link WriteTransactionInterface} which request the reservation of the write
   *     access.
   * @return {@link Access} object which holds and abstract away all connection details.
   * @throws InterruptedException
   */
  public Access reserveWriteAccess(final WriteTransactionInterface wt) throws InterruptedException {
    this.writeAccess.reserve();
    return new TransactionAccess(wt, this.writeAccess);
  }

  /**
   * Acquire write access. This method returns the Access object as soon as all already acquired
   * read access permits have been released. When the write access is acquired, no other access can
   * be acquired (read or write).
   *
   * <p>The returned Access object can be used to read and write in the data base back-end.
   *
   * @param wt - the {@link WriteTransactionInterface} which request the acquisition of the write
   *     access.
   * @return {@link Access} object which holds and abstract away all connection details.
   * @throws InterruptedException
   */
  public Access acquireWriteAccess(final WriteTransactionInterface wt) throws InterruptedException {
    this.writeAccess.lockInterruptibly();
    return wt.getAccess();
  }

  /**
   * Special access to be used by {@link Info}.
   *
   * @param i
   * @return
   */
  public static Access getInfoAccess(final Info i) {
    return new InfoAccess(i);
  }

  /**
   * Special access to be used during the {@link Initialization} of the caosdb server.
   *
   * @param initialization
   * @return
   */
  public static Access getInitAccess(final Initialization initialization) {
    return new InitAccess(initialization);
  }

  /**
   * Special access to be used for {@link AccessControlTransaction}s, mainly authentication and
   * authorization.
   *
   * @param t
   * @return
   */
  public static Access getAccountAccess(final AccessControlTransaction t) {
    return new AccessControlAccess(t);
  }
}
