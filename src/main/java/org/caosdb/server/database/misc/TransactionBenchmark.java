/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.database.misc;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.caosdb.server.CaosDBServer;
import org.caosdb.server.ServerProperties;
import org.caosdb.server.utils.CronJob;
import org.caosdb.server.utils.Info;
import org.caosdb.server.utils.ServerStat;
import org.jdom2.Element;

class Counter implements Serializable {
  private static final long serialVersionUID = -95212746021180579L;
  private int c;

  public Counter(int initial) {
    this.c = initial;
  }

  public void add(int inc) {
    this.c += inc;
  }

  public int toInteger() {
    return c;
  }
}

class Timer implements Serializable {
  private static final long serialVersionUID = -95212746021180579L;
  private long c;

  public Timer(long initial) {
    this.c = initial;
  }

  public void add(long inc) {
    this.c += inc;
  }

  public long toLong() {
    return c;
  }
}

/**
 * The measurement of e.g. a certain action, possibly over multiple runs. A Measurement keeps track
 * of how often the action was measured, and what the total and averaged spent time is. One
 * Measurement object knows nothing about other Measurement objects.
 */
class Measurement implements Serializable {
  private static final long serialVersionUID = -95212746021180579L;
  private final Timer timer;
  private final Counter counter;
  private final String name;

  public Measurement(String name, long time, int count) {
    this.name = name;
    this.timer = new Timer(time);
    this.counter = new Counter(count);
  }

  public void add(long time, int count) {
    this.timer.add(time);
    this.counter.add(count);
  }

  public double getAvg() {
    return (double) this.getAccTime() / this.getCount();
  }

  public StringBuilder toStringBuilder() {
    StringBuilder sb = new StringBuilder();
    sb.append('#');
    sb.append(name);
    sb.append(": ");
    sb.append('(');
    sb.append(this.getCount());
    sb.append(", ");
    sb.append(this.getAccTime());
    sb.append(", ");
    sb.append(String.format(Locale.US, "%3.2f", this.getAvg()));
    sb.append(')');
    return sb;
  }

  public int getCount() {
    return counter.toInteger();
  }

  public long getAccTime() {
    return timer.toLong();
  }

  public String getName() {
    return this.name;
  }
}

class RootBenchmark extends TransactionBenchmark implements ServerStat {

  private static final long serialVersionUID = -95212746021180579L;
  private transient boolean synced = false;

  /**
   * Fetch old data (from before last shutdown) and fill it into this instance.
   *
   * @return
   */
  protected TransactionBenchmark init() {
    final RootBenchmark b = this;
    synchronized (this.measurements) {
      final Runnable updater =
          new Runnable() {
            @Override
            public void run() {
              try {
                synchronized (b.measurements) {
                  Info.getInstance().syncDatabase(b);
                }
              } catch (final Exception e) {
                logger.error(e);
              }
            }
          };

      updater.run();

      new CronJob("SyncTransactionBenchmark", updater, 3600, true); // hourly
    }
    return this;
  }

  @Override
  public final String getName() {
    return "TransactionBenchmark";
  }

  /**
   * Add data from s to this instance.
   *
   * @param s another TransactionBenchmark instance
   */
  @Override
  public final void update(final ServerStat s) {
    if (!this.synced && isActive) {
      final RootBenchmark t = (RootBenchmark) s;
      this.synced = true;
      merge(this, t);
    }
  }

  public void merge(TransactionBenchmark target, TransactionBenchmark src) {
    if (isActive) {
      target.since = Long.min(target.since, src.since);
      synchronized (target.measurements) {
        for (final Measurement m : src.measurements.values()) {
          // add all measurements from src
          target.addMeasurement(m);
        }
      }

      // merge subBenchmarks
      synchronized (src.subBenchmarks) {
        for (final SubBenchmark srcsb : src.getSubBenchmarks()) {
          synchronized (target.subBenchmarks) {
            if (target.subBenchmarks.containsKey(srcsb.getName())) {
              merge(target.subBenchmarks.get(srcsb.getName()), srcsb);
            } else {
              target.subBenchmarks.put(srcsb.getName(), srcsb);
            }
          }
        }
      }
    }
  }
}

class SubBenchmark extends TransactionBenchmark {

  private static final long serialVersionUID = -95212746021180579L;
  private final String name;

  public SubBenchmark(String name) {
    this.name = name;
  }

  @Override
  public String getName() {
    return name;
  }
}

class JdomConverter {
  public Element convert(Measurement m) {
    Element ret = new Element("Measurement");
    ret.setAttribute("name", m.getName());
    ret.setAttribute("acc_time", Long.toString(m.getAccTime()));
    ret.setAttribute("avg_time", String.format(Locale.US, "%3.2f", m.getAvg()));
    ret.setAttribute("count", Integer.toString(m.getCount()));
    return ret;
  }

  public Element convert(TransactionBenchmark b) {
    Element ret = new Element(b.getName());
    ret.setAttribute("since", new Date(b.since).toString());
    ret.setAttribute("since_ms", Long.toString(b.since));
    String calledFrom = b.called_from();
    if (null == calledFrom) {
      calledFrom = "-";
    }
    ret.setAttribute("called_from", calledFrom);
    Iterable<Measurement> measurements = b.getMeasurements();

    synchronized (measurements) {
      for (Measurement m : measurements) {
        ret.addContent(this.convert(m));
      }
    }

    // tree view of subBenchmarks
    Iterable<SubBenchmark> subBenchmarks = b.getSubBenchmarks();
    synchronized (subBenchmarks) {
      for (SubBenchmark s : subBenchmarks) {
        ret.addContent(this.convert(s));
      }
    }
    return ret;
  }
}

/**
 * The TransactionBenchmark object allows to store the time which some actions take. At its core, it
 * consists of:
 * <li>A list of named Measurements.
 * <li>A list of named SubBenchmarks.
 *
 *     <p>Each measurement may be recorded on multiple occasions, the stored Measurement objects
 *     keep a statistic about the total and average recorded times.
 */
public abstract class TransactionBenchmark implements Serializable {

  private static final long serialVersionUID = -95212746021180579L;
  private static final TransactionBenchmark rootService = new RootBenchmark().init();
  public final transient Logger logger = LogManager.getLogger(getClass());
  protected static final transient boolean isActive =
      Boolean.valueOf(
          CaosDBServer.getServerProperty(ServerProperties.KEY_TRANSACTION_BENCHMARK_ENABLED)
              .toLowerCase());
  protected StackTraceElement[] stackTraceElements;

  long since = System.currentTimeMillis();

  protected final Map<String, Measurement> measurements = new HashMap<>();
  protected final Map<String, SubBenchmark> subBenchmarks = new HashMap<>();

  protected TransactionBenchmark() {
    stackTraceElements = Thread.currentThread().getStackTrace();
  }
  ;

  public Iterable<SubBenchmark> getSubBenchmarks() {
    return this.subBenchmarks.values();
  }

  public Iterable<Measurement> getMeasurements() {
    return this.measurements.values();
  }

  /**
   * Add a measurement. The object will be toString()'ed. The string serves as a key.
   *
   * @param object
   * @param time
   */
  public void addMeasurement(final Object object, final long time) {
    this.addMeasurement(object, time, 1);
  }

  /**
   * Add a measurement. The object will be toString()'ed. The string serves as a key.
   *
   * @param object
   * @param time
   * @param count
   */
  public void addMeasurement(final Object object, final long time, int count) {
    if (isActive) {
      String name = object.toString();

      synchronized (this.measurements) {
        Measurement existing = this.measurements.get(name);
        if (existing == null) {
          this.measurements.put(name, new Measurement(name, time, count));
        } else {
          existing.add(time, count);
        }
      }
    }
  }

  /**
   * Add a measurement.
   *
   * @param measurement
   */
  public void addMeasurement(Measurement measurement) {
    this.addMeasurement(measurement.getName(), measurement.getAccTime(), measurement.getCount());
  }

  public static TransactionBenchmark getRootInstance() {
    return rootService;
  }

  @Override
  public String toString() {
    if (isActive) {
      final StringBuilder sb = new StringBuilder();
      sb.append(
          "\nNAME: (execution count, accumulated execution time [ms], avg time per execution [ms])\n");
      synchronized (this.measurements) {
        for (final Entry<String, Measurement> e : this.measurements.entrySet()) {
          final Measurement m = e.getValue();
          sb.append('\n');
          sb.append(m.toStringBuilder());
        }
      }
      return sb.toString();
    } else {
      return "TransactionBenchmark is disabled.";
    }
  }

  public Element toElement() {
    if (isActive) {
      final Element el = new JdomConverter().convert(this);
      // String name = el.getName();
      // if (name != "TransactionBenchmark") {
      //   name = name + ".TransactionBenchmark";
      // }
      // return el.setName(name);
      return el.setName("TransactionBenchmark");
    } else {
      final Element ret = new Element("TransactionBenchmark");
      ret.setAttribute("info", "TransactionBenchmark is disabled.");
      return ret;
    }
  }

  public abstract String getName();

  /**
   * Create a new independent {@link TransactionBenchmark}. Existing benchmarks with the same name
   * are overwritten by this method.
   *
   * <p>This is for starting a series of measurements from a clean slate.
   *
   * <p>However, the Measurements and Benchmarks are still recorded in the {@link RootBenchmark}
   * too.
   *
   * @param name the name of the new {@link TransactionBenchmark}.
   * @return a new {@link TransactionBenchmark}
   */
  public TransactionBenchmark createBenchmark(String name) {
    synchronized (subBenchmarks) {
      SubBenchmark b = new SubBenchmark(name);
      subBenchmarks.put(name, b);
      return b;
    }
  }

  /** Return (and create if necessary) the sub-benchmark with the given name. */
  public TransactionBenchmark getBenchmark(String name) {
    synchronized (subBenchmarks) {
      SubBenchmark existing = subBenchmarks.get(name);
      if (existing != null) {
        return existing;
      } else {
        SubBenchmark b = new SubBenchmark(name);
        subBenchmarks.put(name, b);
        return b;
      }
    }
  }

  /** Return (and create if necessary) the sub-benchmark with the name of the given class. */
  public TransactionBenchmark getBenchmark(Class<?> class1) {
    return this.getBenchmark(class1.getSimpleName());
  }

  /**
   * Create a sub-benchmark with the name of the given class, overwriting existing ones with the
   * same name.
   */
  public TransactionBenchmark createBenchmark(Class<?> class1) {
    return this.createBenchmark(class1.getSimpleName());
  }

  /** Return a String (or null) denoting where this TransactionBenchmark was created from. */
  public String called_from() {
    final int stackSize = stackTraceElements.length;
    if (stackSize <= 5) {
      return null;
    }
    final String[] classComponents = stackTraceElements[0].getClassName().split("\\.");
    if (classComponents.length == 0) {
      return "unsplittable: " + stackTraceElements[0].getClassName();
    }
    final Collection<String> blacklist = List.of("Thread", "Benchmark");
    for (int i = 0; i < stackSize; ++i) {
      StackTraceElement ste = stackTraceElements[i];
      final String className = ste.getClassName();
      if (blacklist.stream().anyMatch((blacklistItem) -> className.contains(blacklistItem))) {
        continue;
      }

      Collection<String> elementStrings = new ArrayList<String>();
      List.of(i, i + 1, i + 2)
          .forEach(
              (j) -> {
                if (j < stackTraceElements.length) {
                  elementStrings.add(prettifyStackTraceElement(stackTraceElements[j]));
                }
              });
      return String.join(" / ", elementStrings);
    }
    return null;
  }

  /**
   * Return a nice String for the StackTraceElement.
   *
   * <p>The String is composed like this: CLASS::METHOD [LINE_NUMBER], with "org.caosdb.server."
   * removed from the class name.
   */
  private static String prettifyStackTraceElement(final StackTraceElement ste) {
    return ste.getClassName().replaceFirst("caosdb\\.server\\.", "")
        + "::"
        + ste.getMethodName().replaceAll("[<>]", "")
        + " ["
        + ste.getLineNumber()
        + "]";
  }
}
