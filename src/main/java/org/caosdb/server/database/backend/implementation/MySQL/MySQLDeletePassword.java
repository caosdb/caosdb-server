/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.database.backend.implementation.MySQL;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import org.caosdb.server.database.access.Access;
import org.caosdb.server.database.backend.interfaces.DeletePasswordImpl;
import org.caosdb.server.database.exceptions.IntegrityException;
import org.caosdb.server.database.exceptions.TransactionException;

public class MySQLDeletePassword extends MySQLTransaction implements DeletePasswordImpl {

  private static final String STMT_DELETE_PASSWORD = "DELETE FROM passwd WHERE principal=?";

  public MySQLDeletePassword(final Access access) {
    super(access);
  }

  @Override
  public void execute(final String user) throws TransactionException {
    try {
      final PreparedStatement stmt = prepareStatement(STMT_DELETE_PASSWORD);
      stmt.setString(1, user);
      stmt.execute();
    } catch (final SQLIntegrityConstraintViolationException e) {
      throw new IntegrityException(e);
    } catch (final SQLException e) {
      throw new TransactionException(e);
    } catch (final ConnectionException e) {
      throw new TransactionException(e);
    }
  }
}
