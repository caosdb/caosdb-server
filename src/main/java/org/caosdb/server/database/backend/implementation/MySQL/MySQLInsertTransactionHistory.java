/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.database.backend.implementation.MySQL;

import java.sql.PreparedStatement;
import org.caosdb.server.database.access.Access;
import org.caosdb.server.database.backend.interfaces.InsertTransactionHistoryImpl;
import org.caosdb.server.database.exceptions.TransactionException;
import org.caosdb.server.entity.EntityID;

public class MySQLInsertTransactionHistory extends MySQLTransaction
    implements InsertTransactionHistoryImpl {

  public MySQLInsertTransactionHistory(final Access access) {
    super(access);
  }

  public static final String STMT_LOG_TRANSACTION =
      "INSERT INTO transaction_log (transaction, realm, username, seconds, nanos, entity_id) VALUES (?,?,?,?,?,?)";

  @Override
  public void execute(
      final String transaction,
      final String realm,
      final String user,
      final long seconds,
      final int nanos,
      final EntityID entity)
      throws TransactionException {
    try {

      final PreparedStatement logStmt = prepareStatement(STMT_LOG_TRANSACTION);

      logStmt.setString(1, transaction);
      logStmt.setString(2, realm);
      logStmt.setString(3, user);
      logStmt.setLong(4, seconds);
      logStmt.setInt(5, nanos);
      logStmt.setString(6, entity.toString());
      logStmt.execute();
    } catch (final Exception e) {
      throw new TransactionException(e);
    }
  }
}
