/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.database.backend.implementation.MySQL;

import static org.caosdb.server.database.backend.implementation.MySQL.DatabaseUtils.bytes2UTF8;

import java.security.NoSuchAlgorithmException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.caosdb.server.accessControl.CredentialsValidator;
import org.caosdb.server.accessControl.HashPasswordValidator;
import org.caosdb.server.database.access.Access;
import org.caosdb.server.database.backend.interfaces.RetrievePasswordValidatorImpl;
import org.caosdb.server.database.exceptions.TransactionException;

public class MySQLRetrievePasswordValidator extends MySQLTransaction
    implements RetrievePasswordValidatorImpl {

  private static final String STMT_RETRIEVE_PASSWORD =
      "SELECT hash, alg, it, salt FROM passwd WHERE principal=? LIMIT 1";

  public MySQLRetrievePasswordValidator(final Access access) {
    super(access);
  }

  @Override
  public CredentialsValidator<String> execute(final String name) throws TransactionException {
    try {
      final PreparedStatement stmt = prepareStatement(STMT_RETRIEVE_PASSWORD);
      stmt.setString(1, name);
      final ResultSet rs = stmt.executeQuery();
      if (rs.next()) {
        return new HashPasswordValidator(
            bytes2UTF8(rs.getBytes("alg")),
            bytes2UTF8(rs.getBytes("hash")),
            bytes2UTF8(rs.getBytes("salt")),
            rs.getInt("it"));
      }
    } catch (final SQLException e) {
      throw new TransactionException(e);
    } catch (final ConnectionException e) {
      throw new TransactionException(e);
    } catch (final NoSuchAlgorithmException e) {
      throw new TransactionException(e);
    }
    return null;
  }
}
