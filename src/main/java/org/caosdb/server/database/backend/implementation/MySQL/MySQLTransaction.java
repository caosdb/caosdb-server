/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.database.backend.implementation.MySQL;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import org.caosdb.server.database.access.Access;
import org.caosdb.server.database.backend.interfaces.BackendTransactionImpl;
import org.caosdb.server.database.misc.DBHelper;
import org.caosdb.server.database.misc.TransactionBenchmark;

public class MySQLTransaction implements BackendTransactionImpl {

  protected final Access access;
  private TransactionBenchmark benchmark;

  public MySQLTransaction(final Access access) {
    this.access = access;
  }

  protected MySQLHelper getMySQLHelper() {
    final DBHelper h = this.access.getHelper("MySQL");
    if (h == null) {
      final MySQLHelper ret = new MySQLHelper();
      this.access.setHelper("MySQL", ret);
      return ret;
    } else {
      return (MySQLHelper) h;
    }
  }

  protected PreparedStatement prepareStatement(final String s)
      throws SQLException, ConnectionException {
    return getMySQLHelper().prepareStatement(s);
  }

  public final void undo() {}

  public final void cleanUp() {}

  @Override
  public TransactionBenchmark getBenchmark() {
    return benchmark;
  }

  @Override
  public void setTransactionBenchmark(TransactionBenchmark b) {
    this.benchmark = b;
  }

  protected void addMeasurement(String string, long l) {
    if (benchmark != null) {
      benchmark.addMeasurement(string, l);
    }
  }
}
