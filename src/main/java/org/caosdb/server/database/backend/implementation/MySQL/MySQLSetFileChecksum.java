/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2023 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2023 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.caosdb.server.database.backend.implementation.MySQL;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import org.caosdb.server.database.access.Access;
import org.caosdb.server.database.backend.interfaces.SetFileChecksumImpl;
import org.caosdb.server.database.exceptions.TransactionException;
import org.caosdb.server.entity.EntityID;

/**
 * Implements {@link SetFileChecksumImpl} for a MySQL/MariaDB back-end.
 *
 * @author Timm Fitschen <t.fitschen@indiscale.com>
 */
public class MySQLSetFileChecksum extends MySQLTransaction implements SetFileChecksumImpl {

  public MySQLSetFileChecksum(final Access access) {
    super(access);
  }

  public static final String STMT_SET_CHECKSUM =
      "UPDATE files SET hash = unhex(?) WHERE EXISTS (SELECT 1 FROM entity_ids AS eids WHERE eids.id = ? AND eids.internal_id = files.file_id)";

  @Override
  public void execute(final EntityID id, final String checksum) {
    try {
      final PreparedStatement stmt = prepareStatement(STMT_SET_CHECKSUM);
      stmt.setString(2, id.toString());
      stmt.setString(1, checksum);
      stmt.execute();
    } catch (SQLException | ConnectionException e) {
      throw new TransactionException(e);
    }
  }
}
