/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2020 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2020 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.database.backend.transaction;

import java.util.HashMap;
import org.caosdb.server.database.exceptions.TransactionException;
import org.caosdb.server.database.proto.VersionHistoryItem;
import org.caosdb.server.entity.EntityInterface;
import org.caosdb.server.entity.Version;

public class RetrieveVersionInfo extends VersionTransaction {

  public RetrieveVersionInfo(EntityInterface e) {
    super(e);
  }

  @Override
  protected void process(HashMap<String, VersionHistoryItem> map) throws TransactionException {
    super.process(map); // Make the map available to the object.
    if (!map.isEmpty()) getEntity().setVersion(getVersion());
  }

  @Override
  protected Version getVersion(String id) {
    return new Version(id);
  }

  public Version getVersion() {
    Version v = getVersion(getEntity().getVersion().getId());
    v.setPredecessors(getPredecessors(v.getId(), false));
    v.setSuccessors(getSuccessors(v.getId(), false));
    v.setHead(v.getSuccessors().isEmpty());
    return v;
  }
}
