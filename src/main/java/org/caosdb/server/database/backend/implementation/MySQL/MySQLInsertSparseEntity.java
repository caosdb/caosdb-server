/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2023 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2023 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.caosdb.server.database.backend.implementation.MySQL;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLIntegrityConstraintViolationException;
import java.sql.Types;
import org.caosdb.server.database.access.Access;
import org.caosdb.server.database.backend.interfaces.InsertSparseEntityImpl;
import org.caosdb.server.database.exceptions.IntegrityException;
import org.caosdb.server.database.exceptions.TransactionException;
import org.caosdb.server.database.proto.SparseEntity;

public class MySQLInsertSparseEntity extends MySQLTransaction implements InsertSparseEntityImpl {

  public MySQLInsertSparseEntity(final Access access) {
    super(access);
  }

  public static final String STMT_INSERT_SPARSE_ENTITY = "call insertEntity(?,?,?,?,?)";
  public static final String STMT_INSERT_FILE_PROPERTIES = "call setFileProperties(?,?,?,?)";

  @Override
  public void execute(final SparseEntity entity) {
    try {
      final PreparedStatement insertEntityStmt = prepareStatement(STMT_INSERT_SPARSE_ENTITY);
      final PreparedStatement insertFilePropsStmt = prepareStatement(STMT_INSERT_FILE_PROPERTIES);

      insertEntityStmt.setString(1, entity.id);
      insertEntityStmt.setString(2, entity.name);
      insertEntityStmt.setString(3, entity.description);
      insertEntityStmt.setString(4, entity.role);
      insertEntityStmt.setString(5, entity.acl);

      try (final ResultSet rs = insertEntityStmt.executeQuery()) {
        if (rs.next()) {
          entity.versionId = DatabaseUtils.bytes2UTF8(rs.getBytes("Version"));
        } else {
          throw new TransactionException("Didn't get the version id back.");
        }
      }

      if (entity.filePath != null) {
        insertFilePropsStmt.setString(1, entity.id);
        insertFilePropsStmt.setString(2, entity.filePath);
        insertFilePropsStmt.setLong(3, entity.fileSize);
        if (entity.fileHash != null) {
          insertFilePropsStmt.setString(4, entity.fileHash);
        } else {
          insertFilePropsStmt.setNull(4, Types.VARCHAR);
        }
        insertFilePropsStmt.execute();
      }
    } catch (final SQLIntegrityConstraintViolationException exc) {
      throw new IntegrityException(exc);
    } catch (final TransactionException exc) {
      throw exc;
    } catch (final Exception exc) {
      throw new TransactionException(exc);
    }
  }
}
