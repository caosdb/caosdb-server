/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2023 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2023 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.caosdb.server.database.backend.transaction;

import org.caosdb.server.database.BackendTransaction;
import org.caosdb.server.database.backend.interfaces.RetrieveCurrentMaxIdImpl;

/**
 * Retrieve the maximum currently known entity id.
 *
 * <p>This is used by the EntityIdRegistry for the legacy ids (i.e. integer ids) to generate
 * sequential ids which have not been used yet.
 *
 * @see {@link EntityIdRegistry}
 * @see {@link LegacyIds}
 * @author Timm Fitschen <t.fitschen@indiscale.com>
 */
public class RetrieveCurrentMaxId extends BackendTransaction {

  private Integer maxId;

  @Override
  protected void execute() {
    RetrieveCurrentMaxIdImpl t = getImplementation(RetrieveCurrentMaxIdImpl.class);
    this.maxId = t.execute();
  }

  public Integer getCurrentMaxId() {
    return this.maxId;
  }
}
