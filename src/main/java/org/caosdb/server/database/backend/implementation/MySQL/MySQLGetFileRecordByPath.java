/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2023 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2023 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.caosdb.server.database.backend.implementation.MySQL;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.caosdb.server.database.access.Access;
import org.caosdb.server.database.backend.interfaces.GetFileRecordByPathImpl;
import org.caosdb.server.database.exceptions.TransactionException;
import org.caosdb.server.database.proto.SparseEntity;

/**
 * Retrieve the entity id of a file with the given path (or null).
 *
 * @author Timm Fitschen <t.fitschen@indiscale.com>
 */
public class MySQLGetFileRecordByPath extends MySQLTransaction implements GetFileRecordByPathImpl {

  public MySQLGetFileRecordByPath(final Access access) {
    super(access);
  }

  public static final String STMT_GET_ID_BY_PATH =
      "SELECT (Select id from entity_ids WHERE internal_id = files.file_id) as entity_id, size, hex(hash) AS file_hash, checked_timestamp FROM files WHERE path=?";

  @Override
  public SparseEntity execute(final String path) throws TransactionException {
    try {
      final PreparedStatement stmt = prepareStatement(STMT_GET_ID_BY_PATH);

      stmt.setString(1, path);
      final ResultSet rs = stmt.executeQuery();
      try {
        if (rs.next()) {
          final SparseEntity ret = new SparseEntity();
          ret.id = rs.getString("entity_id");
          ret.fileHash = rs.getString("file_hash");
          ret.fileSize = rs.getLong("size");
          ret.fileChecked = rs.getLong("checked_timestamp");
          return ret;
        } else {
          return null;
        }
      } finally {
        rs.close();
      }
    } catch (final SQLException e) {
      throw new TransactionException(e);
    } catch (final ConnectionException e) {
      throw new TransactionException(e);
    }
  }
}
