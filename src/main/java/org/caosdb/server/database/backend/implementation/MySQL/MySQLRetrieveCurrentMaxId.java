/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2023 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2023 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.caosdb.server.database.backend.implementation.MySQL;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.caosdb.server.database.access.Access;
import org.caosdb.server.database.backend.interfaces.RetrieveCurrentMaxIdImpl;
import org.caosdb.server.database.exceptions.TransactionException;

/**
 * Implements {@link RetrieveCurrentMaxIdImpl} for a MySQL/MariaDB back-end.
 *
 * @author Timm Fitschen <t.fitschen@indiscale.com>
 */
public class MySQLRetrieveCurrentMaxId extends MySQLTransaction
    implements RetrieveCurrentMaxIdImpl {

  public MySQLRetrieveCurrentMaxId(Access access) {
    super(access);
  }

  public static final String STMT =
      "SELECT max(CAST(entity_id AS UNSIGNED INT)) AS max_id FROM transaction_log WHERE transaction='Insert'";

  @Override
  public Integer execute() {
    try {
      try (PreparedStatement stmt = prepareStatement(STMT)) {

        try (ResultSet rs = stmt.executeQuery()) {
          if (rs.next()) {
            return rs.getInt("max_id");
          } else {
            return 100;
          }
        }
      }
    } catch (SQLException | ConnectionException e) {
      throw new TransactionException(e);
    }
  }
}
