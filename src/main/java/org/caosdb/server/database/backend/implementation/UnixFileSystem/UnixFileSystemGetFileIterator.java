/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.database.backend.implementation.UnixFileSystem;

import java.io.File;
import java.util.Arrays;
import java.util.Iterator;
import org.caosdb.server.FileSystem;
import org.caosdb.server.database.access.Access;
import org.caosdb.server.database.backend.interfaces.GetFileIteratorImpl;
import org.caosdb.server.database.exceptions.TransactionException;
import org.caosdb.server.entity.Message;

public class UnixFileSystemGetFileIterator extends UnixFileSystemTransaction
    implements GetFileIteratorImpl {

  public UnixFileSystemGetFileIterator(final Access access) {
    super(access);
  }

  @Override
  public Iterator<String> execute(final String location) throws TransactionException {
    File base;
    try {
      base = new File(FileSystem.getPath(location));
    } catch (final Message e) {
      throw new TransactionException(e);
    }
    return new FileNameIterator(base, location);
  }

  public static class FileNameIterator implements Iterator<String> {

    private final File base;
    private File[] files = null;
    private FileNameIterator subfiles = null;
    private int i = 0;
    private final String rootPath;

    public FileNameIterator(final File base, final String rootPath) {
      this.rootPath = rootPath.length() == 0 || rootPath.endsWith("/") ? rootPath : rootPath + "/";
      this.base = base;
      this.files = this.base.listFiles();
      Arrays.sort(this.files);
    }

    @Override
    public boolean hasNext() {
      return this.files.length > this.i;
    }

    @Override
    public String next() {
      if (this.subfiles != null) {
        if (this.subfiles.hasNext()) {
          return this.subfiles.next();
        } else {
          this.subfiles = null;
        }
      }
      final File ret = this.files[this.i++];
      if (ret.isDirectory()) {
        this.subfiles = new FileNameIterator(ret, this.rootPath + ret.getName() + "/");
        if (this.subfiles.hasNext()) {
          return next();
        } else {
          return this.rootPath + ret.getName() + "/";
        }
      }
      return this.rootPath + ret.getName();
    }

    @Override
    public void remove() {
      throw new UnsupportedOperationException();
    }
  }
}
