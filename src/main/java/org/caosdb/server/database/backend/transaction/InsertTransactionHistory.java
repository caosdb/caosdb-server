/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2019-2021 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2019-2021 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.caosdb.server.database.backend.transaction;

import org.caosdb.datetime.UTCDateTime;
import org.caosdb.server.database.BackendTransaction;
import org.caosdb.server.database.backend.interfaces.InsertTransactionHistoryImpl;
import org.caosdb.server.database.exceptions.TransactionException;
import org.caosdb.server.entity.EntityInterface;
import org.caosdb.server.entity.InsertEntity;
import org.caosdb.server.entity.UpdateEntity;
import org.caosdb.server.entity.container.TransactionContainer;
import org.caosdb.server.utils.EntityStatus;

/**
 * Record the current transaction in the entities transaction history.
 *
 * @author Timm Fitschen (t.fitschen@indiscale.com)
 * @deprecated To be replaced by versioning history.
 */
@Deprecated
public class InsertTransactionHistory extends BackendTransaction {

  private final TransactionContainer container;
  private final UTCDateTime datetime;
  private final String user;
  private final String realm;

  public InsertTransactionHistory(
      final TransactionContainer container,
      final String realm,
      final String user,
      final UTCDateTime timestamp) {
    this.container = container;
    this.user = user;
    this.datetime = timestamp;
    this.realm = realm;
  }

  @Override
  public void execute() throws TransactionException {
    final InsertTransactionHistoryImpl t = getImplementation(InsertTransactionHistoryImpl.class);

    for (final EntityInterface e : this.container) {
      if (e.getEntityStatus() == EntityStatus.DELETED
          || (e instanceof UpdateEntity && e.getEntityStatus() == EntityStatus.QUALIFIED)
          || (e instanceof InsertEntity && e.getEntityStatus() == EntityStatus.VALID)) {

        t.execute(
            e.getClass().getSimpleName().replace("Entity", ""),
            this.realm,
            this.user,
            this.datetime.getUTCSeconds(),
            this.datetime.getNanoseconds(),
            e.getId());
      }
    }
  }
}
