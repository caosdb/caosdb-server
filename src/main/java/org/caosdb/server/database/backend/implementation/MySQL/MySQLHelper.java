/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2020 Indiscale GmbH <info@indiscale.com>
 * Copyright (C) 2020 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.database.backend.implementation.MySQL;

import java.io.UnsupportedEncodingException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import org.caosdb.server.accessControl.Principal;
import org.caosdb.server.database.misc.DBHelper;
import org.caosdb.server.transaction.ChecksumUpdater;
import org.caosdb.server.transaction.TransactionInterface;
import org.caosdb.server.transaction.WriteTransactionInterface;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Provides cached statements for a MySQL back-end.
 *
 * @author tf
 */
public class MySQLHelper implements DBHelper {

  private Connection connection = null;

  private Logger logger = LoggerFactory.getLogger(getClass());

  /**
   * Initialize a transaction by calling the corresponding SQL procedure.
   *
   * <p>In the database, this adds a row to the transaction table with SRID, user and timestamp.
   */
  public void initTransaction(Connection connection, WriteTransactionInterface transaction)
      throws SQLException {
    try (CallableStatement call = connection.prepareCall("CALL set_transaction(?,?,?,?,?)")) {

      String username = ((Principal) transaction.getTransactor().getPrincipal()).getUsername();
      String realm = ((Principal) transaction.getTransactor().getPrincipal()).getRealm();
      long seconds = transaction.getTimestamp().getUTCSeconds();
      int nanos = transaction.getTimestamp().getNanoseconds();
      byte[] srid = transaction.getSRID().getBytes("UTF-8");

      call.setBytes(1, srid);
      call.setString(2, username);
      call.setString(3, realm);
      call.setLong(4, seconds);
      call.setInt(5, nanos);
      call.execute();

    } catch (UnsupportedEncodingException e) {
      e.printStackTrace();
      System.exit(1);
    }
  }

  public Connection initConnection(TransactionInterface transaction)
      throws ConnectionException, SQLException {
    Connection connection;
    connection = DatabaseConnectionPool.getConnection();

    if (transaction instanceof ChecksumUpdater) {
      connection.setReadOnly(false);
      connection.setAutoCommit(false);
    } else if (transaction instanceof WriteTransactionInterface) {
      connection.setReadOnly(false);
      connection.setAutoCommit(false);
      initTransaction(connection, (WriteTransactionInterface) transaction);
    } else {
      connection.setReadOnly(false);
      connection.setAutoCommit(true);
    }

    return connection;
  }

  public Connection getConnection() throws SQLException, ConnectionException {
    if (this.connection == null) {
      this.connection = initConnection(this.transaction);
    }
    return this.connection;
  }

  /**
   * Prepare a statement from a string. Reuse prepared statements from the cache if available.
   *
   * @param statement
   * @return
   * @throws SQLException
   * @throws ConnectionException
   */
  public PreparedStatement prepareStatement(final String statement)
      throws SQLException, ConnectionException {
    if (this.stmtCache.containsKey(statement)) {
      final PreparedStatement ret = this.stmtCache.get(statement);
      if (!ret.isClosed()) {
        return ret;
      }
    }

    final PreparedStatement stmt = getConnection().prepareStatement(statement);
    this.stmtCache.put(statement, stmt);
    return stmt;
  }

  private HashMap<String, PreparedStatement> stmtCache = new HashMap<String, PreparedStatement>();

  @Override
  public void setHelped(final TransactionInterface transaction) {
    this.transaction = transaction;
  }

  private TransactionInterface transaction = null;

  /** Make all changes permanent. */
  @Override
  public void commit() throws SQLException {
    if (this.connection != null
        && !this.connection.isClosed()
        && !this.connection.getAutoCommit()) {
      this.connection.commit();
    }
  }

  /**
   * Reset SRID variable, close all statements, roll back to last save point and close connection.
   */
  @Override
  public void cleanUp() {

    try {
      if (this.connection != null && !this.connection.isClosed()) {
        try (Statement s = connection.createStatement()) {
          s.execute("SET @SRID = NULL");
        } catch (SQLException e) {
          logger.error("Exception while resetting the @SRID variable.", e);
        }

        // close all cached statements (if possible)
        for (final PreparedStatement stmt : this.stmtCache.values()) {
          try {
            if (!stmt.isClosed()) {
              stmt.close();
            }
          } catch (final SQLException e) {
            logger.warn("Exception while closing a prepared statement.", e);
          }
        }

        try {
          if (!this.connection.getAutoCommit()) {
            this.connection.rollback();
          }
        } catch (final SQLException r) {
          logger.warn("Exception during roll-back attempt.", r);
        }
        this.connection.close();
      }
    } catch (final SQLException e) {
      logger.warn("Exception during clean-up.", e);
    }

    // clear everything
    this.stmtCache.clear();
    this.stmtCache = null;
    this.connection = null;
  }
}
