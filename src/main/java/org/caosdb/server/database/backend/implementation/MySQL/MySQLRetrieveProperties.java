/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2023 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2023 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.caosdb.server.database.backend.implementation.MySQL;

import com.mysql.cj.jdbc.exceptions.MysqlDataTruncation;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.LinkedList;
import java.util.List;
import org.caosdb.server.database.access.Access;
import org.caosdb.server.database.backend.interfaces.RetrievePropertiesImpl;
import org.caosdb.server.database.exceptions.TransactionException;
import org.caosdb.server.database.proto.ProtoProperty;
import org.caosdb.server.entity.EntityID;

/**
 * Retrieve the entity's properties.
 *
 * <p>This implemation transforms the flat structure of properties which is stored in the back end
 * to a deep tree of properties (if applicable) which is the reverse transformation to that
 * happening in MySQLInsertEntityProperties.
 *
 * @see {@link MySQLInsertEntityProperties}
 * @author Timm Fitschen <t.fitschen@indiscale.com>
 */
public class MySQLRetrieveProperties extends MySQLTransaction implements RetrievePropertiesImpl {

  public MySQLRetrieveProperties(final Access access) {
    super(access);
  }

  private static final String stmtStr = "call retrieveEntityProperties(?,?,?)";
  private static final String stmtStr2 = "call retrieveOverrides(?,?,?)";

  @Override
  public LinkedList<ProtoProperty> execute(
      final EntityID entity, final String version, boolean isHead) throws TransactionException {
    try {
      final PreparedStatement prepareStatement = prepareStatement(stmtStr);

      final List<ProtoProperty> propsStage1 =
          retrieveFlatPropertiesStage1("0", entity.toString(), version, prepareStatement);

      for (final ProtoProperty p : propsStage1) {

        p.subProperties =
            retrieveFlatPropertiesStage1(entity.toString(), p.id, version, prepareStatement);
      }
      return DatabaseUtils.transformToDeepPropertyTree(propsStage1, isHead);
    } catch (final SQLException e) {
      throw new TransactionException(e);
    } catch (final ConnectionException e) {
      throw new TransactionException(e);
    }
  }

  private List<ProtoProperty> retrieveFlatPropertiesStage1(
      final String domain, final String entity, final String version, final PreparedStatement stmt)
      throws SQLException, ConnectionException {

    stmt.setString(1, domain);
    stmt.setString(2, entity);
    if (version == null) {
      stmt.setNull(3, Types.VARBINARY);
    } else {
      stmt.setString(3, version);
    }

    final long t1 = System.currentTimeMillis();

    try (ResultSet rs = stmt.executeQuery()) {
      final long t2 = System.currentTimeMillis();
      addMeasurement(this.getClass().getSimpleName() + ".retrieveFlatPropertiesStage1", t2 - t1);

      final List<ProtoProperty> properties = DatabaseUtils.parsePropertyResultset(rs);

      final PreparedStatement retrieveOverrides = prepareStatement(stmtStr2);

      if (properties != null & !properties.isEmpty()) {
        retrieveOverrides(domain, entity, version, retrieveOverrides, properties);
      }

      return properties;
    } catch (MysqlDataTruncation e) {
      throw e;
    }
  }

  private void retrieveOverrides(
      final String domain,
      final String entity,
      final String version,
      final PreparedStatement retrieveOverrides,
      final List<ProtoProperty> props)
      throws SQLException {

    ResultSet rs = null;
    try {
      retrieveOverrides.setString(1, domain);
      retrieveOverrides.setString(2, entity);
      if (version == null) {
        retrieveOverrides.setNull(3, Types.VARBINARY);
      } else {
        retrieveOverrides.setString(3, version);
      }
      final long t1 = System.currentTimeMillis();
      rs = retrieveOverrides.executeQuery();
      final long t2 = System.currentTimeMillis();
      addMeasurement(this.getClass().getSimpleName() + ".retrieveOverrides", t2 - t1);
      DatabaseUtils.parseOverrides(props, rs);

    } finally {
      if (rs != null && !rs.isClosed()) {
        rs.close();
      }
    }
  }
}
