/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2021 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2021 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.caosdb.server.database.backend.implementation.MySQL;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.caosdb.server.database.access.Access;
import org.caosdb.server.database.backend.interfaces.RetrieveEntityACLImpl;
import org.caosdb.server.database.exceptions.TransactionException;
import org.caosdb.server.database.proto.VerySparseEntity;

public class MySQLRetrieveEntityACL extends MySQLTransaction implements RetrieveEntityACLImpl {

  public MySQLRetrieveEntityACL(Access access) {
    super(access);
  }

  public static final String STMT =
      "SELECT a.acl FROM entities AS e LEFT JOIN entity_acl AS a ON (a.id = e.acl) WHERE e.id = ? LIMIT 1";

  @Override
  public VerySparseEntity execute(String id) {
    try (PreparedStatement stmt = prepareStatement(STMT)) {
      stmt.setString(1, id);
      ResultSet rs = stmt.executeQuery();
      if (rs.next()) {
        VerySparseEntity result = new VerySparseEntity();
        result.id = id;
        result.acl = rs.getString(1);
        return result;
      }
    } catch (SQLException | ConnectionException e) {
      throw new TransactionException(e);
    }
    return null;
  }
}
