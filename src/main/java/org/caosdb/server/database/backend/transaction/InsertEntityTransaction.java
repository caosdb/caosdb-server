/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2019-2021 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2019-2021 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.caosdb.server.database.backend.transaction;

import org.caosdb.server.database.BackendTransaction;
import org.caosdb.server.entity.EntityInterface;
import org.caosdb.server.entity.container.TransactionContainer;
import org.caosdb.server.utils.EntityStatus;

public class InsertEntityTransaction extends BackendTransaction {

  private final TransactionContainer container;

  public InsertEntityTransaction(final TransactionContainer container) {
    this.container = container;
  }

  @Override
  public void execute() {
    for (final EntityInterface newEntity : this.container) {
      if (newEntity.getEntityStatus() == EntityStatus.QUALIFIED) {
        execute(new InsertSparseEntity(newEntity));
      }
    }
    for (final EntityInterface newEntity : this.container) {
      if (newEntity.getEntityStatus() == EntityStatus.QUALIFIED && newEntity.hasDatatype()) {
        execute(new InsertEntityDatatype(newEntity));
      }
    }
    for (final EntityInterface newEntity : this.container) {
      if (newEntity.getEntityStatus() == EntityStatus.QUALIFIED) {
        if (newEntity.getQueryTemplateDefinition() != null) {
          execute(new SetQueryTemplateDefinition(newEntity));
        }
        if (newEntity.hasFileProperties()) {
          execute(new InsertFile(newEntity));
        }
        if (newEntity.hasProperties() || newEntity.hasValue()) {
          execute(new InsertEntityProperties(newEntity));
        }
        execute(new InsertParents(newEntity));

        if (newEntity.getEntityStatus() == EntityStatus.QUALIFIED) {
          newEntity.setEntityStatus(EntityStatus.VALID);
        }
      }
    }
  }
}
