/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2021 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2021 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.caosdb.server.database.backend.transaction;

import java.util.List;
import org.caosdb.server.database.BackendTransaction;
import org.caosdb.server.database.backend.interfaces.ListUsersImpl;
import org.caosdb.server.database.proto.ProtoUser;

public class ListUsers extends BackendTransaction {

  private List<ProtoUser> users;

  @Override
  protected void execute() {
    ListUsersImpl t = getImplementation(ListUsersImpl.class);
    users = t.execute();
  }

  public List<ProtoUser> getUsers() {
    return users;
  }
}
