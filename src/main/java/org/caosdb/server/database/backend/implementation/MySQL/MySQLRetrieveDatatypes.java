/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.database.backend.implementation.MySQL;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.caosdb.server.database.access.Access;
import org.caosdb.server.database.backend.interfaces.RetrieveDatatypesImpl;
import org.caosdb.server.database.exceptions.TransactionException;
import org.caosdb.server.database.proto.VerySparseEntity;

public class MySQLRetrieveDatatypes extends MySQLTransaction implements RetrieveDatatypesImpl {

  public MySQLRetrieveDatatypes(final Access access) {
    super(access);
  }

  /**
   * Retrieve (ParentID, ParentName, ParentDescription, ParentRole, ACL) tuple which actually
   * contains the ID, name, description, role and ACL of the datatype. The misleading names should
   * be fixed sometimes (TODO) but this also requires to adjust the code below, which uses {@link
   * DatabaseUtils#parseParentResultSet(ResultSet)}.
   */
  private static final String STMT_GET_DATATYPE =
      "SELECT id AS ParentID, "
          + "(SELECT value FROM name_data WHERE domain_id = 0 AND entity_ID = e.id AND property_id = 20) AS ParentName, "
          + "description AS ParentDescription, "
          + "role AS ParentRole, "
          + "(SELECT acl FROM entity_acl AS a WHERE a.id=e.acl) as ACL "
          + "FROM entities AS e WHERE e.role='DATATYPE'";

  @Override
  public List<VerySparseEntity> execute() throws TransactionException {
    try {
      final PreparedStatement stmt = prepareStatement(STMT_GET_DATATYPE);

      final ResultSet rs = stmt.executeQuery();
      try {
        return DatabaseUtils.parseParentResultSet(rs);
      } finally {
        rs.close();
      }
    } catch (final SQLException e) {
      throw new TransactionException(e);
    } catch (final ConnectionException e) {
      throw new TransactionException(e);
    }
  }
}
