/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.database.backend.transaction;

import java.util.List;
import org.caosdb.server.database.BackendTransaction;
import org.caosdb.server.database.backend.interfaces.GetIDByNameImpl;
import org.caosdb.server.database.exceptions.EntityDoesNotExistException;
import org.caosdb.server.database.exceptions.EntityWasNotUniqueException;
import org.caosdb.server.database.exceptions.TransactionException;
import org.caosdb.server.entity.EntityID;

public class GetIDByName extends BackendTransaction {

  private List<EntityID> list;
  private final boolean unique;
  private final String name;
  private final String role;

  public GetIDByName(final String name) {
    this(name, null, true);
  }

  public GetIDByName(final String name, final boolean unique) {
    this(name, null, unique);
  }

  public GetIDByName(final String name, final String role) {
    this(name, role, true);
  }

  public GetIDByName(final String name, final String role, final boolean unique) {
    this.name = name;
    this.role = role;
    this.unique = unique;
  }

  @Override
  public void execute() throws TransactionException {
    final GetIDByNameImpl t = getImplementation(GetIDByNameImpl.class);

    String limit = null;
    if (this.unique) {
      limit = "2";
    }

    this.list = t.execute(this.name, this.role, limit);

    if (this.list.isEmpty()) {
      throw new EntityDoesNotExistException();
    } else if (this.list.size() > 1) {
      if (this.unique) {
        throw new EntityWasNotUniqueException();
      }
    }
  }

  public List<EntityID> getList() {
    return this.list;
  }

  public EntityID getId() {
    return this.list.get(0);
  }
}
