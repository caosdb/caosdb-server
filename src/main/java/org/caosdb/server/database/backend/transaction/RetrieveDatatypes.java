/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.database.backend.transaction;

import java.util.List;
import org.caosdb.server.database.BackendTransaction;
import org.caosdb.server.database.backend.interfaces.RetrieveDatatypesImpl;
import org.caosdb.server.database.exceptions.TransactionException;
import org.caosdb.server.database.proto.VerySparseEntity;
import org.caosdb.server.entity.Entity;
import org.caosdb.server.entity.EntityID;
import org.caosdb.server.entity.EntityInterface;
import org.caosdb.server.entity.RetrieveEntity;
import org.caosdb.server.entity.container.Container;

public class RetrieveDatatypes extends BackendTransaction {

  private List<VerySparseEntity> list;

  @Override
  public void execute() throws TransactionException {
    final RetrieveDatatypesImpl t = getImplementation(RetrieveDatatypesImpl.class);
    this.list = t.execute();
  }

  public Container<? extends EntityInterface> getDatatypes() {
    final Container<Entity> ret = new Container<Entity>();
    for (final VerySparseEntity vse : this.list) {
      final RetrieveEntity entity = new RetrieveEntity(new EntityID(vse.id));
      entity.setName(vse.name);
      entity.setDescription(vse.description);
      entity.setRole(vse.role);
      ret.add(entity);
    }
    return ret;
  }
}
