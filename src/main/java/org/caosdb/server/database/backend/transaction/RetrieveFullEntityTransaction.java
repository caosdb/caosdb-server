/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 *   Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2020-2021 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2020-2021 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.database.backend.transaction;

import java.util.LinkedList;
import java.util.List;
import org.apache.shiro.subject.Subject;
import org.caosdb.server.database.BackendTransaction;
import org.caosdb.server.database.exceptions.EntityDoesNotExistException;
import org.caosdb.server.datatype.CollectionValue;
import org.caosdb.server.datatype.IndexedSingleValue;
import org.caosdb.server.datatype.ReferenceValue;
import org.caosdb.server.entity.EntityID;
import org.caosdb.server.entity.EntityInterface;
import org.caosdb.server.entity.Message;
import org.caosdb.server.entity.RetrieveEntity;
import org.caosdb.server.entity.Role;
import org.caosdb.server.entity.container.Container;
import org.caosdb.server.entity.wrapper.Property;
import org.caosdb.server.permissions.EntityPermission;
import org.caosdb.server.query.Query;
import org.caosdb.server.query.Query.Selection;
import org.caosdb.server.utils.EntityStatus;

/**
 * Retrieve the full entity from the backend - with all parents, properties, file properties and so
 * on.
 *
 * <p>TODO: This class should rather be called FullEntityRetrieval or FullEntityRetrieveTransaction.
 *
 * <p>When the entity which is to be retrieved has a defined list of {@link Query.Selection} which
 * select properties from referenced entities, the referenced entities are retrieved as well.
 * Otherwise, only the referenced id is retrieved and the entity stays rather flat.
 *
 * @author Timm Fitschen <t.fitschen@indiscale.com>
 */
public class RetrieveFullEntityTransaction extends BackendTransaction {

  private final Container<? extends EntityInterface> container;
  private final Subject subject;

  @SuppressWarnings("unchecked")
  public RetrieveFullEntityTransaction(final EntityInterface entity, Subject subject) {
    this(new Container<>(), subject);
    ((Container<EntityInterface>) this.container).add(entity);
  }

  public RetrieveFullEntityTransaction(
      final Container<? extends EntityInterface> container, final Subject subject) {
    this.container = container;
    this.subject = subject;
  }

  public RetrieveFullEntityTransaction(final EntityID id, Subject subject) {
    this(new RetrieveEntity(id), subject);
  }

  @Override
  public void execute() {
    retrieveFullEntitiesInContainer(this.container);
  }

  /**
   * Retrieve the entities in the container.
   *
   * @param container
   */
  public void retrieveFullEntitiesInContainer(
      final Container<? extends EntityInterface> container) {
    for (final EntityInterface e : container) {
      if (e.hasId() && !e.getId().isTemporary() && e.getEntityStatus() == EntityStatus.QUALIFIED) {
        retrieveFullEntity(e, e.getSelections());
      }
    }
  }

  /**
   * Retrieve a single full entity.
   *
   * <p>If the selections are not empty, retrieve the referenced entities matching the 'selections'
   * as well.
   *
   * <p>This method is called recursively during the retrieval of the referenced entities.
   *
   * @param e The entity.
   * @param selections
   */
  public void retrieveFullEntity(final EntityInterface e, final List<Selection> selections) {
    if (!needMoreThanId(selections)) {
      return;
    }
    execute(new RetrieveSparseEntity(e));

    if (e.getEntityStatus() == EntityStatus.VALID) {
      execute(new RetrieveVersionInfo(e));
      if (e.getRole() == Role.QueryTemplate) {
        execute(new RetrieveQueryTemplateDefinition(e));
      }
      if (needParents(selections)) {
        execute(new RetrieveParents(e));
      }

      if (needProperties(selections)) {
        execute(new RetrieveProperties(e));
      }

      // recursion! retrieveSubEntities calls retrieveFull sometimes, but with reduced selectors.
      if (selections != null && !selections.isEmpty()) {
        retrieveSubEntities(e, selections);
      }
    }
  }

  /**
   * Return true iff anything else than the id is needed for this retrieval.
   *
   * <p>The notorious case, where it is not necessary to retrieve the sparse entity is during
   * `SELECT id FROM ...` queries.
   */
  private boolean needMoreThanId(List<Selection> selections) {
    if (selections == null || selections.isEmpty()) {
      return true;
    } else if (selections.size() == 1 && selections.get(0).isId()) {
      return false;
    }
    return true;
  }

  /**
   * Return true iff the properties need to be retrieved.
   *
   * <p>It is not necessary during `SELECT parent, name, version, ...` queries where no actual
   * properties are being selected.
   */
  private boolean needProperties(List<Selection> selections) {
    if (selections == null || selections.isEmpty()) {
      return true;
    }
    for (Selection s : selections) {
      if (s.isProperty()) {
        return true;
      }
    }
    return false;
  }

  /**
   * Return true iff the parents need to be retrieved.
   *
   * <p>It is not necessary during `SELECT` queries that do not select the parent.
   */
  private boolean needParents(List<Selection> selections) {
    if (selections == null || selections.isEmpty()) {
      return true;
    }
    for (Selection s : selections) {
      if (s.isParent()) {
        return true;
      }
    }
    return false;
  }

  /**
   * Recursively resolve the reference values of the list of reference property `p` (but only the
   * selected sub-properties).
   */
  private void resolveReferenceListProperty(
      final Property p, final List<Selection> selections, final String propertyName) {
    try {
      p.parseValue();
    } catch (final Message m) {
      p.addError(m);
    }

    final CollectionValue values = (CollectionValue) p.getValue();
    for (final IndexedSingleValue sv : values) {
      resolveReferenceValue((ReferenceValue) sv.getWrapped(), selections, propertyName);
    }
  }

  /**
   * Recursively resolve the reference values of the reference property `p` (but only the selected
   * sub-properties).
   */
  private void resolveReferenceProperty(
      final Property p, final List<Selection> selections, final String propertyName) {
    try {
      p.parseValue();
    } catch (final Message m) {
      p.addError(m);
    }

    resolveReferenceValue((ReferenceValue) p.getValue(), selections, propertyName);
  }

  /**
   * Recursively resolve the reference value.
   *
   * <p>To be called by {@link #resolveReferenceListProperty(Property, List, String)} and {@link
   * #resolveReferenceProperty(Property, List, String)}.
   */
  private void resolveReferenceValue(
      final ReferenceValue value, final List<Selection> selections, final String propertyName) {
    final RetrieveEntity ref = new RetrieveEntity(value.getId());

    if (this.subject != null) {
      // recursion! (Only for the matching selections)
      retrieveFullEntity(ref, getSubSelects(selections, propertyName));

      // check whether the referenced entity is readable
      if (!ref.getEntityACL().isPermitted(this.subject, EntityPermission.RETRIEVE_ENTITY)) {
        return;
      }
      value.setEntity(ref, true);
    }
  }

  /**
   * Retrieve the Entities which match the selections and are referenced by the Entity 'e'.
   *
   * @param e
   * @param selections
   */
  public void retrieveSubEntities(final EntityInterface e, final List<Selection> selections) {
    for (final Selection s : selections) {
      final String propertyName = s.getSelector();
      for (final Property p : e.getProperties()) {
        if (s.getSubselection() != null) {
          // The presence of sub-selections means that the properties are
          // expected to be references (list or plain).
          if (p.getValue() != null) {
            if (propertyName.equalsIgnoreCase(p.getName())) {
              if (p.isReference()) {
                // handle (single) reference properties with matching name...
                resolveReferenceProperty(p, selections, propertyName);
                continue;
              } else if (p.isReferenceList()) {
                // handle (list) reference properties with matching name...
                resolveReferenceListProperty(p, selections, propertyName);
                continue;
              }
            } else {
              try {
                final boolean isSubtype =
                    execute(new IsSubType(p.getId(), propertyName)).isSubType();
                if (isSubtype) {
                  // ... handle reference properties that are a subtype of `propertyName`.
                  if (p.getValue() != null) {
                    if (p.isReference()) {
                      resolveReferenceProperty(p, selections, propertyName);
                    } else if (p.isReferenceList()) {
                      resolveReferenceListProperty(p, selections, propertyName);
                    }
                  }

                  // the name is set the the super-types name! Otherwise, clients
                  // wouldn't know what this property is supposed to be.
                  p.setName(propertyName);
                }
              } catch (final EntityDoesNotExistException exc) {
                // unknown parent name.
              }
            }
          }
        } else {
          // no subselections - no need to resolve any references...
          if (!propertyName.equalsIgnoreCase(p.getName())) {
            // ... we only need to cover property sub-typing
            try {
              final boolean isSubtype = execute(new IsSubType(p.getId(), propertyName)).isSubType();
              if (isSubtype) {
                p.setName(propertyName);
              }
            } catch (final EntityDoesNotExistException exc) {
              // unknown parent name.
            }
          }
        }
      }
    }
  }

  /**
   * Return all non-null subselects of those selections which match the given select String.
   *
   * <p>Effectively, this reduces the depth of the selections by one (and drops non-matching
   * selections).
   *
   * @param selections
   * @param select
   * @return A new list of Selections.
   */
  public List<Selection> getSubSelects(final List<Selection> selections, final String select) {
    final List<Selection> result = new LinkedList<>();
    for (final Selection s : selections) {
      if (s.getSelector().equalsIgnoreCase(select) && s.getSubselection() != null) {
        result.add(s.getSubselection());
      }
    }
    return result;
  }

  public Container<? extends EntityInterface> getContainer() {
    return container;
  }
}
