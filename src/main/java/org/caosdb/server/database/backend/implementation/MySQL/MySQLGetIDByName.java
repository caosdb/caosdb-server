/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2023 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2023 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.caosdb.server.database.backend.implementation.MySQL;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Types;
import java.util.LinkedList;
import java.util.List;
import org.caosdb.server.database.access.Access;
import org.caosdb.server.database.backend.interfaces.GetIDByNameImpl;
import org.caosdb.server.database.exceptions.TransactionException;
import org.caosdb.server.entity.EntityID;

/**
 * Retrieve the entity id of an entity with the given name (or null).
 *
 * @author Timm Fitschen <t.fitschen@indiscale.com>
 */
public class MySQLGetIDByName extends MySQLTransaction implements GetIDByNameImpl {

  public MySQLGetIDByName(final Access access) {
    super(access);
  }

  /**
   * Resolves the (primary) name of an entity to an id. This query is not necessarily unique.
   * Therefore {@link #STMT_AND_ROLE}, {@link #STMT_NOT_ROLE}, and {@link #STMT_LIMIT} can as
   * additional conditions.
   */
  public static final String STMT_GET_ID_BY_NAME = "call getIdByName(?, ?, ?)";

  @Override
  public List<EntityID> execute(final String name, final String role, final String limit)
      throws TransactionException {
    try {
      final PreparedStatement stmt = prepareStatement(STMT_GET_ID_BY_NAME);

      stmt.setString(1, name);
      if (role != null) {
        stmt.setString(2, role);
      } else {
        stmt.setNull(2, Types.VARCHAR);
      }
      if (limit != null) {
        stmt.setString(3, limit);
      } else {
        stmt.setNull(3, Types.VARCHAR);
      }
      try (ResultSet rs = stmt.executeQuery()) {
        final List<EntityID> ret = new LinkedList<>();
        while (rs.next()) {
          ret.add(new EntityID(rs.getString("id")));
        }

        return ret;
      }
    } catch (final Exception e) {
      throw new TransactionException(e);
    }
  }
}
