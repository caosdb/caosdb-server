/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2019 IndiScale GmbH
 * Copyright (C) 2019 Timm Fitschen (t.fitschen@indiscale.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.database.backend.transaction;

import java.util.Set;
import org.apache.commons.jcs.access.behavior.ICacheAccess;
import org.caosdb.server.accessControl.Role;
import org.caosdb.server.caching.Cache;
import org.caosdb.server.database.CacheableBackendTransaction;
import org.caosdb.server.database.backend.interfaces.RetrieveRoleImpl;
import org.caosdb.server.database.exceptions.TransactionException;

public class RetrieveRole extends CacheableBackendTransaction<String, Role> {

  private static final ICacheAccess<String, Role> cache = Cache.getCache("BACKEND_UserRoles");

  private final String role_name;
  private Role role;

  public Role getRole() {
    return this.role;
  }

  public RetrieveRole(final String role) {
    super(cache);
    this.role_name = role;
  }

  @Override
  public Role executeNoCache() throws TransactionException {
    final RetrieveRoleImpl t = getImplementation(RetrieveRoleImpl.class);
    return t.retrieve(this.role_name);
  }

  @Override
  protected void process(final Role t) throws TransactionException {
    this.role = t;
  }

  @Override
  protected String getKey() {
    return this.role_name;
  }

  public static void removeCached(final String name) {
    cache.remove(name);
  }

  public static void removeCached(Set<String> roles) {
    roles.forEach(RetrieveRole::removeCached);
  }
}
