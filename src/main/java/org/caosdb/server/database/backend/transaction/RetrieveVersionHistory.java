/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2020 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2020 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.database.backend.transaction;

import java.util.HashMap;
import org.caosdb.datetime.UTCDateTime;
import org.caosdb.server.database.exceptions.TransactionException;
import org.caosdb.server.database.proto.VersionHistoryItem;
import org.caosdb.server.entity.EntityInterface;
import org.caosdb.server.entity.Version;

public class RetrieveVersionHistory extends VersionTransaction {

  public RetrieveVersionHistory(EntityInterface e) {
    super(e);
  }

  @Override
  protected void process(HashMap<String, VersionHistoryItem> map) throws TransactionException {
    super.process(map);
    if (!map.isEmpty()) getEntity().setVersion(getHistory());
  }

  @Override
  protected Version getVersion(String id) {
    Version v = new Version(id);
    VersionHistoryItem i = getHistoryItems().get(v.getId());
    if (i != null) {
      v.setDate(UTCDateTime.UTCSeconds(i.seconds, i.nanos));
      v.setUsername(i.username);
      v.setRealm(i.realm);
    }
    return v;
  }

  private Version getHistory() {
    Version v = getVersion(getEntity().getVersion().getId());
    v.setSuccessors(getSuccessors(v.getId(), true));
    v.setPredecessors(getPredecessors(v.getId(), true));
    v.setHead(v.getSuccessors().isEmpty());
    v.setCompleteHistory(true);
    return v;
  }
}
