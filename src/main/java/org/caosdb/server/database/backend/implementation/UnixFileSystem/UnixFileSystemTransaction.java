/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.database.backend.implementation.UnixFileSystem;

import java.io.File;
import java.io.IOException;
import org.caosdb.server.FileSystem;
import org.caosdb.server.database.access.Access;
import org.caosdb.server.database.backend.interfaces.BackendTransactionImpl;
import org.caosdb.server.database.misc.DBHelper;
import org.caosdb.server.database.misc.TransactionBenchmark;
import org.caosdb.server.entity.Message;

public abstract class UnixFileSystemTransaction implements BackendTransactionImpl {

  private final Access access;
  private TransactionBenchmark benchmark;

  public UnixFileSystemTransaction(final Access access) {
    this.access = access;
  }

  @Override
  public void setTransactionBenchmark(TransactionBenchmark b) {
    this.benchmark = b;
  }

  protected File getFile(final String path) throws IOException, Message {
    return new File(getHelper().getBasePath() + path);
  }

  protected String getBasePath() throws IOException, Message {
    return getHelper().getBasePath();
  }

  UnixFileSystemHelper getHelper() throws IOException, Message {
    final DBHelper dbHelper = this.access.getHelper("UnixFileSystemHelper");
    if (dbHelper == null) {
      final String basepath = FileSystem.getPath("") + "/";
      final UnixFileSystemHelper helper = new UnixFileSystemHelper(basepath);
      this.access.setHelper("UnixFileSystemHelper", helper);
      return helper;
    } else {
      return (UnixFileSystemHelper) dbHelper;
    }
  }

  @Override
  public TransactionBenchmark getBenchmark() {
    return benchmark;
  }
}
