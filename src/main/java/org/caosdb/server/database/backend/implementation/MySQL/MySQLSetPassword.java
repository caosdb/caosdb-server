/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.database.backend.implementation.MySQL;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import org.caosdb.server.database.access.Access;
import org.caosdb.server.database.backend.interfaces.SetPasswordImpl;
import org.caosdb.server.database.exceptions.TransactionException;
import org.caosdb.server.utils.Utils;

public class MySQLSetPassword extends MySQLTransaction implements SetPasswordImpl {

  public static final String algorithm = "SHA-512";

  private static final SecureRandom rand = new SecureRandom();

  private static final String STMT_SET_PASSWORD =
      "INSERT INTO passwd (principal, hash, alg, it, salt) VALUES (?,?,?,?,?) ON DUPLICATE KEY UPDATE hash=?, alg=?, it=?, salt=?";

  public MySQLSetPassword(final Access access) {
    super(access);
  }

  @Override
  public void execute(final String name, final String password) throws TransactionException {
    try {
      final PreparedStatement stmt = prepareStatement(STMT_SET_PASSWORD);
      final String salt = new BigInteger(130, rand).toString(32);
      final int it = rand.nextInt(7000) + 3000;
      String hash = Utils.hash(MessageDigest.getInstance(algorithm), password, salt, it);

      if (hash == null) {
        hash = "No password.";
      }
      stmt.setString(1, name);
      stmt.setString(2, hash);
      stmt.setString(3, algorithm);
      stmt.setInt(4, it);
      stmt.setString(5, salt);
      stmt.setString(6, hash);
      stmt.setString(7, algorithm);
      stmt.setInt(8, it);
      stmt.setString(9, salt);
      stmt.execute();
    } catch (final SQLException e) {
      throw new TransactionException(e);
    } catch (final ConnectionException e) {
      throw new TransactionException(e);
    } catch (final NoSuchAlgorithmException e) {
      throw new TransactionException(e);
    }
  }
}
