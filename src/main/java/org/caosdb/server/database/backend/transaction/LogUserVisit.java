/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2021 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2021 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.caosdb.server.database.backend.transaction;

import java.util.HashSet;
import org.caosdb.server.accessControl.UserSources;
import org.caosdb.server.database.BackendTransaction;
import org.caosdb.server.database.backend.interfaces.LogUserVisitImpl;
import org.caosdb.server.database.backend.interfaces.UpdateUserImpl;
import org.caosdb.server.database.backend.interfaces.UpdateUserRolesImpl;
import org.caosdb.server.database.exceptions.TransactionException;
import org.caosdb.server.database.proto.ProtoUser;

public class LogUserVisit extends BackendTransaction {

  private String realm;
  private String username;
  private String type;
  private long timestamp;

  public LogUserVisit(long timestamp, String realm, String username, String type) {
    this.timestamp = timestamp;
    this.realm = realm;
    this.username = username;
    this.type = type;
  }

  @Override
  protected void execute() throws TransactionException {
    final LogUserVisitImpl t = getImplementation(LogUserVisitImpl.class);
    if (!t.logUserReturnIsKnown(timestamp, realm, username, type)) {
      // User is unknown. Make it known
      ProtoUser user = new ProtoUser();
      user.realm = realm;
      user.name = username;
      user.email = UserSources.getDefaultUserEmail(realm, username);
      user.status = UserSources.getDefaultUserStatus(realm, username);
      user.roles = new HashSet<>(UserSources.getDefaultRoles(realm, username));

      UpdateUserImpl insertUser = getImplementation(UpdateUserImpl.class);
      insertUser.execute(user);
      UpdateUserRolesImpl setRoles = getImplementation(UpdateUserRolesImpl.class);
      setRoles.updateUserRoles(user.realm, user.name, user.roles);
      t.logUserReturnIsKnown(timestamp, realm, username, type);
    }
  }
}
