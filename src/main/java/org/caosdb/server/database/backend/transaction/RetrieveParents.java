/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2019 IndiScale GmbH
 * Copyright (C) 2019 Timm Fitschen (t.fitschen@indiscale.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.database.backend.transaction;

import java.util.LinkedList;
import java.util.List;
import org.apache.commons.jcs.access.behavior.ICacheAccess;
import org.caosdb.server.caching.Cache;
import org.caosdb.server.database.CacheableBackendTransaction;
import org.caosdb.server.database.backend.interfaces.RetrieveParentsImpl;
import org.caosdb.server.database.exceptions.TransactionException;
import org.caosdb.server.database.proto.VerySparseEntity;
import org.caosdb.server.entity.EntityID;
import org.caosdb.server.entity.EntityInterface;
import org.caosdb.server.entity.RetrieveEntity;
import org.caosdb.server.entity.wrapper.Parent;

// TODO Problem with the caching.
// When an old entity version has a parent which is deleted, the name is
// still in the cached VerySparseEntity. This can be resolved by using a
// similar strategy as in RetrieveProperties.java where the name etc. are
// retrieved in a second step. Thus the deletion doesn't slip through
// unnoticed.
//
// Changes are necessary in the backend-api, i.e. mysqlbackend and the
// interfaces as well.
//
// See also a failing test in caosdb-pyinttest:
// tests/test_version.py::test_bug_cached_parent_name_in_old_version
public class RetrieveParents
    extends CacheableBackendTransaction<String, LinkedList<VerySparseEntity>> {

  private static final ICacheAccess<String, LinkedList<VerySparseEntity>> cache =
      Cache.getCache("BACKEND_EntityParents");

  /**
   * To be called by DeleteEntityProperties on execution.
   *
   * @param id
   */
  public static void removeCached(final String idVersion) {
    if (idVersion != null && cache != null) {
      cache.remove(idVersion);
    }
  }

  private final EntityInterface entity;

  public RetrieveParents(final EntityInterface entity) {
    super(cache);
    this.entity = entity;
  }

  @Override
  public LinkedList<VerySparseEntity> executeNoCache() throws TransactionException {
    final RetrieveParentsImpl t = getImplementation(RetrieveParentsImpl.class);
    return t.execute(this.entity.getId(), this.entity.getVersion().getId());
  }

  @Override
  protected void process(final LinkedList<VerySparseEntity> t) throws TransactionException {
    this.entity.getParents().clear();

    parseParentsFromVerySparseEntity(this.entity, t);
  }

  private void parseParentsFromVerySparseEntity(
      final EntityInterface entity, final List<VerySparseEntity> pars) {
    for (final VerySparseEntity vsp : pars) {
      final Parent p = new Parent(new RetrieveEntity(new EntityID(vsp.id)));
      p.setName(vsp.name);
      p.setDescription(vsp.description);
      entity.addParent(p);
    }
  }

  @Override
  protected String getKey() {
    return this.entity.getIdVersion();
  }
}
