/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.database.backend.implementation.MySQL;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.caosdb.server.database.access.Access;
import org.caosdb.server.database.backend.interfaces.SyncStatsImpl;
import org.caosdb.server.database.exceptions.TransactionException;
import org.caosdb.server.utils.ServerStat;

public class MySQLSyncStats extends MySQLTransaction implements SyncStatsImpl {

  private static final String STMT_READ_SYNC_STATS = "SELECT value FROM stats WHERE name=?";
  private static final String STMT_INSERT_SYNC_STATS =
      "INSERT INTO stats (name, value) VALUES (?,?)";
  private static final String STMT_UPDATE_SYNC_STATS = "UPDATE stats SET value = ? WHERE name=?";

  public MySQLSyncStats(final Access access) {
    super(access);
  }

  @Override
  public ServerStat read(final String name) throws TransactionException {
    try {

      final PreparedStatement read = prepareStatement(STMT_READ_SYNC_STATS);

      read.setString(1, name);
      final ResultSet rs = read.executeQuery();
      try {
        if (rs.next()) {

          final byte[] bytes = rs.getBytes(1);
          final ServerStat ret = getStats(bytes);

          return ret;
        }
      } finally {
        rs.close();
      }
    } catch (final ClassNotFoundException e) {
      throw new TransactionException(e);
    } catch (final IOException e) {
      throw new TransactionException(e);
    } catch (final SQLException e) {
      throw new TransactionException(e);
    } catch (final ConnectionException e) {
      throw new TransactionException(e);
    }
    return null;
  }

  @Override
  public void update(final String name, final ServerStat s) throws TransactionException {
    try {
      final byte[] bytes = getBytes(s);
      final PreparedStatement update = prepareStatement(STMT_UPDATE_SYNC_STATS);
      update.setBytes(1, bytes);
      update.setString(2, name);
      update.execute();
    } catch (final SQLException e) {
      throw new TransactionException(e);
    } catch (final ConnectionException e) {
      throw new TransactionException(e);
    } catch (final IOException e) {
      throw new TransactionException(e);
    }
  }

  @Override
  public void insert(final String name, final ServerStat s) throws TransactionException {
    try {

      final byte[] bytes = getBytes(s);
      final PreparedStatement insert = prepareStatement(STMT_INSERT_SYNC_STATS);

      insert.setString(1, name);
      insert.setBytes(2, bytes);
      insert.execute();
    } catch (final SQLException e) {
      throw new TransactionException(e);
    } catch (final IOException e) {
      throw new TransactionException(e);
    } catch (final ConnectionException e) {
      throw new TransactionException(e);
    }
  }

  private ServerStat getStats(final byte[] bytes) throws ClassNotFoundException, IOException {
    final ByteArrayInputStream bis = new ByteArrayInputStream(bytes);
    ObjectInput in = null;
    try {
      in = new ObjectInputStream(bis);
      return (ServerStat) in.readObject();
    } finally {
      try {
        if (in != null) {
          in.close();
        }
      } catch (final IOException ex) {
      }
      try {
        bis.close();
      } catch (final IOException ex) {
      }
    }
  }

  private byte[] getBytes(final ServerStat s) throws IOException {
    final ByteArrayOutputStream bos = new ByteArrayOutputStream();
    ObjectOutput out = null;
    try {
      out = new ObjectOutputStream(bos);
      out.writeObject(s);
      return bos.toByteArray();
    } finally {
      try {
        if (out != null) {
          out.close();
        }
      } catch (final IOException ex) {
      }
      try {
        bos.close();
      } catch (final IOException ex) {
      }
    }
  }
}
