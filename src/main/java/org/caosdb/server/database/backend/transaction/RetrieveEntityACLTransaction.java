/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2021 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2021 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.caosdb.server.database.backend.transaction;

import org.caosdb.server.database.CacheableBackendTransaction;
import org.caosdb.server.database.backend.interfaces.RetrieveEntityACLImpl;
import org.caosdb.server.database.exceptions.TransactionException;
import org.caosdb.server.database.proto.VerySparseEntity;
import org.caosdb.server.entity.EntityID;
import org.caosdb.server.permissions.EntityACL;

public class RetrieveEntityACLTransaction
    extends CacheableBackendTransaction<EntityID, VerySparseEntity> {

  private EntityID id;
  private EntityACL entityAcl;

  public RetrieveEntityACLTransaction(EntityID id) {
    // TODO
    super(null);
    this.id = id;
  }

  @Override
  public VerySparseEntity executeNoCache() throws TransactionException {
    RetrieveEntityACLImpl t = getImplementation(RetrieveEntityACLImpl.class);
    return t.execute(getKey().toString());
  }

  @Override
  protected void process(VerySparseEntity t) throws TransactionException {
    this.entityAcl = EntityACL.fromJSON(t.acl);
  }

  @Override
  protected EntityID getKey() {
    return id;
  }

  public EntityACL getEntityAcl() {
    return entityAcl;
  }

  public RetrieveEntityACLTransaction reuse(EntityID id) {
    this.id = id;
    this.entityAcl = null;
    return this;
  }

  public static void removeCached(EntityID entityID) {
    // TODO
  }
}
