/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2020 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2020 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.database.backend.implementation.MySQL;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.LinkedList;
import org.caosdb.server.database.access.Access;
import org.caosdb.server.database.backend.interfaces.RetrieveVersionHistoryImpl;
import org.caosdb.server.database.exceptions.TransactionException;
import org.caosdb.server.database.proto.VersionHistoryItem;
import org.caosdb.server.entity.EntityID;

/**
 * Transaction to retrieve all versions of an entity.
 *
 * <p>Creates a mapping ID :: (VersionHistoryItem)
 */
public class MySQLRetrieveVersionHistory extends MySQLTransaction
    implements RetrieveVersionHistoryImpl {

  public static final String VERSION_HISTORY_STMT = "CALL get_version_history(?)";

  public MySQLRetrieveVersionHistory(final Access access) {
    super(access);
  }

  @Override
  public HashMap<String, VersionHistoryItem> execute(final EntityID entityId) {

    final HashMap<String, VersionHistoryItem> result = new HashMap<>();
    try {
      final PreparedStatement s = prepareStatement(VERSION_HISTORY_STMT);
      s.setString(1, entityId.toString());
      final ResultSet rs = s.executeQuery();

      while (rs.next()) {
        final String childId = DatabaseUtils.bytes2UTF8(rs.getBytes("child"));
        final String parentId = DatabaseUtils.bytes2UTF8(rs.getBytes("parent"));
        final Long childSeconds = rs.getLong("child_seconds");
        final Integer childNanos = rs.getInt("child_nanos");
        final String childUsername = DatabaseUtils.bytes2UTF8(rs.getBytes("child_username"));
        final String childRealm = DatabaseUtils.bytes2UTF8(rs.getBytes("child_realm"));
        VersionHistoryItem v = result.get(childId);
        if (v == null) {
          v = new VersionHistoryItem();
          v.id = childId;
          v.seconds = childSeconds;
          v.nanos = childNanos;
          v.username = childUsername;
          v.realm = childRealm;
          result.put(childId, v);
        }

        if (parentId != null) {
          if (v.parents == null) {
            v.parents = new LinkedList<>();
          }
          v.parents.add(parentId);
        }
      }
    } catch (SQLException | ConnectionException e) {
      throw new TransactionException(e);
    }
    return result;
  }
}
