/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2023 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2023 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.caosdb.server.database.backend.implementation.MySQL;

import static java.sql.Types.BIGINT;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import org.caosdb.server.database.access.Access;
import org.caosdb.server.database.backend.interfaces.InsertEntityPropertiesImpl;
import org.caosdb.server.database.exceptions.IntegrityException;
import org.caosdb.server.database.exceptions.TransactionException;
import org.caosdb.server.database.proto.FlatProperty;
import org.caosdb.server.datatype.AbstractCollectionDatatype;
import org.caosdb.server.datatype.AbstractDatatype.Table;
import org.caosdb.server.datatype.CollectionValue;
import org.caosdb.server.datatype.IndexedSingleValue;
import org.caosdb.server.datatype.ReferenceValue;
import org.caosdb.server.datatype.SingleValue;
import org.caosdb.server.entity.EntityID;
import org.caosdb.server.entity.EntityInterface;
import org.caosdb.server.entity.wrapper.Property;

/**
 * Insert the entity's properties.
 *
 * <p>This implementation transforms a deep tree of properties into a flat representation. This
 * transformation is a MySQL-backend implementation detail and should not be leaked to the clients.
 *
 * <p>The reverse transformation happens in MySQLRetrieveProperties.
 *
 * @see {@link MySQLRetrieveProperties}
 * @author Timm Fitschen <t.fitschen@indiscale.com>
 */
public class MySQLInsertEntityProperties extends MySQLTransaction
    implements InsertEntityPropertiesImpl {

  public static final String STMT_INSERT_ENTITY_PROPERTY =
      "call insertEntityProperty(?,?,?,?,?,?,?,?,?,?,?,?)";

  public MySQLInsertEntityProperties(final Access access) {
    super(access);
  }

  @Override
  public void execute(EntityInterface entity) {
    final List<Property> stage1Inserts = new LinkedList<>();
    final List<Property> stage2Inserts = new LinkedList<>();

    final int domainCount = DatabaseUtils.deriveStage1Inserts(stage1Inserts, entity);

    try {
      final Deque<EntityID> replacementIds = registerReplacementId(domainCount);

      DatabaseUtils.deriveStage2Inserts(stage2Inserts, stage1Inserts, replacementIds, entity);

      insertStages(stage1Inserts, entity.getDomain(), entity.getId());
      insertStages(stage2Inserts, entity.getId(), null);
    } catch (final SQLIntegrityConstraintViolationException exc) {
      throw new IntegrityException(exc);
    } catch (SQLException | ConnectionException e) {
      throw new TransactionException(e);
    }
  }

  private void insertFlatProperty(
      final EntityID domain,
      final EntityID entity,
      final FlatProperty fp,
      final Table table,
      final Long unit_sig)
      throws TransactionException {
    try {
      final PreparedStatement stmt = prepareStatement(STMT_INSERT_ENTITY_PROPERTY);

      stmt.setString(1, domain.toString());
      stmt.setString(2, entity.toString());
      stmt.setString(3, fp.id);
      stmt.setString(4, table.toString());

      stmt.setString(5, fp.value);
      if (unit_sig != null) {
        stmt.setLong(6, unit_sig);
      } else {
        stmt.setNull(6, BIGINT);
      }
      stmt.setString(7, fp.status);
      stmt.setString(8, fp.name);
      stmt.setString(9, fp.desc);
      stmt.setString(10, fp.type_id);
      stmt.setString(11, fp.collection);
      stmt.setInt(12, fp.idx);
      stmt.execute();
    } catch (final SQLIntegrityConstraintViolationException exc) {
      throw new IntegrityException(exc);
    } catch (final SQLException | ConnectionException exc) {
      throw new TransactionException(exc);
    }
  }

  private void insertStages(
      final List<Property> inserts, final EntityID domain, final EntityID entity)
      throws TransactionException {

    for (final Property property : inserts) {

      // prepare flat property
      final FlatProperty fp = new FlatProperty();
      Table table = Table.null_data;
      Long unit_sig = null;
      fp.id = property.getId().toString();
      fp.idx = property.getPIdx();
      fp.status = property.getStatementStatus().name();

      if (property.getStatementStatus() == ReplacementStatus.REPLACEMENT) {
        // special treatment: swap value and id. This is part of the back-end specification for the
        // representation of replacement. The reason why this happens here (and
        // not in the replacement class for instance) is that the original
        // Property must not be changed for this. Otherwise we would have to
        // change it back after the insertion or internally used replacement ids
        // would be leaked.

        // value is to be the id of the property which is being replaced
        fp.value = fp.id.toString();

        // id is to be the replacement id (an internally used/private id)
        fp.id = ((ReferenceValue) property.getValue()).getId().toString();
        table = Table.reference_data;
      } else {

        if (property.hasUnit()) {
          unit_sig = property.getUnit().getSignature();
        }
        if (property.hasValue()) {
          if (property.getValue() instanceof CollectionValue) {
            // insert collection of values
            final CollectionValue v = (CollectionValue) property.getValue();
            final Iterator<IndexedSingleValue> iterator = v.iterator();
            final SingleValue firstValue = iterator.next();

            // insert 2nd to nth item
            for (int i = 1; i < v.size(); i++) {
              final SingleValue vi = iterator.next();
              fp.idx = i;
              if (vi == null) {
                fp.value = null;
                table = Table.null_data;
              } else {
                fp.value = vi.toDatabaseString();
                table = vi.getTable();
              }
              insertFlatProperty(
                  domain, entity != null ? entity : property.getDomain(), fp, table, unit_sig);
            }

            // insert first item
            fp.idx = 0;
            if (firstValue == null) {
              fp.value = null;
              table = Table.null_data;
            } else {
              fp.value = firstValue.toDatabaseString();
              table = firstValue.getTable();
            }

          } else {
            // insert single value
            fp.value = ((SingleValue) property.getValue()).toDatabaseString();
            if (property instanceof Property && ((Property) property).isName()) {
              table = Table.name_data;
            } else {
              table = ((SingleValue) property.getValue()).getTable();
            }
          }
        }
        if (property.isNameOverride()) {
          fp.name = property.getName();
        }
        if (property.isDescOverride()) {
          fp.desc = property.getDescription();
        }
        if (property.isDatatypeOverride()) {
          if (property.getDatatype() instanceof AbstractCollectionDatatype) {
            fp.type_id =
                ((AbstractCollectionDatatype) property.getDatatype())
                    .getDatatype()
                    .getId()
                    .toString();
            fp.collection =
                ((AbstractCollectionDatatype) property.getDatatype()).getCollectionName();
          } else {
            fp.type_id = property.getDatatype().getId().toString();
          }
        }
      }

      insertFlatProperty(
          domain, entity != null ? entity : property.getDomain(), fp, table, unit_sig);
    }
  }

  public static final String STMT_REGISTER_SUBDOMAIN = "call registerReplacementIds(?)";

  public Deque<EntityID> registerReplacementId(final int domainCount)
      throws SQLException, ConnectionException {
    final PreparedStatement stmt = prepareStatement(STMT_REGISTER_SUBDOMAIN);
    stmt.setInt(1, domainCount);
    try (final ResultSet rs = stmt.executeQuery()) {
      final Deque<EntityID> ret = new ArrayDeque<>();
      while (rs.next()) {
        ret.add(new EntityID(rs.getString(1)));
      }
      return ret;
    }
  }
}
