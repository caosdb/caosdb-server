/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.database.backend.implementation.MySQL;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.LinkedList;
import org.caosdb.server.database.access.Access;
import org.caosdb.server.database.backend.interfaces.RetrieveParentsImpl;
import org.caosdb.server.database.exceptions.TransactionException;
import org.caosdb.server.database.proto.VerySparseEntity;
import org.caosdb.server.entity.EntityID;

public class MySQLRetrieveParents extends MySQLTransaction implements RetrieveParentsImpl {

  public MySQLRetrieveParents(final Access access) {
    super(access);
  }

  private static final String stmtStr = "call retrieveEntityParents(?, ?)";

  @Override
  public LinkedList<VerySparseEntity> execute(final EntityID id, final String version)
      throws TransactionException {
    try {
      ResultSet rs = null;
      try {
        final PreparedStatement prepareStatement = prepareStatement(stmtStr);

        prepareStatement.setString(1, id.toString());
        if (version == null) {
          prepareStatement.setNull(2, Types.VARBINARY);
        } else {
          prepareStatement.setString(2, version);
        }
        rs = prepareStatement.executeQuery();
        return DatabaseUtils.parseParentResultSet(rs);
      } finally {
        if (rs != null && !rs.isClosed()) {
          rs.close();
        }
      }
    } catch (final SQLException e) {
      throw new TransactionException(e);
    } catch (final ConnectionException e) {
      throw new TransactionException(e);
    }
  }
}
