/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 *   Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2019,2020 IndiScale GmbH
 * Copyright (C) 2019,2020 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.database.backend.transaction;

import org.apache.commons.jcs.access.behavior.ICacheAccess;
import org.caosdb.server.caching.Cache;
import org.caosdb.server.database.CacheableBackendTransaction;
import org.caosdb.server.database.backend.interfaces.RetrieveSparseEntityImpl;
import org.caosdb.server.database.exceptions.TransactionException;
import org.caosdb.server.database.proto.SparseEntity;
import org.caosdb.server.entity.EntityID;
import org.caosdb.server.entity.EntityInterface;
import org.caosdb.server.entity.RetrieveEntity;
import org.caosdb.server.utils.EntityStatus;

public class RetrieveSparseEntity extends CacheableBackendTransaction<String, SparseEntity> {

  private final EntityInterface entity;
  private static final ICacheAccess<String, SparseEntity> cache =
      Cache.getCache("BACKEND_SparseEntities");

  /**
   * To be called by {@link UpdateSparseEntity} and {@link DeleteEntityTransaction} on execution.
   *
   * @param entity
   */
  public static void removeCached(final EntityInterface entity) {
    if (entity != null && cache != null) {
      cache.remove(entity.getId().toString());
      cache.remove(entity.getIdVersion());
    }
  }

  public RetrieveSparseEntity(final EntityInterface entity) {
    super(cache);
    this.entity = entity;
  }

  public RetrieveSparseEntity(final EntityID id, final String version) {
    this(new RetrieveEntity(id));
    this.entity.getVersion().setId(version);
  }

  public RetrieveSparseEntity(SparseEntity entity) {
    this(new RetrieveEntity(new EntityID(entity.id)));
    this.entity.getVersion().setId(entity.versionId);
  }

  @Override
  public SparseEntity executeNoCache() throws TransactionException {
    final RetrieveSparseEntityImpl t = getImplementation(RetrieveSparseEntityImpl.class);
    final SparseEntity ret = t.execute(getEntity().getId(), getEntity().getVersion().getId());
    if (ret == null) {
      this.entity.setEntityStatus(EntityStatus.NONEXISTENT);
    }
    return ret;
  }

  @Override
  protected void process(final SparseEntity t) throws TransactionException {
    if (t != null) {
      this.entity.parseSparseEntity(t);
      this.entity.setEntityStatus(EntityStatus.VALID);
    }
  }

  @Override
  protected String getKey() {
    if ("HEAD".equalsIgnoreCase(entity.getVersion().getId())) {
      return this.entity.getId().toString();
    } else if (entity.hasVersion()
        && entity.getVersion().getId().toUpperCase().startsWith("HEAD")) {
      return null;
    }
    return this.entity.getIdVersion();
  }

  public EntityInterface getEntity() {
    return this.entity;
  }
}
