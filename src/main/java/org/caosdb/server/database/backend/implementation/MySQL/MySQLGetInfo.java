/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.database.backend.implementation.MySQL;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import org.caosdb.server.database.access.Access;
import org.caosdb.server.database.backend.interfaces.GetInfoImpl;
import org.caosdb.server.database.exceptions.TransactionException;
import org.caosdb.server.database.proto.ProtoInfo;

public class MySQLGetInfo extends MySQLTransaction implements GetInfoImpl {

  public MySQLGetInfo(final Access access) {
    super(access);
  }

  public static final String STMT_GET_INFO =
      "SELECT (SELECT COUNT(id) FROM entities WHERE role='RECORD') as RecordsCount, (SELECT COUNT(id) FROM entities WHERE role='PROPERTY') as PropertiesCount, (SELECT COUNT(id) FROM entities WHERE role='RECORDTYPE') as RecordTypesCount, (SELECT COUNT(file_id) FROM files) as FilesCount, (SELECT sum(size) FROM files) AS FSSize";

  @Override
  public void execute(final ProtoInfo protoInfo) throws TransactionException {
    try {
      final PreparedStatement stmt = prepareStatement(STMT_GET_INFO);

      final ResultSet rs = stmt.executeQuery();
      try {
        if (rs.next()) {
          protoInfo.recordsCount = rs.getInt("RecordsCount");
          protoInfo.propertiesCount = rs.getInt("PropertiesCount");
          protoInfo.recordTypesCount = rs.getInt("RecordTypesCount");
          protoInfo.filesCount = rs.getInt("FilesCount");
          protoInfo.fssize = rs.getLong("FSSize");
        }
      } finally {
        rs.close();
      }
    } catch (final Exception e) {
      throw new TransactionException(e);
    }
  }
}
