/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2021 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2021 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.caosdb.server.database.backend.implementation.MySQL;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import org.caosdb.server.database.access.Access;
import org.caosdb.server.database.backend.interfaces.DeleteRoleImpl;
import org.caosdb.server.database.exceptions.TransactionException;
import org.caosdb.server.utils.ServerMessages;

public class MySQLDeleteRole extends MySQLTransaction implements DeleteRoleImpl {

  public MySQLDeleteRole(final Access access) {
    super(access);
  }

  private static final String STMT_DELETE_ROLE = "DELETE FROM roles WHERE name=?;";

  @Override
  public void delete(final String role) throws TransactionException {
    try {
      final PreparedStatement stmt = prepareStatement(STMT_DELETE_ROLE);
      stmt.setString(1, role);
      stmt.execute();
    } catch (final SQLIntegrityConstraintViolationException e) {
      throw new TransactionException(ServerMessages.ROLE_CANNOT_BE_DELETED);
    } catch (final SQLException e) {
      throw new TransactionException(e);
    } catch (final ConnectionException e) {
      throw new TransactionException(e);
    }
  }
}
