/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2021 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2021 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.caosdb.server.database.backend.implementation.MySQL;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.Map;
import org.caosdb.server.accessControl.Role;
import org.caosdb.server.database.access.Access;
import org.caosdb.server.database.backend.interfaces.RetrieveRoleImpl;
import org.caosdb.server.database.exceptions.TransactionException;
import org.caosdb.server.database.proto.ProtoUser;
import org.caosdb.server.permissions.PermissionRule;
import org.eclipse.jetty.util.ajax.JSON;

public class MySQLRetrieveRole extends MySQLTransaction implements RetrieveRoleImpl {

  public MySQLRetrieveRole(final Access access) {
    super(access);
  }

  public static final String STMT_RETRIEVE_ROLE =
      "SELECT r.description AS description, p.permissions AS permissions FROM roles AS r LEFT JOIN permissions AS p ON (r.name = p.role) WHERE r.name=?";
  public static final String STMT_RETRIEVE_USERS =
      "SELECT u.realm, u.user FROM user_roles AS u WHERE u.role = ?";

  @Override
  public Role retrieve(final String role) throws TransactionException {
    Role ret = null;
    try (final PreparedStatement stmt = prepareStatement(STMT_RETRIEVE_ROLE)) {
      stmt.setString(1, role);
      final ResultSet rs = stmt.executeQuery();
      if (rs.next()) {
        ret = new Role();
        ret.name = role;
        ret.description = rs.getString("description");
        ret.permission_rules = parse(rs.getString("permissions"));
      } else {
        return null;
      }
    } catch (final SQLException e) {
      throw new TransactionException(e);
    } catch (final ConnectionException e) {
      throw new TransactionException(e);
    }

    try (final PreparedStatement stmt = prepareStatement(STMT_RETRIEVE_USERS)) {
      stmt.setString(1, role);
      final ResultSet rs = stmt.executeQuery();
      if (rs.next()) {
        ret.users = new LinkedList<>();

        ret.users.add(nextUser(rs));
        while (rs.next()) {
          ret.users.add(nextUser(rs));
        }
      }
    } catch (final SQLException e) {
      throw new TransactionException(e);
    } catch (final ConnectionException e) {
      throw new TransactionException(e);
    }
    return ret;
  }

  private ProtoUser nextUser(ResultSet rs) throws SQLException {
    ProtoUser nextUser = new ProtoUser();
    nextUser.realm = rs.getString("realm");
    nextUser.name = rs.getString("user");
    return nextUser;
  }

  @SuppressWarnings("unchecked")
  private LinkedList<PermissionRule> parse(String string) {
    if (string == null) return null;
    final Object[] maps = (Object[]) JSON.parse(string);
    final LinkedList<PermissionRule> ret = new LinkedList<>();
    for (final Object map : maps) {
      ret.add(PermissionRule.parse((Map<String, String>) map));
    }

    return ret;
  }
}
