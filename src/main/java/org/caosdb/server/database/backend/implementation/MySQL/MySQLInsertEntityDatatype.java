/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2023 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2023 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.caosdb.server.database.backend.implementation.MySQL;

import java.sql.PreparedStatement;
import java.sql.SQLIntegrityConstraintViolationException;
import org.caosdb.server.database.access.Access;
import org.caosdb.server.database.backend.interfaces.InsertEntityDatatypeImpl;
import org.caosdb.server.database.exceptions.IntegrityException;
import org.caosdb.server.database.exceptions.TransactionException;
import org.caosdb.server.database.proto.SparseEntity;

/**
 * Insert an entity's (meaning: an abstract property's) data type.
 *
 * <p>This inserts the abstract property's data type as opposed to the (overridden data type which
 * is being inserted/updated via InsertEntityPropertiesImpl.
 *
 * @see {@link InsertEntityPropertiesImpl}
 * @author Timm Fitschen <t.fitschen@indiscale.com>
 */
public class MySQLInsertEntityDatatype extends MySQLTransaction
    implements InsertEntityDatatypeImpl {

  public MySQLInsertEntityDatatype(Access access) {
    super(access);
  }

  /**
   * Inserts atomic data types of properties into the data_type table. Has two parameters, the
   * property_id and the data type name.
   */
  public static final String STMT_INSERT_ENTITY_DATATYPE = "call insertEntityDataType(?, ?)";

  /**
   * Inserts collection data types of properties into the data_type table. Has two parameters, the
   * property_id and the type of collection (e.g. 'LIST').
   */
  public static final String STMT_INSERT_ENTITY_COLLECTION = "call insertEntityCollection(?, ?)";

  @Override
  public void execute(final SparseEntity entity) {
    try {
      final PreparedStatement insertEntityDatatypeStmt =
          prepareStatement(STMT_INSERT_ENTITY_DATATYPE);

      insertEntityDatatypeStmt.setString(1, entity.id);
      insertEntityDatatypeStmt.setString(2, entity.datatype_id);

      insertEntityDatatypeStmt.execute();

      if (entity.collection != null) {
        final PreparedStatement insertEntityCollectionStmt =
            prepareStatement(STMT_INSERT_ENTITY_COLLECTION);

        insertEntityCollectionStmt.setString(1, entity.id);
        insertEntityCollectionStmt.setString(2, entity.collection);

        insertEntityCollectionStmt.execute();
      }

    } catch (final SQLIntegrityConstraintViolationException exc) {
      throw new IntegrityException(exc);
    } catch (final TransactionException exc) {
      throw exc;
    } catch (final Exception exc) {
      throw new TransactionException(exc);
    }
  }
}
