/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.database.proto;

import java.io.Serializable;

/**
 * VerySparseEntity is used for storing only id, name, description, and role of an Entity. That's
 * basically everything stored to the entities table and everything which is needed for a
 * constructing a ParentEntity.
 *
 * @author tf
 */
public class VerySparseEntity implements Serializable {

  private static final long serialVersionUID = 7370925076064714740L;

  public String id = null;
  public String name = null;
  public String description = null;
  public String role = null;
  public String acl = null;
}
