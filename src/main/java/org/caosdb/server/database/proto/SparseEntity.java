/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.database.proto;

/**
 * Rather abstract representation of a sparse Entity used by the data base abstraction layer.
 * Especially needed for caching.
 *
 * @author tf
 */
public class SparseEntity extends VerySparseEntity {

  private static final long serialVersionUID = -560259468853956476L;

  public String collection = null;
  public String datatype_id = null;
  public String datatype_name = null;
  public String fileHash = null;
  public String filePath = null;
  public Long fileSize = null;
  public Long fileChecked = null;
  public String versionId = null;

  @Override
  public String toString() {
    return new StringBuilder()
        .append(this.id)
        .append(this.name)
        .append(this.description)
        .append(this.role)
        .append(this.collection)
        .append(this.datatype_id)
        .append(this.datatype_name)
        .append(this.fileHash)
        .append(this.filePath)
        .append(this.fileSize)
        .append(this.versionId)
        .toString();
  }
}
