/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2019 IndiScale GmbH
 * Copyright (C) 2019,2020 Timm Fitschen (t.fitschen@indiscale.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.caching;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.Properties;
import org.apache.commons.jcs.JCS;
import org.apache.commons.jcs.access.behavior.ICacheAccess;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.caosdb.server.CaosDBServer;
import org.caosdb.server.ServerProperties;

/**
 * A CacheHelper implementation which is configured statically via the {@link
 * ServerProperties#KEY_CACHE_CONF_LOC} properties file.
 *
 * @author Timm Fitschen (t.fitschen@indiscale.com)
 */
public class JCSCacheHelper implements CacheHelper {

  protected static Logger logger = LogManager.getLogger(JCSCacheHelper.class);

  // A No-Operation configuration for JCS
  private static Properties getNOPCachingProperties() {
    Properties ret = new Properties();
    ret.setProperty(
        "jcs.default.cacheattributes", "org.apache.commons.jcs.engine.CompositeCacheAttributes");
    ret.setProperty("jcs.default.cacheattributes.MaxObjects", "0");
    return ret;
  }

  // configure JCS
  static {
    init();
  }

  public static void init() {
    final boolean disabled = !CaosDBServer.useCache();
    init(CaosDBServer.getServerProperty(ServerProperties.KEY_CACHE_CONF_LOC), disabled);
  }

  public static void init(String configFileLocation, boolean disabled) {
    Properties config = null;
    if (disabled) {
      config = getNOPCachingProperties();
      logger.info("Configuring JCS Caching: disabled");
    } else {
      try {
        Properties p = new Properties();
        final InputStream is = new FileInputStream(configFileLocation);
        p.load(is);
        is.close();
        config = p;
      } catch (final FileNotFoundException e) {
        logger.error(e);
        config = getNOPCachingProperties();
      } catch (final IOException e) {
        logger.error(e);
        config = getNOPCachingProperties();
      }
      logger.info("Configuring JCS Caching with {}", config);
    }

    // If the JCS config is updated/reset, it has to be shut down before.
    JCS.shutdown();
    JCS.setConfigProperties(config);
  }

  @Override
  public <K, V extends Serializable> ICacheAccess<K, V> getCache(final String name) {
    final ICacheAccess<K, V> cache = JCS.getInstance(name);
    logger.info(
        "Caching configuration for {}:\n+----{}\n+----{}",
        name,
        cache.getCacheAttributes(),
        cache.getDefaultElementAttributes());

    return cache;
  }
}
