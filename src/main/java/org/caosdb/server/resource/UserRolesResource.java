/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.resource;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.List;
import org.caosdb.server.CaosDBException;
import org.caosdb.server.accessControl.ACMPermissions;
import org.caosdb.server.accessControl.UserSources;
import org.caosdb.server.database.backend.implementation.MySQL.ConnectionException;
import org.caosdb.server.entity.Message;
import org.caosdb.server.transaction.RetrieveUserRolesTransaction;
import org.caosdb.server.transaction.UpdateUserRolesTransaction;
import org.caosdb.server.utils.ServerMessages;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.restlet.data.Status;
import org.restlet.representation.Representation;

public class UserRolesResource extends AbstractCaosDBServerResource {

  @Override
  protected Representation httpGetInChildClass()
      throws ConnectionException,
          IOException,
          SQLException,
          CaosDBException,
          NoSuchAlgorithmException,
          Exception {
    final String user = getRequestedItems()[0];
    final String realm =
        (getRequestAttributes().get("realm") != null
            ? (String) getRequestAttributes().get("realm")
            : UserSources.guessRealm(user, UserSources.getDefaultRealm()));

    getUser().checkPermission(ACMPermissions.PERMISSION_RETRIEVE_USER_ROLES(realm, user));

    final RetrieveUserRolesTransaction t = new RetrieveUserRolesTransaction(realm, user);
    try {
      t.execute();
    } catch (final Message m) {
      if (m == ServerMessages.ACCOUNT_DOES_NOT_EXIST) {
        return error(m, Status.CLIENT_ERROR_NOT_FOUND);
      }
      throw m;
    }

    final Element root = generateRootElement();
    root.addContent(t.getUserRolesElement());
    return ok(new Document(root));
  }

  @Override
  protected Representation httpPutInChildClass(final Representation entity)
      throws ConnectionException, JDOMException, Exception, xmlNotWellFormedException {
    final String user = getRequestedItems()[0];
    final String realm =
        (getRequestAttributes().get("realm") != null
            ? (String) getRequestAttributes().get("realm")
            : UserSources.guessRealm(user, UserSources.getDefaultRealm()));

    final HashSet<String> roles =
        parseRoles(parseEntity(entity).getRootElement().getChildren("Role"));

    final UpdateUserRolesTransaction t = new UpdateUserRolesTransaction(realm, user, roles);
    try {
      t.execute();
    } catch (final Message m) {
      if (m == ServerMessages.ROLE_DOES_NOT_EXIST) {
        return error(m, Status.CLIENT_ERROR_CONFLICT);
      } else if (m == ServerMessages.ACCOUNT_DOES_NOT_EXIST) {
        return error(m, Status.CLIENT_ERROR_NOT_FOUND);
      }
      throw m;
    }

    final Element root = generateRootElement();
    root.addContent(t.getUserRolesElement());
    return ok(new Document(root));
  }

  private HashSet<String> parseRoles(final List<Element> roleElements) {
    final HashSet<String> roles = new HashSet<String>();
    for (final Element roleElem : roleElements) {
      roles.add(roleElem.getAttributeValue("name"));
    }
    return roles;
  }
}
