/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.resource;

import java.io.IOException;
import java.io.OutputStream;
import java.io.Writer;
import org.caosdb.server.CaosDBServer;
import org.caosdb.server.ServerProperties;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.ProcessingInstruction;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;
import org.restlet.data.MediaType;
import org.restlet.data.Reference;
import org.restlet.representation.WriterRepresentation;

/**
 * The Restlet Extension (restlet.ext) packages support two implementations of the xml API DOM and
 * SAX - w3c.DOM and oracle's JAXP. Both of them are implementations of API which are not primarily
 * designed for java implementation and thus are a bit awkward to handle.<br>
 * <br>
 * This class provides support for JDOM - an genuine java xml processor (http://www.jdom.org/).<br>
 * <br>
 * It extends restlet.representation.WriterRepresentation and overrides its <code>
 * write(Writer writer)</code> method. This method is called by the Restlet API. It commits a Writer
 * to which the generated <code>Document document</code> is written by the <code>
 * jdom.outputter.output(document writer)</code><br>
 * <br>
 * The JdomRepresentation may be implemented as <code>@Get, @Put, @Delete ...</code> methods in a
 * ServerResource class, which have to return an xml document.
 *
 * @author Timm Fitschen
 */
public class JdomRepresentation extends WriterRepresentation {

  private final Document document;
  private final String indent;

  /**
   * Constructor.
   *
   * @param document A JDOM Document parsed from elsewhere.
   * @param indent Usually a number of whitespaces for better human readbility.
   * @param xslPath A String containing a meaningful path to a xslt file.
   */
  public JdomRepresentation(final Document document, final String indent, final String xslPath) {
    super(MediaType.TEXT_XML);
    this.indent = indent;
    this.document = document;
    Element noscript = new Element("noscript");
    Element div = new Element("h1");

    div.addContent("Please enable JavaScript!");
    noscript.addContent(div);
    document.getRootElement().addContent(0, noscript);
    if (xslPath != null && document != null) {
      addStyleSheet(document, xslPath);
    }
  }

  public static final String getWebUIRef(final Reference reference) {
    return reference.getHostIdentifier()
        + CaosDBServer.getServerProperty(ServerProperties.KEY_CONTEXT_ROOT)
        + "/webinterface/";
  }

  /** adds the xslt processing instruction to the document. */
  protected final void addStyleSheet(Document document, String xslPath) {
    final ProcessingInstruction pi =
        new ProcessingInstruction("xml-stylesheet", "type=\"text/xsl\" href=\"" + xslPath + "\" ");
    document.getContent().add(0, pi);
  }

  @Override
  public void write(final Writer writer) throws IOException {
    if (this.document != null && this.document.hasRootElement()) {
      final XMLOutputter outputter = new XMLOutputter();
      final Format newFormat = Format.getPrettyFormat();
      if (this.indent != null) {
        newFormat.setIndent(this.indent);
      }
      outputter.setFormat(newFormat);
      outputter.output(this.document, writer);
    }
  }

  @Override
  public void write(final OutputStream out) throws IOException {
    if (this.document != null && this.document.hasRootElement()) {
      final XMLOutputter outputter = new XMLOutputter();
      final Format newFormat = Format.getPrettyFormat();
      if (this.indent != null) {
        newFormat.setIndent(this.indent);
      }
      outputter.setFormat(newFormat);
      outputter.output(this.document, out);
    }
  }
}
