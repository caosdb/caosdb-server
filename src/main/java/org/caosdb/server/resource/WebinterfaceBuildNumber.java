/**
 * ** header v3.0 This file is a part of the CaosDB Project.
 *
 * <p>Copyright (c) 2019 IndiScale GmbH Copyright (c) 2019 Daniel Hornung <d.hornung@indiscale.com>
 *
 * <p>This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * <p>This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * <p>You should have received a copy of the GNU Affero General Public License along with this
 * program. If not, see <https://www.gnu.org/licenses/>.
 *
 * <p>** end header
 */
package org.caosdb.server.resource;

import org.caosdb.server.utils.WebinterfaceUtils;
import org.restlet.data.Status;
import org.restlet.resource.Get;
import org.restlet.resource.ResourceException;
import org.restlet.resource.ServerResource;

/**
 * This {@link ServerResource} exposes the current build number of the web interface.
 *
 * <p>It is mainly used for testing and debugging. A User can determine the current build number of
 * the web interface.
 *
 * @author Timm Fitschen (t.fitschen@indiscale.com)
 */
public class WebinterfaceBuildNumber extends ServerResource {

  private WebinterfaceUtils utils;

  @Override
  protected void doInit() throws ResourceException {
    super.doInit();
    this.utils = WebinterfaceUtils.getInstance(getRequest());
  }

  /**
   * Return the current build number of the web interface.
   *
   * <p>If the build number could not be determined the server responds with 404 - Not Found.
   * Reasons for this include an out-dated web interface, the web interface has not been build yet
   * or the web interface is not available at all.
   *
   * @return the current build number.
   */
  @Get("text")
  public String getBuildNumber() {
    String buildNumber = utils.getBuildNumber();

    if (buildNumber == null) {
      setStatus(Status.CLIENT_ERROR_NOT_FOUND, "Build number could not be determined.");
    }
    return buildNumber;
  }
}
