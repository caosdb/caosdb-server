/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2021 Timm Fitschen <t.fitschen@indiscale.com>
 * Copyright (C) 2021 IndiScale GmbH <info@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.resource;

import org.caosdb.server.entity.EntityInterface;
import org.caosdb.server.entity.container.RetrieveContainer;
import org.caosdb.server.permissions.EntityPermission;
import org.caosdb.server.resource.transaction.RetrieveEntityResource;

/**
 * Resource which returns the entity with the owner information attached to it.
 *
 * <p>Note: 'Owners' are all roles with the {@link EntityPermission#EDIT_ACL}
 *
 * @author Timm Fitschen (t.fitschen@indiscale.com)
 */
public class EntityOwnerResource extends RetrieveEntityResource {

  @Override
  protected void handleRetrieveContainer(RetrieveContainer container) {
    super.handleRetrieveContainer(container);
    for (final EntityInterface e : container) {
      // with this flag, the owner info will be loaded as well.
      e.setFlag("owner", null);
    }
  }
}
