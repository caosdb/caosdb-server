/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.resource;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.logging.Level;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AccountException;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.CredentialsException;
import org.apache.shiro.subject.Subject;
import org.caosdb.server.CaosDBException;
import org.caosdb.server.CaosDBServer;
import org.caosdb.server.accessControl.AuthenticationUtils;
import org.caosdb.server.accessControl.RealmUsernamePasswordToken;
import org.caosdb.server.accessControl.UserSources;
import org.caosdb.server.database.backend.implementation.MySQL.ConnectionException;
import org.caosdb.server.entity.xml.ToElementable;
import org.caosdb.server.utils.ServerMessages;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.restlet.data.Form;
import org.restlet.data.Reference;
import org.restlet.data.Status;
import org.restlet.representation.Representation;

public class AuthenticationResource extends AbstractCaosDBServerResource {

  /** Returns single "Login" element. XSLT will create a form. */
  @Override
  protected Representation httpGetInChildClass()
      throws ConnectionException,
          IOException,
          SQLException,
          CaosDBException,
          NoSuchAlgorithmException,
          Exception {
    final Document doc = new Document();
    final Element rootElem = generateRootElement();
    doc.setRootElement(rootElem);

    final Element loginElem = new Element("Login");
    rootElem.addContent(loginElem);

    return ok(doc);
  }

  @Override
  protected Representation httpDeleteInChildClass()
      throws ConnectionException,
          SQLException,
          CaosDBException,
          IOException,
          NoSuchAlgorithmException,
          Exception {
    final Subject user = SecurityUtils.getSubject();
    if (user.isAuthenticated()) {
      user.logout();
      getResponse().getCookieSettings().addAll(AuthenticationUtils.getLogoutCookies());
      return success(ServerMessages.LOGOUT_INFO);
    }
    getResponse().setStatus(Status.CLIENT_ERROR_FORBIDDEN);
    return null;
  }

  @Override
  protected Representation httpPostInChildClass(final Representation entity)
      throws ConnectionException, JDOMException, Exception, xmlNotWellFormedException {

    getUser().logout();

    String realm = null;
    String username = null;
    String password = null;

    // get realm, username and password from HTTP Form
    final Form f = new Form(entity);
    if (!f.isEmpty()) {
      username = f.getFirstValue("username");
      realm =
          f.getFirstValue("realm", UserSources.guessRealm(username, UserSources.getDefaultRealm()));
      password = f.getFirstValue("password");
    }

    if (username != null && password != null) {
      // try login with these credentials.
      final RealmUsernamePasswordToken t =
          new RealmUsernamePasswordToken(realm, username, password);
      try {
        getUser().login(t);
        final Reference referrerRef = getRequest().getReferrerRef();
        if (referrerRef != null && !"logout".equals(referrerRef.getLastSegment())) {
          getResponse().redirectSeeOther(referrerRef);
        }
        return success(null);

      } catch (final CredentialsException | AccountException e) {
        getLogger().log(Level.INFO, "LOGIN_FAILED", e);
      } catch (final AuthenticationException e) {
        if (CaosDBServer.isDebugMode()) {
          getLogger().log(Level.INFO, "LOGIN_FAILED", e);
        } else {
          getLogger().log(Level.INFO, "LOGIN_FAILED\t" + e.getMessage());
        }
      }
    }

    getResponse().setStatus(Status.CLIENT_ERROR_UNAUTHORIZED);
    return null;
  }

  private JdomRepresentation success(final ToElementable msg) {

    final Document doc = new Document();
    final Element rootElem = generateRootElement();
    doc.setRootElement(rootElem);

    if (msg != null) {
      msg.addToElement(rootElem);
    }

    return ok(doc);
  }
}
