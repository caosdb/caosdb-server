/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.resource;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import org.caosdb.server.CaosDBException;
import org.caosdb.server.database.backend.implementation.MySQL.ConnectionException;
import org.jdom2.Document;
import org.jdom2.Element;
import org.restlet.representation.Representation;

public class DefaultResource extends AbstractCaosDBServerResource {

  private Element responseBody = null;

  public DefaultResource(final Element response) {
    this.responseBody = response;
  }

  public DefaultResource() {}

  @Override
  protected Representation httpGetInChildClass()
      throws ConnectionException,
          IOException,
          SQLException,
          CaosDBException,
          NoSuchAlgorithmException,
          Exception {
    final Document doc = new org.jdom2.Document();
    final Element root = generateRootElement();
    if (this.responseBody != null) {
      root.addContent(this.responseBody);
    }
    doc.setRootElement(root);
    return ok(doc);
  }
}
