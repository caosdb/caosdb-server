/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2021,2023 Timm Fitschen <t.fitschen@indiscale.com>
 * Copyright (C) 2021,2023 IndiScale GmbH <info@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.caosdb.server.resource.transaction;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import org.apache.commons.fileupload.FileItemIterator;
import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.caosdb.server.CaosDBException;
import org.caosdb.server.FileSystem;
import org.caosdb.server.database.backend.implementation.MySQL.ConnectionException;
import org.caosdb.server.entity.DeleteEntity;
import org.caosdb.server.entity.EntityID;
import org.caosdb.server.entity.FileProperties;
import org.caosdb.server.entity.InsertEntity;
import org.caosdb.server.entity.Message;
import org.caosdb.server.entity.UpdateEntity;
import org.caosdb.server.entity.container.WritableContainer;
import org.caosdb.server.transaction.WriteTransaction;
import org.caosdb.server.transaction.WriteTransactionInterface;
import org.caosdb.server.utils.ServerMessages;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.restlet.data.MediaType;
import org.restlet.ext.fileupload.RestletFileUpload;
import org.restlet.representation.Representation;

/**
 * Handles POST, PUT, and DELETE requests for Entities.
 *
 * <p>The GET requests (Retrieval) is handled in the superclass {@link RetrieveEntityResource}.
 *
 * @author Timm Fitschen <t.fitschen@indiscale.com>
 */
public class EntityResource extends RetrieveEntityResource {

  /**
   * Handle entity deletions (DELETE requests).
   *
   * <p>Note: The list of entity "specifier" is treated strictly as a list of entity ids. You cannot
   * delete an entity by name.
   */
  @Override
  protected final Representation httpDeleteInChildClass() throws Exception {

    final WritableContainer container =
        new WritableContainer(getUser(), getTimestamp(), getSRID(), getFlags());
    final Document doc = new Document();

    for (final String item : getRequestedItems()) {
      final String[] elem = item.split("@", 1);
      String id = null;
      String version = null;
      id = elem[0];
      if (elem.length > 1) {
        version = elem[1];
      }

      if (id != null) {
        container.add(new DeleteEntity(new EntityID(id), version));
      }
    }

    final WriteTransactionInterface delete = new WriteTransaction(container);
    delete.execute();

    final Element rootElem = generateRootElement();
    container.addToElement(rootElem);
    doc.setRootElement(rootElem);

    return ok(doc);
  }

  /** Handle entity insertions (POST requests). */
  @Override
  protected final Representation httpPostInChildClass(final Representation entity)
      throws xmlNotWellFormedException, Exception {

    final WritableContainer entityContainer =
        new WritableContainer(getUser(), getTimestamp(), getSRID(), getFlags());

    List<Element> insertEntities;
    if (entity.getMediaType() != null
        && entity.getMediaType().equals(MediaType.MULTIPART_FORM_DATA, true)) {
      insertEntities = parseMultipartEntity(entityContainer).getChildren();
    } else {
      insertEntities = parseEntity(entity).getRootElement().getChildren();
    }

    for (final Element e : insertEntities) {
      entityContainer.add(new InsertEntity(e));
    }

    final WriteTransactionInterface insert = new WriteTransaction(entityContainer);
    insert.execute();

    final Element rootElem = generateRootElement();
    entityContainer.addToElement(rootElem);
    final Document doc = new Document();
    doc.setRootElement(rootElem);

    return ok(doc);
  }

  /**
   * Parse the body of requests with content type "multipart/form-data". This is specific for
   * requests which also contain file blobs.
   */
  private Element parseMultipartEntity(final WritableContainer container)
      throws FileUploadException,
          IOException,
          Message,
          xmlNotWellFormedException,
          NoSuchAlgorithmException,
          CaosDBException {
    final DiskFileItemFactory factory = new DiskFileItemFactory();
    factory.setSizeThreshold(1000240);
    final RestletFileUpload upload = new RestletFileUpload(factory);
    final FileItemIterator iter = upload.getItemIterator(getRequest().getEntity());

    FileItemStream item = iter.next();

    final Element element;
    // First part of the multi-part form data entity must be the xml
    // representation of the files.
    // Name of the form field: FileRepresentation
    if (item.isFormField()) {
      if (item.getFieldName().equals("FileRepresentation")) {
        element = parseEntity(item.openStream()).getRootElement();
      } else {
        throw ServerMessages.NO_FILE_REPRESENTATION_SUBMITTED;
      }
    } else {
      throw ServerMessages.FORM_CONTAINS_UNSUPPORTED_CONTENT;
    }

    // Get files, store to tmp dir.
    while (iter.hasNext()) {
      item = iter.next();
      final FileProperties file = FileSystem.upload(item, getSRID());
      container.addFile(item.getName(), file);
    }

    // transform xml elements to entities and add them to the container.
    return element;
  }

  /** Handle entity updates (PUT requests). */
  @Override
  protected final Representation httpPutInChildClass(final Representation entity)
      throws ConnectionException, JDOMException, Exception, xmlNotWellFormedException {

    final WritableContainer entityContainer =
        new WritableContainer(getUser(), getTimestamp(), getSRID(), getFlags());
    final Document doc = new Document();

    List<Element> updateEntities;
    if (getRequest().getEntity().getMediaType() != null
        && getRequest().getEntity().getMediaType().equals(MediaType.MULTIPART_FORM_DATA, true)) {
      updateEntities = parseMultipartEntity(entityContainer).getChildren();
    } else {
      updateEntities = parseEntity(getRequest().getEntity()).getRootElement().getChildren();
    }

    for (final Element e : updateEntities) {
      entityContainer.add(new UpdateEntity(e));
    }

    final WriteTransactionInterface update = new WriteTransaction(entityContainer);
    update.execute();

    final Element rootElem = generateRootElement();
    entityContainer.addToElement(rootElem);
    doc.setRootElement(rootElem);

    return ok(doc);
  }
}
