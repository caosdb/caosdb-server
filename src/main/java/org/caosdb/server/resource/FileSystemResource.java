/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.resource;

import static java.net.URLDecoder.decode;
import static org.caosdb.server.FileSystem.getFromFileSystem;

import java.io.File;
import java.io.IOException;
import org.caosdb.server.database.backend.implementation.MySQL.ConnectionException;
import org.caosdb.server.database.exceptions.EntityDoesNotExistException;
import org.caosdb.server.database.misc.TransactionBenchmark;
import org.caosdb.server.entity.EntityInterface;
import org.caosdb.server.entity.Message;
import org.caosdb.server.entity.Message.MessageType;
import org.caosdb.server.permissions.EntityPermission;
import org.caosdb.server.transaction.RetrieveSparseEntityByPath;
import org.caosdb.server.utils.FileUtils;
import org.caosdb.server.utils.ServerMessages;
import org.jdom2.Attribute;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.restlet.data.Disposition;
import org.restlet.data.MediaType;
import org.restlet.data.Status;
import org.restlet.representation.FileRepresentation;
import org.restlet.representation.Representation;

/**
 * Download files via GET method from the file system directly without making the detour through the
 * database. Other methods are not supported
 *
 * @author Timm Fitschen
 */
public class FileSystemResource extends AbstractCaosDBServerResource {

  public static Message ORPHANED_FILE_WARNING =
      new Message(
          MessageType.Warning,
          "Orphaned file. The file is not tracked. This is probably a harmless inconsistency but it might be a sign of other problems.");

  /**
   * Download a File from the CaosDBFileSystem. Only one File per Request.
   *
   * @author Timm Fitschen
   * @return InputRepresentation
   * @throws IOException
   */
  @Override
  protected final Representation httpGetInChildClass() throws Exception {
    final long t1 = System.currentTimeMillis();
    final String specifier =
        decode(
            (getRequest().getAttributes().containsKey("path")
                ? (String) getRequest().getAttributes().get("path")
                : ""),
            "UTF-8");

    final Element rootElem = generateRootElement();

    final File file = getFile(specifier);

    if (file == null) {
      return error(ServerMessages.ENTITY_DOES_NOT_EXIST, Status.CLIENT_ERROR_NOT_FOUND);
    }

    if (file.isDirectory()) {
      String path = (specifier.endsWith("/") ? specifier : specifier + "/");
      String url =
          getUtils().getServerRootURI()
              + "/FileSystem"
              + (path.startsWith("/") ? path : ("/" + path));

      final Element folder = new Element("dir");
      folder.setAttribute("path", path);
      folder.setAttribute("name", file.getName());
      folder.setAttribute("url", url);

      final boolean thumbnailsExist =
          new File(file.getAbsolutePath() + File.separator + ".thumbnails").exists();
      for (final File child : file.listFiles()) {
        Element celem = null;
        if (child.isDirectory()) {
          if (child.getName().equals(".thumbnails")) {
            continue;
          }
          celem = new Element("dir");
          celem.setAttribute("name", child.getName() + "/");
        } else {
          celem = getFileElement(specifier, child);
        }

        if (thumbnailsExist) {
          final Attribute thumbnailAttribute = getThumbnailAttribute(file, child, url);
          if (thumbnailAttribute != null) {
            celem.setAttribute(thumbnailAttribute);
          }
        }

        folder.addContent(celem);
      }
      rootElem.addContent(folder);
      final long t2 = System.currentTimeMillis();

      getBenchmark().addMeasurement(this.getClass().getSimpleName(), t2 - t1);
      rootElem.addContent(getBenchmark().toElement());
      final Document doc = new Document(rootElem);
      return ok(doc);

    } else {

      try {
        getEntity(specifier).checkPermission(EntityPermission.RETRIEVE_FILE);
      } catch (EntityDoesNotExistException exception) {
        // This file in the file system has no corresponding File record. It
        // shall not be retrieved by anyone.
        return error(ServerMessages.AUTHORIZATION_ERROR, Status.CLIENT_ERROR_FORBIDDEN);
      }

      final MediaType mt = MediaType.valueOf(FileUtils.getMimeType(file));
      final FileRepresentation ret = new FileRepresentation(file, mt);
      ret.setDisposition(new Disposition(Disposition.TYPE_ATTACHMENT));

      return ret;
    }
  }

  TransactionBenchmark getBenchmark() {
    return TransactionBenchmark.getRootInstance().getBenchmark(getClass());
  }

  protected Attribute getThumbnailAttribute(
      final File file, final File child, final String referenceString) {
    final String thpath =
        file.getAbsolutePath() + File.separator + ".thumbnails" + File.separator + child.getName();

    final File th = new File(thpath);
    if (th.exists()) {
      return new Attribute(
          "thumbnail", referenceString.replaceFirst("FileSystem", "Thumbnails") + child.getName());
    }
    return null;
  }

  /**
   * Return an element for the given file on a file system.
   *
   * <p>If there is no File entity for this file, an element without `id` is returned, instead a
   * corresponding message is added to the element.
   */
  Element getFileElement(final String directory, final File file) throws Exception {
    final Element celem = new Element("file");
    celem.setAttribute("name", file.getName());

    try {
      final String entId =
          getEntityID((directory.endsWith("/") ? directory : directory + "/") + file.getName());
      celem.setAttribute("id", entId);
    } catch (EntityDoesNotExistException exception) {
      // This file in the file system has no corresponding File record.
      ORPHANED_FILE_WARNING.addToElement(celem);
    }
    return celem;
  }

  protected String getEntityID(final String path) throws Exception {
    final EntityInterface fileEnt = getEntity(path);
    return fileEnt.getId().toString();
  }

  /**
   * Throws EntityDoesNotExistException when there is not entity with that path.
   *
   * @param path
   * @return
   * @throws Exception
   */
  private EntityInterface getEntity(final String path) throws Exception {
    final long t1 = System.currentTimeMillis();
    final EntityInterface e;
    final RetrieveSparseEntityByPath t = new RetrieveSparseEntityByPath(path);
    t.execute();
    e = t.getEntity();
    final long t2 = System.currentTimeMillis();
    getBenchmark().addMeasurement(this.getClass().getSimpleName() + ".getEntity", t2 - t1);
    return e;
  }

  protected File getFile(final String path) throws Exception {
    final File ret = getFromFileSystem(path);
    return ret;
  }

  @Override
  protected Representation httpPostInChildClass(final Representation entity)
      throws ConnectionException, JDOMException {
    this.setStatus(org.restlet.data.Status.CLIENT_ERROR_METHOD_NOT_ALLOWED);
    return null;
  }
}
