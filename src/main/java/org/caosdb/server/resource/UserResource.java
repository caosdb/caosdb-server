/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2023 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2023 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.caosdb.server.resource;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import org.caosdb.server.CaosDBException;
import org.caosdb.server.CaosDBServer;
import org.caosdb.server.ServerProperties;
import org.caosdb.server.accessControl.ACMPermissions;
import org.caosdb.server.accessControl.UserSources;
import org.caosdb.server.accessControl.UserStatus;
import org.caosdb.server.database.backend.implementation.MySQL.ConnectionException;
import org.caosdb.server.entity.Message;
import org.caosdb.server.transaction.DeleteUserTransaction;
import org.caosdb.server.transaction.InsertUserTransaction;
import org.caosdb.server.transaction.RetrieveUserTransaction;
import org.caosdb.server.transaction.UpdateUserTransaction;
import org.caosdb.server.utils.ServerMessages;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.restlet.data.Form;
import org.restlet.data.Status;
import org.restlet.representation.Representation;

/**
 * This class handles requests for Users.
 *
 * @author Timm Fitschen <t.fitschen@indiscale.com>
 */
public class UserResource extends AbstractCaosDBServerResource {

  @Override
  protected Representation httpGetInChildClass()
      throws ConnectionException,
          IOException,
          SQLException,
          CaosDBException,
          NoSuchAlgorithmException,
          Exception {

    final Document doc = new Document();
    final Element rootElem = generateRootElement();

    if (getRequestedItems().length > 0) {
      try {
        final String username = getRequestedItems()[0];
        final String realm =
            (getRequestAttributes().containsKey("realm")
                ? (String) getRequestAttributes().get("realm")
                : UserSources.guessRealm(username, UserSources.getDefaultRealm()));

        getUser().checkPermission(ACMPermissions.PERMISSION_RETRIEVE_USER_INFO(realm, username));

        final RetrieveUserTransaction t = new RetrieveUserTransaction(realm, username);
        t.execute();

        rootElem.addContent(t.getUserElement());
      } catch (final Message m) {
        if (m == ServerMessages.ACCOUNT_DOES_NOT_EXIST) {
          return error(ServerMessages.ACCOUNT_DOES_NOT_EXIST, Status.CLIENT_ERROR_NOT_FOUND);
        }
        throw m;
      }
    }

    doc.setRootElement(rootElem);
    return ok(doc);
  }

  @Override
  protected Representation httpPutInChildClass(final Representation entity)
      throws ConnectionException, JDOMException, Exception, xmlNotWellFormedException {

    try {
      final Form form = new Form(entity);
      final String username = getRequestedItems()[0];
      final String realm =
          (getRequestAttributes().containsKey("realm")
              ? (String) getRequestAttributes().get("realm")
              : UserSources.guessRealm(username));
      final String password = form.getFirstValue("password");
      final String email = form.getFirstValue("email");

      final UserStatus status =
          form.getFirstValue("status") != null
              ? UserStatus.valueOf(form.getFirstValue("status").toUpperCase())
              : null;
      String userEntity = null;
      if (form.getFirst("entity") != null) {
        if (form.getFirstValue("entity").isEmpty()) {
          userEntity = "";
        } else {
          userEntity = form.getFirstValue("entity");
        }
      }

      final UpdateUserTransaction t =
          new UpdateUserTransaction(realm, username, status, email, userEntity, password);
      t.execute();

      final Document doc = new Document();
      final Element rootElem = generateRootElement();

      rootElem.addContent(t.getUserElement());
      doc.setRootElement(rootElem);
      return ok(doc);
    } catch (final Message m) {
      if (m == ServerMessages.ACCOUNT_DOES_NOT_EXIST) {
        return error(m, Status.CLIENT_ERROR_NOT_FOUND);
      } else if (m == ServerMessages.ENTITY_DOES_NOT_EXIST) {
        return error(m, Status.CLIENT_ERROR_CONFLICT);
      } else {
        return error(m, Status.CLIENT_ERROR_BAD_REQUEST);
      }
    } catch (final NumberFormatException e) {
      return error(ServerMessages.CANNOT_PARSE_INT_VALUE, Status.CLIENT_ERROR_BAD_REQUEST);
    }
  }

  @Override
  protected Representation httpPostInChildClass(final Representation entity)
      throws ConnectionException, JDOMException, Exception, xmlNotWellFormedException {

    final Form form = new Form(entity);
    final String username = form.getFirstValue("username");
    final String password = form.getFirstValue("password");
    final String email = form.getFirstValue("email");
    final UserStatus status =
        UserStatus.valueOf(
            form.getFirstValue(
                    "status",
                    CaosDBServer.getServerProperty(ServerProperties.KEY_NEW_USER_DEFAULT_ACTIVITY))
                .toUpperCase());
    String userEntity = null;
    if (form.getFirst("entity") != null) {
      userEntity = form.getFirstValue("entity");
    }

    final InsertUserTransaction t =
        new InsertUserTransaction(username, password, email, status, userEntity);
    try {
      t.execute();
    } catch (final Message m) {
      if (m == ServerMessages.ACCOUNT_NAME_NOT_UNIQUE) {
        return error(m, Status.CLIENT_ERROR_CONFLICT);
      } else {
        return error(m, Status.CLIENT_ERROR_BAD_REQUEST);
      }
    }

    final Document doc = new Document();
    final Element rootElem = generateRootElement();

    rootElem.addContent(t.getUserElement());
    doc.setRootElement(rootElem);
    return ok(doc);
  }

  @Override
  protected Representation httpDeleteInChildClass()
      throws ConnectionException,
          SQLException,
          CaosDBException,
          IOException,
          NoSuchAlgorithmException,
          Exception {

    final Document doc = new Document();
    final Element rootElem = generateRootElement();

    final String username = getRequestedItems()[0];
    final DeleteUserTransaction t = new DeleteUserTransaction(username);
    try {
      t.execute();
    } catch (final Message m) {
      if (m == ServerMessages.ACCOUNT_DOES_NOT_EXIST) {
        return error(m, Status.CLIENT_ERROR_NOT_FOUND);
      }
      throw m;
    }
    rootElem.addContent(t.getUserElement());

    doc.setRootElement(rootElem);
    return ok(doc);
  }
}
