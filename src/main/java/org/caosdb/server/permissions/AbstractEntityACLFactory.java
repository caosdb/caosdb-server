/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2019-2021 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2019-2021 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.caosdb.server.permissions;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

public abstract class AbstractEntityACLFactory<T extends EntityACL> {

  private final Map<ResponsibleAgent, Long> normalGrants = new HashMap<>();
  private final Map<ResponsibleAgent, Long> priorityGrants = new HashMap<>();
  private final Map<ResponsibleAgent, Long> normalDenials = new HashMap<>();
  private final Map<ResponsibleAgent, Long> priorityDenials = new HashMap<>();

  public void grant(final ResponsibleAgent role, final int... permissionBitNumber) {
    grant(role, false, permissionBitNumber);
  }

  public void deny(final ResponsibleAgent role, final int... permissionBitNumber) {
    deny(role, false, permissionBitNumber);
  }

  public void grant(
      final ResponsibleAgent role, final boolean priority, final int... permissionBitNumber) {
    for (final int i : permissionBitNumber) {
      grant(role, priority, EntityPermission.getEntityPermission(i));
    }
  }

  public void deny(
      final ResponsibleAgent role, final boolean priority, final int... permissionBitNumber) {
    for (final int i : permissionBitNumber) {
      deny(role, priority, EntityPermission.getEntityPermission(i));
    }
  }

  public void grant(final ResponsibleAgent role, final String... permission) {
    grant(role, false, permission);
  }

  public void deny(final ResponsibleAgent role, final String... permission) {
    deny(role, false, permission);
  }

  public void grant(
      final ResponsibleAgent role, final boolean priority, final String... permission) {
    for (final String s : permission) {
      if (s.contains("*")) {
        grant(role, priority, EntityPermission.getPermissionsPerWildCard(s));
      } else {
        grant(role, priority, EntityPermission.getEntityPermission(s));
      }
    }
  }

  public void deny(
      final ResponsibleAgent role, final boolean priority, final String... permission) {
    for (final String s : permission) {
      if (s.contains("*")) {
        deny(role, priority, EntityPermission.getPermissionsPerWildCard(s));
      } else {
        deny(role, priority, EntityPermission.getEntityPermission(s));
      }
    }
  }

  public void deny(
      final ResponsibleAgent role,
      final boolean priority,
      final Collection<EntityPermission> permission) {
    for (final EntityPermission p : permission) {
      deny(role, priority, p);
    }
  }

  public void grant(
      final ResponsibleAgent role,
      final boolean priority,
      final Collection<EntityPermission> permission) {
    for (final EntityPermission p : permission) {
      grant(role, priority, p);
    }
  }

  public void grant(
      final ResponsibleAgent role, final boolean priority, final EntityPermission... permission) {
    if (priority) {
      addACI(this.priorityGrants, role, permission);
    } else {
      addACI(this.normalGrants, role, permission);
    }
  }

  public void deny(
      final ResponsibleAgent role, final boolean priority, final EntityPermission... permission) {
    if (priority) {
      addACI(this.priorityDenials, role, permission);
    } else {
      addACI(this.normalDenials, role, permission);
    }
  }

  private static void addACI(
      final Map<ResponsibleAgent, Long> map,
      final ResponsibleAgent role,
      final EntityPermission permission) {
    long bitSet = permission.getBitSet();
    try {
      bitSet |= map.get(role);
    } catch (final NullPointerException e) {
      // do nothing
    }
    map.put(role, bitSet);
  }

  private static void addACI(
      final Map<ResponsibleAgent, Long> map,
      final ResponsibleAgent role,
      final EntityPermission[] permission) {
    for (final EntityPermission p : permission) {
      addACI(map, role, p);
    }
  }

  private EntityACI[] toEntityACIArray(
      final Map<ResponsibleAgent, Long> map, final long modBitSet) {
    final EntityACI[] ret = new EntityACI[map.size()];
    int i = 0;
    for (final Entry<ResponsibleAgent, Long> e : map.entrySet()) {
      ret[i++] = new EntityACI(e.getKey(), e.getValue() | modBitSet);
    }
    return ret;
  }

  public T create() {
    normalize();
    final ArrayList<EntityACI> acis = new ArrayList<>();
    Collections.addAll(acis, toEntityACIArray(this.normalGrants, 0));
    Collections.addAll(acis, toEntityACIArray(this.normalDenials, Long.MIN_VALUE));
    Collections.addAll(acis, toEntityACIArray(this.priorityGrants, EntityACL.MIN_PRIORITY_BITSET));
    Collections.addAll(
        acis,
        toEntityACIArray(this.priorityDenials, Long.MIN_VALUE | EntityACL.MIN_PRIORITY_BITSET));
    return create(acis);
  }

  /**
   * Normalize the permission rules.
   *
   * <p>This means that rules which are overriden by other rules are removed. E.g. a granting rule
   * for the permission X and the agent P would be removed if there is a denial of X (for P) with
   * the same or a higher priority. Likewise, A denial of Y for agent Q would be removed if there is
   * a granting rule of Y (for Q) with a higher priority.
   */
  private void normalize() {
    // 1. run through all prioritized denials and remove overriden rules
    // (priority grants, normal grants and normal denials)
    Iterator<Entry<ResponsibleAgent, Long>> iterator = this.priorityDenials.entrySet().iterator();
    while (iterator.hasNext()) {
      Entry<ResponsibleAgent, Long> next = iterator.next();
      final ResponsibleAgent agent = next.getKey();
      long bitset = next.getValue();
      if (bitset == 0L) {
        iterator.remove();
        continue;
      }
      if (this.priorityGrants.containsKey(agent)) {
        this.priorityGrants.put(agent, this.priorityGrants.get(agent) & ~bitset);
      }
      if (this.normalDenials.containsKey(agent)) {
        this.normalDenials.put(agent, this.normalDenials.get(agent) & ~bitset);
      }
      if (this.normalGrants.containsKey(agent)) {
        this.normalGrants.put(agent, this.normalGrants.get(agent) & ~bitset);
      }
    }
    // 2. run through all prioritized grants and remove overriden rules (normal
    // denials and grants)
    iterator = this.priorityGrants.entrySet().iterator();
    while (iterator.hasNext()) {
      Entry<ResponsibleAgent, Long> next = iterator.next();
      final ResponsibleAgent agent = next.getKey();
      long bitset = next.getValue();
      if (bitset == 0L) {
        iterator.remove();
        continue;
      }
      if (this.normalDenials.containsKey(agent)) {
        this.normalDenials.put(agent, this.normalDenials.get(agent) & ~bitset);
      }
      if (this.normalGrants.containsKey(agent)) {
        this.normalGrants.put(agent, this.normalGrants.get(agent) & ~bitset);
      }
    }
    // 3. run through all normal denials and remove overriden rules (normal grants)
    iterator = this.normalDenials.entrySet().iterator();
    while (iterator.hasNext()) {
      Entry<ResponsibleAgent, Long> next = iterator.next();
      final ResponsibleAgent agent = next.getKey();
      long bitset = next.getValue();
      if (bitset == 0L) {
        iterator.remove();
        continue;
      }
      if (this.normalGrants.containsKey(agent)) {
        this.normalGrants.put(agent, this.normalGrants.get(agent) & ~bitset);
      }
    }
    // finally, remove all remaining empty grants
    iterator = this.normalGrants.entrySet().iterator();
    while (iterator.hasNext()) {
      Entry<ResponsibleAgent, Long> next = iterator.next();
      long bitset = next.getValue();
      if (bitset == 0L) {
        iterator.remove();
      }
    }
  }

  public void clear() {
    this.normalGrants.clear();
    this.normalDenials.clear();
    this.priorityGrants.clear();
    this.priorityDenials.clear();
  }

  protected abstract T create(Collection<EntityACI> acis);

  /**
   * Remove all rules of the `other` EntityACL from this factory.
   *
   * <p>This is mainly used for removing all rules which belong to the global entity ACL from this
   * ACL before storing it to the backend.
   *
   * @param other
   * @return the same object with changed rule set.
   */
  public AbstractEntityACLFactory<T> remove(EntityACL other) {
    if (other != null) {
      normalize();
      for (EntityACI aci : other.getRules()) {
        if (EntityACL.isAllowance(aci.getBitSet())) {
          if (EntityACL.isPriorityBitSet(aci.getBitSet())) {
            Long bitset = this.priorityGrants.get(aci.getResponsibleAgent());
            if (bitset == null) {
              continue;
            }
            long bitset2 = bitset;
            bitset2 &= aci.getBitSet();
            bitset ^= bitset2;
            this.priorityGrants.put(aci.getResponsibleAgent(), bitset);
          } else {
            Long bitset = this.normalGrants.get(aci.getResponsibleAgent());
            if (bitset == null) {
              continue;
            }
            long bitset2 = bitset;
            bitset2 &= aci.getBitSet();
            bitset ^= bitset2;
            this.normalGrants.put(aci.getResponsibleAgent(), bitset);
          }
        } else {
          if (EntityACL.isPriorityBitSet(aci.getBitSet())) {
            Long bitset = this.priorityDenials.get(aci.getResponsibleAgent());
            if (bitset == null) {
              continue;
            }
            long bitset2 = bitset;
            bitset2 &= aci.getBitSet();
            bitset ^= bitset2;
            this.priorityDenials.put(aci.getResponsibleAgent(), bitset);
          } else {
            Long bitset = this.normalDenials.get(aci.getResponsibleAgent());
            if (bitset == null) {
              continue;
            }
            long bitset2 = bitset;
            bitset2 &= aci.getBitSet();
            bitset ^= bitset2;
            this.normalDenials.put(aci.getResponsibleAgent(), bitset);
          }
        }
      }
    }
    return this;
  }
}
