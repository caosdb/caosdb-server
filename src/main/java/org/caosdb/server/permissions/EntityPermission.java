/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.permissions;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.caosdb.server.CaosDBException;
import org.caosdb.server.entity.xml.ToElementable;
import org.jdom2.Element;

public class EntityPermission extends Permission {

  private static final long serialVersionUID = -8935713878537140286L;
  private static List<EntityPermission> instances = new ArrayList<>();
  private final int bitNumber;
  private final org.caosdb.api.entity.v1.EntityPermission mapping;
  public static final Permission EDIT_PRIORITY_ACL =
      new Permission(
          "ADMIN:ENTITY:EDIT:PRIORITY_ACL",
          "The permission to edit (add/delete) the prioritized rules of an acl of an entity.");

  public static ToElementable getAllEntityPermissions() {
    final Element entityPermissionsElement = new Element("EntityPermissions");
    for (final EntityPermission e : instances) {
      final Element element = e.toElement();
      element.setAttribute("description", e.getDescription());
      entityPermissionsElement.addContent(element);
    }

    entityPermissionsElement.addContent(EntityACL.GLOBAL_PERMISSIONS.toElement());

    return new ToElementable() {

      @Override
      public void addToElement(final Element ret) {
        ret.addContent(entityPermissionsElement);
      }
    };
  }

  private EntityPermission(
      final String shortName,
      final String description,
      final int bitNumber,
      org.caosdb.api.entity.v1.EntityPermission mapping) {
    super(shortName, description);
    this.bitNumber = bitNumber;
    this.mapping = mapping;
    if (bitNumber > 61) {
      throw new CaosDBException(
          "This bitNumber is too big. This implementation only handles bitNumbers up to 61.");
    }
    if (instances.contains(this)) {
      throw new CaosDBException("This EntityPermission is defined already.");
    } else {
      instances.add(this);
    }
  }

  public static Set<EntityPermission> getPermissionsPerWildCard(final String s) {
    final Pattern pattern = Pattern.compile(s.replaceAll("\\*", ".*"));

    final Set<EntityPermission> ret = new HashSet<>();
    for (final EntityPermission p : instances) {
      final Matcher m = pattern.matcher(p.getShortName());
      if (m.matches()) {
        ret.add(p);
      }
    }
    return ret;
  }

  public static EntityPermission getEntityPermission(final String name) {
    for (final EntityPermission p : instances) {
      if (p.getShortName().equals(name)) {
        return p;
      }
    }
    throw new IllegalArgumentException("Permission is not defined.");
  }

  public static EntityPermission getEntityPermission(final int bitNumber) {
    for (final EntityPermission p : instances) {
      if (p.getBitNumber() == bitNumber) {
        return p;
      }
    }
    throw new IllegalArgumentException("Permission is not defined.");
  }

  public static EntityPermission getEntityPermission(
      final org.caosdb.api.entity.v1.EntityPermission permission) {
    for (final EntityPermission p : instances) {
      if (p.getMapping() == permission) {
        return p;
      }
    }
    throw new IllegalArgumentException("Permission not found." + permission.name());
  }

  public long getBitSet() {
    return (long) Math.pow(2, getBitNumber());
  }

  public int getBitNumber() {
    return this.bitNumber;
  }

  public Element toElement() {
    final Element ret = new Element("Permission");
    ret.setAttribute("name", getShortName());
    return ret;
  }

  public org.caosdb.api.entity.v1.EntityPermission getMapping() {
    return mapping;
  }

  // Note: Please remember to transfer all changes to the following entity
  // permissions to src/doc/entity-permission-table.rst.
  public static final EntityPermission RETRIEVE_ENTITY =
      new EntityPermission(
          "RETRIEVE:ENTITY",
          "Permission to retrieve the full entity (name, description, data type, ...) with all parents and properties (unless prohibited by another rule on the property level).",
          4,
          org.caosdb.api.entity.v1.EntityPermission.ENTITY_PERMISSION_RETRIEVE_ENTITY);
  public static final EntityPermission RETRIEVE_ACL =
      new EntityPermission(
          "RETRIEVE:ACL",
          "Permission to retrieve the full and final ACL of this entity.",
          5,
          org.caosdb.api.entity.v1.EntityPermission.ENTITY_PERMISSION_RETRIEVE_ACL);
  public static final EntityPermission RETRIEVE_HISTORY =
      new EntityPermission(
          "RETRIEVE:HISTORY",
          "Permission to retrieve the history of this entity.",
          6,
          org.caosdb.api.entity.v1.EntityPermission.ENTITY_PERMISSION_RETRIEVE_HISTORY);
  public static final EntityPermission RETRIEVE_OWNER =
      new EntityPermission(
          "RETRIEVE:OWNER",
          "Permission to retrieve the owner(s) of this entity.",
          9,
          org.caosdb.api.entity.v1.EntityPermission.ENTITY_PERMISSION_RETRIEVE_OWNER);
  public static final EntityPermission RETRIEVE_FILE =
      new EntityPermission(
          "RETRIEVE:FILE",
          "Permission to download the file belonging to this entity.",
          10,
          org.caosdb.api.entity.v1.EntityPermission.ENTITY_PERMISSION_RETRIEVE_FILE);
  public static final EntityPermission DELETE =
      new EntityPermission(
          "DELETE",
          "Permission to delete this entity.",
          1,
          org.caosdb.api.entity.v1.EntityPermission.ENTITY_PERMISSION_DELETE);
  public static final EntityPermission EDIT_ACL =
      new EntityPermission(
          "EDIT:ACL",
          "Permission to change the user-specified part of this entity's ACL. Roles with this Permission are called 'Owners'.",
          0,
          org.caosdb.api.entity.v1.EntityPermission.ENTITY_PERMISSION_EDIT_ACL);
  public static final EntityPermission UPDATE_DESCRIPTION =
      new EntityPermission(
          "UPDATE:DESCRIPTION",
          "Permission to change the value of this entity.",
          11,
          org.caosdb.api.entity.v1.EntityPermission.ENTITY_PERMISSION_UPDATE_DESCRIPTION);
  public static final EntityPermission UPDATE_VALUE =
      new EntityPermission(
          "UPDATE:VALUE",
          "Permission to change the value of this entity.",
          12,
          org.caosdb.api.entity.v1.EntityPermission.ENTITY_PERMISSION_UPDATE_VALUE);
  public static final EntityPermission UPDATE_ROLE =
      new EntityPermission(
          "UPDATE:ROLE",
          "Permission to change the role of this entity.",
          13,
          org.caosdb.api.entity.v1.EntityPermission.ENTITY_PERMISSION_UPDATE_ROLE);
  public static final EntityPermission UPDATE_REMOVE_PARENT =
      new EntityPermission(
          "UPDATE:PARENT:REMOVE",
          "Permission  to remove parents from this entity.",
          14,
          org.caosdb.api.entity.v1.EntityPermission.ENTITY_PERMISSION_UPDATE_REMOVE_PARENT);
  public static final EntityPermission UPDATE_ADD_PARENT =
      new EntityPermission(
          "UPDATE:PARENT:ADD",
          "Permission to add a parent to this entity.",
          15,
          org.caosdb.api.entity.v1.EntityPermission.ENTITY_PERMISSION_UPDATE_ADD_PARENT);
  public static final EntityPermission UPDATE_REMOVE_PROPERTY =
      new EntityPermission(
          "UPDATE:PROPERTY:REMOVE",
          "Permission to remove properties from this entity.",
          16,
          org.caosdb.api.entity.v1.EntityPermission.ENTITY_PERMISSION_UPDATE_REMOVE_PROPERTY);
  public static final EntityPermission UPDATE_ADD_PROPERTY =
      new EntityPermission(
          "UPDATE:PROPERTY:ADD",
          "Permission to add a property to this entity.",
          17,
          org.caosdb.api.entity.v1.EntityPermission.ENTITY_PERMISSION_UPDATE_ADD_PROPERTY);
  public static final EntityPermission UPDATE_NAME =
      new EntityPermission(
          "UPDATE:NAME",
          "Permission to change the name of this entity.",
          19,
          org.caosdb.api.entity.v1.EntityPermission.ENTITY_PERMISSION_UPDATE_NAME);
  public static final EntityPermission UPDATE_DATA_TYPE =
      new EntityPermission(
          "UPDATE:DATA_TYPE",
          "Permission to change the data type of this entity.",
          20,
          org.caosdb.api.entity.v1.EntityPermission.ENTITY_PERMISSION_UPDATE_DATA_TYPE);
  public static final EntityPermission UPDATE_REMOVE_FILE =
      new EntityPermission(
          "UPDATE:FILE:REMOVE",
          "Permission to delete the file of this entity.",
          21,
          org.caosdb.api.entity.v1.EntityPermission.ENTITY_PERMISSION_UPDATE_REMOVE_FILE);
  public static final EntityPermission UPDATE_ADD_FILE =
      new EntityPermission(
          "UPDATE:FILE:ADD",
          "Permission to set a file for this entity.",
          22,
          org.caosdb.api.entity.v1.EntityPermission.ENTITY_PERMISSION_UPDATE_ADD_FILE);
  public static final EntityPermission UPDATE_MOVE_FILE =
      new EntityPermission(
          "UPDATE:FILE:MOVE",
          "Permission to move an existing file to a new location.",
          23,
          org.caosdb.api.entity.v1.EntityPermission.ENTITY_PERMISSION_UPDATE_MOVE_FILE);
  public static final EntityPermission USE_AS_REFERENCE =
      new EntityPermission(
          "USE:AS_REFERENCE",
          "Permission to refer to this entity via a reference property.",
          24,
          org.caosdb.api.entity.v1.EntityPermission.ENTITY_PERMISSION_USE_AS_REFERENCE);
  public static final EntityPermission USE_AS_PROPERTY =
      new EntityPermission(
          "USE:AS_PROPERTY",
          "Permission to implement this entity as a property.",
          25,
          org.caosdb.api.entity.v1.EntityPermission.ENTITY_PERMISSION_USE_AS_PROPERTY);
  public static final EntityPermission USE_AS_PARENT =
      new EntityPermission(
          "USE:AS_PARENT",
          "Permission to use this entity as a super type for other entities.",
          26,
          org.caosdb.api.entity.v1.EntityPermission.ENTITY_PERMISSION_USE_AS_PARENT);
  public static final EntityPermission USE_AS_DATA_TYPE =
      new EntityPermission(
          "USE:AS_DATA_TYPE",
          "Permission to use this entity as a data type for reference properties.",
          27,
          org.caosdb.api.entity.v1.EntityPermission.ENTITY_PERMISSION_USE_AS_DATA_TYPE);
  public static final EntityPermission UPDATE_QUERY_TEMPLATE_DEFINITION =
      new EntityPermission(
          "UPDATE:QUERY_TEMPLATE_DEFINITION",
          "Permission to update the query template definition of this QueryTemplate",
          28,
          org.caosdb.api.entity.v1.EntityPermission
              .ENTITY_PERMISSION_UPDATE_QUERY_TEMPLATE_DEFINITION);
}
