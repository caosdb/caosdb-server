/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2021 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2021 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.caosdb.server.permissions;

import java.util.HashMap;
import org.jdom2.Attribute;
import org.jdom2.Element;

public class Role implements ResponsibleAgent {

  public static final Role OWNER_ROLE = new Role("?OWNER?");
  public static final Role OTHER_ROLE = new Role("?OTHER?");
  public static final Role ANONYMOUS_ROLE = new Role("anonymous");
  public static final Role ADMINISTRATION = new Role("administration");

  private final String role;

  private Role(final String role) {
    this.role = role;
  }

  @Override
  public void addToElement(final Element e) {
    e.setAttribute(toAttribute());
  }

  private Attribute toAttribute() {
    return new Attribute("role", this.role);
  }

  @Override
  public String toString() {
    return this.role;
  }

  @Override
  public boolean equals(final Object obj) {
    return obj instanceof Role && this.role.equals(obj.toString());
  }

  @Override
  public int hashCode() {
    return toString().hashCode();
  }

  public static Role create(final String role) {
    if (OWNER_ROLE.toString().equals(role)) {
      return OWNER_ROLE;
    } else if (OTHER_ROLE.toString().equals(role)) {
      return OTHER_ROLE;
    } else {
      return new Role(role);
    }
  }

  @Override
  public void addToMap(final HashMap<String, Object> map) {
    map.put("role", this.role);
  }
}
