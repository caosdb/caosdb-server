/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2021 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2021 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.caosdb.server.permissions;

import static org.caosdb.server.permissions.Role.OTHER_ROLE;
import static org.caosdb.server.permissions.Role.OWNER_ROLE;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.shiro.subject.Subject;
import org.caosdb.server.CaosDBServer;
import org.caosdb.server.ServerProperties;
import org.caosdb.server.accessControl.AuthenticationUtils;
import org.caosdb.server.accessControl.Principal;
import org.caosdb.server.database.exceptions.TransactionException;
import org.eclipse.jetty.util.ajax.JSON;
import org.jdom2.DataConversionException;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;

public class EntityACL {

  public static final EntityACL GLOBAL_PERMISSIONS = loadGlobalPermissions();
  public static final long MIN_PRIORITY_BITSET = 4611686018427387904L;
  public static final long OWNER_BITSET = 1;
  private final Collection<EntityACI> acl;

  public EntityACL(final Collection<EntityACI> acl) {
    if (acl != null) {
      this.acl = acl;
    } else {
      this.acl = new ArrayList<>();
    }
  }

  private static EntityACL loadGlobalPermissions() {
    SAXBuilder saxBuilder = new SAXBuilder();
    File file =
        new File(
            CaosDBServer.getServerProperty(ServerProperties.KEY_GLOBAL_ENTITY_PERMISSIONS_FILE)
                .trim());

    try {
      Document doc = saxBuilder.build(file);
      Element root = doc.getRootElement();
      return parseFromElement(root);
    } catch (JDOMException e) {
      e.printStackTrace();
      System.exit(1);
    } catch (IOException e) {
      e.printStackTrace();
      System.exit(1);
    }
    return null;
  }

  public EntityACL(final EntityACI... aci) {
    this.acl = new ArrayList<>();
    for (final EntityACI a : aci) {
      this.acl.add(a);
    }
  }

  public static final EntityACL getOwnerACLFor(final Subject subject) {
    // TODO handle the case where a valid non-guest user (e.g. me@PAM,
    // you@CaosDB) is (just temporarily) authenticated via a
    // OneTimeAuthenticationToken
    if (AuthenticationUtils.isAnonymous(subject)
        || AuthenticationUtils.isFromOneTimeTokenRealm(subject)) {
      return new EntityACLFactory().create();
    }
    return getOwnerACLFor((Principal) subject.getPrincipal());
  }

  public static final EntityACL getOwnerACLFor(final ResponsibleAgent agent) {
    final EntityACLFactory f = new EntityACLFactory();
    f.grant(agent, "*");
    return f.create();
  }

  public static final Set<EntityPermission> getPermissionsFromBitSet(final long bitSet) {
    final Set<EntityPermission> ret = new HashSet<>();
    final boolean[] ba = convertToArray(bitSet);
    for (int i = 0; i < 62; i++) {
      if (ba[i]) {
        ret.add(EntityPermission.getEntityPermission(i));
      }
    }
    return ret;
  }

  public boolean isPermitted(
      final Subject subject, final org.apache.shiro.authz.Permission permission) {
    if (subject.isPermitted(permission)) {
      return true;
    }
    if (permission instanceof EntityPermission) {
      final List<EntityACI> localAcl = new ArrayList<>(this.acl);
      localAcl.addAll(GLOBAL_PERMISSIONS.acl);
      final Set<EntityPermission> permissions = getPermissionsFor(subject, localAcl);
      return permissions.contains(permission);
    }
    return false;
  }

  public static final Set<EntityPermission> getPermissionsFor(
      final Subject subject, final Collection<EntityACI> entityACL) {
    final List<Long> acl = new ArrayList<>();
    final List<ResponsibleAgent> owners = getOwners(entityACL);
    final List<Long> forOthers = new ArrayList<>();
    for (final EntityACI aci : entityACL) {
      if (aci.getResponsibleAgent().equals(OWNER_ROLE) && subjectIsOwner(subject, owners)) {
        acl.add(aci.getBitSet());
        break;
      }
      if (subjectHasRole(subject, aci.getResponsibleAgent())) {
        acl.add(aci.getBitSet());
      } else if (subjectHasPrincipal(subject, aci.getResponsibleAgent())) {
        acl.add(aci.getBitSet());
      } else if (aci.getResponsibleAgent().equals(OTHER_ROLE)) {
        forOthers.add(aci.getBitSet());
      }
    }
    if (acl.isEmpty()) {
      // There haven't been any rules which apply to this subject. Thus the rules for ?OTHER? apply.
      acl.addAll(forOthers);
    }
    return getPermissionsFromBitSet(getResultingACL(acl));
  }

  private static boolean subjectHasPrincipal(
      final Subject subject, final ResponsibleAgent responsibleAgent) {
    if (responsibleAgent instanceof Principal) {
      return responsibleAgent.equals(subject.getPrincipals().getPrimaryPrincipal());
    }
    return false;
  }

  private static boolean subjectHasRole(
      final Subject subject, final ResponsibleAgent responsibleAgent) {
    if (responsibleAgent instanceof Role) {
      return subject.hasRole(responsibleAgent.toString());
    }
    return false;
  }

  private static boolean subjectIsOwner(
      final Subject subject, final List<ResponsibleAgent> owners) {
    for (final ResponsibleAgent owner : owners) {
      if ((owner instanceof Role && subject.hasRole(owner.toString()))
          || (owner instanceof Principal && subject.getPrincipal().equals(owner))) {
        return true;
      }
    }
    return false;
  }

  public List<ResponsibleAgent> getOwners() {
    return getOwners(this.acl);
  }

  public static final List<ResponsibleAgent> getOwners(final Collection<EntityACI> acl) {
    final List<ResponsibleAgent> owners = new ArrayList<>();
    for (final EntityACI aci : acl) {
      if (aci.isGrant()
          && isOwnerBitSet(aci.getBitSet())
          && !aci.getResponsibleAgent().equals(OWNER_ROLE)) {
        owners.add(aci.getResponsibleAgent());
      }
    }
    return owners;
  }

  public static final boolean isOwnerBitSet(final long bitSet) {
    return (bitSet & OWNER_BITSET) != 0;
  }

  public static final long getResultingACL(final Collection<Long> acl) {
    long allowance = 0;
    long denial = Long.MIN_VALUE;
    long priorityAllowance = 0;
    long priorityDenial = Long.MIN_VALUE;

    for (final long aci : acl) {
      if (isPriorityBitSet(aci)) {
        if (isDenial(aci)) {
          priorityDenial = priorityDenial | aci;
        } else {
          priorityAllowance = priorityAllowance | aci;
        }
      } else {
        if (isDenial(aci)) {
          denial = denial | aci;
        } else {
          allowance = allowance | aci;
        }
      }
    }
    return ((allowance & ~denial) | (priorityAllowance & ~MIN_PRIORITY_BITSET)) & ~priorityDenial;
  }

  public static final boolean isPriorityBitSet(final long bitSet) {
    return (bitSet & MIN_PRIORITY_BITSET) != 0;
  }

  public static final boolean isAllowance(final long bitSet) {
    return bitSet >= 0;
  }

  public static final boolean isDenial(final long bitSet) {
    return bitSet < 0;
  }

  public static final String serialize(final EntityACL entityACL) {
    return toJSON(entityACL);
  }

  public static final EntityACL deserialize(final String input) {
    if (input == null || input.isEmpty()) {
      return new EntityACL((Collection<EntityACI>) null);
    }

    return fromJSON(input);
  }

  public final Element toElement() {
    final Element ret = new Element("EntityACL");

    final List<EntityACI> localAcl = new ArrayList<>(this.acl);
    localAcl.addAll(GLOBAL_PERMISSIONS.acl);
    for (final EntityACI aci : localAcl) {
      final boolean isDenial = isDenial(aci.getBitSet());
      final boolean isPriority = isPriorityBitSet(aci.getBitSet());
      final Element e = new Element(isDenial ? "Deny" : "Grant");
      aci.getResponsibleAgent().addToElement(e);
      e.setAttribute("priority", Boolean.toString(isPriority));

      final boolean[] bitSet = convertToArray(aci.getBitSet());
      for (int i = 0; i < 62; i++) {
        if (bitSet[i]) {
          e.addContent(EntityPermission.getEntityPermission(i).toElement());
        }
      }
      ret.addContent(e);
    }
    return ret;
  }

  public static final EntityACL getPriorityEntityACL(final EntityACL acl) {
    final List<EntityACI> priorityAcl = new ArrayList<>();
    for (final EntityACI aci : acl.acl) {
      if (isPriorityBitSet(aci.getBitSet())) {
        priorityAcl.add(aci);
      }
    }
    return new EntityACL(priorityAcl);
  }

  /**
   * Example
   *
   * <p><globalPermission> <Grant priority="false" role="%OWNER%"><Permission name="*"/></Grant>
   * </globalPermissions>
   *
   * @param e
   * @return
   */
  public static final EntityACL parseFromElement(final Element e) {
    final EntityACLFactory factory = new EntityACLFactory();

    for (final Element c : e.getChildren()) {
      boolean priority;
      try {
        priority =
            c.getAttribute("priority") != null && c.getAttribute("priority").getBooleanValue();
      } catch (final DataConversionException e1) {
        throw new TransactionException(e1);
      }
      ResponsibleAgent agent = null;
      final String role = c.getAttributeValue("role");
      if (role != null && !role.isEmpty()) {
        agent = Role.create(role);
      } else {
        final String realm = c.getAttributeValue("realm");
        final String username = c.getAttributeValue("username");
        if (username != null && !username.isEmpty()) {
          agent = new Principal(realm, username);
        } else {
          continue;
        }
      }
      for (final Element p : c.getChildren("Permission")) {
        final String permission = p.getAttributeValue("name");
        if (c.getName().equals("Grant")) {
          factory.grant(agent, priority, permission);
        } else if (c.getName().equals("Deny")) {
          factory.deny(agent, priority, permission);
        }
      }
    }
    return factory.remove(GLOBAL_PERMISSIONS).create();
  }

  public static BitSet convert(final long value) {
    final BitSet bits = new BitSet();
    final boolean[] ba = convertToArray(value);
    for (int i = 0; i < 64; i++) {
      bits.set(i, ba[i]);
    }
    return bits;
  }

  public static boolean[] convertToArray(long value) {
    final boolean[] ret = new boolean[64];
    for (int i = 0; i < 64; i++) {
      ret[i] = value % 2L != 0L;
      value = value >>> 1;
    }
    return ret;
  }

  public String serialize() {
    return EntityACL.serialize(this);
  }

  public static final EntityACL combine(final EntityACL... acls) {
    final List<EntityACI> newACL = new ArrayList<>();
    for (final EntityACL acl : acls) {
      newACL.addAll(acl.acl);
    }
    return new EntityACL(newACL);
  }

  public EntityACL getPriorityEntityACL() {
    return getPriorityEntityACL(this);
  }

  @Override
  public boolean equals(final Object obj) {
    if (obj instanceof EntityACL) {
      final EntityACL that = (EntityACL) obj;
      final Set<EntityACI> thatAcis = new HashSet<>();
      thatAcis.addAll(that.acl);
      final Set<EntityACI> thisAcis = new HashSet<>();
      thisAcis.addAll(this.acl);
      return thatAcis.equals(thisAcis);
    }
    return false;
  }

  public Element getPermissionsFor(final Subject subject) {
    final Element ret = new Element("Permissions");
    final List<EntityACI> localAcl = new ArrayList<>(this.acl);
    localAcl.addAll(GLOBAL_PERMISSIONS.acl);
    final Set<EntityPermission> permissionsFor = getPermissionsFor(subject, localAcl);
    for (final EntityPermission p : permissionsFor) {
      ret.addContent(p.toElement());
    }
    return ret;
  }

  public static EntityACL fromJSON(final String input) {
    final Object parse = JSON.parse(input);
    final List<EntityACI> acl = new ArrayList<>();
    if (parse.getClass().isArray()) {
      final Object[] array = (Object[]) parse;
      for (final Object aci : array) {
        if (aci instanceof Map) {
          final Map<?, ?> map = (Map<?, ?>) aci;

          if (map.get("role") != null) {
            acl.add(
                new EntityACI(
                    Role.create((String) map.get("role")),
                    Long.parseLong(map.get("bitSet").toString())));
          } else if (map.get("username") != null) {
            acl.add(
                new EntityACI(
                    new Principal((String) map.get("realm"), (String) map.get("username")),
                    Long.parseLong(map.get("bitSet").toString())));
          }
        }
      }
    }
    return new EntityACL(acl);
  }

  public static String toJSON(final EntityACL acl) {
    final List<Map<String, Object>> list = new ArrayList<>();

    for (final EntityACI aci : acl.acl) {
      list.add(aci.toMap());
    }
    return JSON.toString(list);
  }

  public Collection<EntityACI> getRules() {
    return Collections.unmodifiableCollection(this.acl);
  }
}
