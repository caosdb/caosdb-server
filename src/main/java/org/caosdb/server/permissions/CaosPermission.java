/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.permissions;

import java.util.Collection;
import java.util.HashSet;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.Permission;
import org.apache.shiro.subject.Subject;

public class CaosPermission extends HashSet<PermissionRule> implements Permission {

  public CaosPermission(final HashSet<PermissionRule> rules) {
    super(rules);
  }

  public Collection<String> getStringPermissions(Subject subject) {
    HashSet<String> grant = new HashSet<>();
    HashSet<String> prio_grant = new HashSet<>();
    HashSet<String> deny = new HashSet<>();
    HashSet<String> prio_deny = new HashSet<>();

    for (PermissionRule r : this) {
      String p = subject == null ? r.getPermission() : r.getPermission(subject).toString();
      if (r.isGrant()) {
        if (r.isPriority()) {
          prio_grant.add(p);
        } else {
          grant.add(p);
        }
      } else {
        if (r.isPriority()) {
          prio_deny.add(p);
        } else {
          deny.add(p);
        }
      }
    }

    grant.removeAll(deny);
    grant.addAll(prio_grant);
    grant.removeAll(prio_deny);
    return grant;
  }

  private static final long serialVersionUID = 2136265443788256009L;

  @Override
  public boolean implies(final Permission p) {
    boolean grant = false;
    boolean deny = false;
    boolean grant_priority = false;
    Subject subject = SecurityUtils.getSubject();

    for (final PermissionRule r : this) {
      if (r.getPermission(subject).implies(p)) {
        if (r.isGrant()) {
          if (r.isPriority()) {
            grant_priority = true;
          } else {
            grant = true;
          }
        } else {
          if (r.isPriority()) {
            return false;
          } else {
            deny = true;
          }
        }
      }
    }
    return (grant && !deny) || grant_priority;
  }
}
