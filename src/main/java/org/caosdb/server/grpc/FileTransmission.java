package org.caosdb.server.grpc;

import java.util.concurrent.locks.ReentrantLock;
import org.caosdb.api.entity.v1.FileTransmissionSettings;
import org.caosdb.api.entity.v1.RegistrationStatus;
import org.caosdb.server.entity.FileProperties;

public abstract class FileTransmission {

  protected ReentrantLock lock = new ReentrantLock();
  protected final String id;
  protected final long createdTimestamp;
  protected long touchedTimestamp;
  protected final RegistrationStatus status;

  public FileTransmission(final String id) {
    this.id = id;
    this.status = RegistrationStatus.REGISTRATION_STATUS_ACCEPTED;
    this.createdTimestamp = System.currentTimeMillis();
    this.touchedTimestamp = createdTimestamp;
  }

  public abstract void cleanUp();

  public long getCreatedTimestamp() {
    return createdTimestamp;
  }

  public long getTouchedTimestamp() {
    return this.touchedTimestamp;
  }

  public void touch() {
    this.touchedTimestamp = System.currentTimeMillis();
  }

  public String getId() {
    return this.id;
  }

  public RegistrationStatus getRegistrationStatus() {
    return status;
  }

  public long getMaxChunkSize() {
    // 2^24, 16.78 MB
    return 16777216;
  }

  public long getMaxFileSize() {
    // 2^30, 1.074 GB
    return 1073741824;
  }

  public abstract FileProperties getFile(final String fileId);

  public abstract FileTransmissionSettings getTransmissionSettings();

  public boolean isExpired() {
    return System.currentTimeMillis() - touchedTimestamp > 10 * 60 * 1000; // older than 10 min
  }
}
