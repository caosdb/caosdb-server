package org.caosdb.server.grpc;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import org.caosdb.api.entity.v1.FileTransmissionId;
import org.caosdb.server.entity.FileProperties;
import org.caosdb.server.utils.Utils;

public class FileUploadRegistration {

  private final Map<String, FileUpload> registeredUploads = new HashMap<>();

  public FileTransmission registerFileUpload() {
    final FileUpload result = new FileUpload(Utils.getUID());
    register(result);
    return result;
  }

  private void register(final FileUpload fileTransmission) {

    synchronized (registeredUploads) {
      registeredUploads.put(fileTransmission.getId(), fileTransmission);
    }
  }

  public FileProperties getUploadFile(final FileTransmissionId uploadId) {
    final String fileId = uploadId.getFileId();
    final String registrationId = uploadId.getRegistrationId();
    final FileTransmission fileTransmission;

    synchronized (registeredUploads) {
      fileTransmission = registeredUploads.get(registrationId);
    }
    return fileTransmission.getFile(fileId);
  }

  public FileUpload getFileUpload(final String registrationId) {
    synchronized (registeredUploads) {
      return registeredUploads.get(registrationId);
    }
  }

  public void cleanUp(final boolean all) {
    synchronized (registeredUploads) {
      final List<String> cleanUp = new LinkedList<>();
      for (final Entry<String, FileUpload> entry : registeredUploads.entrySet()) {
        if (all || entry.getValue().isExpired()) {
          cleanUp.add(entry.getKey());
        }
      }
      for (final String key : cleanUp) {
        registeredUploads.get(key).cleanUp();
        registeredUploads.remove(key);
      }
    }
  }

  public void cleanUp() {
    cleanUp(false);
  }
}
