/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2023 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2023 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.caosdb.server.grpc;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;
import org.apache.shiro.SecurityUtils;
import org.caosdb.api.entity.v1.AtomicDataType;
import org.caosdb.api.entity.v1.CollectionValues;
import org.caosdb.api.entity.v1.DataType;
import org.caosdb.api.entity.v1.Entity;
import org.caosdb.api.entity.v1.EntityACL;
import org.caosdb.api.entity.v1.EntityPermission;
import org.caosdb.api.entity.v1.EntityPermissionRule;
import org.caosdb.api.entity.v1.EntityRole;
import org.caosdb.api.entity.v1.FileDescriptor;
import org.caosdb.api.entity.v1.Importance;
import org.caosdb.api.entity.v1.ListDataType;
import org.caosdb.api.entity.v1.Parent;
import org.caosdb.api.entity.v1.ReferenceDataType;
import org.caosdb.api.entity.v1.SpecialValue;
import org.caosdb.server.datatype.AbstractDatatype;
import org.caosdb.server.datatype.BooleanValue;
import org.caosdb.server.datatype.CollectionValue;
import org.caosdb.server.datatype.FileDatatype;
import org.caosdb.server.datatype.GenericValue;
import org.caosdb.server.datatype.ListDatatype;
import org.caosdb.server.datatype.ReferenceDatatype;
import org.caosdb.server.datatype.ReferenceDatatype2;
import org.caosdb.server.datatype.Value;
import org.caosdb.server.entity.EntityID;
import org.caosdb.server.entity.EntityInterface;
import org.caosdb.server.entity.FileProperties;
import org.caosdb.server.entity.MagicTypes;
import org.caosdb.server.entity.RetrieveEntity;
import org.caosdb.server.entity.Role;
import org.caosdb.server.entity.StatementStatus;
import org.caosdb.server.entity.UpdateEntity;
import org.caosdb.server.entity.container.TransactionContainer;
import org.caosdb.server.entity.wrapper.Property;
import org.caosdb.server.permissions.EntityACLFactory;
import org.caosdb.server.utils.EntityStatus;
import org.caosdb.server.utils.ServerMessages;

/**
 * Utility class for converting GRPC's native objects into our own CaosDB objects.
 *
 * @author Timm Fitschen <t.fitschen@indiscale.com>
 */
public class GrpcToCaosDBConverters {

  public Role convert(final EntityRole role) {
    switch (role) {
      case ENTITY_ROLE_FILE:
        return Role.File;
      case ENTITY_ROLE_PROPERTY:
        return Role.Property;
      case ENTITY_ROLE_RECORD:
        return Role.Record;
      case ENTITY_ROLE_RECORD_TYPE:
        return Role.RecordType;
      default:
        return null;
    }
  }

  public Property getUnit(final String unitStr) {
    final EntityInterface magicUnit = MagicTypes.UNIT.getEntity();
    final Property unit = new Property(new RetrieveEntity());
    unit.setDescription(magicUnit.getDescription());
    unit.setName(magicUnit.getName());
    unit.setId(magicUnit.getId());
    unit.setDatatype(magicUnit.getDatatype());
    unit.setStatementStatus(StatementStatus.FIX);
    unit.setValue(new GenericValue(unitStr));
    unit.setEntityStatus(EntityStatus.QUALIFIED);
    return unit;
  }

  public Value getValue(final String valString) {
    return new GenericValue(valString);
  }

  /**
   * Set the content of {@code entity} to that of the grpc message object {@code from}. Also return
   * {@code entity} at the end.
   */
  public EntityInterface convert(final Entity from, final EntityInterface entity) {
    entity.setName(from.getName().isEmpty() ? null : from.getName());
    entity.setDescription(from.getDescription().isBlank() ? null : from.getDescription());
    if (!from.getUnit().isBlank()) {
      entity.addProperty(getUnit(from.getUnit()));
    }
    if (from.hasDataType()) {
      entity.setDatatype(convert(from.getDataType()));
    }
    if (from.hasValue()) {
      entity.setValue(convert(from.getValue()));
    }

    if (from.getPropertiesCount() > 0) {
      final StatementStatus defaultImportance =
          entity.getRole() == Role.RecordType ? StatementStatus.RECOMMENDED : StatementStatus.FIX;
      entity.getProperties().addAll(convertProperties(from.getPropertiesList(), defaultImportance));
    }
    if (from.getParentsCount() > 0) {
      entity.getParents().addAll(convertParents(from.getParentsList()));
    }
    if (entity.getRole() == Role.File && from.hasFileDescriptor()) {
      entity.setFileProperties(convert(from.getFileDescriptor()));
    }
    return entity;
  }

  private Value convert(final org.caosdb.api.entity.v1.Value value) {
    switch (value.getValueCase()) {
      case LIST_VALUES:
        return convertListValue(value.getListValues());
      case SCALAR_VALUE:
        return convertScalarValue(value.getScalarValue());
      default:
        break;
    }
    return null;
  }

  private CollectionValue convertListValue(final CollectionValues collectionValues) {
    final CollectionValue result = new CollectionValue();
    collectionValues
        .getValuesList()
        .forEach(
            (v) -> {
              result.add(convertScalarValue(v));
            });
    return result;
  }

  private Value convertScalarValue(final org.caosdb.api.entity.v1.ScalarValue value) {
    switch (value.getScalarValueCase()) {
      case BOOLEAN_VALUE:
        return BooleanValue.valueOf(value.getBooleanValue());
      case DOUBLE_VALUE:
        return new GenericValue(value.getDoubleValue());
      case INTEGER_VALUE:
        return new GenericValue(Long.toString(value.getIntegerValue()));
      case SPECIAL_VALUE:
        return convertSpecial(value.getSpecialValue());
      case STRING_VALUE:
        return new GenericValue(value.getStringValue());
      default:
        break;
    }
    return null;
  }

  private Value convertSpecial(final SpecialValue specialValue) {
    if (specialValue == SpecialValue.SPECIAL_VALUE_EMPTY_STRING) {
      return new GenericValue("");
    }
    return null;
  }

  private AbstractDatatype convert(final DataType dataType) {
    switch (dataType.getDataTypeCase()) {
      case ATOMIC_DATA_TYPE:
        return convertAtomicType(dataType.getAtomicDataType());
      case LIST_DATA_TYPE:
        return convertListDataType(dataType.getListDataType());
      case REFERENCE_DATA_TYPE:
        return convertReferenceDataType(dataType.getReferenceDataType());
      default:
        break;
    }
    return null;
  }

  private ReferenceDatatype convertReferenceDataType(final ReferenceDataType referenceDataType) {
    final String name = referenceDataType.getName();
    if (name.equalsIgnoreCase("REFERENCE")) {
      return new ReferenceDatatype();
    } else if (name.equalsIgnoreCase("FILE")) {
      return new FileDatatype();
    }
    return new ReferenceDatatype2(name);
  }

  private AbstractDatatype convertAtomicType(final AtomicDataType dataType) {
    switch (dataType) {
      case ATOMIC_DATA_TYPE_BOOLEAN:
        return AbstractDatatype.datatypeFactory("BOOLEAN");
      case ATOMIC_DATA_TYPE_DATETIME:
        return AbstractDatatype.datatypeFactory("DATETIME");
      case ATOMIC_DATA_TYPE_DOUBLE:
        return AbstractDatatype.datatypeFactory("DOUBLE");
      case ATOMIC_DATA_TYPE_INTEGER:
        return AbstractDatatype.datatypeFactory("INTEGER");
      case ATOMIC_DATA_TYPE_TEXT:
        return AbstractDatatype.datatypeFactory("TEXT");
      default:
        return null;
    }
  }

  private AbstractDatatype convertListDataType(final ListDataType dataType) {
    switch (dataType.getListDataTypeCase()) {
      case ATOMIC_DATA_TYPE:
        return new ListDatatype(convertAtomicType(dataType.getAtomicDataType()));
      case REFERENCE_DATA_TYPE:
        return new ListDatatype(convertReferenceDataType(dataType.getReferenceDataType()));
      default:
        return null;
    }
  }

  private FileProperties convert(final FileDescriptor fileDescriptor) {
    return new FileProperties(
        null,
        fileDescriptor.getPath(),
        fileDescriptor.getSize() == 0 ? null : fileDescriptor.getSize());
  }

  private Collection<Property> convertProperties(
      final List<org.caosdb.api.entity.v1.Property> propertiesList,
      final StatementStatus defaultImportance) {
    final Collection<Property> result = new LinkedList<>();
    propertiesList.forEach(
        prop -> {
          result.add(convert(prop, defaultImportance));
        });
    return result;
  }

  private Property convert(
      final org.caosdb.api.entity.v1.Property e, final StatementStatus defaultImportance) {
    final Property result = new Property(new RetrieveEntity());

    try {
      result.setId(e.getId().isBlank() ? null : new EntityID(e.getId()));
    } catch (final NumberFormatException exc) {
      result.addError(ServerMessages.ENTITY_DOES_NOT_EXIST);
    }
    result.setName(e.getName().isBlank() ? null : e.getName());
    result.setDescription(e.getDescription().isBlank() ? null : e.getDescription());
    if (!e.getUnit().isBlank()) {
      result.addProperty(getUnit(e.getUnit()));
    }
    if (e.hasDataType()) {
      result.setDatatype(convert(e.getDataType()));
    }
    if (e.hasValue()) {
      result.setValue(convert(e.getValue()));
    }
    if (e.getImportance() != Importance.IMPORTANCE_UNSPECIFIED) {
      result.setStatementStatus(convert(e.getImportance()));
    } else {
      result.setStatementStatus(defaultImportance);
    }
    // TODO remove this hard-coded setting when the API supports flags
    if (result.getFlag("inheritance") == null) {
      result.setFlag("inheritance", "fix");
    }

    return result;
  }

  private StatementStatus convert(final Importance importance) {
    switch (importance) {
      case IMPORTANCE_FIX:
        return StatementStatus.FIX;
      case IMPORTANCE_OBLIGATORY:
        return StatementStatus.OBLIGATORY;
      case IMPORTANCE_RECOMMENDED:
        return StatementStatus.RECOMMENDED;
      case IMPORTANCE_SUGGESTED:
        return StatementStatus.SUGGESTED;
      default:
        return null;
    }
  }

  private Collection<org.caosdb.server.entity.wrapper.Parent> convertParents(
      final List<Parent> parentsList) {
    final Collection<org.caosdb.server.entity.wrapper.Parent> result = new LinkedList<>();
    parentsList.forEach(
        e -> {
          result.add(convert(e));
        });
    return result;
  }

  private org.caosdb.server.entity.wrapper.Parent convert(final Parent e) {
    final org.caosdb.server.entity.wrapper.Parent result =
        new org.caosdb.server.entity.wrapper.Parent(new RetrieveEntity());

    try {
      result.setId(e.getId().isBlank() ? null : new EntityID(e.getId()));
    } catch (final NumberFormatException exc) {
      result.addError(ServerMessages.ENTITY_DOES_NOT_EXIST);
    }
    result.setName(e.getName().isBlank() ? null : e.getName());
    return result;
  }

  public TransactionContainer convertAcls(List<EntityACL> aclsList) {
    TransactionContainer result =
        new TransactionContainer(
            SecurityUtils.getSubject(), System.currentTimeMillis(), UUID.randomUUID().toString());

    aclsList.forEach(
        acl -> {
          result.add(convert(acl));
        });
    return result;
  }

  private EntityInterface convert(EntityACL acl) {
    try {
      EntityID id = new EntityID(acl.getId());
      UpdateEntity result = new UpdateEntity(id);
      result.setEntityACL(convertAcl(acl));
      return result;
    } catch (NumberFormatException exc) {
      UpdateEntity result = new UpdateEntity();
      result.addError(ServerMessages.ENTITY_DOES_NOT_EXIST);
      return result;
    }
  }

  private org.caosdb.server.permissions.EntityACL convertAcl(EntityACL acl) {
    EntityACLFactory fac = new EntityACLFactory();
    for (EntityPermissionRule rule : acl.getRulesList()) {
      if (rule.getGrant()) {
        fac.grant(
            org.caosdb.server.permissions.Role.create(rule.getRole()),
            rule.getPriority(),
            convert(rule.getPermissionsList()));
      } else {
        fac.deny(
            org.caosdb.server.permissions.Role.create(rule.getRole()),
            rule.getPriority(),
            convert(rule.getPermissionsList()));
      }
    }
    return fac.remove(org.caosdb.server.permissions.EntityACL.GLOBAL_PERMISSIONS).create();
  }

  private org.caosdb.server.permissions.EntityPermission[] convert(
      List<EntityPermission> permissionsList) {
    ArrayList<org.caosdb.server.permissions.EntityPermission> result =
        new ArrayList<>(permissionsList.size());
    permissionsList.forEach(
        (p) -> {
          result.add(org.caosdb.server.permissions.EntityPermission.getEntityPermission(p));
        });
    return result.toArray(new org.caosdb.server.permissions.EntityPermission[0]);
  }
}
