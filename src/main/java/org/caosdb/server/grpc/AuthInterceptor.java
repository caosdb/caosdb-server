/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2021 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2021 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.caosdb.server.grpc;

import static org.caosdb.server.utils.Utils.URLDecodeWithUTF8;

import io.grpc.Context;
import io.grpc.Contexts;
import io.grpc.ForwardingServerCall;
import io.grpc.Metadata;
import io.grpc.Metadata.Key;
import io.grpc.ServerCall;
import io.grpc.ServerCall.Listener;
import io.grpc.ServerCallHandler;
import io.grpc.ServerInterceptor;
import io.grpc.Status;
import java.util.Base64;
import java.util.function.Predicate;
import java.util.regex.Pattern;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.util.ThreadContext;
import org.caosdb.server.CaosDBServer;
import org.caosdb.server.ServerProperties;
import org.caosdb.server.accessControl.AnonymousAuthenticationToken;
import org.caosdb.server.accessControl.AuthenticationUtils;
import org.caosdb.server.accessControl.RealmUsernamePasswordToken;
import org.caosdb.server.accessControl.SelfValidatingAuthenticationToken;
import org.caosdb.server.accessControl.SessionToken;
import org.caosdb.server.accessControl.UserSources;
import org.caosdb.server.utils.Utils;
import org.restlet.data.CookieSetting;

/**
 * ServerInterceptor for Authentication. If the authentication succeeds or if the caller is
 * anonymous, the {@link Context} of the {@link ServerCall} is updated with a {@link Subject}
 * instance. If the request does not succeed the call is closed with {@link Status#UNAUTHENTICATED}.
 *
 * @author Timm Fitschen <t.fitschen@indiscale.com>
 */
public class AuthInterceptor implements ServerInterceptor {

  public static final Status PLEASE_LOG_IN =
      Status.UNAUTHENTICATED.withDescription("Please log in!");
  public static final Key<String> AUTHENTICATION_HEADER =
      Key.of("authentication", Metadata.ASCII_STRING_MARSHALLER);
  public static final Key<String> AUTHORIZATION_HEADER =
      Key.of("Authorization", Metadata.ASCII_STRING_MARSHALLER);
  public static final Key<String> COOKIE_HEADER =
      Key.of("Cookie", Metadata.ASCII_STRING_MARSHALLER);
  public static final Context.Key<Subject> SUBJECT_KEY = Context.key("subject");
  public static final String BASIC_SCHEME_PREFIX = "Basic ";
  public static final Pattern SESSION_TOKEN_COOKIE_PREFIX_PATTERN =
      Pattern.compile("^\\s*" + AuthenticationUtils.SESSION_TOKEN_COOKIE + "\\s*=\\s*");
  public static final Predicate<String> SESSION_TOKEN_COOKIE_PREFIX_PREDICATE =
      SESSION_TOKEN_COOKIE_PREFIX_PATTERN.asPredicate();

  @SuppressWarnings("unused")
  public static Subject bindSubject() {
    Subject subject = (Subject) SUBJECT_KEY.get();
    ThreadContext.bind(subject);
    return subject;
  }

  public final Metadata expiredSessionMetadata() {
    Metadata metadata = new Metadata();
    metadata.put(CookieSetter.SET_COOKIE, CookieSetter.EXPIRED_SESSION_COOKIE);
    return metadata;
  }

  /**
   * A no-op listener. This class is used for failed authentications. We couldn't return a null
   * instead because the documentation of the {@link ServerInterceptor} explicitely forbids it.
   *
   * @author Timm Fitschen <t.fitschen@indiscale.com>
   */
  static class NoOpListener<ReqT> extends Listener<ReqT> {}

  /** Whether the anonymous login is possible or not. */
  private final boolean isAuthOptional() {
    return CaosDBServer.getServerProperty(ServerProperties.KEY_AUTH_OPTIONAL)
        .equalsIgnoreCase("true");
  }

  /**
   * Login via username and password with the basic authentication scheme and return the logged-in
   * subject.
   */
  private Subject basicAuth(final String base64) {
    final String plain = new String(Base64.getDecoder().decode(base64));
    final String[] split = plain.split(":", 2);
    final String username = split[0];
    final String password = split[1];
    final RealmUsernamePasswordToken token =
        new RealmUsernamePasswordToken(
            UserSources.guessRealm(username, UserSources.getDefaultRealm()), username, password);
    final Subject subject = SecurityUtils.getSubject();
    subject.login(token);
    return subject;
  }

  @Override
  public <ReqT, RespT> Listener<ReqT> interceptCall(
      final ServerCall<ReqT, RespT> call,
      final Metadata headers,
      final ServerCallHandler<ReqT, RespT> next) {
    ThreadContext.remove();

    String authentication = headers.get(AUTHENTICATION_HEADER);
    if (authentication == null) {
      authentication = headers.get(AUTHORIZATION_HEADER);
    }
    if (authentication == null) {
      authentication = getSessionToken(headers.get(COOKIE_HEADER));
    }
    Status status =
        Status.UNKNOWN.withDescription(
            "An unknown error occured during authentication. Please report a bug.");
    if (authentication == null && isAuthOptional()) {
      return anonymous(call, headers, next);
    } else if (authentication == null) {
      status = PLEASE_LOG_IN;
    } else if (authentication.startsWith(BASIC_SCHEME_PREFIX)) {
      return basicAuth(authentication.substring(BASIC_SCHEME_PREFIX.length()), call, headers, next);
    } else if (SESSION_TOKEN_COOKIE_PREFIX_PREDICATE.test(authentication)) {
      return sessionTokenAuth(
          SESSION_TOKEN_COOKIE_PREFIX_PATTERN.split(authentication, 2)[1], call, headers, next);
    } else {
      status = Status.UNAUTHENTICATED.withDescription("Unsupported authentication scheme.");
    }
    call.close(status, expiredSessionMetadata());
    return new NoOpListener<ReqT>();
  }

  private String getSessionToken(String cookies) {
    if (cookies != null)
      for (String cookie : cookies.split("\\s*;\\s*")) {
        if (SESSION_TOKEN_COOKIE_PREFIX_PREDICATE.test(cookie)) {
          return cookie;
        }
      }
    return null;
  }

  /**
   * Login via AuthenticationToken and add the resulting subject to the call context.
   *
   * @see #updateContext(Subject, ServerCall, Metadata, ServerCallHandler) for more information.
   */
  private <ReqT, RespT> Listener<ReqT> sessionTokenAuth(
      String sessionTokenCookie,
      ServerCall<ReqT, RespT> call,
      Metadata headers,
      ServerCallHandler<ReqT, RespT> next) {
    try {
      final String tokenString = URLDecodeWithUTF8(sessionTokenCookie.split(";")[0]);

      final Subject subject = sessionTokenAuth(tokenString);
      return updateContext(subject, call, headers, next, "sessionToken: " + tokenString);
    } catch (final AuthenticationException e) {
      final Status status =
          Status.UNAUTHENTICATED.withDescription(
              "Authentication failed. SessionToken was invalid.");
      call.close(status, expiredSessionMetadata());
      return new NoOpListener<ReqT>();
    }
  }

  /** Login via AuthenticationToken and return the logged-in subject. */
  private Subject sessionTokenAuth(String tokenString) {
    Subject subject = SecurityUtils.getSubject();
    subject.login(SelfValidatingAuthenticationToken.parse(tokenString));
    return subject;
  }

  /**
   * Login via username and password with the basic authentication scheme and add the resulting
   * subject to the call context.
   *
   * @see #updateContext(Subject, ServerCall, Metadata, ServerCallHandler) for more information.
   */
  private <ReqT, RespT> Listener<ReqT> basicAuth(
      final String base64,
      final ServerCall<ReqT, RespT> call,
      final Metadata headers,
      final ServerCallHandler<ReqT, RespT> next) {
    try {
      final Subject subject = basicAuth(base64);
      return updateContext(
          subject,
          call,
          headers,
          next,
          "basic: " + base64 + " thread: " + Thread.currentThread().getName());
    } catch (final AuthenticationException e) {
      final Status status =
          Status.UNAUTHENTICATED.withDescription(
              "Authentication failed. Username or password wrong.");
      call.close(status, expiredSessionMetadata());
      return new NoOpListener<ReqT>();
    }
  }

  /**
   * Login as anonymous and add the anonymous subject to the call context.
   *
   * @see #updateContext(Subject, ServerCall, Metadata, ServerCallHandler) for more information.
   */
  private <ReqT, RespT> Listener<ReqT> anonymous(
      final ServerCall<ReqT, RespT> call,
      final Metadata headers,
      final ServerCallHandler<ReqT, RespT> next) {
    final Subject subject = anonymous();
    return updateContext(subject, call, headers, next, "anonymous");
  }

  /** Login as anonymous. */
  private Subject anonymous() {
    final Subject anonymous = SecurityUtils.getSubject();
    anonymous.login(AnonymousAuthenticationToken.getInstance());
    return anonymous;
  }

  /**
   * Add the subject to the call context. This is done the grpcic way by returning a listener which
   * does exactly that.
   */
  private <ReqT, RespT> Listener<ReqT> updateContext(
      final Subject subject,
      final ServerCall<ReqT, RespT> call,
      final Metadata headers,
      final ServerCallHandler<ReqT, RespT> next,
      final String tag) {
    final Context context = Context.current().withValue(SUBJECT_KEY, subject);
    ServerCall<ReqT, RespT> cookieSetter = new CookieSetter<>(call, subject, tag);
    return Contexts.interceptCall(context, cookieSetter, headers, next);
  }
}

final class CookieSetter<ReqT, RespT>
    extends ForwardingServerCall.SimpleForwardingServerCall<ReqT, RespT> {
  public static final String EXPIRED_SESSION_COOKIE =
      AuthenticationUtils.SESSION_TOKEN_COOKIE
          + "=expired; Path=/; HttpOnly; SameSite=Strict; Max-Age=0";
  public static final Key<String> SET_COOKIE =
      Key.of("Set-Cookie", Metadata.ASCII_STRING_MARSHALLER);
  private Subject subject;

  protected CookieSetter(ServerCall<ReqT, RespT> delegate, Subject subject, String tag) {
    super(delegate);
    this.subject = subject;
  }

  String getSessionTimeoutSeconds() {
    int ms =
        Integer.parseInt(CaosDBServer.getServerProperty(ServerProperties.KEY_SESSION_TIMEOUT_MS));
    int seconds = (int) Math.floor(ms / 1000);
    return Integer.toString(seconds);
  }

  @Override
  public void sendHeaders(Metadata headers) {
    setSessionCookies(headers);
    super.sendHeaders(headers);
  }

  private void setSessionCookies(Metadata headers) {
    // if authenticated as a normal user: generate and set session cookie.
    if (subject.isAuthenticated()
        && !AnonymousAuthenticationToken.PRINCIPAL.equals(subject.getPrincipal())) {
      final SessionToken sessionToken = SessionToken.generate(subject);
      if (sessionToken != null && sessionToken.isValid()) {

        final CookieSetting sessionTokenCookie =
            AuthenticationUtils.createSessionTokenCookie(sessionToken);
        if (sessionTokenCookie != null) {
          // TODO add "Secure;" to cookie setting
          headers.put(
              SET_COOKIE,
              AuthenticationUtils.SESSION_TOKEN_COOKIE
                  + "="
                  + Utils.URLEncodeWithUTF8(sessionToken.toString())
                  + "; Path=/; HttpOnly; SameSite=Strict; Max-Age="
                  + getSessionTimeoutSeconds());
        }
      }
    } else if (AnonymousAuthenticationToken.PRINCIPAL.equals(subject.getPrincipal())) {
      // this is anonymous, do nothing
      headers.toString();
    } else {
      headers.put(SET_COOKIE, EXPIRED_SESSION_COOKIE);
    }
  }
}
