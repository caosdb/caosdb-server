package org.caosdb.server.grpc;

import com.google.protobuf.ByteString;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import org.caosdb.api.entity.v1.TransmissionStatus;
import org.caosdb.server.entity.FileProperties;

public class UploadBuffer {

  TransmissionStatus status = TransmissionStatus.TRANSMISSION_STATUS_UNSPECIFIED;
  private final File tmpFile;

  public UploadBuffer(final File tmpFile) {
    this.tmpFile = tmpFile;
  }

  public TransmissionStatus write(final ByteString data) throws IOException {
    switch (status) {
      case TRANSMISSION_STATUS_UNSPECIFIED:
        status = TransmissionStatus.TRANSMISSION_STATUS_GO_ON;
        break;
      case TRANSMISSION_STATUS_GO_ON:
        break;
      default:
        throw new RuntimeException("Wrong transmission state.");
    }
    try (final FileOutputStream fileOutputStream =
        new FileOutputStream(tmpFile, tmpFile.exists())) {
      data.writeTo(fileOutputStream);
    }
    return status;
  }

  public FileProperties toFileProperties(final String fileId) {
    switch (status) {
      case TRANSMISSION_STATUS_SUCCESS:
        break;
      case TRANSMISSION_STATUS_GO_ON:
        status = TransmissionStatus.TRANSMISSION_STATUS_SUCCESS;
        break;
      default:
        throw new RuntimeException("Wrong transmission state.");
    }
    final FileProperties result =
        new FileProperties(null, tmpFile.getAbsolutePath(), tmpFile.length());
    result.setFile(tmpFile);
    result.setTmpIdentifier(fileId);
    return result;
  }
}
