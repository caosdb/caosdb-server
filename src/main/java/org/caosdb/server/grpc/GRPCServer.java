/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2021 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2021 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package org.caosdb.server.grpc;

import io.grpc.Server;
import io.grpc.ServerInterceptors;
import io.grpc.ServerServiceDefinition;
import io.grpc.netty.GrpcSslContexts;
import io.grpc.netty.NettyServerBuilder;
import io.netty.handler.ssl.ApplicationProtocolConfig;
import io.netty.handler.ssl.ApplicationProtocolConfig.Protocol;
import io.netty.handler.ssl.ApplicationProtocolConfig.SelectedListenerFailureBehavior;
import io.netty.handler.ssl.ApplicationProtocolConfig.SelectorFailureBehavior;
import io.netty.handler.ssl.SslContext;
import io.netty.handler.ssl.SslContextBuilder;
import java.io.File;
import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import javax.net.ssl.KeyManagerFactory;
import org.caosdb.server.CaosDBServer;
import org.caosdb.server.ServerProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This is the main class of the gRPC end-point.
 *
 * <p>Here, the http and https servers are startet.
 *
 * @author Timm Fitschen <t.fitschen@indiscale.com>
 */
public class GRPCServer {
  private static GRPCServer instance = new GRPCServer();

  private static String getServerProperty(final String key) {
    return CaosDBServer.getServerProperty(key);
  }

  private static final Logger logger = LoggerFactory.getLogger(GRPCServer.class.getName());

  private final AuthInterceptor authInterceptor = new AuthInterceptor();
  private final LoggingInterceptor loggingInterceptor = new LoggingInterceptor();

  /**
   * Create an ssl context.
   *
   * <p>Read the server certificate from the Java Key Store. Also, use the server properties for
   * enabling desired TLS protocols and cipher suites.
   *
   * @return An SslContext for a https grpc end-point.
   * @throws NoSuchAlgorithmException
   * @throws UnrecoverableKeyException
   * @throws KeyStoreException
   * @throws CertificateException
   * @throws IOException
   */
  private SslContext buildSslContext()
      throws NoSuchAlgorithmException,
          UnrecoverableKeyException,
          KeyStoreException,
          CertificateException,
          IOException {
    final KeyManagerFactory kmf =
        KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
    final char[] password =
        getServerProperty(ServerProperties.KEY_CERTIFICATES_KEY_STORE_PASSWORD).toCharArray();
    kmf.init(
        KeyStore.getInstance(
            new File(getServerProperty(ServerProperties.KEY_CERTIFICATES_KEY_STORE_PATH)),
            password),
        password);

    final String[] protocols =
        getServerProperty(ServerProperties.KEY_HTTPS_ENABLED_PROTOCOLS).split("\\s*,\\s*|\\s+");
    final List<String> ciphers =
        Arrays.asList(
            getServerProperty(ServerProperties.KEY_HTTPS_ENABLED_CIPHER_SUITES)
                .split("\\s*,\\s*|\\s+"));
    final ApplicationProtocolConfig config =
        new ApplicationProtocolConfig(
            Protocol.NPN_AND_ALPN,
            SelectorFailureBehavior.FATAL_ALERT,
            SelectedListenerFailureBehavior.FATAL_ALERT,
            protocols);
    final SslContextBuilder builder =
        GrpcSslContexts.configure(
            SslContextBuilder.forServer(kmf).applicationProtocolConfig(config).ciphers(ciphers));

    return builder.build();
  }

  /**
   * @return A list of services which should be added to the gRPC end-point.
   */
  private List<ServerServiceDefinition> getEnabledServices() {
    final List<ServerServiceDefinition> services = new LinkedList<>();

    final AccessControlManagementServiceImpl accessControlManagementService =
        new AccessControlManagementServiceImpl();
    services.add(
        ServerInterceptors.intercept(
            accessControlManagementService, loggingInterceptor, authInterceptor));

    final GeneralInfoServiceImpl generalInfoService = new GeneralInfoServiceImpl();
    services.add(
        ServerInterceptors.intercept(generalInfoService, loggingInterceptor, authInterceptor));

    final FileTransmissionServiceImpl fileTransmissionService = new FileTransmissionServiceImpl();
    services.add(
        ServerInterceptors.intercept(fileTransmissionService, loggingInterceptor, authInterceptor));

    final EntityTransactionServiceImpl entityTransactionService =
        new EntityTransactionServiceImpl(fileTransmissionService);
    services.add(
        ServerInterceptors.intercept(
            entityTransactionService, loggingInterceptor, authInterceptor));

    return services;
  }

  /**
   * Create a server listening on the specified port. If `tls` is true, the server is a HTTPS
   * server. Otherwise, HTTP.
   *
   * @param port
   * @param tls indicate whether the server uses tls or not
   * @return A new, unstarted server instance.
   * @throws UnrecoverableKeyException
   * @throws NoSuchAlgorithmException
   * @throws KeyStoreException
   * @throws CertificateException
   * @throws IOException
   */
  private Server buildServer(final int port, final boolean tls)
      throws UnrecoverableKeyException,
          NoSuchAlgorithmException,
          KeyStoreException,
          CertificateException,
          IOException {
    final NettyServerBuilder builder = NettyServerBuilder.forPort(port);

    if (tls) {
      final SslContext sslContext = buildSslContext();
      builder.sslContext(sslContext);
    }
    for (final ServerServiceDefinition service : getEnabledServices()) {
      builder.addService(service);
    }

    return builder.build();
  }

  /**
   * Main method of the gRPC end-point which starts the server(s) with HTTP or HTTPS. Whether a
   * server is started or not depends on the server properites {@link
   * ServerProperties#KEY_GRPC_SERVER_PORT_HTTP} and {@link
   * ServerProperties#KEY_GRPC_SERVER_PORT_HTTPS}.
   *
   * @throws IOException
   * @throws InterruptedException
   * @throws KeyStoreException
   * @throws NoSuchAlgorithmException
   * @throws CertificateException
   * @throws UnrecoverableKeyException
   */
  public static void startServer()
      throws IOException,
          InterruptedException,
          KeyStoreException,
          NoSuchAlgorithmException,
          CertificateException,
          UnrecoverableKeyException {

    boolean started = false;
    final String port_https_str = getServerProperty(ServerProperties.KEY_GRPC_SERVER_PORT_HTTPS);
    if (port_https_str != null && !port_https_str.isEmpty()) {
      final Integer port_https = Integer.parseInt(port_https_str);
      final Server server = instance.buildServer(port_https, true);
      CaosDBServer.addPreShutdownHook(new ServerStopper(server));
      server.start();
      started = true;
      logger.info("Started GRPC (HTTPS) on port {}", port_https);
    }

    final String port_http_str = getServerProperty(ServerProperties.KEY_GRPC_SERVER_PORT_HTTP);
    if (port_http_str != null && !port_http_str.isEmpty()) {
      final Integer port_http = Integer.parseInt(port_http_str);

      final Server server = instance.buildServer(port_http, false);
      CaosDBServer.addPreShutdownHook(new ServerStopper(server));
      server.start();
      logger.info("Started GRPC (HTTP) on port {}", port_http);

    } else if (!started) {
      logger.warn(
          "No GRPC Server has been started. Please configure {} or {} to do so.",
          ServerProperties.KEY_GRPC_SERVER_PORT_HTTP,
          ServerProperties.KEY_GRPC_SERVER_PORT_HTTPS);
    }
  }

  private static class ServerStopper implements Runnable {

    private final Server server;

    public ServerStopper(final Server server) {
      this.server = server;
    }

    @Override
    public void run() {
      try {
        if (!server.isShutdown()) {
          server.shutdown();
          server.awaitTermination(60, TimeUnit.SECONDS);
        }
      } catch (final InterruptedException e) {
        logger.warn("Could not shutdown the GRPC server on port {}", server.getPort());
      }
    }
  }
}
