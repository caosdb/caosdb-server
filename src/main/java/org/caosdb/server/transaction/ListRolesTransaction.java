/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2021 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2021 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.caosdb.server.transaction;

import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.caosdb.server.accessControl.ACMPermissions;
import org.caosdb.server.accessControl.Role;
import org.caosdb.server.database.backend.transaction.ListRoles;
import org.caosdb.server.database.proto.ProtoUser;

public class ListRolesTransaction extends AccessControlTransaction {

  private List<Role> roles = null;

  @Override
  protected void transaction() throws Exception {
    Subject currentUser = SecurityUtils.getSubject();
    roles =
        execute(new ListRoles(), getAccess()).getRoles().stream()
            .filter(
                role ->
                    currentUser.isPermitted(
                        ACMPermissions.PERMISSION_RETRIEVE_ROLE_DESCRIPTION(role.name)))
            .collect(Collectors.toList());

    // remove users. the list will only contain name and description.
    for (Role role : roles) {
      if (role.users != null) {
        Iterator<ProtoUser> iterator = role.users.iterator();
        while (iterator.hasNext()) {
          ProtoUser user = iterator.next();
          if (!currentUser.isPermitted(
              ACMPermissions.PERMISSION_RETRIEVE_USER_ROLES(user.realm, user.name))) {
            iterator.remove();
          }
        }
      }
    }
  }

  public List<Role> getRoles() {
    return roles;
  }
}
