/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.transaction;

import java.util.Set;
import org.apache.shiro.SecurityUtils;
import org.caosdb.server.accessControl.ACMPermissions;
import org.caosdb.server.database.backend.transaction.RetrieveRole;
import org.caosdb.server.database.backend.transaction.SetPermissionRules;
import org.caosdb.server.permissions.PermissionRule;
import org.caosdb.server.utils.ServerMessages;

public class UpdatePermissionRulesTransaction extends AccessControlTransaction {

  private final Set<PermissionRule> rules;
  private final String role;

  public UpdatePermissionRulesTransaction(final String role, final Set<PermissionRule> rules) {
    this.role = role;
    this.rules = rules;
  }

  @Override
  protected void transaction() throws Exception {
    SecurityUtils.getSubject()
        .checkPermission(ACMPermissions.PERMISSION_UPDATE_ROLE_PERMISSIONS(this.role));

    if (execute(new RetrieveRole(this.role), getAccess()).getRole() == null) {
      throw ServerMessages.ROLE_DOES_NOT_EXIST;
    }

    execute(new SetPermissionRules(this.role, this.rules), getAccess());
  }
}
