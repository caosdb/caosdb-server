/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.transaction;

import org.caosdb.server.database.backend.transaction.GetFileRecordByPath;
import org.caosdb.server.database.backend.transaction.RetrieveSparseEntity;
import org.caosdb.server.entity.EntityInterface;
import org.caosdb.server.entity.container.TransactionContainer;

public class RetrieveSparseEntityByPath extends Transaction<TransactionContainer> {

  private EntityInterface entity;
  private String path;

  public RetrieveSparseEntityByPath(String path) {
    super(new TransactionContainer());
    this.path = path;
  }

  @Override
  public boolean logHistory() {
    return false;
  }

  @Override
  protected void init() throws Exception {
    // acquire weak access
    setAccess(getAccessManager().acquireReadAccess(this));
  }

  @Override
  protected void preCheck() throws InterruptedException, Exception {}

  @Override
  protected void postCheck() {}

  @Override
  protected void preTransaction() throws InterruptedException {}

  @Override
  protected void transaction() throws Exception {
    final GetFileRecordByPath r = execute(new GetFileRecordByPath(path), getAccess());
    RetrieveSparseEntity e = execute(new RetrieveSparseEntity(r.getEntity()), getAccess());
    entity = e.getEntity();
  }

  @Override
  protected void postTransaction() throws Exception {}

  @Override
  protected void cleanUp() {
    // release weak access
    getAccess().release();
  }

  public EntityInterface getEntity() {
    return entity;
  }
}
