/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.transaction;

import java.util.Set;
import org.apache.shiro.SecurityUtils;
import org.caosdb.server.accessControl.ACMPermissions;
import org.caosdb.server.accessControl.Role;
import org.caosdb.server.database.backend.transaction.InsertRole;
import org.caosdb.server.database.backend.transaction.RetrieveRole;
import org.caosdb.server.database.backend.transaction.SetPermissionRules;
import org.caosdb.server.entity.Message;
import org.caosdb.server.utils.ServerMessages;

public class InsertRoleTransaction extends AccessControlTransaction {

  private final Role role;

  public InsertRoleTransaction(final Role role) {
    this.role = role;
  }

  private void checkPermissions() throws Message {
    SecurityUtils.getSubject().checkPermission(ACMPermissions.PERMISSION_INSERT_ROLE());

    if (execute(new RetrieveRole(this.role.name), getAccess()).getRole() != null) {
      throw ServerMessages.ROLE_NAME_IS_NOT_UNIQUE;
    }
  }

  @Override
  protected void transaction() throws Exception {
    checkPermissions();

    execute(new InsertRole(this.role), getAccess());
    if (this.role.permission_rules != null) {
      execute(
          new SetPermissionRules(this.role.name, Set.copyOf(this.role.permission_rules)),
          getAccess());
    }
    RetrieveRole.removeCached(this.role.name);
  }
}
