/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2021 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2021 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.caosdb.server.transaction;

import java.util.HashSet;
import java.util.Set;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.caosdb.server.accessControl.ACMPermissions;
import org.caosdb.server.accessControl.Principal;
import org.caosdb.server.accessControl.UserSources;
import org.caosdb.server.accessControl.UserStatus;
import org.caosdb.server.database.backend.transaction.RetrieveRole;
import org.caosdb.server.database.backend.transaction.RetrieveUser;
import org.caosdb.server.database.backend.transaction.SetPassword;
import org.caosdb.server.database.backend.transaction.UpdateUser;
import org.caosdb.server.database.backend.transaction.UpdateUserRoles;
import org.caosdb.server.database.exceptions.TransactionException;
import org.caosdb.server.database.proto.ProtoUser;
import org.caosdb.server.entity.Entity;
import org.caosdb.server.entity.EntityID;
import org.caosdb.server.entity.Message;
import org.caosdb.server.entity.RetrieveEntity;
import org.caosdb.server.entity.container.RetrieveContainer;
import org.caosdb.server.utils.EntityStatus;
import org.caosdb.server.utils.ServerMessages;
import org.caosdb.server.utils.Utils;
import org.jdom2.Element;

/**
 * This transaction also checks if the current user has sufficient permissions to make the update.
 */
public class UpdateUserTransaction extends AccessControlTransaction {

  private final String password;
  private final ProtoUser user;
  private HashSet<String> newRoles;
  private Set<String> oldRoles;

  public UpdateUserTransaction(
      final String realm,
      final String username,
      final UserStatus status,
      final String email,
      final String entity,
      final String password) {
    this.user = new ProtoUser();
    this.user.realm =
        realm == null
            ? UserSources.guessRealm(username, UserSources.getInternalRealm().getName())
            : realm;
    this.user.name = username;
    this.user.status = status;
    this.user.email = email;
    this.user.entity = entity;
    this.password = password;
  }

  public UpdateUserTransaction(ProtoUser user, String password) {
    this.user = user;
    if (this.user.realm == null) {
      this.user.realm =
          UserSources.guessRealm(this.user.name, UserSources.getInternalRealm().getName());
    }
    this.password = password;
  }

  private void checkPermissions() throws Message {
    Principal principal = new Principal(this.user.realm, this.user.name);
    ProtoUser oldUser = execute(new RetrieveUser(principal), getAccess()).getUser();
    if (oldUser == null) {
      throw ServerMessages.ACCOUNT_DOES_NOT_EXIST;
    }

    if (this.password != null) {
      // passwords cannot be updated for any but the internal password
      // storage.
      if (!this.user.realm.equals(UserSources.getInternalRealm().getName())) {
        throw new TransactionException(
            "The password from the realm '"
                + this.user.realm
                + "' cannot be updated. Only the users from the realm '"
                + UserSources.getInternalRealm().getName()
                + "' can change their passwords from within caosdb.");
      }

      SecurityUtils.getSubject()
          .checkPermission(
              ACMPermissions.PERMISSION_UPDATE_USER_PASSWORD(this.user.realm, this.user.name));

      if (this.password.isEmpty()) {
        // reset password
        execute(new SetPassword(this.user.name, null), getAccess());
      } else {
        Utils.checkPasswordStrength(this.password);
        execute(new SetPassword(this.user.name, this.password), getAccess());
      }
    }

    if (this.user.roles != null) {
      Set<String> oldRoles = oldUser.roles;
      if (!this.user.roles.equals(oldRoles)) {
        SecurityUtils.getSubject()
            .checkPermission(
                ACMPermissions.PERMISSION_UPDATE_USER_ROLES(this.user.realm, this.user.name));
      }
      this.oldRoles = oldRoles;
      this.newRoles = this.user.roles;
    }
  }

  @Override
  protected void transaction() throws Exception {
    checkPermissions();

    if (isToBeUpdated()) {
      execute(new UpdateUser(this.user), getAccess());
    }
    if (this.newRoles != null) {
      execute(new UpdateUserRoles(this.user.realm, this.user.name, this.user.roles), getAccess());
      RetrieveRole.removeCached(newRoles);
      if (this.oldRoles != null) {
        RetrieveRole.removeCached(oldRoles);
      }
    }
  }

  private boolean isToBeUpdated() throws Exception {
    Subject current_user = SecurityUtils.getSubject();
    boolean isToBeUpdated = false;
    final ProtoUser validUser =
        execute(new RetrieveUser(new Principal(this.user.realm, this.user.name)), getAccess())
            .getUser();
    if (this.user.status != null && (validUser == null || this.user.status != validUser.status)) {
      current_user.checkPermission(
          ACMPermissions.PERMISSION_UPDATE_USER_STATUS(this.user.realm, this.user.name));
      isToBeUpdated = true;
    } else if (validUser != null) {
      this.user.status = validUser.status;
    }
    if (this.user.email != null
        && (validUser == null || !this.user.email.equals(validUser.email))) {
      current_user.checkPermission(
          ACMPermissions.PERMISSION_UPDATE_USER_EMAIL(this.user.realm, this.user.name));
      if (!Utils.isRFC822Compliant(this.user.email)) {
        throw ServerMessages.EMAIL_NOT_WELL_FORMED;
      }

      isToBeUpdated = true;
    } else if (validUser != null) {
      this.user.email = validUser.email;
    }
    if (this.user.entity != null
        && (validUser == null || !this.user.entity.equals(validUser.entity))) {
      current_user.checkPermission(
          ACMPermissions.PERMISSION_UPDATE_USER_ENTITY(this.user.realm, this.user.name));
      isToBeUpdated = true;

      if (this.user.entity.isEmpty()) {
        // this means that the entity is to be reset.
        this.user.entity = null;
      } else {
        checkEntityExists(new EntityID(this.user.entity));
      }
    } else if (validUser != null) {
      this.user.entity = validUser.entity;
    }

    return isToBeUpdated;
  }

  public static void checkEntityExists(final EntityID entity) throws Exception {
    final RetrieveContainer c = new RetrieveContainer(null, null, null, null);
    final Entity e = new RetrieveEntity(entity);
    c.add(e);
    final Retrieve t = new Retrieve(c);
    t.execute();
    if (e.getEntityStatus() != EntityStatus.VALID) {
      throw ServerMessages.ENTITY_DOES_NOT_EXIST;
    }
  }

  public Element getUserElement() {
    return RetrieveUserTransaction.getUserElement(this.user);
  }
}
