/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.transaction;

import java.util.Iterator;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.AuthorizationException;
import org.apache.shiro.subject.Subject;
import org.caosdb.server.accessControl.ACMPermissions;
import org.caosdb.server.accessControl.Role;
import org.caosdb.server.database.backend.transaction.RetrieveRole;
import org.caosdb.server.database.proto.ProtoUser;
import org.caosdb.server.utils.ServerMessages;

public class RetrieveRoleTransaction extends AccessControlTransaction {

  private final String name;
  private Role role;
  private Subject transactor;

  public RetrieveRoleTransaction(final String name, Subject transactor) {
    this.transactor = transactor;
    this.name = name;
  }

  public RetrieveRoleTransaction(final String name) {
    this(name, SecurityUtils.getSubject());
  }

  @Override
  protected void transaction() throws Exception {
    if (!transactor.isPermitted(ACMPermissions.PERMISSION_RETRIEVE_ROLE_DESCRIPTION(this.name))) {
      throw new AuthorizationException("You are not permitted to retrieve this role");
    }
    this.role = execute(new RetrieveRole(this.name), getAccess()).getRole();
    if (this.role == null) {
      throw ServerMessages.ROLE_DOES_NOT_EXIST;
    }
    if (this.role.users != null) {
      Iterator<ProtoUser> iterator = this.role.users.iterator();
      while (iterator.hasNext()) {
        ProtoUser user = iterator.next();
        if (!transactor.isPermitted(
            ACMPermissions.PERMISSION_RETRIEVE_USER_ROLES(user.realm, user.name))) {
          iterator.remove();
        }
      }
    }
  }

  public Role getRole() {
    return this.role;
  }
}
