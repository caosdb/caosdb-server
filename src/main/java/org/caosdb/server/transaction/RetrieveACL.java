/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2021 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2021 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.caosdb.server.transaction;

import com.google.protobuf.ProtocolStringList;
import java.util.UUID;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.AuthorizationException;
import org.caosdb.server.database.backend.transaction.RetrieveEntityACLTransaction;
import org.caosdb.server.entity.EntityID;
import org.caosdb.server.entity.EntityInterface;
import org.caosdb.server.entity.RetrieveEntity;
import org.caosdb.server.entity.container.TransactionContainer;
import org.caosdb.server.permissions.EntityACL;
import org.caosdb.server.permissions.EntityPermission;

public class RetrieveACL extends Transaction<TransactionContainer> {

  public RetrieveACL(ProtocolStringList idList) {
    super(
        new TransactionContainer(
            SecurityUtils.getSubject(), System.currentTimeMillis(), UUID.randomUUID().toString()));
    for (String strId : idList) {
      getContainer().add(new RetrieveEntity(new EntityID(strId)));
    }
  }

  @Override
  public boolean logHistory() {
    return false;
  }

  @Override
  protected void init() throws Exception {
    // acquire weak access
    setAccess(getAccessManager().acquireReadAccess(this));
  }

  @Override
  protected void preCheck() throws InterruptedException, Exception {}

  @Override
  protected void postCheck() {}

  @Override
  protected void preTransaction() throws InterruptedException {}

  @Override
  protected void transaction() throws Exception {
    RetrieveEntityACLTransaction t = new RetrieveEntityACLTransaction(null);
    for (EntityInterface e : getContainer()) {
      EntityACL acl = execute(t.reuse(e.getId()), getAccess()).getEntityAcl();
      if (acl != null && acl.isPermitted(getTransactor(), EntityPermission.RETRIEVE_ACL)) {
        e.setEntityACL(acl);
      } else if (acl != null
          && acl.isPermitted(getTransactor(), EntityPermission.RETRIEVE_ENTITY)) {
        throw new AuthorizationException("You are not permitted to update this entity's ACL.");
      } else {
        e.addError(org.caosdb.server.utils.ServerMessages.ENTITY_DOES_NOT_EXIST);
      }
    }
  }

  @Override
  protected void postTransaction() throws Exception {}

  @Override
  protected void cleanUp() {
    getAccess().release();
  }
}
