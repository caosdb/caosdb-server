/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.transaction;

import java.util.HashSet;
import org.caosdb.server.database.backend.transaction.RetrievePermissionRules;
import org.caosdb.server.database.backend.transaction.RetrieveRole;
import org.caosdb.server.permissions.PermissionRule;
import org.caosdb.server.utils.ServerMessages;

public class RetrievePermissionRulesTransaction extends AccessControlTransaction {

  private final String role;
  private HashSet<PermissionRule> rules;

  public RetrievePermissionRulesTransaction(final String role) {
    this.role = role;
  }

  @Override
  protected void transaction() throws Exception {
    if (execute(new RetrieveRole(this.role), getAccess()).getRole() == null) {
      throw ServerMessages.ROLE_DOES_NOT_EXIST;
    }
    this.rules = execute(new RetrievePermissionRules(this.role), getAccess()).getRules();
  }

  public HashSet<PermissionRule> getRules() {
    return this.rules;
  }
}
