/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.transaction;

import java.util.Set;
import org.caosdb.datetime.UTCDateTime;
import org.caosdb.server.accessControl.Principal;
import org.caosdb.server.accessControl.UserSources;
import org.caosdb.server.utils.ServerMessages;
import org.jdom2.Element;

public class RetrieveUserRolesTransaction implements TransactionInterface {

  private final String user;
  private Set<String> roles;
  private final String realm;
  private UTCDateTime timestamp;

  public RetrieveUserRolesTransaction(final String realm, final String user) {
    this.timestamp = UTCDateTime.SystemMillisToUTCDateTime(System.currentTimeMillis());
    this.realm = realm;
    this.user = user;
  }

  public static Element getUserRolesElement(final Set<String> roles) {
    final Element user_elem = new Element("Roles");
    if (roles != null) {
      for (final String role : roles) {
        final Element role_elem = new Element("Role");
        role_elem.setAttribute("name", role);
        user_elem.addContent(role_elem);
      }
    }
    return user_elem;
  }

  public Element getUserRolesElement() {
    return getUserRolesElement(getRoles());
  }

  @Override
  public void execute() throws Exception {
    if (UserSources.isUserExisting(new Principal(this.realm, this.user))) {
      this.roles = UserSources.resolveRoles(this.realm, this.user);
    } else {
      throw ServerMessages.ACCOUNT_DOES_NOT_EXIST;
    }
  }

  public Set<String> getRoles() {
    return this.roles;
  }

  @Override
  public UTCDateTime getTimestamp() {
    return timestamp;
  }
}
