/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2021 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2021 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.caosdb.server.transaction;

import org.apache.shiro.SecurityUtils;
import org.caosdb.server.CaosDBServer;
import org.caosdb.server.ServerProperties;
import org.caosdb.server.accessControl.ACMPermissions;
import org.caosdb.server.accessControl.Principal;
import org.caosdb.server.accessControl.UserSources;
import org.caosdb.server.accessControl.UserStatus;
import org.caosdb.server.database.backend.transaction.SetPassword;
import org.caosdb.server.database.backend.transaction.UpdateUser;
import org.caosdb.server.database.backend.transaction.UpdateUserRoles;
import org.caosdb.server.database.proto.ProtoUser;
import org.caosdb.server.entity.EntityID;
import org.caosdb.server.entity.Message;
import org.caosdb.server.utils.ServerMessages;
import org.caosdb.server.utils.Utils;
import org.jdom2.Element;

public class InsertUserTransaction extends AccessControlTransaction {

  private final ProtoUser user;
  private final String password;

  public InsertUserTransaction(
      final String username,
      final String password,
      final String email,
      final UserStatus status,
      final String entity) {
    this(new ProtoUser(), password);
    this.user.realm = UserSources.getInternalRealm().getName();
    this.user.name = username;
    this.user.email = email;
    this.user.status = status;
    this.user.entity = entity;
  }

  public InsertUserTransaction(ProtoUser user, String password) {
    this.user = user;
    this.password = password;
  }

  @Override
  protected void transaction() throws Exception {
    SecurityUtils.getSubject()
        .checkPermission(ACMPermissions.PERMISSION_INSERT_USER(this.user.realm));

    checkUserName(this.user.name);

    if (this.user.email != null && !Utils.isRFC822Compliant(this.user.email)) {
      throw ServerMessages.EMAIL_NOT_WELL_FORMED;
    }

    if (this.user.entity != null) {
      UpdateUserTransaction.checkEntityExists(new EntityID(this.user.entity));
    }

    if (this.password != null) {
      Utils.checkPasswordStrength(this.password);
    }

    execute(new SetPassword(this.user.name, this.password), getAccess());
    execute(new UpdateUser(this.user), getAccess());
    execute(new UpdateUserRoles(this.user.realm, this.user.name, this.user.roles), getAccess());
  }

  /*
   * Check requirements for user names (length, character set). Default config is POSIX compliant.
   */
  private void checkUserName(String name) throws Message {
    String regex = CaosDBServer.getServerProperty(ServerProperties.KEY_USER_NAME_VALID_REGEX);
    if (!name.matches(regex)) {
      throw ServerMessages.INVALID_USER_NAME(
          CaosDBServer.getServerProperty(ServerProperties.KEY_USER_NAME_INVALID_MESSAGE));
    }

    if (UserSources.isUserExisting(new Principal(this.user.realm, this.user.name))) {
      throw ServerMessages.ACCOUNT_NAME_NOT_UNIQUE;
    }
  }

  public Element getUserElement() {
    return RetrieveUserTransaction.getUserElement(this.user);
  }
}
