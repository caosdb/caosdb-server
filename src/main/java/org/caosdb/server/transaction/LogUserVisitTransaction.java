/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2021 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2021 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.caosdb.server.transaction;

import org.caosdb.server.database.backend.transaction.LogUserVisit;

public class LogUserVisitTransaction extends AccessControlTransaction {

  private String realm;
  private String username;
  private String type;
  private long timestamp;

  public LogUserVisitTransaction(long timestamp, String realm, String username, String type) {
    this.timestamp = timestamp;
    this.realm = realm;
    this.username = username;
    this.type = type;
  }

  @Override
  protected void transaction() throws Exception {
    execute(new LogUserVisit(timestamp, realm, username, type), getAccess());
  }
}
