/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2019-2021,2023 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2019-2021,2023 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.caosdb.server;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Properties;
import java.util.TimeZone;
import java.util.UUID;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.config.Ini;
import org.apache.shiro.config.Ini.Section;
import org.apache.shiro.env.BasicIniEnvironment;
import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.util.ThreadContext;
import org.caosdb.server.accessControl.AnonymousAuthenticationToken;
import org.caosdb.server.accessControl.AnonymousRealm;
import org.caosdb.server.accessControl.AuthenticationUtils;
import org.caosdb.server.accessControl.CaosDBAuthorizingRealm;
import org.caosdb.server.accessControl.CaosDBDefaultRealm;
import org.caosdb.server.accessControl.ConsumedInfoCleanupJob;
import org.caosdb.server.accessControl.OneTimeAuthenticationToken;
import org.caosdb.server.accessControl.SessionToken;
import org.caosdb.server.accessControl.SessionTokenRealm;
import org.caosdb.server.database.BackendTransaction;
import org.caosdb.server.database.access.Access;
import org.caosdb.server.database.backend.transaction.RetrieveDatatypes;
import org.caosdb.server.database.misc.TransactionBenchmark;
import org.caosdb.server.datatype.AbstractDatatype;
import org.caosdb.server.entity.EntityInterface;
import org.caosdb.server.entity.Role;
import org.caosdb.server.entity.container.Container;
import org.caosdb.server.grpc.GRPCServer;
import org.caosdb.server.jobs.core.AccessControl;
import org.caosdb.server.jobs.core.CheckStateTransition;
import org.caosdb.server.logging.RequestErrorLogMessage;
import org.caosdb.server.resource.AuthenticationResource;
import org.caosdb.server.resource.DefaultResource;
import org.caosdb.server.resource.EntityOwnerResource;
import org.caosdb.server.resource.EntityPermissionsResource;
import org.caosdb.server.resource.FileSystemResource;
import org.caosdb.server.resource.InfoResource;
import org.caosdb.server.resource.LogoutResource;
import org.caosdb.server.resource.PermissionRulesResource;
import org.caosdb.server.resource.RolesResource;
import org.caosdb.server.resource.ScriptingResource;
import org.caosdb.server.resource.ServerPropertiesResource;
import org.caosdb.server.resource.SharedFileResource;
import org.caosdb.server.resource.ThumbnailsResource;
import org.caosdb.server.resource.UserResource;
import org.caosdb.server.resource.UserRolesResource;
import org.caosdb.server.resource.Webinterface;
import org.caosdb.server.resource.WebinterfaceBuildNumber;
import org.caosdb.server.resource.transaction.EntityNamesResource;
import org.caosdb.server.resource.transaction.EntityResource;
import org.caosdb.server.scripting.ScriptingPermissions;
import org.caosdb.server.transaction.ChecksumUpdater;
import org.caosdb.server.utils.FileUtils;
import org.caosdb.server.utils.Initialization;
import org.caosdb.server.utils.Observable;
import org.caosdb.server.utils.Observer;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.impl.StdSchedulerFactory;
import org.restlet.Application;
import org.restlet.Component;
import org.restlet.Context;
import org.restlet.Request;
import org.restlet.Response;
import org.restlet.Restlet;
import org.restlet.Server;
import org.restlet.data.CookieSetting;
import org.restlet.data.Parameter;
import org.restlet.data.Protocol;
import org.restlet.data.Reference;
import org.restlet.data.ServerInfo;
import org.restlet.data.Status;
import org.restlet.engine.Engine;
import org.restlet.routing.Redirector;
import org.restlet.routing.Route;
import org.restlet.routing.Router;
import org.restlet.routing.Template;
import org.restlet.routing.TemplateRoute;
import org.restlet.routing.Variable;
import org.restlet.util.Series;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CaosDBServer extends Application {

  private static Logger logger = LoggerFactory.getLogger(CaosDBServer.class);
  private static ServerProperties SERVER_PROPERTIES = null;
  private static Component component = null;
  private static ArrayList<Runnable> postShutdownHooks = new ArrayList<Runnable>();
  private static ArrayList<Runnable> preShutdownHooks = new ArrayList<Runnable>();
  private static boolean START_BACKEND = true;
  private static boolean NO_TLS = false;
  public static final String REQUEST_TIME_LOGGER = "REQUEST_TIME_LOGGER";
  public static final String REQUEST_ERRORS_LOGGER = "REQUEST_ERRORS_LOGGER";
  private static boolean USE_CACHE = true;
  private static Scheduler SCHEDULER;

  public static String getServerProperty(final String key) {
    return getServerProperties().getProperty(key);
  }

  /**
   * This main method starts up a web application that will listen on a port defined in the config
   * file.
   *
   * @param args One option temporarily (for testing) available: silent: If present: disable
   *     System.out-stream (stream to a NullPrintStream). This makes the response of the database
   *     amazingly faster.
   * @throws IOException
   * @throws FileNotFoundException
   * @throws SecurityException
   * @throws Exception If problems occur.
   */
  public static void main(final String[] args)
      throws SecurityException, FileNotFoundException, IOException {
    try {
      parseArguments(args);
      initScheduler();
      initServerProperties();
      initCaching();
      initTimeZone();
      initOneTimeTokens();
      initShiro();
      initBackend();
      initWebServer();
      initShutDownHook();
    } catch (final Exception e1) {
      logger.error("Could not start the server.", e1);
      System.exit(1);
    }
  }

  /**
   * Parse the command line arguments.
   *
   * <ul>
   *   <li>"--no-backend": flag to run caosdb without any backend (for testing purposes)
   *   <li>"--no-tls": flag to start only a http server (no https server)
   * </ul>
   *
   * <p>The --no-backend flag is only available in the debug mode which is controlled by the
   * `caosdb.debug` JVM Property.
   *
   * @param args
   */
  private static void parseArguments(final String[] args) {
    for (final String s : args) {
      if (s.equals("--no-backend")) {
        START_BACKEND = false;
      } else if (s.equals("--no-tls")) {
        NO_TLS = true;
      }
    }
    START_BACKEND = START_BACKEND || !isDebugMode(); // always start backend if not in debug mode
  }

  private static void initScheduler() throws SchedulerException {
    SCHEDULER = StdSchedulerFactory.getDefaultScheduler();
    SCHEDULER.start();
  }

  public static void initServerProperties() throws IOException {
    SERVER_PROPERTIES = ServerProperties.initServerProperties();
  }

  public static void initCaching() {
    USE_CACHE =
        !Boolean.parseBoolean(CaosDBServer.getServerProperty(ServerProperties.KEY_CACHE_DISABLE));
  }

  /**
   * Precedence order:
   *
   * <ol>
   *   <li>ServerProperty "TIMEZONE"
   *   <li>JVM property "user.timezone"
   *   <li>Environment variable "TZ"
   *   <li>Output of posix' "date +%Z"
   * </ol>
   *
   * @throws InterruptedException
   * @throws IOException
   */
  public static void initTimeZone() throws InterruptedException, IOException {
    final String serverProperty = getServerProperty(ServerProperties.KEY_TIMEZONE);

    // Setup an observer which updates the server's time zone when the relevant ServerProperty
    // changes (this can only happen when in debug mode).
    if (isDebugMode()) {
      SERVER_PROPERTIES.acceptObserver(
          new Observer() {

            @Override
            public boolean notifyObserver(String e, Observable sender) {
              if (e.equals(ServerProperties.KEY_TIMEZONE)) {

                TimeZone newZoneId =
                    TimeZone.getTimeZone(getServerProperty(ServerProperties.KEY_TIMEZONE));
                TimeZone.setDefault(newZoneId);
              }
              return true; // true means, keep this observer.
            }
          });
    }

    if (serverProperty != null && !serverProperty.isEmpty()) {
      logger.info(
          "SET TIMEZONE = "
              + serverProperty
              + " from ServerProperty `"
              + ServerProperties.KEY_TIMEZONE
              + "`.");
      TimeZone.setDefault(TimeZone.getTimeZone(ZoneId.of(serverProperty)));
      logger.info("TIMEZONE = " + TimeZone.getDefault());
      return;
    }

    final String prop = System.getProperty("user.timezone");
    if (prop != null && !prop.isEmpty()) {
      logger.info("SET TIMEZONE = " + prop + " from JVM property `user.timezone`.");
      TimeZone.setDefault(TimeZone.getTimeZone(ZoneId.of(prop)));
      logger.info("TIMEZONE = " + TimeZone.getDefault());
      return;
    }

    final String envVar = System.getenv("TZ");
    if (envVar != null && !envVar.isEmpty()) {
      logger.info("SET TIMEZONE = " + envVar + " from evironment variable `TZ`.");
      TimeZone.setDefault(TimeZone.getTimeZone(ZoneId.of(envVar)));
      logger.info("TIMEZONE = " + TimeZone.getDefault());
      return;
    }

    final String fromDate = getTimeZoneFromDate();
    if (fromDate != null && fromDate.isEmpty()) {
      logger.info("SET TIMEZONE = " + fromDate + " from `date +%Z`.");
      TimeZone.setDefault(TimeZone.getTimeZone(ZoneId.of(fromDate)));
      logger.info("TIMEZONE = " + TimeZone.getDefault());
      return;
    }

    logger.warn("COULD NOT SET TIMEZONE. DEFAULTS TO " + TimeZone.getDefault());
  }

  public static String getTimeZoneFromDate() throws InterruptedException, IOException {
    final StringBuffer outputStringBuffer = new StringBuffer();
    final Process cmd = Runtime.getRuntime().exec(new String[] {"date", "+%z"});
    final int status = cmd.waitFor();

    if (status != 0) {
      logger.warn(
          "Could not determine time zone from `date +%z`. The command returned with exit code "
              + cmd.exitValue());
      return null;
    }
    final Reader r = new InputStreamReader(cmd.getInputStream());
    final BufferedReader buf = new BufferedReader(r);
    String line;
    while ((line = buf.readLine()) != null) {
      outputStringBuffer.append(line);
    }

    if (outputStringBuffer.length() > 0) {
      return outputStringBuffer.toString().trim();
    } else {
      throw new RuntimeException("Output of `date +%z` command was empty.");
    }
  }

  public static void initOneTimeTokens() throws Exception {
    OneTimeAuthenticationToken.initConfig();
    ConsumedInfoCleanupJob.scheduleDaily();
  }

  public static void initShiro() {
    // init Shiro (user authentication/authorization and session management)
    final Ini config = getShiroConfig();
    initShiro(config);

    // Init ACMPermissions.ALL -  the whole point is to fill all these
    // permissions into ACMPermissions.ALL for retrieval by clients. If we don't
    // do this, every work, but the list of known permissions grows over time
    // (as soon as these classes are used for the first time)
    logger.debug("Register permissions: ", ScriptingPermissions.PERMISSION_EXECUTION("*"));
    logger.debug("Register permissions: ", CheckStateTransition.STATE_PERMISSIONS.toString());
    logger.debug(
        "Register permissions: ", CheckStateTransition.PERMISSION_STATE_FORCE_FINAL.toString());
    logger.debug("Register permissions: ", AccessControl.TRANSACTION_PERMISSIONS.toString());
  }

  public static Ini getShiroConfig() {
    final Ini config = new Ini();
    final Section mainSec = config.addSection("main");
    mainSec.put("CaosDB", CaosDBDefaultRealm.class.getCanonicalName());
    mainSec.put("SessionTokenValidator", SessionTokenRealm.class.getCanonicalName());
    mainSec.put("CaosDBAuthorizingRealm", CaosDBAuthorizingRealm.class.getCanonicalName());
    mainSec.put("AnonymousRealm", AnonymousRealm.class.getCanonicalName());
    mainSec.put(
        "securityManager.realms",
        "$CaosDB, $SessionTokenValidator, $CaosDBAuthorizingRealm, $AnonymousRealm");

    // disable shiro's default session management. We have quasi-stateless
    // sessions
    // using our SessionToken class.
    mainSec.put(
        "securityManager.subjectDAO.sessionStorageEvaluator.sessionStorageEnabled", "false");
    return config;
  }

  public static void initShiro(final Ini config) {
    final BasicIniEnvironment env = new BasicIniEnvironment(config);
    final SecurityManager securityManager = env.getSecurityManager();
    SecurityUtils.setSecurityManager(securityManager);
  }

  public static void initBackend() throws Exception {
    if (START_BACKEND) {
      try (final Initialization init = Initialization.setUp()) {
        BackendTransaction.init();

        // init benchmark
        TransactionBenchmark.getRootInstance();

        // Role
        Role.init(init.getAccess());

        // Data types
        initDatatypes(init.getAccess());

        // check for chown script
        FileUtils.testChownScript();

        // ChecksumUpdater
        ChecksumUpdater.start();

        ThreadContext.remove();
      }
    } else {
      logger.info("NO BACKEND");
    }
  }

  private static void initWebServer() throws Exception {
    /* For the host, the property can't be used directly since blank should mean
    all interfaces, not localhost; which means replacing a blank value with
    null. */
    final String server_bind_address_property =
        getServerProperty(ServerProperties.KEY_SERVER_BIND_ADDRESS);
    final String server_bind_address =
        server_bind_address_property.length() == 0 ? null : server_bind_address_property;
    final int port_https =
        Integer.parseInt(getServerProperty(ServerProperties.KEY_SERVER_PORT_HTTPS));
    final int port_http =
        Integer.parseInt(getServerProperty(ServerProperties.KEY_SERVER_PORT_HTTP));
    int port_redirect_https;
    try {
      port_redirect_https =
          Integer.parseInt(getServerProperty(ServerProperties.KEY_REDIRECT_HTTP_TO_HTTPS_PORT));
    } catch (final NumberFormatException e) {
      port_redirect_https = port_https;
    }
    final int initialConnections =
        Integer.parseInt(getServerProperty(ServerProperties.KEY_INITIAL_CONNECTIONS));
    final int maxTotalConnections =
        Integer.parseInt(getServerProperty(ServerProperties.KEY_MAX_CONNECTIONS));

    if (NO_TLS) {
      runHTTPServer(server_bind_address, port_http, initialConnections, maxTotalConnections);
    } else {
      runHTTPSServer(
          server_bind_address,
          port_https,
          port_http,
          port_redirect_https,
          initialConnections,
          maxTotalConnections);
    }
    GRPCServer.startServer();
  }

  private static void initDatatypes(final Access access) throws Exception {
    final RetrieveDatatypes t = new RetrieveDatatypes();
    t.setAccess(access);
    t.executeTransaction();
    final Container<? extends EntityInterface> dts = t.getDatatypes();
    AbstractDatatype.initializeDatatypes(dts);
  }

  /**
   * Start an http server. This is insecure!
   *
   * @throws Exception
   */
  private static void runHTTPServer(
      final String server_bind_address,
      final int port_http,
      final int initialConnections,
      final int maxTotalConnections)
      throws Exception {
    Engine.getInstance()
        .getRegisteredServers()
        .add(new org.restlet.ext.jetty.HttpServerHelper(null));

    // Create a component.
    component = new CaosDBComponent();

    final Server httpServer =
        new Server(
            (Context) null,
            Arrays.asList(Protocol.HTTP),
            server_bind_address,
            port_http,
            (Restlet) null,
            "org.restlet.ext.jetty.HttpServerHelper");
    component.getServers().add(httpServer);

    // set initial and maximal connections
    final Series<Parameter> parameters = httpServer.getContext().getParameters();
    parameters.add("initialConnections", Integer.toString(initialConnections));
    parameters.add("maxTotalConnections", Integer.toString(maxTotalConnections));

    // Create an application (this class).
    final Application application = new CaosDBServer();
    application
        .getStatusService()
        .setContactEmail(getServerProperty(ServerProperties.KEY_ADMIN_EMAIL));
    application
        .getStatusService()
        .setHomeRef(new Reference(getServerProperty(ServerProperties.KEY_CONTEXT_ROOT) + "/"));

    // Attach the application to the component with a defined contextRoot.
    component
        .getDefaultHost()
        .attach(getServerProperty(ServerProperties.KEY_CONTEXT_ROOT), application);

    component.start();

    if (port_http == 0) {
      System.err.println("ACTUAL HTTP PORT:" + httpServer.getActualPort());
    }
  }

  /**
   * Starts a https server running on the specified `port_https`, listening also for http
   * connections on `port_http` and redirect any http connections to `port_redirect_https`.
   *
   * @author Timm Fitschen
   * @param server_bind_address IP address to listen on (null means all interfaces).
   * @param port_https Listen on this port for https connections.
   * @param port_http Listen on this port for http connections and send http-to-https redirect with
   *     different port.
   * @param port_redirect_https Redirect any http connections to this port.
   * @throws Exception if problems occur starting up this server.
   */
  private static void runHTTPSServer(
      final String server_bind_address,
      final int port_https,
      final int port_http,
      final int port_redirect_https,
      final int initialConnections,
      final int maxTotalConnections)
      throws Exception {

    Engine.getInstance().getRegisteredServers().add(new CaosDBServerConnectorHelper(null));

    // Create a component.
    component = new CaosDBComponent();

    final Server httpsServer =
        new Server(
            (Context) null,
            Arrays.asList(Protocol.HTTPS),
            server_bind_address,
            port_https,
            (Restlet) null,
            "org.caosdb.server.CaosDBServerConnectorHelper");
    component.getServers().add(httpsServer);

    // redirector http to https
    if (port_http != 0) {
      logger.info("Redirecting to " + port_redirect_https);
      component
          .getServers()
          .add(Protocol.HTTP, server_bind_address, port_http)
          .setNext(new HttpToHttpsRedirector(port_redirect_https));
    }

    // set initial and maximal connections
    final Series<Parameter> parameters = httpsServer.getContext().getParameters();
    parameters.add("initialConnections", Integer.toString(initialConnections));
    parameters.add("maxTotalConnections", Integer.toString(maxTotalConnections));
    parameters.add("sslContextFactory", "org.restlet.engine.ssl.DefaultSslContextFactory");
    parameters.add(
        "disabledProtocols", getServerProperty(ServerProperties.KEY_HTTPS_DISABLED_PROTOCOLS));
    parameters.add(
        "enabledProtocols", getServerProperty(ServerProperties.KEY_HTTPS_ENABLED_PROTOCOLS));
    parameters.add(
        "enabledCipherSuites", getServerProperty(ServerProperties.KEY_HTTPS_ENABLED_CIPHER_SUITES));
    parameters.add(
        "disabledCipherSuites",
        getServerProperty(ServerProperties.KEY_HTTPS_DISABLED_CIPHER_SUITES));
    parameters.add(
        "keyStorePath", getServerProperty(ServerProperties.KEY_CERTIFICATES_KEY_STORE_PATH));
    parameters.add(
        "keyStorePassword",
        getServerProperty(ServerProperties.KEY_CERTIFICATES_KEY_STORE_PASSWORD));
    parameters.add(
        "keyPassword", getServerProperty(ServerProperties.KEY_CERTIFICATES_KEY_PASSWORD));
    parameters.add("keyStoreType", "JKS");
    parameters.add("certAlias", "1");

    // Create an application (this class).
    final Application application = new CaosDBServer();
    application
        .getStatusService()
        .setContactEmail(
            getServerProperty(ServerProperties.KEY_ADMIN_NAME)
                + " <"
                + getServerProperty(ServerProperties.KEY_ADMIN_EMAIL)
                + ">");

    application
        .getStatusService()
        .setHomeRef(new Reference(getServerProperty(ServerProperties.KEY_CONTEXT_ROOT) + "/"));

    component
        .getDefaultHost()
        .attach(getServerProperty(ServerProperties.KEY_CONTEXT_ROOT), application);

    component.start();

    if (port_https == 0) {
      System.err.println("ACTUAL HTTPS PORT:" + httpsServer.getActualPort());
    }
  }

  /**
   * Specify the dispatching restlet that maps URIs to their associated resources for processing.
   *
   * @return A Router restlet that implements dispatching.
   */
  @Override
  public Restlet createInboundRoot() {
    /** Authenticator. Protects the protectedRouter. */
    final CaosAuthenticator authenticator = new CaosAuthenticator(getContext());

    /** ProtectedRouter. Is protected by the authenticator. */
    final Router protectedRouter =
        new Router(getContext()) {
          @Override
          public void handle(final Request request, final Response response) {
            try {
              super.handle(request, response);
            } catch (final NullPointerException e) {
              response.setStatus(Status.CLIENT_ERROR_REQUEST_URI_TOO_LONG);
              response.setEntity(null);
            }
          }
        };

    /**
     * BaseRouter. Routes the call to some special Resources, which are available without any
     * authentication (e.g. login and start pages, scripts for the web interface). Everything else
     * is forwarded to the Authenticator.
     */
    final Router baseRouter =
        new Router(getContext()) {
          @Override
          public void handle(final Request request, final Response response) {
            try {
              super.handle(request, response);

              // after everything, set session cookies
              setSessionCookies(response);

            } finally {
              // remove subject and all other session data from this thread so
              // that we can reuse the thread.
              ThreadContext.remove();
            }
          }

          private void setSessionCookies(final Response response) {

            final Subject subject = SecurityUtils.getSubject();
            // if authenticated as a normal user: generate and set session cookie.
            if (subject.isAuthenticated()
                && subject.getPrincipal() != AnonymousAuthenticationToken.PRINCIPAL) {
              final SessionToken sessionToken = SessionToken.generate(subject);

              // set session token cookie (httpOnly, secure cookie which
              // is used to recognize a user session)
              final CookieSetting sessionTokenCookie =
                  AuthenticationUtils.createSessionTokenCookie(sessionToken);
              if (sessionTokenCookie != null) {
                response.getCookieSettings().add(sessionTokenCookie);
              }

              // set session timeout cookie (secure cookie which may be
              // used
              // by the user interfaces for anything)
              final CookieSetting sessionTimeoutCookie =
                  AuthenticationUtils.createSessionTimeoutCookie(sessionToken);
              if (sessionTimeoutCookie != null) {
                response.getCookieSettings().add(sessionTimeoutCookie);
              }
            }
          }
        };

    // These routes can be used without logging in:
    baseRouter.attach("/webinterface/version/build", WebinterfaceBuildNumber.class);
    baseRouter
        .attach("/webinterface/{path}", Webinterface.class)
        .getTemplate()
        .getDefaultVariable()
        .setType(Variable.TYPE_URI_PATH);
    baseRouter
        .attach("/login?username={username}", AuthenticationResource.class)
        .setMatchingQuery(true);
    baseRouter.attach("/login", AuthenticationResource.class);
    baseRouter.attach("", authenticator).setMatchingQuery(false);

    // root every other request to the authenticator
    final TemplateRoute tr = baseRouter.attach(authenticator);
    tr.setTemplate(
        new Template(null) {
          @Override
          public int parse(
              final String formattedString,
              final java.util.Map<String, Object> variables,
              final boolean loggable) {
            if (formattedString.startsWith("/")) {
              return 0;
            }
            return -1;
          }

          @Override
          public int match(final String formattedString) {
            if (formattedString.startsWith("/")) {
              return 0;
            }
            return -1;
          }
        });
    // baseRouter.attachDefault(authenticator);

    final CaosAuthorizer authorizer = new CaosAuthorizer();

    // The protectedRouter handles the request if the authorization
    // succeeds.
    authorizer.setNext(protectedRouter);

    // After authentication comes authorization...
    authenticator.setNext(authorizer);

    protectedRouter.attach("/scripting", ScriptingResource.class);
    protectedRouter.attach("/Entities/names", EntityNamesResource.class);
    protectedRouter.attach("/Entity/names", EntityNamesResource.class);
    protectedRouter.attach("/Entities", EntityResource.class);
    protectedRouter.attach("/Entities/", EntityResource.class);
    protectedRouter.attach("/Entities/{specifier}", EntityResource.class);
    protectedRouter.attach("/Entity", EntityResource.class);
    protectedRouter.attach("/Entity/", EntityResource.class);
    protectedRouter.attach("/Entity/{specifier}", EntityResource.class);
    protectedRouter.attach("/Users", UserResource.class);
    protectedRouter.attach("/Users/", UserResource.class);
    protectedRouter.attach("/Users/{specifier}", UserResource.class);
    protectedRouter.attach("/User", UserResource.class);
    protectedRouter.attach("/User/", UserResource.class);
    protectedRouter.attach("/User/{specifier}", UserResource.class);
    protectedRouter.attach("/User/{realm}/", UserResource.class);
    protectedRouter.attach("/User/{realm}/{specifier}", UserResource.class);
    protectedRouter.attach("/Users/{realm}/", UserResource.class);
    protectedRouter.attach("/Users/{realm}/{specifier}", UserResource.class);
    protectedRouter.attach("/UserRoles/{specifier}", UserRolesResource.class);
    protectedRouter.attach("/EntityPermissions", EntityPermissionsResource.class);
    protectedRouter.attach("/EntityPermissions/", EntityPermissionsResource.class);
    protectedRouter.attach("/EntityPermissions/{specifier}", EntityPermissionsResource.class);
    protectedRouter.attach("/Owner/{specifier}", EntityOwnerResource.class);
    protectedRouter.attach(
        "/FileSystem",
        new Redirector(getContext(), "/FileSystem/", Redirector.MODE_CLIENT_PERMANENT));
    protectedRouter.attach("/FileSystem/", FileSystemResource.class);
    // FileSystem etc. needs to accept parameters which contain slashes and would otherwise be
    // split at the first separator
    protectedRouter
        .attach("/FileSystem/{path}", FileSystemResource.class)
        .getTemplate()
        .getDefaultVariable()
        .setType(Variable.TYPE_URI_PATH);
    protectedRouter
        .attach("/Thumbnails/{path}", ThumbnailsResource.class)
        .getTemplate()
        .getDefaultVariable()
        .setType(Variable.TYPE_URI_PATH);
    protectedRouter.attach("/Shared", SharedFileResource.class);
    protectedRouter.attach("/Shared/", SharedFileResource.class);
    protectedRouter
        .attach("/Shared/{path}", SharedFileResource.class)
        .getTemplate()
        .getDefaultVariable()
        .setType(Variable.TYPE_URI_PATH);
    ;
    protectedRouter.attach("/Info", InfoResource.class);
    protectedRouter.attach("/Info/", InfoResource.class);
    protectedRouter.attach("/Role", RolesResource.class);
    protectedRouter.attach("/Role/{specifier}", RolesResource.class);
    protectedRouter.attach("/PermissionRules/{specifier}", PermissionRulesResource.class);
    protectedRouter.attach("/PermissionRules/{realm}/{specifier}", PermissionRulesResource.class);
    protectedRouter.attach("/login?username={username}", AuthenticationResource.class);
    protectedRouter.attach("/logout", LogoutResource.class);
    protectedRouter.attach("/_server_properties", ServerPropertiesResource.class);
    protectedRouter.attachDefault(DefaultResource.class);

    /*
     * Dirty Hack - no clean solution found yet (except for patching the restlet framework). The
     * logging handler causes a NullPointerException when the Template.match method logs a warning.
     * This warning (seemingly) always means that the RequestUri cannot be matched because its to
     * long and causes a StackOverflow. Therefore we want to generate an HTTP 414 error.
     */

    final Handler handler =
        new Handler() {

          @Override
          public void publish(final LogRecord record) {
            if (record.getLevel() == Level.WARNING
                && record
                    .getMessage()
                    .startsWith(
                        "StackOverflowError exception encountered while matching this string")) {
              // cause a NullPointerException
              throw new NullPointerException();
            }
          }

          @Override
          public void flush() {}

          @Override
          public void close() throws SecurityException {}
        };

    routes:
    for (final Route r : protectedRouter.getRoutes()) {
      if (r instanceof TemplateRoute) {
        final TemplateRoute t = (TemplateRoute) r;
        for (final Handler h : t.getTemplate().getLogger().getHandlers()) {
          if (h == handler) {
            continue routes;
          }
        }
        t.getTemplate().getLogger().addHandler(handler);
      }
    }

    return baseRouter;
  }

  private static ServerInfo serverInfo = null;

  public static ServerInfo getServerInfo() {
    if (serverInfo == null) {
      serverInfo = new ServerInfo();
      serverInfo.setAgent("CaosDB Server");
    }
    return serverInfo;
  }

  private static void callPostShutdownHooks() {
    for (final Runnable r : postShutdownHooks) {
      try {
        final Thread t = new Thread(r);
        t.start();
        t.join();
      } catch (final Exception e) {
        e.printStackTrace();
      }
    }
  }

  private static void callPreShutdownHooks() {
    for (final Runnable r : preShutdownHooks) {
      try {
        final Thread t = new Thread(r);
        t.start();
        t.join();
      } catch (final Exception e) {
        e.printStackTrace();
      }
    }
  }

  /** Add a shutdown hook which runs after the Restlet Server has been shut down. */
  public static void addPostShutdownHook(final Thread t) {
    postShutdownHooks.add(t);
  }

  /** Add a shutdown hook which runs before the Restlet Server is being shut down. */
  public static void addPreShutdownHook(final Runnable runnable) {
    preShutdownHooks.add(runnable);
  }

  private static void initShutDownHook() {
    Runtime.getRuntime()
        .addShutdownHook(
            new Thread("SHUTDOWN_HTTP_SERVER") {
              @Override
              public void run() {
                callPreShutdownHooks();
                try {
                  component.stop();
                  System.err.print("Stopping HTTP server [OK]\n");
                } catch (final Exception e) {
                  System.err.print("Stopping HTTP server [failed]\n");
                  e.printStackTrace();
                } finally {
                  callPostShutdownHooks();
                }
              }
            });
  }

  public static Component getComponent() {
    return component;
  }

  public static boolean isDebugMode() {
    return Boolean.getBoolean("caosdb.debug");
  }

  /**
   * Set a server property to a new value. This might not have an immediate effect if classes did
   * already read an older configuration and stick to that.
   *
   * @param key, the server property.
   * @param value, the new value.
   */
  public static void setProperty(final String key, final String value) {
    SERVER_PROPERTIES.setProperty(key, value);
  }

  public static Properties getServerProperties() {
    return SERVER_PROPERTIES;
  }

  public static void scheduleJob(final JobDetail job, final Trigger trigger)
      throws SchedulerException {
    SCHEDULER.scheduleJob(job, trigger);
  }

  public static boolean useCache() {
    return USE_CACHE;
  }
}

class CaosDBComponent extends Component {

  private static Logger request_error_logger =
      LoggerFactory.getLogger(CaosDBServer.REQUEST_ERRORS_LOGGER);
  private static Logger request_time_logger =
      LoggerFactory.getLogger(CaosDBServer.REQUEST_TIME_LOGGER);

  public CaosDBComponent() {
    super();
    String responseLogFormat =
        CaosDBServer.getServerProperty(ServerProperties.KEY_REST_RESPONSE_LOG_FORMAT);
    if ("OFF".equalsIgnoreCase(responseLogFormat)) {
      getLogService().setEnabled(false);
    } else if (responseLogFormat != null && !responseLogFormat.isEmpty()) {
      getLogService().setResponseLogFormat(responseLogFormat);
    }
    setName(CaosDBServer.getServerProperty(ServerProperties.KEY_SERVER_NAME));
    setOwner(CaosDBServer.getServerProperty(ServerProperties.KEY_SERVER_OWNER));
  }

  /**
   * This function is doing the actual work as soon as a request arrives. In this case this consists
   * in: - Logging the request, the response and the current time - The response gets updated with
   * server info - The request gets updated with an SRID (server request ID)
   *
   * <p>Apart from that super.handle will be called.
   *
   * <p>The main purpose of the SRID is to allow efficient debugging by checking the error log for
   * the request causing the error.
   */
  @Override
  public void handle(final Request request, final Response response) {
    final long t1 = System.currentTimeMillis();
    // The server request ID is just a long random number
    request.getAttributes().put("SRID", UUID.randomUUID().toString());
    response.setServerInfo(CaosDBServer.getServerInfo());
    super.handle(request, response);
    log(request, response, t1);
  }

  private void log(final Request request, final Response response, final long t1) {
    if (response.getStatus() == Status.SERVER_ERROR_INTERNAL) {
      final Object object = request.getAttributes().get("THROWN");
      Throwable t = null;
      if (object instanceof Throwable) {
        t = (Throwable) object;
      }

      request_error_logger.error(
          "SRID: {}\n{}",
          request.getAttributes().get("SRID").toString(),
          new RequestErrorLogMessage(request, response),
          t);
    }
    request_time_logger.trace(
        "SRID: {} - Dur: {}",
        request.getAttributes().get("SRID").toString(),
        Long.toString(System.currentTimeMillis() - t1));
  }
}
