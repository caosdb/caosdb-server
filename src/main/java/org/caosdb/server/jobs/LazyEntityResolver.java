/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2023 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2023 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.caosdb.server.jobs;

import org.caosdb.server.database.backend.transaction.RetrieveParents;
import org.caosdb.server.database.backend.transaction.RetrieveProperties;
import org.caosdb.server.entity.EntityInterface;
import org.caosdb.server.entity.container.ParentContainer;
import org.caosdb.server.entity.container.PropertyContainer;
import org.caosdb.server.entity.wrapper.EntityWrapper;
import org.caosdb.server.transaction.Transaction;

/**
 * Implementation of {@link EntityInterface} which retrieves the parents and properties only when
 * requested via getProperties and getParents.
 *
 * <p>This is particularly useful for jobs sharing the same entity object between jobs which need
 * different parts of it while reducing the logic of checking whether the entity is "only" sparse,
 * or has been retrieved fully and so on.
 *
 * <p>However, when the entities parents or properties are being retrieved after the transaction's
 * access has been released, an {@link IllegalStateException} will be thrown.
 *
 * @author Timm Fitschen <t.fitschen@indiscale.com>
 */
public class LazyEntityResolver extends EntityWrapper {

  private Transaction<?> transaction;
  private boolean propertiesResolved = false;
  private boolean parentsResolved = false;

  /**
   * Constructor.
   *
   * <p>Initialize this with a sparse entity.
   */
  public LazyEntityResolver(EntityInterface entity, Transaction<?> transaction) {
    super(entity);
    this.transaction = transaction;
  }

  public void resolveParents() {
    if (parentsResolved) return;
    parentsResolved = true;
    transaction.execute(new RetrieveParents(getWrapped()), transaction.getAccess());
  }

  private void resolveProperties() {
    if (propertiesResolved) return;
    propertiesResolved = true;
    transaction.execute(new RetrieveProperties(getWrapped()), transaction.getAccess());
  }

  public void resolveAll() {
    resolveParents();
    resolveProperties();
    // release the transaction for the garbage collector
    this.transaction = null;
  }

  @Override
  public ParentContainer getParents() {
    resolveParents();
    return super.getParents();
  }

  @Override
  public boolean hasParents() {
    resolveParents();
    return super.hasParents();
  }

  @Override
  public PropertyContainer getProperties() {
    resolveProperties();
    return super.getProperties();
  }

  @Override
  public boolean hasProperties() {
    resolveProperties();
    return super.hasProperties();
  }
}
