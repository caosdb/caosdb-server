/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.jobs.core;

import org.caosdb.server.CaosDBServer;
import org.caosdb.server.ServerProperties;
import org.caosdb.server.accessControl.AuthenticationUtils;
import org.caosdb.server.entity.EntityInterface;
import org.caosdb.server.entity.InsertEntity;
import org.caosdb.server.entity.UpdateEntity;
import org.caosdb.server.jobs.ContainerJob;
import org.caosdb.server.permissions.EntityACI;
import org.caosdb.server.utils.EntityStatus;
import org.caosdb.server.utils.ServerMessages;

public class CheckEntityACLRoles extends ContainerJob {

  @Override
  protected void run() {
    for (final EntityInterface entity : getContainer()) {
      if ((entity instanceof UpdateEntity || entity instanceof InsertEntity)
          && entity.getEntityACL() != null) {
        for (final EntityACI aci : entity.getEntityACL().getRules()) {
          if (!AuthenticationUtils.isResponsibleAgentExistent(aci.getResponsibleAgent())) {
            if (CaosDBServer.getServerProperty(ServerProperties.KEY_CHECK_ENTITY_ACL_ROLES_MODE)
                .equalsIgnoreCase("MUST")) {
              entity.addError(ServerMessages.ROLE_DOES_NOT_EXIST);
              entity.setEntityStatus(EntityStatus.UNQUALIFIED);
            } else {
              entity.addWarning(ServerMessages.ROLE_DOES_NOT_EXIST);
            }
            entity.addInfo(
                "User Role `" + aci.getResponsibleAgent().toString() + "` does not exist.");
          }
        }
      }
    }
  }
}
