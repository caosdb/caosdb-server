/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.jobs.core;

import de.timmfitschen.easyunits.parser.ParserException;
import org.caosdb.server.datatype.SingleValue;
import org.caosdb.server.entity.EntityInterface;
import org.caosdb.server.entity.MagicTypes;
import org.caosdb.server.jobs.EntityJob;
import org.caosdb.server.utils.EntityStatus;
import org.caosdb.server.utils.ServerMessages;
import org.caosdb.unit.CaosDBSystemOfUnits;
import org.caosdb.unit.Unit;
import org.caosdb.unit.UnknownUnit;

public class ParseUnit extends EntityJob {

  @Override
  protected void run() {
    for (final EntityInterface p : getEntity().getProperties()) {
      if (p.hasId() && MagicTypes.getType(p.getId()) == MagicTypes.UNIT) {
        final String unitStr = ((SingleValue) p.getValue()).toDatabaseString();

        try {
          Unit unit;
          unit = CaosDBSystemOfUnits.getUnit(unitStr);
          if (unit instanceof UnknownUnit) {
            getEntity().addWarning(ServerMessages.UNKNOWN_UNIT);
          }
          getEntity().setUnit(unit);
          appendJob(UpdateUnitConverters.class);
        } catch (final ParserException e) {
          getEntity().addError(ServerMessages.CANNOT_PARSE_UNIT);
          getEntity().setEntityStatus(EntityStatus.UNQUALIFIED);
        }
      }
    }
  }
}
