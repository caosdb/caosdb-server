/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.jobs.core;

import org.caosdb.server.CaosDBServer;
import org.caosdb.server.ServerProperties;
import org.caosdb.server.entity.Message;
import org.caosdb.server.jobs.FlagJob;
import org.caosdb.server.jobs.JobAnnotation;
import org.caosdb.server.utils.ServerMessages;
import org.caosdb.server.utils.Utils;
import org.caosdb.server.utils.mail.Mail;

@JobAnnotation(
    flag = "testMail",
    description =
        "Value may be any mail address. Send a test mail to the given mail address. Works only if server is in debug mode.")
public class TestMail extends FlagJob {

  private static final String NAME =
      CaosDBServer.getServerProperty(ServerProperties.KEY_NO_REPLY_NAME);
  private static final String EMAIL =
      CaosDBServer.getServerProperty(ServerProperties.KEY_NO_REPLY_EMAIL);

  @Override
  protected void job(final String recipient) {
    if (CaosDBServer.isDebugMode() && recipient != null) {
      if (Utils.isRFC822Compliant(recipient)) {
        final Mail m = new Mail(NAME, EMAIL, null, recipient, "Test mail", "This is a test mail.");
        m.send();
        getContainer().addMessage(new Message("A mail has been sent to " + recipient));
      } else {
        getContainer().addMessage(ServerMessages.EMAIL_NOT_WELL_FORMED);
      }
    }
  }
}
