/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.jobs.core;

import java.util.Map.Entry;
import org.caosdb.server.jobs.ContainerJob;
import org.caosdb.server.jobs.FlagJob;
import org.caosdb.server.jobs.Job;
import org.caosdb.server.jobs.JobAnnotation;
import org.caosdb.server.jobs.TransactionStage;

@JobAnnotation(stage = TransactionStage.INIT)
public class LoadContainerFlagJobs extends ContainerJob {

  @Override
  protected void run() {
    if (getContainer().getFlags() != null) {
      for (final Entry<String, String> flag : getContainer().getFlags().entrySet()) {
        final Job j = Job.getJob(flag.getKey(), JobFailureSeverity.ERROR, null, getTransaction());
        if (j != null && j instanceof ContainerJob) {
          if (j instanceof FlagJob) {
            ((FlagJob) j).setValue(flag.getValue());
          }
          getTransaction().getSchedule().add(j);
        }
      }
    }
  }
}
