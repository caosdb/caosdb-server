/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2023 Timm Fitschen <t.fitschen@indiscale.com>
 * Copyright (C) 2023 IndiScale <info@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.caosdb.server.jobs.core;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.caosdb.server.accessControl.ACMPermissions;
import org.caosdb.server.entity.DeleteEntity;
import org.caosdb.server.entity.EntityInterface;
import org.caosdb.server.entity.InsertEntity;
import org.caosdb.server.entity.Role;
import org.caosdb.server.entity.UpdateEntity;
import org.caosdb.server.jobs.ContainerJob;
import org.caosdb.server.jobs.JobAnnotation;
import org.caosdb.server.jobs.TransactionStage;
import org.caosdb.server.transaction.Retrieve;
import org.caosdb.server.utils.EntityStatus;
import org.caosdb.server.utils.ServerMessages;

/**
 * Checks the TRANSACTION:* permissions before a transaction begins.
 *
 * <p>Users need TRANSACTION:INSERT:?ENTITY_ROLE? permission to insert an entity of the particular
 * entity role. Likewise, they need the TRANSACTION:UPDATE or TRANSACTION:DELETE permissions.
 *
 * @author Timm Fitschen <f.fitschen@indiscale.com>
 */
@JobAnnotation(stage = TransactionStage.INIT)
public class AccessControl extends ContainerJob {

  public static class TransactionPermission extends ACMPermissions {

    public static final String ENTITY_ROLE_PARAMETER = "?ENTITY_ROLE?";

    public TransactionPermission(String permission, String description) {
      super(permission, description);
    }

    public final String toString(Role entityRole) {
      String roleString = entityRole == null ? "" : entityRole.toString();
      return toString().replace(ENTITY_ROLE_PARAMETER, roleString);
    }

    public final String toString(String transaction, Role entityRole) {
      return "TRANSACTION:"
          + transaction
          + (entityRole != null ? (":" + entityRole.toString()) : "");
    }

    public static String init() {
      return TransactionPermission.class.getSimpleName();
    }
  }

  public static final TransactionPermission TRANSACTION_PERMISSIONS =
      new TransactionPermission(
          "TRANSACTION:*",
          "Permission to execute any writable transaction. This permission only allows to execute these transactions in general. The necessary entities permissions are not implied.");
  public static final TransactionPermission UPDATE =
      new TransactionPermission(
          "TRANSACTION:UPDATE:" + TransactionPermission.ENTITY_ROLE_PARAMETER,
          "Permission to update entities of a given role (e.g. Record, File, RecordType, or Property).");
  public static final TransactionPermission DELETE =
      new TransactionPermission(
          "TRANSACTION:DELETE:" + TransactionPermission.ENTITY_ROLE_PARAMETER,
          "Permission to delete entities of a given role (e.g. Record, File, RecordType, or Property).");
  public static final TransactionPermission INSERT =
      new TransactionPermission(
          "TRANSACTION:INSERT:" + TransactionPermission.ENTITY_ROLE_PARAMETER,
          "Permission to insert entities of a given role (e.g. Record, File, RecordType, or Property).");

  @Override
  protected void run() {
    final Subject subject = SecurityUtils.getSubject();

    if (getTransaction() instanceof Retrieve) {
      return;
    }

    for (final EntityInterface e : getContainer()) {

      if (e instanceof InsertEntity) {
        if (subject.isPermitted(INSERT.toString(e.getRole()))) {
          continue;
        }
      } else if (e instanceof DeleteEntity) {
        if (subject.isPermitted(DELETE.toString(e.getRole()))) {
          continue;
        }
      } else if (e instanceof UpdateEntity) {
        if (subject.isPermitted(UPDATE.toString(e.getRole()))) {
          continue;
        }
      }

      e.setEntityStatus(EntityStatus.UNQUALIFIED);
      e.addMessage(ServerMessages.AUTHORIZATION_ERROR);
    }
  }
}
