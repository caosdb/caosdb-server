/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2019-2021,2023 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2019-2021,2023 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.caosdb.server.jobs.core;

import java.util.ArrayList;
import java.util.List;
import org.caosdb.api.entity.v1.MessageCode;
import org.caosdb.server.entity.EntityInterface;
import org.caosdb.server.entity.InsertEntity;
import org.caosdb.server.entity.Message;
import org.caosdb.server.entity.Message.MessageType;
import org.caosdb.server.entity.StatementStatus;
import org.caosdb.server.entity.UpdateEntity;
import org.caosdb.server.entity.wrapper.Property;
import org.caosdb.server.jobs.EntityJob;
import org.caosdb.server.jobs.ScheduledJob;
import org.caosdb.server.utils.EntityStatus;

/**
 * Add all those properties from the parent to the child which have the same importance (or higher).
 *
 * @author Timm Fitschen <t.fitschen@indiscale.com>
 */
public class Inheritance extends EntityJob {

  /*
   * Storing which properties of the properties of the parents should be inherited by the child.
   */
  public enum INHERITANCE_MODE {
    NONE, // inherit no properties from this parent
    ALL, // inherit all inheritable properties, alias for suggested
    OBLIGATORY, // inherit only obligatory properties
    RECOMMENDED, // inherit obligatory and recommended properties
    SUGGESTED, // inherit all inheritable properties, alias for all
    FIX, // inherit fix properties only (deprecated)
  };

  public static final Message ILLEGAL_INHERITANCE_MODE =
      new Message(
          MessageType.Warning,
          MessageCode.MESSAGE_CODE_UNKNOWN,
          "Unknown value for flag \"inheritance\". None of the parent's properties have been transfered to the child.");

  @Override
  protected void run() {
    if (getEntity() instanceof InsertEntity || getEntity() instanceof UpdateEntity) {
      if (getEntity().getEntityStatus() == EntityStatus.QUALIFIED && getEntity().hasParents()) {
        final ArrayList<Property> transfer = new ArrayList<>();
        parentLoop:
        for (final EntityInterface parent : getEntity().getParents()) {
          try {
            if (parent.getFlags().get("inheritance") == null) {
              break parentLoop;
            }
            final INHERITANCE_MODE inheritance =
                INHERITANCE_MODE.valueOf(parent.getFlags().get("inheritance").toUpperCase());

            // mark inheritance flag as done
            parent.setFlag("inheritance", null);
            if (inheritance == INHERITANCE_MODE.NONE) {
              break parentLoop;
            }

            runJobFromSchedule(getEntity(), CheckParValid.class);

            // try to get the parent entity from the current transaction container
            EntityInterface foreign = resolve(parent);

            collectInheritedProperties(transfer, foreign, inheritance);
          } catch (final IllegalArgumentException e) {
            parent.addWarning(ILLEGAL_INHERITANCE_MODE);
            break parentLoop;
          }
        }

        // transfer properties if they are not implemented yet
        outerLoop:
        for (final Property prop : transfer) {
          for (final Property eprop : getEntity().getProperties()) {
            if (prop.hasId() && eprop.hasId() && prop.getId().equals(eprop.getId())) {
              continue outerLoop;
            }
          }
          // prop's Datatype might need to be resolved.
          final ScheduledJob job = this.appendJob(prop, CheckDatatypePresent.class);
          getTransaction().getSchedule().runJob(job);

          getEntity().addProperty(new Property(prop.getWrapped()));
        }
      }

      // implement properties
      if (getEntity().getEntityStatus() == EntityStatus.QUALIFIED && getEntity().hasProperties()) {
        propertyLoop:
        for (final Property property : getEntity().getProperties()) {
          final ArrayList<Property> transfer = new ArrayList<>();
          try {
            if (property.getFlags().get("inheritance") == null) {
              break propertyLoop;
            }
            final INHERITANCE_MODE inheritance =
                INHERITANCE_MODE.valueOf(property.getFlags().get("inheritance").toUpperCase());

            // mark inheritance flag as done
            property.setFlag("inheritance", null);
            if (inheritance == INHERITANCE_MODE.NONE) {
              break propertyLoop;
            }

            EntityInterface abstractProperty = null;
            if (getEntity().hasParents()) {
              outer:
              for (EntityInterface par : getEntity().getParents()) {
                par = resolve(par);
                for (final EntityInterface prop : par.getProperties()) {
                  if ((property.hasId() && property.getId().equals(prop.getId()))
                      || property.hasName() && property.getName().equals(prop.getName())) {
                    abstractProperty = prop;
                    break outer;
                  }
                }
              }
            }
            if (abstractProperty == null) {
              abstractProperty = resolve(property);
            }
            collectInheritedProperties(transfer, abstractProperty, inheritance);
          } catch (final IllegalArgumentException e) {
            property.addWarning(ILLEGAL_INHERITANCE_MODE);
            break propertyLoop;
          }

          // transfer properties if they are not implemented yet
          outerLoop:
          for (final Property prop : transfer) {
            for (final Property eprop : property.getProperties()) {
              if (prop.hasId() && eprop.hasId() && prop.getId().equals(eprop.getId())) {
                continue outerLoop;
              }
            }
            // prop's Datatype might need to be resolved.
            final ScheduledJob job = this.appendJob(prop, CheckDatatypePresent.class);
            getTransaction().getSchedule().runJob(job);

            property.addProperty(new Property(prop.getWrapped()));
          }
        }
      }
    }
  }

  /**
   * Put all those properties from the `from` entity into the `transfer` List which match the
   * INHERITANCE_MODE.
   *
   * <p>That means:
   *
   * @param transfer
   * @param from
   * @param inheritance
   */
  private void collectInheritedProperties(
      final List<Property> transfer,
      final EntityInterface from,
      final INHERITANCE_MODE inheritance) {
    if (from.hasProperties()) {
      for (final Property propProperty : from.getProperties()) {
        switch (inheritance) {
            // the following cases are ordered according to their importance level and use a
            // fall-through.
          case ALL:
          case SUGGESTED:
            if (propProperty.getStatementStatus() == StatementStatus.SUGGESTED) {
              transfer.add(propProperty);
            }
            // fall-through!
          case RECOMMENDED:
            if (propProperty.getStatementStatus() == StatementStatus.RECOMMENDED) {
              transfer.add(propProperty);
            }
            // fall-through!
          case OBLIGATORY:
            if (propProperty.getStatementStatus() == StatementStatus.OBLIGATORY) {
              transfer.add(propProperty);
            }
            break;
          case FIX:
            if (propProperty.getStatementStatus() == StatementStatus.FIX) {
              transfer.add(propProperty);
              propProperty.addWarning(
                  new Message(
                      MessageType.Warning,
                      "DeprecationWarning: The inheritance of fix properties is deprecated and will be removed from the API in the near future. Clients have to copy fix properties by themselves, if necessary."));
            }
          default:
            break;
        }
      }
    }
  }
}
