/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.jobs.core;

import org.caosdb.server.entity.FileProperties;
import org.caosdb.server.jobs.FilesJob;
import org.caosdb.server.utils.EntityStatus;
import org.caosdb.server.utils.FileUtils;
import org.caosdb.server.utils.ServerMessages;

/**
 * Match the checksum and the size of a file record to the uploaded file's checksum and size.
 *
 * @author tf
 */
public class MatchFileProp extends FilesJob {
  @Override
  public final void run() {

    // does the entity have a file?
    if (getEntity().hasFileProperties()) {
      // normativeFileProperties is how the file must look like (path,
      // checksum, size)
      final FileProperties normativeFileProperties = getEntity().getFileProperties();

      // descriptiveFileProperties is how the copied/uploaded/... file
      // ACTUALLY looks like. (path, checksum, size)
      final FileProperties descriptiveFileProperties =
          getFile(normativeFileProperties.getTmpIdentifier());

      if (descriptiveFileProperties != null) {
        checkChecksum(normativeFileProperties, descriptiveFileProperties);
        checkSize(normativeFileProperties, descriptiveFileProperties);
      }
    }
  }

  private void checkSize(final FileProperties normFP, final FileProperties descFP) {
    // get size if necessary
    if (!descFP.hasSize()) {
      descFP.setSize(descFP.getFile().length());
    }

    if (normFP.hasSize()) {
      // normFP and descFP have a size -> must be equal
      if (!descFP.getSize().equals(normFP.getSize())) {
        getEntity().setEntityStatus(EntityStatus.UNQUALIFIED);
        getEntity().addError(ServerMessages.SIZE_TEST_FAILED);
      }
    } else {
      normFP.setSize(descFP.getSize());
    }
  }

  private void checkChecksum(final FileProperties normFP, final FileProperties descFP) {
    if (normFP.hasChecksum()) {
      // normFP has checksum -> need to test immediately

      // get descFP checksum if necessary
      if (!descFP.hasChecksum()) {
        descFP.setChecksum(FileUtils.getChecksum(descFP.getFile()));
      }

      // normFP's and descFP's checksum must be equal
      if (!descFP.getChecksum().equalsIgnoreCase(normFP.getChecksum())) {
        getEntity().setEntityStatus(EntityStatus.UNQUALIFIED);
        getEntity().addError(ServerMessages.CHECKSUM_TEST_FAILED);
      }
    } else if (descFP.hasChecksum()) {
      // normFP doesn't have a checksum but descFP has -> copy to normFP
      normFP.setChecksum(descFP.getChecksum());
    }
  }
}
