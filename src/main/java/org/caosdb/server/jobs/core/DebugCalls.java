/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.jobs.core;

import org.caosdb.server.CaosDBServer;
import org.caosdb.server.jobs.FlagJob;
import org.caosdb.server.jobs.JobAnnotation;
import org.caosdb.server.jobs.TransactionStage;

@JobAnnotation(
    flag = "debug",
    stage = TransactionStage.INIT,
    values = {DebugCalls.ex_null_p},
    description =
        "This job for debugging and is available in debug mode only. Otherwise the flag is ignored. It throws an exception on purpose.",
    loadOnDefault = false)
public class DebugCalls extends FlagJob {

  public static final String ex_null_p = "throwNullPointerException";

  @Override
  protected void job(final String value) {
    if (CaosDBServer.isDebugMode()) {
      if (value != null) {
        if (value.equalsIgnoreCase(ex_null_p)) {
          throw new NullPointerException();
        }
      }
    }
  }
}
