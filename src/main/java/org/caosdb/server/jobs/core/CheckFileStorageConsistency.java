/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.jobs.core;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.caosdb.server.CaosDBServer;
import org.caosdb.server.FileSystem;
import org.caosdb.server.database.exceptions.TransactionException;
import org.caosdb.server.entity.Message;
import org.caosdb.server.jobs.FlagJob;
import org.caosdb.server.jobs.JobAnnotation;
import org.caosdb.server.transaction.FileStorageConsistencyCheck;
import org.caosdb.server.transaction.Retrieve;
import org.caosdb.server.transaction.Transaction;
import org.caosdb.server.utils.FileUtils;
import org.caosdb.server.utils.Observable;
import org.caosdb.server.utils.Observer;
import org.caosdb.server.utils.Undoable;
import org.jdom2.Document;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

@JobAnnotation(
    flag = "fileStorageConsistency",
    loadOnDefault = false,
    transaction = Retrieve.class,
    description =
        "Invokes a consistency test of the file storage. The value can have any of:\n"
            + "  - a string with a timeout in ms (prefixed -t): `-t 120000`\n"
            + "  - a test case which can be run if the server is in debug mode (prefixed\n"
            + "    -c)\n"
            + "  - a filesystem location whose children are checked recursively,\n"
            + "    relative to the caosroot dilesystem location.\n")
public class CheckFileStorageConsistency extends FlagJob {

  public static final Pattern parseArgs =
      Pattern.compile("\\s*-t\\s*([0-9]+)\\s*|\\s*-c\\s*([^\\s]+)\\s*|\\s*([^\\s]+\\s*)");
  private int timeout = 1000 * 30; // milliseconds
  private String location = "";

  public static enum TestCase {
    FILE_DOES_NOT_EXIST,
    FILE_WAS_MODIFIED,
    UNKNOWN_FILE
  };

  @Override
  protected void job(final String value) {
    if (value != null) {
      final Matcher matcher = parseArgs.matcher((value));
      while (matcher.find()) {
        if (matcher.group(1) != null) {
          // group one is timeout (-t$ms)
          try {
            this.timeout = Integer.valueOf(matcher.group(1)) * 1000;
          } catch (final NumberFormatException e) {
          }
        }
        if (matcher.group(2) != null && CaosDBServer.isDebugMode()) {
          // group two is TestCase (-c$testcase)
          try {
            test(TestCase.valueOf(matcher.group(2)));
            continue;
          } catch (final IllegalArgumentException e) {
          } catch (final IOException | Message e) {
            throw new TransactionException(e);
          }
        }
        if (matcher.group(3) != null) {
          // group three is file system location
          this.location = matcher.group(3);
        }
      }
    }

    final FileStorageConsistencyCheck test = new FileStorageConsistencyCheck(this.location);
    test.setOnFinish(
        new Runnable() {
          @Override
          public void run() {

            final Document doc = new Document(test.toElement());
            final XMLOutputter out = new XMLOutputter(Format.getPrettyFormat());
            try {
              out.output(doc, new PrintStream(new File("./ConsistencyTest.xml")));
            } catch (final FileNotFoundException e) {
              throw new TransactionException(e);
            } catch (final IOException e) {
              throw new TransactionException(e);
            }
          }
        });
    test.start();
    try {
      test.join(this.timeout);
    } catch (final InterruptedException e1) {
      throw new TransactionException(e1);
    }

    if (test.isAlive()) {
      // add info: not read yet
      getContainer()
          .addMessage(
              new Message(
                  "Test took too long. The results will be written to './ConsistencyTest.xml'"));
    } else {
      // add info/warning/error
      final Exception e = test.getException();
      if (e != null) {
        throw new TransactionException(e);
      }

      getContainer().addMessage(test);
    }
  }

  private void test(final TestCase testCase) throws IOException, Message {

    switch (testCase) {
      case FILE_DOES_NOT_EXIST:
        final File f1 = FileSystem.getFromFileSystem("debug/test_file_storage_consistency");
        if (f1 == null) {
          return;
        }
        final File tmp1 =
            new File(
                FileSystem.getTmp()
                    + "test_file_storage_consistecy"
                    + Integer.toString(hashCode()));

        Undoable undo1 = FileUtils.rename(f1, tmp1);

        getTransaction()
            .acceptObserver(
                new Observer() {
                  @Override
                  public boolean notifyObserver(final String e, final Observable sender) {
                    if (e.equals(Transaction.CLEAN_UP)) {
                      undo1.undo();
                      return false;
                    }
                    return true;
                  }
                });
        break;

      case UNKNOWN_FILE:
        final File dir = new File(FileSystem.getPath("debug/"));

        final File f3;
        if (dir.exists()) {
          f3 = new File(FileSystem.getPath("debug/test_file_storage_consistency"));
          f3.createNewFile();
        } else {
          f3 = new File(FileSystem.getPath("debug"));
          f3.mkdir();
        }

        getTransaction()
            .acceptObserver(
                new Observer() {
                  @Override
                  public boolean notifyObserver(final String e, final Observable sender) {
                    if (e.equals(Transaction.CLEAN_UP)) {
                      f3.delete();
                      return false;
                    }
                    return true;
                  }
                });

        break;
      case FILE_WAS_MODIFIED:
        final File f2 = FileSystem.getFromFileSystem("debug/test_file_storage_consistency");
        if (f2 == null) {
          return;
        }

        final String oldPath2 = f2.getAbsolutePath();
        final File tmp2 =
            new File(
                FileSystem.getTmp()
                    + "test_file_storage_consistecy"
                    + Integer.toString(hashCode()));

        final Undoable undo2 = org.caosdb.server.utils.FileUtils.rename(f2, tmp2);

        final File overridden = new File(oldPath2);
        overridden.createNewFile();

        final PrintStream out = new PrintStream(overridden);
        out.println("asdfasdfasdfasdfasdf");
        out.flush();
        out.close();

        getTransaction()
            .acceptObserver(
                new Observer() {
                  @Override
                  public boolean notifyObserver(final String e, final Observable sender) {
                    if (e.equals(Transaction.CLEAN_UP)) {
                      undo2.undo();
                      return false;
                    }
                    return true;
                  }
                });
        break;
      default:
        break;
    }
  }
}
