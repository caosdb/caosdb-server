/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2020 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2020 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.caosdb.server.jobs.core;

import java.util.List;
import org.caosdb.server.entity.EntityInterface;
import org.caosdb.server.entity.Message;
import org.caosdb.server.entity.wrapper.Property;
import org.caosdb.server.jobs.JobAnnotation;
import org.caosdb.server.jobs.TransactionStage;
import org.caosdb.server.utils.EntityStatus;

@JobAnnotation(stage = TransactionStage.CHECK)
public class InheritInitialState extends EntityStateJob {

  @Override
  protected void run() {
    if (getEntity().getEntityStatus() != EntityStatus.QUALIFIED) {
      return;
    }
    try {
      final State parentState = getFirstParentState();
      if (parentState != null) {
        getEntity().addMessage(parentState);
        parentState.getStateEntity();
        getEntity().setEntityACL(parentState.getStateACL());
      }
    } catch (final Message e) {
      getEntity().addError(e);
    }
  }

  private State getFirstParentState() throws Message {
    for (EntityInterface par : getEntity().getParents()) {
      par = resolve(par);
      final List<Property> stateProperties = getStateProperties(par, false);
      if (stateProperties.size() > 0) {
        return createState(stateProperties.get(0));
      }
    }
    return null;
  }
}
