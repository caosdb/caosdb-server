/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.jobs.core;

import org.caosdb.server.entity.EntityInterface;
import org.caosdb.server.entity.Message;
import org.caosdb.server.entity.xml.ToElementable;
import org.caosdb.server.jobs.ContainerJob;
import org.caosdb.server.jobs.JobAnnotation;
import org.caosdb.server.jobs.TransactionStage;
import org.caosdb.server.utils.EntityStatus;
import org.caosdb.server.utils.ServerMessages;

@JobAnnotation(stage = TransactionStage.POST_CHECK)
public class Strict extends ContainerJob {
  // TODO load strict job via unified flag interface

  @Override
  protected void run() {
    if (getContainer().getStatus() == EntityStatus.QUALIFIED) {
      for (final EntityInterface entity : getContainer()) {
        loop:
        for (final ToElementable m : entity.getMessages()) {
          if (m instanceof Message && ((Message) m).getType().equalsIgnoreCase("Warning")) {
            entity.setEntityStatus(EntityStatus.UNQUALIFIED);
            entity.addError(ServerMessages.WARNING_OCCURED);
            break loop;
          }
        }
      }
    }
  }
}
