/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.jobs.core;

import org.caosdb.server.FileSystem;
import org.caosdb.server.entity.FileProperties;
import org.caosdb.server.entity.Message;
import org.caosdb.server.jobs.EntityJob;
import org.caosdb.server.utils.EntityStatus;
import org.caosdb.server.utils.ServerMessages;

/**
 * Check whether the target path of a file entity is valid. That is, the target path is in the file
 * system and not used by any other file.
 *
 * @author tf
 */
public class CheckTargetPathValid extends EntityJob {

  @Override
  public final void run() {
    if (getEntity().hasFileProperties()) {
      final FileProperties file = getEntity().getFileProperties();

      if (!file.hasPath()) {
        // the file doesn't have a path property at all
        getEntity().setEntityStatus(EntityStatus.UNQUALIFIED);
        getEntity().addError(ServerMessages.FILE_HAS_NO_TARGET_PATH);
        return;
      }

      try {
        FileSystem.checkTarget(
            getEntity(), getTransaction().getAccess(), getTransaction().getTransactionBenchmark());
      } catch (final Message m) {
        getEntity().setEntityStatus(EntityStatus.UNQUALIFIED);
        getEntity().addError(m);
        return;
      }
    }
  }
}
