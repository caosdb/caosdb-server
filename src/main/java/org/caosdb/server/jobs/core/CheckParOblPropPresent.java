/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2019-2021 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2019-2021 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.caosdb.server.jobs.core;

import org.caosdb.server.entity.EntityInterface;
import org.caosdb.server.entity.Message;
import org.caosdb.server.entity.Message.MessageType;
import org.caosdb.server.entity.StatementStatus;
import org.caosdb.server.entity.wrapper.Parent;
import org.caosdb.server.entity.wrapper.Property;
import org.caosdb.server.jobs.EntityJob;
import org.caosdb.server.utils.EntityStatus;
import org.caosdb.server.utils.ServerMessages;

/**
 * Check whether an entity implements all obligatory properties of all parents.
 *
 * @author Timm Fitschen <t.fitschen@indiscale.com>
 */
public class CheckParOblPropPresent extends EntityJob {

  public static final String OBL_IMPORTANCE_FLAG_KEY = "force-missing-obligatory";
  public static final Message ILLEGAL_FLAG_VALUE =
      new Message(MessageType.Warning, "Illegal value for flag 'force-missing-obligatory'.");

  @Override
  public final void run() {
    if (getEntity().getEntityStatus() != EntityStatus.QUALIFIED
        || getEntity() instanceof Property
        || getEntity() instanceof Parent) {
      return;
    }
    if (getEntity().hasParents()) {

      handleImportanceFlags();
      if (getFailureSeverity() == JobFailureSeverity.IGNORE) {
        // importance is to be ignored
        return;
      }

      // inherit properties
      runJobFromSchedule(getEntity(), Inheritance.class);

      // get importance-flags

      // loop over all parents
      for (final EntityInterface parent : getEntity().getParents()) {
        final EntityInterface resolvedParent = resolve(parent);
        checkObligatoryPropertiesMissing(resolvedParent);
      }
    }
  }

  /**
   * Check if all obligatory properties of the given parent are present.
   *
   * <p>Add an error or warning if not.
   *
   * @param parent
   * @throws Message
   */
  private void checkObligatoryPropertiesMissing(final EntityInterface parent) {
    // does this parent have properties?
    if (parent.hasProperties()) {

      // loop over all properties of this parent
      outerLoop:
      for (final EntityInterface parentProperty : parent.getProperties()) {

        // only obligatory properties are of interest.
        if (parentProperty.hasStatementStatus()
            && parentProperty.getStatementStatus() == StatementStatus.OBLIGATORY) {

          // loop over all properties of the entity
          for (final EntityInterface entityProperty : getEntity().getProperties()) {

            if (isSubType(resolve(entityProperty), parentProperty)) {
              // continue outer loop means that we
              // found an entityProperty that
              // implements the obligatory
              // parentProperty, continue with the
              // other parentProperties.
              continue outerLoop;
            }
          }

          // No entityProperty has been found which
          // implements this parentProperty. Add the
          // respective messages.
          switch (getFailureSeverity()) {
            case ERROR:
              // TODO add information WHICH property is
              // missing.
              getEntity().addError(ServerMessages.OBLIGATORY_PROPERTY_MISSING);
              break;
            case WARN:
              // TODO add information WHICH property is
              // missing.
              getEntity().addWarning(ServerMessages.OBLIGATORY_PROPERTY_MISSING);
              break;
            default:
              // should never be reached, but ignoring is the way to go anyway
              break;
          }
        }
      }
    }
  }

  /** Look at the entity's importance flag first, at the container's flag second. */
  private void handleImportanceFlags() {
    final String globalFlag =
        getTransaction().getContainer().getFlags().get(OBL_IMPORTANCE_FLAG_KEY);
    final String entityFlag =
        getEntity().getFlags().getOrDefault(OBL_IMPORTANCE_FLAG_KEY, globalFlag);
    if (entityFlag != null) {
      try {
        setFailureSeverity(JobFailureSeverity.valueOf(entityFlag.toUpperCase()));
      } catch (final IllegalArgumentException e) {
        getEntity().addWarning(ILLEGAL_FLAG_VALUE);
      }
    }
  }
}
