/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.jobs.core;

import java.io.File;
import org.caosdb.server.FileSystem;
import org.caosdb.server.entity.Entity;
import org.caosdb.server.entity.EntityInterface;
import org.caosdb.server.entity.FileProperties;
import org.caosdb.server.entity.Message;
import org.caosdb.server.jobs.EntityJob;
import org.caosdb.server.jobs.JobAnnotation;
import org.caosdb.server.jobs.TransactionStage;
import org.caosdb.server.utils.EntityStatus;
import org.caosdb.server.utils.Observable;
import org.caosdb.server.utils.Observer;

@JobAnnotation(stage = TransactionStage.INIT)
public class PickUp extends EntityJob implements Observer {

  @Override
  protected void run() {
    final EntityInterface entity = getEntity();
    if (entity.hasFileProperties()) {
      final FileProperties normativeFileProperties = entity.getFileProperties();
      if (normativeFileProperties.isPickupable()) {
        try {
          entity.acceptObserver(this);
          this.dropOffBoxPath = normativeFileProperties.getTmpIdentifier();
          final FileProperties descriptiveFileProperties =
              FileSystem.pickUp(this.dropOffBoxPath, getRequestId());
          normativeFileProperties.setFile(descriptiveFileProperties.getFile());
          normativeFileProperties.setThumbnail(descriptiveFileProperties.getThumbnail());
          normativeFileProperties.setTmpIdentifier(null);
          if (!normativeFileProperties.hasSize()) {
            normativeFileProperties.setSize(normativeFileProperties.getFile().length());
          }
        } catch (final Message m) {
          entity.addMessage(m);
          if (m.getType().equalsIgnoreCase("Error")) {
            entity.setEntityStatus(EntityStatus.UNQUALIFIED);
          }
        }
        this.rollBack = true;
      }
    }
  }

  private boolean rollBack = false;
  private String dropOffBoxPath = null;

  @Override
  public boolean notifyObserver(final String e, final Observable o) {
    if (e == Entity.ENTITY_STATUS_CHANGED_EVENT
        && o == getEntity()
        && this.rollBack
        && getEntity().getEntityStatus() == EntityStatus.UNQUALIFIED) {
      final File target = new File(FileSystem.getDropOffBox() + this.dropOffBoxPath);
      getEntity().getFileProperties().getFile().renameTo(target);
      this.rollBack = false;
      return false;
    }
    return true;
  }
}
