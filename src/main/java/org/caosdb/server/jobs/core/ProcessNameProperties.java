/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2023 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2023 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.caosdb.server.jobs.core;

import static org.caosdb.server.entity.MagicTypes.NAME;

import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;
import org.caosdb.server.database.exceptions.EntityDoesNotExistException;
import org.caosdb.server.datatype.AbstractDatatype;
import org.caosdb.server.entity.EntityID;
import org.caosdb.server.entity.EntityInterface;
import org.caosdb.server.entity.Message;
import org.caosdb.server.entity.wrapper.Parent;
import org.caosdb.server.entity.wrapper.Property;
import org.caosdb.server.jobs.EntityJob;
import org.caosdb.server.jobs.JobAnnotation;
import org.caosdb.server.jobs.TransactionStage;
import org.caosdb.server.utils.EntityStatus;
import org.caosdb.server.utils.ServerMessages;

/**
 * Checks if a property is a subproperty of "name" and flags the entity accordingly.
 *
 * <p>This enables a different behavior in the search.
 *
 * <p>To be called after CheckPropValid and Inheritance.
 *
 * @author Timm Fitschen <t.fitschen@indiscale.com>
 */
@JobAnnotation(stage = TransactionStage.PRE_TRANSACTION)
public class ProcessNameProperties extends EntityJob {

  @Override
  protected void run() {
    try {
      for (final Property prop : getEntity().getProperties()) {
        final Collection<Object> parents = getNearestValidParents(prop);
        if (parents != null) {
          for (final Object par : parents) {
            if (isValidAndSubTypeOfName(par)) {
              doProcessNameProperty(prop);
              break;
            }
          }
        }
      }
    } catch (final Message m) {
      getEntity().addError(m);
      getEntity().setEntityStatus(EntityStatus.UNQUALIFIED);
    }
  }

  private void doProcessNameProperty(final Property prop) throws Message {
    final AbstractDatatype textdt = AbstractDatatype.datatypeFactory("TEXT");
    if (prop.hasDatatype()) {
      if (!prop.getDatatype().equals(textdt)) {
        throw ServerMessages.NAME_PROPERTIES_MUST_BE_TEXT;
      }
    } else {
      prop.setDatatype(textdt);
    }
    prop.setIsName(true);
  }

  private boolean isValidAndSubTypeOfName(final Object par) {
    if (par != null && par instanceof EntityID && !((EntityID) par).isTemporary()) {
      final boolean isName = par.equals(NAME.getId());
      return isName || isValidSubType((EntityID) par, NAME.getId());
    }
    return false;
  }

  private Set<Object> getNearestValidParents(final EntityInterface entity) {
    final HashSet<Object> set = new HashSet<Object>();
    getNearestValidParents(entity, set);
    return set;
  }

  private void getNearestValidParents(final EntityInterface entity, final Set<Object> set) {
    for (final Parent par : entity.getParents()) {
      if (par.hasParents() && par.hasId()) {
        // parent has parents and id
        if (set.add(par.getId())) {
          getNearestValidParents(par, set);
        }
      } else if (par.hasParents() && par.hasName()) {
        // parent has parents and name
        if (set.add(par.getName())) {
          getNearestValidParents(par, set);
        }
      } else if (par.hasId() && !par.getId().isTemporary()) {
        // parent is valid
        set.add(par.getId());
      } else if (par.hasId()) {
        // get parent from container
        final EntityInterface parentEntity = getEntityById(par.getId());
        if (parentEntity != null && parentEntity.hasParents()) {
          if (set.add(parentEntity.getId())) {
            getNearestValidParents(parentEntity, set);
          }
          continue;
        }
      }
      if (par.hasName()) {
        // get parent from container
        final EntityInterface parentEntity = getEntityByName(par.getName());
        if (parentEntity != null && parentEntity.hasParents()) {
          if (set.add(parentEntity.getName())) {
            getNearestValidParents(parentEntity, set);
          }
        }
      }
      // else: parent has no name and no id
    }
  }

  private Collection<Object> getNearestValidParents(final Property prop) {
    if (prop.hasParents()) {
      final Collection<Object> ret = new LinkedList<Object>();
      for (final Parent par : prop.getParents()) {
        if (par.hasId()) {
          ret.add(par.getId());
        }
      }
      return ret;
    } else if (prop.hasId()) {
      try {
        EntityInterface propertyEntity = resolve(prop.getId());
        if (propertyEntity != null) {
          return getNearestValidParents(propertyEntity);
        }
      } catch (EntityDoesNotExistException e) {
        // return null
      }
      return null;
    } else {
      return null;
    }
  }
}
