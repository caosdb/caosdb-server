/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2021 Indiscale GmbH <info@indiscale.com>
 * Copyright (C) 2021 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.jobs.core;

/**
 * Describes the severity of a failure of a job (esp. consitency checks).
 *
 * <ul>
 *   <li>{@link #ERROR} - a failure throws an error and the transaction will not succeed.
 *   <li>{@link #WARN} - a failure produces a warning but the transaction may succeed anyway.
 *   <li>{@link #IGNORE} - the job may fail silently. Jobs can also decide to not run at all if that
 *       is not necessary in this case.
 *       <p>Jobs where this doesn't make sense at all may also ignore the severity. It is rather a
 *       convenient way to configure jobs where this makes sense than a definitive normative
 *       property of a job.
 *
 * @author Timm Fitschen <t.fitschen@indiscale.com>
 */
public enum JobFailureSeverity {
  ERROR,
  WARN,
  IGNORE,
}
