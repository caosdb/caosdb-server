/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.jobs.core;

import org.caosdb.api.entity.v1.MessageCode;
import org.caosdb.server.entity.EntityInterface;
import org.caosdb.server.entity.Message;
import org.caosdb.server.entity.Message.MessageType;
import org.caosdb.server.jobs.FlagJob;
import org.caosdb.server.jobs.JobAnnotation;
import org.caosdb.server.jobs.TransactionStage;
import org.caosdb.server.query.Query;
import org.caosdb.server.query.Query.ParsingException;
import org.caosdb.server.transaction.Retrieve;
import org.caosdb.server.utils.EntityStatus;
import org.caosdb.server.utils.ServerMessages;

@JobAnnotation(flag = "query", stage = TransactionStage.INIT, transaction = Retrieve.class)
public class ExecuteQuery extends FlagJob {

  @Override
  protected void job(final String value) {

    if (value != null) {
      // run paging job first
      getTransaction().getSchedule().runJob(null, Paging.class);

      final Query queryInstance = new Query(value, getTransaction());
      getContainer().setQuery(queryInstance);
      try {
        queryInstance.execute();
      } catch (final ParsingException e) {
        getContainer().addError(ServerMessages.QUERY_PARSING_ERROR(e.getMessage()));
      } catch (final UnsupportedOperationException e) {
        getContainer().addError(ServerMessages.QUERY_EXCEPTION);
        getContainer()
            .addMessage(new Message(MessageType.Info, (MessageCode) null, e.getMessage()));
      }
      getContainer().addMessage(queryInstance);

      int startIndex = 0;
      int endIndex = getContainer().size();

      if (((Retrieve) getTransaction()).hasPaging()) {
        Retrieve.Paging paging = ((Retrieve) getTransaction()).getPaging();
        startIndex = Math.min(getContainer().size(), paging.startIndex);
        endIndex = Math.min(getContainer().size(), paging.endIndex);
      }

      int ii = 0;
      for (final EntityInterface entity : getContainer()) {
        if (ii >= startIndex && ii < endIndex) {
          getTransaction().getSchedule().addAll(loadJobs(entity, getTransaction()));
        } else {
          entity.setEntityStatus(EntityStatus.IGNORE);
        }
        ii++;
      }
    }
  }
}
