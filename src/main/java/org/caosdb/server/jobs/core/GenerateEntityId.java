/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2023 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2023 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 */
package org.caosdb.server.jobs.core;

import org.caosdb.server.entity.EntityInterface;
import org.caosdb.server.entity.InsertEntity;
import org.caosdb.server.entity.Message;
import org.caosdb.server.jobs.EntityJob;
import org.caosdb.server.jobs.JobAnnotation;
import org.caosdb.server.jobs.TransactionStage;
import org.caosdb.server.transaction.WriteTransactionInterface;
import org.caosdb.server.utils.EntityStatus;

/**
 * Generates entity ids for newly inserted entities.
 *
 * @author Timm Fitschen <t.fitschen@indiscale.com>
 */
@JobAnnotation(stage = TransactionStage.PRE_TRANSACTION)
public class GenerateEntityId extends EntityJob {

  @Override
  protected void run() throws Message {
    EntityInterface entity = getEntity();
    if (entity instanceof InsertEntity
        && getTransaction() instanceof WriteTransactionInterface
        && entity.getEntityStatus() == EntityStatus.QUALIFIED) {
      String id = ((WriteTransactionInterface) getTransaction()).generateId();
      entity.setId(id);
    }
  }
}
