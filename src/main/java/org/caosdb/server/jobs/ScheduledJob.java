/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2020,2023 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2020,2023 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package org.caosdb.server.jobs;

import org.caosdb.server.entity.Message;

/**
 * ScheduledJob is a wrapper class for jobs held by the Scheduler.
 *
 * <p>It is mainly a means to have simplified interface for the Scheduler which also measures the
 * execution time of the job "from outside".
 *
 * @author Timm Fitschen <t.fitschen@indiscale.com>
 */
public class ScheduledJob {

  long runtime = 0;
  private final Job job;
  private long startTime = -1;

  ScheduledJob(final Job job) {
    if (job == null) {
      throw new NullPointerException("job was null.");
    }
    this.job = job;
  }

  public void run() {
    if (!hasStarted()) {
      start();
      try {
        this.job.run();
      } catch (final Message m) {
        this.job.getTarget().addError(m);
      }
      finish();
    }
  }

  @Override
  public String toString() {
    return this.job.toString();
  }

  /** Does not actually start the job, but only sets the startTime. */
  private void start() {
    this.startTime = System.currentTimeMillis();
  }

  /** Calculate and set the runtime, and add the measurement. */
  private void finish() {
    this.runtime += System.currentTimeMillis() - this.startTime;
    this.job
        .getContainer()
        .getTransactionBenchmark()
        .addMeasurement(this.job.getClass().getSimpleName(), this.runtime);
  }

  void pause() {
    this.runtime += System.currentTimeMillis() - this.startTime;
  }

  void unpause() {
    start();
  }

  private boolean hasStarted() {
    return this.startTime != -1;
  }

  /** Return the state of the inner Job. */
  public TransactionStage getTransactionStage() {
    return this.job.getTransactionStage();
  }

  public boolean skip() {
    try {
      return this.job.getTarget().skipJob();
    } catch (final NullPointerException e) {
      throw e;
    }
  }

  public Job getJob() {
    return job;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj instanceof ScheduledJob) {
      ScheduledJob other = (ScheduledJob) obj;
      return this.job.equals(other.job);
    }
    return false;
  }

  @Override
  public int hashCode() {
    return job.hashCode();
  }
}
