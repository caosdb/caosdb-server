/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.datetime;

import java.util.TimeZone;
import org.caosdb.server.datatype.AbstractDatatype.Table;
import org.jdom2.Element;

public class SemiCompleteDateTime extends FragmentDateTime implements Interval {

  UTCDateTime inclusiveLowerBound;
  UTCDateTime exclusiveUpperBound;

  @Override
  public void addToElement(final Element e) {
    throw new UnsupportedOperationException("This DateTime cannot be added to an xml element.");
  }

  @Override
  public Table getTable() {
    throw new UnsupportedOperationException("This DateTime cannot be stored to the database.");
  }

  @Override
  public String toDatabaseString() {
    throw new UnsupportedOperationException("This DateTime cannot be stored to the database.");
  }

  @Override
  public String toDateTimeString(final TimeZone tz) {
    final StringBuilder sb = new StringBuilder();
    if (this.year < 0) {
      sb.append('-');
    }
    for (int i = Integer.toString(Math.abs(this.year)).length(); i < 4; i++) {
      sb.append('0');
    }
    sb.append(Math.abs(this.year));
    if (this.month != null) {
      sb.append('-');
      if (this.month < 10) {
        sb.append('0');
      }
      sb.append(this.month);
      if (this.dom != null) {
        sb.append('-');
        if (this.dom < 10) {
          sb.append('0');
        }
        sb.append(this.dom);
        if (this.hour != null) {
          sb.append('T');
          if (this.hour < 10) {
            sb.append('0');
          }
          sb.append(this.hour);
          if (this.minute != null) {
            sb.append(':');
            if (this.minute < 10) {
              sb.append('0');
            }
            sb.append(this.minute);
          }
        }
      }
    }
    return sb.toString();
  }

  public SemiCompleteDateTime(
      final Integer year,
      final Integer month,
      final Integer dom,
      final Integer hour,
      final Integer minute,
      final Integer second,
      final Integer nanosecond,
      final TimeZone tz) {
    super(year, month, dom, hour, minute, second, nanosecond, tz);
    init();
  }

  private void init() {
    if (this.second == null && this.nanosecond == null) {
      if (this.year != null) {
        if (this.month != null) {
          if (this.dom != null) {
            if (this.hour != null) {
              if (this.minute != null) {
                this.inclusiveLowerBound =
                    new UTCDateTime(
                        this.year,
                        this.month,
                        this.dom,
                        this.hour,
                        this.minute,
                        0,
                        null,
                        this.timeZone);
                this.exclusiveUpperBound =
                    new UTCDateTime(
                        this.year,
                        this.month,
                        this.dom,
                        this.hour,
                        this.minute + 1,
                        0,
                        null,
                        this.timeZone,
                        true);
                return;
              } else {
                this.inclusiveLowerBound =
                    new UTCDateTime(
                        this.year, this.month, this.dom, this.hour, 0, 0, null, this.timeZone);
                this.exclusiveUpperBound =
                    new UTCDateTime(
                        this.year,
                        this.month,
                        this.dom,
                        this.hour + 1,
                        0,
                        0,
                        null,
                        this.timeZone,
                        true);
                return;
              }
            } else if (this.minute == null) {
              this.inclusiveLowerBound =
                  new UTCDateTime(this.year, this.month, this.dom, 0, 0, 0, null, this.timeZone);
              this.exclusiveUpperBound =
                  new UTCDateTime(
                      this.year, this.month, this.dom + 1, 0, 0, 0, null, this.timeZone, true);
              return;
            }
          } else if (this.hour == null && this.minute == null) {
            this.inclusiveLowerBound =
                new UTCDateTime(this.year, this.month, 1, 0, 0, 0, null, this.timeZone);
            this.exclusiveUpperBound =
                new UTCDateTime(this.year, this.month + 1, 1, 0, 0, 0, null, this.timeZone, true);
            return;
          }
        } else if (this.dom == null && this.hour == null && this.minute == null) {
          this.inclusiveLowerBound = new UTCDateTime(this.year, 1, 1, 0, 0, 0, null, this.timeZone);
          this.exclusiveUpperBound =
              new UTCDateTime(this.year + 1, 1, 1, 0, 0, 0, null, this.timeZone, true);
          return;
        }
      }
    }
    throw new IllegalArgumentException(
        "This is no SemiCompleteDateTime: ("
            + this.year
            + ","
            + this.month
            + ","
            + this.dom
            + ","
            + this.hour
            + ","
            + this.minute
            + ","
            + this.second
            + ","
            + this.nanosecond
            + ")");
  }

  @Override
  public UTCDateTime getInclusiveLowerBound() {
    return this.inclusiveLowerBound;
  }

  @Override
  public UTCDateTime getExclusiveUpperBound() {
    return this.exclusiveUpperBound;
  }

  @Override
  public String getILB_NF1() {
    return this.inclusiveLowerBound.getILB_NF1();
  }

  @Override
  public String getEUB_NF1() {
    return this.exclusiveUpperBound.getILB_NF1();
  }

  @Override
  public String getILB_NF2() {
    return this.inclusiveLowerBound.getILB_NF2();
  }

  @Override
  public String getEUB_NF2() {
    return this.exclusiveUpperBound.getILB_NF2();
  }
}
