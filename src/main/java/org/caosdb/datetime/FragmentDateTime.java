/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */

/**
 * @review Daniel Hornung 2022-03-04
 */
package org.caosdb.datetime;

import java.util.Objects;
import java.util.TimeZone;
import org.caosdb.server.datatype.Value;

public abstract class FragmentDateTime implements DateTimeInterface {

  protected Integer year = null;
  protected Integer month = null;
  protected Integer dom = null;
  protected Integer hour = null;
  protected Integer minute = null;
  protected Integer second = null;
  protected Integer nanosecond = null;
  protected TimeZone timeZone = TimeZone.getDefault();

  public FragmentDateTime(
      final Integer year,
      final Integer month,
      final Integer dom,
      final Integer hour,
      final Integer minute,
      final Integer second,
      final Integer nanosecond,
      final TimeZone tz) {
    this.year = year;
    this.month = month;
    this.dom = dom;
    this.hour = hour;
    this.minute = minute;
    this.second = second;
    this.nanosecond = nanosecond;
    this.timeZone = tz;
  }

  @Override
  public boolean equals(Value val) {
    if (val instanceof FragmentDateTime) {
      FragmentDateTime that = (FragmentDateTime) val;
      return Objects.equals(that.toDatabaseString(), this.toDatabaseString());
    }
    return false;
  }
}
